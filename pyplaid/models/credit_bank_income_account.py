# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json


from typing import List, Optional
from pydantic import BaseModel, Field, StrictStr
from pyplaid.models.credit_bank_income_account_type import CreditBankIncomeAccountType
from pyplaid.models.depository_account_subtype import DepositoryAccountSubtype
from pyplaid.models.owner import Owner
from pydantic import ValidationError

class CreditBankIncomeAccount(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    account_id: Optional[StrictStr] = Field(None, description="Plaid's unique identifier for the account.")
    mask: Optional[StrictStr] = Field(None, description="The last 2-4 alphanumeric characters of an account's official account number. Note that the mask may be non-unique between an Item's accounts, and it may also not match the mask that the bank displays to the user.")
    name: Optional[StrictStr] = Field(None, description="The name of the bank account.")
    official_name: Optional[StrictStr] = Field(None, description="The official name of the bank account.")
    subtype: Optional[DepositoryAccountSubtype] = None
    type: Optional[CreditBankIncomeAccountType] = None
    owners: Optional[List[Owner]] = None

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> CreditBankIncomeAccount:
        """Create an instance of CreditBankIncomeAccount from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)
        # override the default output from pydantic by calling `to_dict()` of each item in owners (list)
        _items = []
        if self.owners:
            for _item in self.owners:
                if _item:
                    _items.append(_item.to_dict())
            _dict['owners'] = _items

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> CreditBankIncomeAccount:
        """Create an instance of CreditBankIncomeAccount from a dict"""
        if type(obj) is not dict:
            return CreditBankIncomeAccount.parse_obj(obj)

        return CreditBankIncomeAccount.parse_obj({
            "account_id": obj.get("account_id"),
            "mask": obj.get("mask"),
            "name": obj.get("name"),
            "official_name": obj.get("official_name"),
            "subtype": obj.get("subtype"),
            "type": obj.get("type"),
            "owners": [Owner.from_dict(_item) for _item in obj.get("owners")]
        })


