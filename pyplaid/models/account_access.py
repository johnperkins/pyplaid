# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json


from typing import Optional
from pydantic import BaseModel, Field, StrictBool, StrictStr
from pyplaid.models.account_product_access_nullable import AccountProductAccessNullable
from pydantic import ValidationError

class AccountAccess(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    unique_id: StrictStr = Field(..., description="The unique account identifier for this account. This value must match that returned by the data access API for this account.")
    authorized: Optional[StrictBool] = Field(True, description="Allow the application to see this account (and associated details, including balance) in the list of accounts  If unset, defaults to `true`.")
    account_product_access: Optional[AccountProductAccessNullable] = None

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> AccountAccess:
        """Create an instance of AccountAccess from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)
        # override the default output from pydantic by calling `to_dict()` of account_product_access
        if self.account_product_access:
            _dict['account_product_access'] = self.account_product_access.to_dict()

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> AccountAccess:
        """Create an instance of AccountAccess from a dict"""
        if type(obj) is not dict:
            return AccountAccess.parse_obj(obj)

        return AccountAccess.parse_obj({
            "unique_id": obj.get("unique_id"),
            "authorized": obj.get("authorized") if obj.get("authorized") is not None else True,
            "account_product_access": AccountProductAccessNullable.from_dict(obj.get("account_product_access"))
        })


