# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json


from typing import List, Optional
from pydantic import BaseModel, Field
from pyplaid.models.health_incident import HealthIncident
from pyplaid.models.product_status import ProductStatus
from pydantic import ValidationError

class InstitutionStatus(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    item_logins: Optional[ProductStatus] = None
    transactions_updates: Optional[ProductStatus] = None
    auth: Optional[ProductStatus] = None
    identity: Optional[ProductStatus] = None
    investments_updates: Optional[ProductStatus] = None
    liabilities_updates: Optional[ProductStatus] = None
    liabilities: Optional[ProductStatus] = None
    investments: Optional[ProductStatus] = None
    health_incidents: Optional[List[HealthIncident]] = Field(None, description="Details of recent health incidents associated with the institution.")

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> InstitutionStatus:
        """Create an instance of InstitutionStatus from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)
        # override the default output from pydantic by calling `to_dict()` of item_logins
        if self.item_logins:
            _dict['item_logins'] = self.item_logins.to_dict()
        # override the default output from pydantic by calling `to_dict()` of transactions_updates
        if self.transactions_updates:
            _dict['transactions_updates'] = self.transactions_updates.to_dict()
        # override the default output from pydantic by calling `to_dict()` of auth
        if self.auth:
            _dict['auth'] = self.auth.to_dict()
        # override the default output from pydantic by calling `to_dict()` of identity
        if self.identity:
            _dict['identity'] = self.identity.to_dict()
        # override the default output from pydantic by calling `to_dict()` of investments_updates
        if self.investments_updates:
            _dict['investments_updates'] = self.investments_updates.to_dict()
        # override the default output from pydantic by calling `to_dict()` of liabilities_updates
        if self.liabilities_updates:
            _dict['liabilities_updates'] = self.liabilities_updates.to_dict()
        # override the default output from pydantic by calling `to_dict()` of liabilities
        if self.liabilities:
            _dict['liabilities'] = self.liabilities.to_dict()
        # override the default output from pydantic by calling `to_dict()` of investments
        if self.investments:
            _dict['investments'] = self.investments.to_dict()
        # override the default output from pydantic by calling `to_dict()` of each item in health_incidents (list)
        _items = []
        if self.health_incidents:
            for _item in self.health_incidents:
                if _item:
                    _items.append(_item.to_dict())
            _dict['health_incidents'] = _items

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> InstitutionStatus:
        """Create an instance of InstitutionStatus from a dict"""
        if type(obj) is not dict:
            return InstitutionStatus.parse_obj(obj)

        return InstitutionStatus.parse_obj({
            "item_logins": ProductStatus.from_dict(obj.get("item_logins")),
            "transactions_updates": ProductStatus.from_dict(obj.get("transactions_updates")),
            "auth": ProductStatus.from_dict(obj.get("auth")),
            "identity": ProductStatus.from_dict(obj.get("identity")),
            "investments_updates": ProductStatus.from_dict(obj.get("investments_updates")),
            "liabilities_updates": ProductStatus.from_dict(obj.get("liabilities_updates")),
            "liabilities": ProductStatus.from_dict(obj.get("liabilities")),
            "investments": ProductStatus.from_dict(obj.get("investments")),
            "health_incidents": [HealthIncident.from_dict(_item) for _item in obj.get("health_incidents")]
        })


