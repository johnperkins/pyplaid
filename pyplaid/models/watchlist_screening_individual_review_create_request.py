# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json


from typing import List, Optional
from pydantic import BaseModel, Field, StrictStr, constr
from pydantic import ValidationError

class WatchlistScreeningIndividualReviewCreateRequest(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    confirmed_hits: List[StrictStr] = Field(..., description="Hits to mark as a true positive after thorough manual review. These hits will never recur or be updated once dismissed. In most cases, confirmed hits indicate that the customer should be rejected.")
    dismissed_hits: List[StrictStr] = Field(..., description="Hits to mark as a false positive after thorough manual review. These hits will never recur or be updated once dismissed.")
    comment: Optional[constr(strict=True, min_length=1)] = Field(None, description="A comment submitted by a team member as part of reviewing a watchlist screening.")
    client_id: Optional[StrictStr] = Field(None, description="Your Plaid API `client_id`. The `client_id` is required and may be provided either in the `PLAID-CLIENT-ID` header or as part of a request body.")
    secret: Optional[StrictStr] = Field(None, description="Your Plaid API `secret`. The `secret` is required and may be provided either in the `PLAID-SECRET` header or as part of a request body.")
    watchlist_screening_id: StrictStr = Field(..., description="ID of the associated screening.")

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> WatchlistScreeningIndividualReviewCreateRequest:
        """Create an instance of WatchlistScreeningIndividualReviewCreateRequest from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> WatchlistScreeningIndividualReviewCreateRequest:
        """Create an instance of WatchlistScreeningIndividualReviewCreateRequest from a dict"""
        if type(obj) is not dict:
            return WatchlistScreeningIndividualReviewCreateRequest.parse_obj(obj)

        return WatchlistScreeningIndividualReviewCreateRequest.parse_obj({
            "confirmed_hits": obj.get("confirmed_hits"),
            "dismissed_hits": obj.get("dismissed_hits"),
            "comment": obj.get("comment"),
            "client_id": obj.get("client_id"),
            "secret": obj.get("secret"),
            "watchlist_screening_id": obj.get("watchlist_screening_id")
        })


