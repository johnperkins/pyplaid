# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json


from typing import Optional
from pydantic import BaseModel, Field, StrictStr
from pydantic import ValidationError

class SecurityOverride(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    isin: Optional[StrictStr] = Field(None, description="12-character ISIN, a globally unique securities identifier.")
    cusip: Optional[StrictStr] = Field(None, description="9-character CUSIP, an identifier assigned to North American securities.")
    sedol: Optional[StrictStr] = Field(None, description="7-character SEDOL, an identifier assigned to securities in the UK.")
    name: Optional[StrictStr] = Field(None, description="A descriptive name for the security, suitable for display.")
    ticker_symbol: Optional[StrictStr] = Field(None, description="The security’s trading symbol for publicly traded securities, and otherwise a short identifier if available.")
    currency: Optional[StrictStr] = Field(None, description="Either a valid `iso_currency_code` or `unofficial_currency_code`")

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> SecurityOverride:
        """Create an instance of SecurityOverride from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> SecurityOverride:
        """Create an instance of SecurityOverride from a dict"""
        if type(obj) is not dict:
            return SecurityOverride.parse_obj(obj)

        return SecurityOverride.parse_obj({
            "isin": obj.get("isin"),
            "cusip": obj.get("cusip"),
            "sedol": obj.get("sedol"),
            "name": obj.get("name"),
            "ticker_symbol": obj.get("ticker_symbol"),
            "currency": obj.get("currency")
        })


