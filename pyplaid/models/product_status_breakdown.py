# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json


from typing import Optional
from pydantic import BaseModel, Field, StrictFloat, StrictStr, validator
from pydantic import ValidationError

class ProductStatusBreakdown(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    success: StrictFloat = Field(..., description="The percentage of login attempts that are successful, expressed as a decimal.")
    error_plaid: StrictFloat = Field(..., description="The percentage of logins that are failing due to an internal Plaid issue, expressed as a decimal. ")
    error_institution: StrictFloat = Field(..., description="The percentage of logins that are failing due to an issue in the institution's system, expressed as a decimal.")
    refresh_interval: Optional[StrictStr] = Field(None, description="The `refresh_interval` may be `DELAYED` or `STOPPED` even when the success rate is high. This value is only returned for Transactions status breakdowns.")

    @validator('refresh_interval')
    def refresh_interval_validate_enum(cls, v):
        if v not in ('NORMAL', 'DELAYED', 'STOPPED'):
            raise ValueError("must validate the enum values ('NORMAL', 'DELAYED', 'STOPPED')")
        return v

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> ProductStatusBreakdown:
        """Create an instance of ProductStatusBreakdown from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> ProductStatusBreakdown:
        """Create an instance of ProductStatusBreakdown from a dict"""
        if type(obj) is not dict:
            return ProductStatusBreakdown.parse_obj(obj)

        return ProductStatusBreakdown.parse_obj({
            "success": obj.get("success"),
            "error_plaid": obj.get("error_plaid"),
            "error_institution": obj.get("error_institution"),
            "refresh_interval": obj.get("refresh_interval")
        })


