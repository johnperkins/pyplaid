# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json


from typing import Optional
from pydantic import BaseModel, Field, StrictStr
from pydantic import ValidationError

class SandboxProcessorTokenCreateRequestOptions(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    override_username: Optional[StrictStr] = Field('user_good', description="Test username to use for the creation of the Sandbox Item. Default value is `user_good`.")
    override_password: Optional[StrictStr] = Field('pass_good', description="Test password to use for the creation of the Sandbox Item. Default value is `pass_good`.")

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> SandboxProcessorTokenCreateRequestOptions:
        """Create an instance of SandboxProcessorTokenCreateRequestOptions from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> SandboxProcessorTokenCreateRequestOptions:
        """Create an instance of SandboxProcessorTokenCreateRequestOptions from a dict"""
        if type(obj) is not dict:
            return SandboxProcessorTokenCreateRequestOptions.parse_obj(obj)

        return SandboxProcessorTokenCreateRequestOptions.parse_obj({
            "override_username": obj.get("override_username") if obj.get("override_username") is not None else 'user_good',
            "override_password": obj.get("override_password") if obj.get("override_password") is not None else 'pass_good'
        })


