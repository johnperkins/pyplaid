# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json


from typing import Optional
from pydantic import BaseModel, Field, StrictBool
from pydantic import ValidationError

class AccountProductAccess(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    account_data: Optional[StrictBool] = Field(True, description="Allow the application to access account data. Only used by certain partners. If relevant to the partner and unset, defaults to `true`.")
    statements: Optional[StrictBool] = Field(True, description="Allow the application to access bank statements. Only used by certain partners. If relevant to the partner and unset, defaults to `true`.")
    tax_documents: Optional[StrictBool] = Field(True, description="Allow the application to access tax documents. Only used by certain partners. If relevant to the partner and unset, defaults to `true`.")

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> AccountProductAccess:
        """Create an instance of AccountProductAccess from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> AccountProductAccess:
        """Create an instance of AccountProductAccess from a dict"""
        if type(obj) is not dict:
            return AccountProductAccess.parse_obj(obj)

        return AccountProductAccess.parse_obj({
            "account_data": obj.get("account_data") if obj.get("account_data") is not None else True,
            "statements": obj.get("statements") if obj.get("statements") is not None else True,
            "tax_documents": obj.get("tax_documents") if obj.get("tax_documents") is not None else True
        })


