# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json


from typing import Optional
from pydantic import BaseModel, Field, StrictStr
from pyplaid.models.report_type import ReportType
from pydantic import ValidationError

class CreditRelayRefreshRequest(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    client_id: Optional[StrictStr] = Field(None, description="Your Plaid API `client_id`. The `client_id` is required and may be provided either in the `PLAID-CLIENT-ID` header or as part of a request body.")
    secret: Optional[StrictStr] = Field(None, description="Your Plaid API `secret`. The `secret` is required and may be provided either in the `PLAID-SECRET` header or as part of a request body.")
    relay_token: StrictStr = Field(..., description="The `relay_token` granting access to the report you would like to refresh.")
    report_type: ReportType = ...
    webhook: Optional[StrictStr] = Field(None, description="The URL registered to receive webhooks when the report of a Relay Token has been refreshed.")

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> CreditRelayRefreshRequest:
        """Create an instance of CreditRelayRefreshRequest from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> CreditRelayRefreshRequest:
        """Create an instance of CreditRelayRefreshRequest from a dict"""
        if type(obj) is not dict:
            return CreditRelayRefreshRequest.parse_obj(obj)

        return CreditRelayRefreshRequest.parse_obj({
            "client_id": obj.get("client_id"),
            "secret": obj.get("secret"),
            "relay_token": obj.get("relay_token"),
            "report_type": obj.get("report_type"),
            "webhook": obj.get("webhook")
        })


