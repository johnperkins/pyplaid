# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json

from datetime import datetime
from typing import List, Optional
from pydantic import BaseModel, Field, StrictFloat, StrictStr
from pyplaid.models.asset_report_item import AssetReportItem
from pyplaid.models.asset_report_user import AssetReportUser
from pydantic import ValidationError

class AssetReport(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    asset_report_id: StrictStr = Field(..., description="A unique ID identifying an Asset Report. Like all Plaid identifiers, this ID is case sensitive.")
    client_report_id: Optional[StrictStr] = Field(..., description="An identifier you determine and submit for the Asset Report.")
    date_generated: datetime = Field(..., description="The date and time when the Asset Report was created, in [ISO 8601](https://wikipedia.org/wiki/ISO_8601) format (e.g. \"2018-04-12T03:32:11Z\").")
    days_requested: StrictFloat = Field(..., description="The duration of transaction history you requested")
    user: AssetReportUser = ...
    items: List[AssetReportItem] = Field(..., description="Data returned by Plaid about each of the Items included in the Asset Report.")

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> AssetReport:
        """Create an instance of AssetReport from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)
        # override the default output from pydantic by calling `to_dict()` of user
        if self.user:
            _dict['user'] = self.user.to_dict()
        # override the default output from pydantic by calling `to_dict()` of each item in items (list)
        _items = []
        if self.items:
            for _item in self.items:
                if _item:
                    _items.append(_item.to_dict())
            _dict['items'] = _items

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> AssetReport:
        """Create an instance of AssetReport from a dict"""
        if type(obj) is not dict:
            return AssetReport.parse_obj(obj)

        return AssetReport.parse_obj({
            "asset_report_id": obj.get("asset_report_id"),
            "client_report_id": obj.get("client_report_id"),
            "date_generated": obj.get("date_generated"),
            "days_requested": obj.get("days_requested"),
            "user": AssetReportUser.from_dict(obj.get("user")),
            "items": [AssetReportItem.from_dict(_item) for _item in obj.get("items")]
        })


