# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json


from typing import Optional
from pydantic import BaseModel, Field, StrictBool, StrictStr
from pyplaid.models.identity_verification_request_user import IdentityVerificationRequestUser
from pydantic import ValidationError

class IdentityVerificationCreateRequest(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    is_shareable: StrictBool = Field(..., description="A flag specifying whether you would like Plaid to expose a shareable URL for the verification being created.")
    template_id: StrictStr = Field(..., description="ID of the associated Identity Verification template.")
    gave_consent: StrictBool = Field(..., description="A flag specifying whether the end user has already agreed to a privacy policy specifying that their data will be shared with Plaid for verification purposes.  If `gave_consent` is set to `true`, the `accept_tos` step will be marked as `skipped` and the end user's session will start at the next step requirement.")
    user: IdentityVerificationRequestUser = ...
    client_id: Optional[StrictStr] = Field(None, description="Your Plaid API `client_id`. The `client_id` is required and may be provided either in the `PLAID-CLIENT-ID` header or as part of a request body.")
    secret: Optional[StrictStr] = Field(None, description="Your Plaid API `secret`. The `secret` is required and may be provided either in the `PLAID-SECRET` header or as part of a request body.")
    is_idempotent: Optional[StrictBool] = Field(None, description="An optional flag specifying how you would like Plaid to handle attempts to create an Identity Verification when an Identity Verification already exists for the provided `client_user_id` and `template_id`. If idempotency is enabled, Plaid will return the existing Identity Verification. If idempotency is disabled, Plaid will reject the request with a `400 Bad Request` status code if an Identity Verification already exists for the supplied `client_user_id` and `template_id`.")

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> IdentityVerificationCreateRequest:
        """Create an instance of IdentityVerificationCreateRequest from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)
        # override the default output from pydantic by calling `to_dict()` of user
        if self.user:
            _dict['user'] = self.user.to_dict()

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> IdentityVerificationCreateRequest:
        """Create an instance of IdentityVerificationCreateRequest from a dict"""
        if type(obj) is not dict:
            return IdentityVerificationCreateRequest.parse_obj(obj)

        return IdentityVerificationCreateRequest.parse_obj({
            "is_shareable": obj.get("is_shareable"),
            "template_id": obj.get("template_id"),
            "gave_consent": obj.get("gave_consent") if obj.get("gave_consent") is not None else False,
            "user": IdentityVerificationRequestUser.from_dict(obj.get("user")),
            "client_id": obj.get("client_id"),
            "secret": obj.get("secret"),
            "is_idempotent": obj.get("is_idempotent")
        })


