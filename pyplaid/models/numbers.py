# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json


from typing import Optional
from pydantic import BaseModel, Field, StrictStr
from pydantic import ValidationError

class Numbers(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    account: Optional[StrictStr] = Field(None, description="Will be used for the account number.")
    ach_routing: Optional[StrictStr] = Field(None, description="Must be a valid ACH routing number.")
    ach_wire_routing: Optional[StrictStr] = Field(None, description="Must be a valid wire transfer routing number.")
    eft_institution: Optional[StrictStr] = Field(None, description="EFT institution number. Must be specified alongside `eft_branch`.")
    eft_branch: Optional[StrictStr] = Field(None, description="EFT branch number. Must be specified alongside `eft_institution`.")
    international_bic: Optional[StrictStr] = Field(None, description="Bank identifier code (BIC). Must be specified alongside `international_iban`.")
    international_iban: Optional[StrictStr] = Field(None, description="International bank account number (IBAN). If no account number is specified via `account`, will also be used as the account number by default. Must be specified alongside `international_bic`.")
    bacs_sort_code: Optional[StrictStr] = Field(None, description="BACS sort code")

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> Numbers:
        """Create an instance of Numbers from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> Numbers:
        """Create an instance of Numbers from a dict"""
        if type(obj) is not dict:
            return Numbers.parse_obj(obj)

        return Numbers.parse_obj({
            "account": obj.get("account"),
            "ach_routing": obj.get("ach_routing"),
            "ach_wire_routing": obj.get("ach_wire_routing"),
            "eft_institution": obj.get("eft_institution"),
            "eft_branch": obj.get("eft_branch"),
            "international_bic": obj.get("international_bic"),
            "international_iban": obj.get("international_iban"),
            "bacs_sort_code": obj.get("bacs_sort_code")
        })


