# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json


from typing import List, Optional
from pydantic import BaseModel, Field, StrictStr
from pyplaid.models.document_metadata import DocumentMetadata
from pyplaid.models.plaid_error import PlaidError
from pyplaid.models.taxform import Taxform
from pydantic import ValidationError

class IncomeVerificationTaxformsGetResponse(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    request_id: Optional[StrictStr] = Field(None, description="A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive.")
    document_metadata: List[DocumentMetadata] = ...
    taxforms: List[Taxform] = Field(..., description="A list of forms.")
    error: Optional[PlaidError] = None

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> IncomeVerificationTaxformsGetResponse:
        """Create an instance of IncomeVerificationTaxformsGetResponse from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)
        # override the default output from pydantic by calling `to_dict()` of each item in document_metadata (list)
        _items = []
        if self.document_metadata:
            for _item in self.document_metadata:
                if _item:
                    _items.append(_item.to_dict())
            _dict['document_metadata'] = _items
        # override the default output from pydantic by calling `to_dict()` of each item in taxforms (list)
        _items = []
        if self.taxforms:
            for _item in self.taxforms:
                if _item:
                    _items.append(_item.to_dict())
            _dict['taxforms'] = _items
        # override the default output from pydantic by calling `to_dict()` of error
        if self.error:
            _dict['error'] = self.error.to_dict()

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> IncomeVerificationTaxformsGetResponse:
        """Create an instance of IncomeVerificationTaxformsGetResponse from a dict"""
        if type(obj) is not dict:
            return IncomeVerificationTaxformsGetResponse.parse_obj(obj)

        return IncomeVerificationTaxformsGetResponse.parse_obj({
            "request_id": obj.get("request_id"),
            "document_metadata": [DocumentMetadata.from_dict(_item) for _item in obj.get("document_metadata")],
            "taxforms": [Taxform.from_dict(_item) for _item in obj.get("taxforms")],
            "error": PlaidError.from_dict(obj.get("error"))
        })


