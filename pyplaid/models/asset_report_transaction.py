# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json

from datetime import date
from typing import List, Optional
from pydantic import BaseModel, Field, StrictBool, StrictFloat, StrictStr, validator
from pyplaid.models.credit_category import CreditCategory
from pyplaid.models.location import Location
from pyplaid.models.payment_meta import PaymentMeta
from pydantic import ValidationError

class AssetReportTransaction(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    transaction_type: Optional[StrictStr] = Field(None, description="Please use the `payment_channel` field, `transaction_type` will be deprecated in the future.  `digital:` transactions that took place online.  `place:` transactions that were made at a physical location.  `special:` transactions that relate to banks, e.g. fees or deposits.  `unresolved:` transactions that do not fit into the other three types. ")
    pending_transaction_id: Optional[StrictStr] = Field(None, description="The ID of a posted transaction's associated pending transaction, where applicable.")
    category_id: Optional[StrictStr] = Field(None, description="The ID of the category to which this transaction belongs. For a full list of categories, see [`/categories/get`](https://plaid.com/docs/api/products/transactions/#categoriesget).  If the `transactions` object was returned by an Assets endpoint such as `/asset_report/get/` or `/asset_report/pdf/get`, this field will only appear in an Asset Report with Insights.")
    category: Optional[List[StrictStr]] = Field(None, description="A hierarchical array of the categories to which this transaction belongs. For a full list of categories, see [`/categories/get`](https://plaid.com/docs/api/products/transactions/#categoriesget).  If the `transactions` object was returned by an Assets endpoint such as `/asset_report/get/` or `/asset_report/pdf/get`, this field will only appear in an Asset Report with Insights.")
    location: Optional[Location] = None
    payment_meta: Optional[PaymentMeta] = None
    account_owner: Optional[StrictStr] = Field(None, description="The name of the account owner. This field is not typically populated and only relevant when dealing with sub-accounts.")
    name: Optional[StrictStr] = Field(None, description="The merchant name or transaction description.  If the `transactions` object was returned by a Transactions endpoint such as `/transactions/get`, this field will always appear. If the `transactions` object was returned by an Assets endpoint such as `/asset_report/get/` or `/asset_report/pdf/get`, this field will only appear in an Asset Report with Insights.")
    original_description: Optional[StrictStr] = Field(..., description="The string returned by the financial institution to describe the transaction. For transactions returned by `/transactions/get`, this field is in beta and will be omitted unless the client is both enrolled in the closed beta program and has set `options.include_original_description` to `true`.")
    account_id: StrictStr = Field(..., description="The ID of the account in which this transaction occurred.")
    amount: StrictFloat = Field(..., description="The settled value of the transaction, denominated in the transactions's currency, as stated in `iso_currency_code` or `unofficial_currency_code`. Positive values when money moves out of the account; negative values when money moves in. For example, debit card purchases are positive; credit card payments, direct deposits, and refunds are negative.")
    iso_currency_code: Optional[StrictStr] = Field(..., description="The ISO-4217 currency code of the transaction. Always `null` if `unofficial_currency_code` is non-null.")
    unofficial_currency_code: Optional[StrictStr] = Field(..., description="The unofficial currency code associated with the transaction. Always `null` if `iso_currency_code` is non-`null`. Unofficial currency codes are used for currencies that do not have official ISO currency codes, such as cryptocurrencies and the currencies of certain countries.  See the [currency code schema](https://plaid.com/docs/api/accounts#currency-code-schema) for a full listing of supported `iso_currency_code`s.")
    var_date: date = Field(..., alias="date", description="For pending transactions, the date that the transaction occurred; for posted transactions, the date that the transaction posted. Both dates are returned in an [ISO 8601](https://wikipedia.org/wiki/ISO_8601) format ( `YYYY-MM-DD` ).")
    pending: StrictBool = Field(..., description="When `true`, identifies the transaction as pending or unsettled. Pending transaction details (name, type, amount, category ID) may change before they are settled.")
    transaction_id: StrictStr = Field(..., description="The unique ID of the transaction. Like all Plaid identifiers, the `transaction_id` is case sensitive.")
    merchant_name: Optional[StrictStr] = Field(None, description="The merchant name, as extracted by Plaid from the `name` field.")
    check_number: Optional[StrictStr] = Field(None, description="The check number of the transaction. This field is only populated for check transactions.")
    date_transacted: Optional[StrictStr] = Field(None, description="The date on which the transaction took place, in IS0 8601 format.")
    credit_category: Optional[CreditCategory] = None

    @validator('transaction_type')
    def transaction_type_validate_enum(cls, v):
        if v not in ('digital', 'place', 'special', 'unresolved'):
            raise ValueError("must validate the enum values ('digital', 'place', 'special', 'unresolved')")
        return v

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> AssetReportTransaction:
        """Create an instance of AssetReportTransaction from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)
        # override the default output from pydantic by calling `to_dict()` of location
        if self.location:
            _dict['location'] = self.location.to_dict()
        # override the default output from pydantic by calling `to_dict()` of payment_meta
        if self.payment_meta:
            _dict['payment_meta'] = self.payment_meta.to_dict()
        # override the default output from pydantic by calling `to_dict()` of credit_category
        if self.credit_category:
            _dict['credit_category'] = self.credit_category.to_dict()

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> AssetReportTransaction:
        """Create an instance of AssetReportTransaction from a dict"""
        if type(obj) is not dict:
            return AssetReportTransaction.parse_obj(obj)

        return AssetReportTransaction.parse_obj({
            "transaction_type": obj.get("transaction_type"),
            "pending_transaction_id": obj.get("pending_transaction_id"),
            "category_id": obj.get("category_id"),
            "category": obj.get("category"),
            "location": Location.from_dict(obj.get("location")),
            "payment_meta": PaymentMeta.from_dict(obj.get("payment_meta")),
            "account_owner": obj.get("account_owner"),
            "name": obj.get("name"),
            "original_description": obj.get("original_description"),
            "account_id": obj.get("account_id"),
            "amount": obj.get("amount"),
            "iso_currency_code": obj.get("iso_currency_code"),
            "unofficial_currency_code": obj.get("unofficial_currency_code"),
            "var_date": obj.get("date"),
            "pending": obj.get("pending"),
            "transaction_id": obj.get("transaction_id"),
            "merchant_name": obj.get("merchant_name"),
            "check_number": obj.get("check_number"),
            "date_transacted": obj.get("date_transacted"),
            "credit_category": CreditCategory.from_dict(obj.get("credit_category"))
        })


