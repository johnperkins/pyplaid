# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json



from pydantic import BaseModel, Field, StrictStr, conint
from pydantic import ValidationError

class TransferRepaymentReturn(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    transfer_id: StrictStr = Field(..., description="The unique identifier of the guaranteed transfer that was returned.")
    event_id: conint(strict=True, ge=0) = Field(..., description="The unique identifier of the corresponding `returned` transfer event.")
    amount: StrictStr = Field(..., description="The value of the returned transfer.")
    iso_currency_code: StrictStr = Field(..., description="The currency of the repayment, e.g. \"USD\".")

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> TransferRepaymentReturn:
        """Create an instance of TransferRepaymentReturn from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> TransferRepaymentReturn:
        """Create an instance of TransferRepaymentReturn from a dict"""
        if type(obj) is not dict:
            return TransferRepaymentReturn.parse_obj(obj)

        return TransferRepaymentReturn.parse_obj({
            "transfer_id": obj.get("transfer_id"),
            "event_id": obj.get("event_id"),
            "amount": obj.get("amount"),
            "iso_currency_code": obj.get("iso_currency_code")
        })


