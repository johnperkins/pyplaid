# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json


from typing import Optional
from pydantic import BaseModel, Field, StrictStr
from pyplaid.models.signal_address_data import SignalAddressData
from pyplaid.models.signal_person_name import SignalPersonName
from pydantic import ValidationError

class SignalUser(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    name: Optional[SignalPersonName] = None
    phone_number: Optional[StrictStr] = Field(None, description="The user's phone number, in E.164 format: +{countrycode}{number}. For example: \"+14151234567\"")
    email_address: Optional[StrictStr] = Field(None, description="The user's email address.")
    address: Optional[SignalAddressData] = None

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> SignalUser:
        """Create an instance of SignalUser from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)
        # override the default output from pydantic by calling `to_dict()` of name
        if self.name:
            _dict['name'] = self.name.to_dict()
        # override the default output from pydantic by calling `to_dict()` of address
        if self.address:
            _dict['address'] = self.address.to_dict()

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> SignalUser:
        """Create an instance of SignalUser from a dict"""
        if type(obj) is not dict:
            return SignalUser.parse_obj(obj)

        return SignalUser.parse_obj({
            "name": SignalPersonName.from_dict(obj.get("name")),
            "phone_number": obj.get("phone_number"),
            "email_address": obj.get("email_address"),
            "address": SignalAddressData.from_dict(obj.get("address"))
        })


