# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json

from datetime import datetime
from typing import Optional
from pydantic import BaseModel, Field, StrictStr, constr
from pyplaid.models.documentary_verification import DocumentaryVerification
from pyplaid.models.identity_verification_status import IdentityVerificationStatus
from pyplaid.models.identity_verification_step_summary import IdentityVerificationStepSummary
from pyplaid.models.identity_verification_template_reference import IdentityVerificationTemplateReference
from pyplaid.models.identity_verification_user_data import IdentityVerificationUserData
from pyplaid.models.kyc_check_details import KYCCheckDetails
from pydantic import ValidationError

class IdentityVerificationCreateResponse(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    id: StrictStr = Field(..., description="ID of the associated Identity Verification attempt.")
    client_user_id: constr(strict=True, min_length=1) = Field(..., description="An identifier to help you connect this object to your internal systems. For example, your database ID corresponding to this object.")
    created_at: datetime = Field(..., description="An ISO8601 formatted timestamp.")
    completed_at: Optional[datetime] = Field(..., description="An ISO8601 formatted timestamp.")
    previous_attempt_id: Optional[StrictStr] = Field(..., description="The ID for the Identity Verification preceding this session. This field will only be filled if the current Identity Verification is a retry of a previous attempt.")
    shareable_url: Optional[StrictStr] = Field(..., description="A shareable URL that can be sent directly to the user to complete verification")
    template: IdentityVerificationTemplateReference = ...
    user: IdentityVerificationUserData = ...
    status: IdentityVerificationStatus = ...
    steps: IdentityVerificationStepSummary = ...
    documentary_verification: Optional[DocumentaryVerification] = ...
    kyc_check: Optional[KYCCheckDetails] = ...
    watchlist_screening_id: Optional[StrictStr] = Field(..., description="ID of the associated screening.")
    request_id: StrictStr = Field(..., description="A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive.")

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> IdentityVerificationCreateResponse:
        """Create an instance of IdentityVerificationCreateResponse from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)
        # override the default output from pydantic by calling `to_dict()` of template
        if self.template:
            _dict['template'] = self.template.to_dict()
        # override the default output from pydantic by calling `to_dict()` of user
        if self.user:
            _dict['user'] = self.user.to_dict()
        # override the default output from pydantic by calling `to_dict()` of steps
        if self.steps:
            _dict['steps'] = self.steps.to_dict()
        # override the default output from pydantic by calling `to_dict()` of documentary_verification
        if self.documentary_verification:
            _dict['documentary_verification'] = self.documentary_verification.to_dict()
        # override the default output from pydantic by calling `to_dict()` of kyc_check
        if self.kyc_check:
            _dict['kyc_check'] = self.kyc_check.to_dict()

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> IdentityVerificationCreateResponse:
        """Create an instance of IdentityVerificationCreateResponse from a dict"""
        if type(obj) is not dict:
            return IdentityVerificationCreateResponse.parse_obj(obj)

        return IdentityVerificationCreateResponse.parse_obj({
            "id": obj.get("id"),
            "client_user_id": obj.get("client_user_id"),
            "created_at": obj.get("created_at"),
            "completed_at": obj.get("completed_at"),
            "previous_attempt_id": obj.get("previous_attempt_id"),
            "shareable_url": obj.get("shareable_url"),
            "template": IdentityVerificationTemplateReference.from_dict(obj.get("template")),
            "user": IdentityVerificationUserData.from_dict(obj.get("user")),
            "status": obj.get("status"),
            "steps": IdentityVerificationStepSummary.from_dict(obj.get("steps")),
            "documentary_verification": DocumentaryVerification.from_dict(obj.get("documentary_verification")),
            "kyc_check": KYCCheckDetails.from_dict(obj.get("kyc_check")),
            "watchlist_screening_id": obj.get("watchlist_screening_id"),
            "request_id": obj.get("request_id")
        })


