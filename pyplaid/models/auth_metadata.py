# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json


from typing import Optional
from pydantic import BaseModel
from pyplaid.models.auth_supported_methods import AuthSupportedMethods
from pydantic import ValidationError

class AuthMetadata(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    supported_methods: Optional[AuthSupportedMethods] = ...

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> AuthMetadata:
        """Create an instance of AuthMetadata from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)
        # override the default output from pydantic by calling `to_dict()` of supported_methods
        if self.supported_methods:
            _dict['supported_methods'] = self.supported_methods.to_dict()

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> AuthMetadata:
        """Create an instance of AuthMetadata from a dict"""
        if type(obj) is not dict:
            return AuthMetadata.parse_obj(obj)

        return AuthMetadata.parse_obj({
            "supported_methods": AuthSupportedMethods.from_dict(obj.get("supported_methods"))
        })


