# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json



from pydantic import BaseModel, Field, StrictStr, validator
from pydantic import ValidationError

class DepositSwitchTargetAccount(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    account_number: StrictStr = Field(..., description="Account number for deposit switch destination")
    routing_number: StrictStr = Field(..., description="Routing number for deposit switch destination")
    account_name: StrictStr = Field(..., description="The name of the deposit switch destination account, as it will be displayed to the end user in the Deposit Switch interface. It is not required to match the name used in online banking.")
    account_subtype: StrictStr = Field(..., description="The account subtype of the account, either `checking` or `savings`.")

    @validator('account_subtype')
    def account_subtype_validate_enum(cls, v):
        if v not in ('checking', 'savings'):
            raise ValueError("must validate the enum values ('checking', 'savings')")
        return v

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> DepositSwitchTargetAccount:
        """Create an instance of DepositSwitchTargetAccount from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> DepositSwitchTargetAccount:
        """Create an instance of DepositSwitchTargetAccount from a dict"""
        if type(obj) is not dict:
            return DepositSwitchTargetAccount.parse_obj(obj)

        return DepositSwitchTargetAccount.parse_obj({
            "account_number": obj.get("account_number"),
            "routing_number": obj.get("routing_number"),
            "account_name": obj.get("account_name"),
            "account_subtype": obj.get("account_subtype")
        })


