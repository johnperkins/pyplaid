# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json



from pydantic import BaseModel, Field, StrictInt
from pydantic import ValidationError

class DisbursementLimits(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    average_amount: StrictInt = Field(..., description="Average disbursement amount in a single transaction, in dollars.")
    maximum_amount: StrictInt = Field(..., description="Maximum disbursement amount in a single transaction, in dollars.")
    monthly_amount: StrictInt = Field(..., description="Monthly disbursement amount, in dollars.")

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> DisbursementLimits:
        """Create an instance of DisbursementLimits from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> DisbursementLimits:
        """Create an instance of DisbursementLimits from a dict"""
        if type(obj) is not dict:
            return DisbursementLimits.parse_obj(obj)

        return DisbursementLimits.parse_obj({
            "average_amount": obj.get("average_amount"),
            "maximum_amount": obj.get("maximum_amount"),
            "monthly_amount": obj.get("monthly_amount")
        })


