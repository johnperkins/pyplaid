# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json


from typing import Optional
from pydantic import BaseModel, Field, StrictBool, StrictFloat, StrictStr, constr
from pyplaid.models.signal_device import SignalDevice
from pyplaid.models.signal_user import SignalUser
from pydantic import ValidationError

class SignalEvaluateRequest(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    client_id: Optional[StrictStr] = Field(None, description="Your Plaid API `client_id`. The `client_id` is required and may be provided either in the `PLAID-CLIENT-ID` header or as part of a request body.")
    secret: Optional[StrictStr] = Field(None, description="Your Plaid API `secret`. The `secret` is required and may be provided either in the `PLAID-SECRET` header or as part of a request body.")
    access_token: StrictStr = Field(..., description="The access token associated with the Item data is being requested for.")
    account_id: StrictStr = Field(..., description="The Plaid `account_id` of the account whose verification status is to be modified. The `account_id` is returned in the `/accounts/get` endpoint as well as the [`onSuccess`](/docs/link/ios/#link-ios-onsuccess-linkSuccess-metadata-accounts-id) callback metadata.  This will return an [`INVALID_ACCOUNT_ID`](/docs/errors/invalid-input/#invalid_account_id) error if the account has been removed at the bank or if the `account_id` is no longer valid.")
    client_transaction_id: constr(strict=True, max_length=36, min_length=1) = Field(..., description="The unique ID that you would like to use to refer to this transaction. For your convenience mapping your internal data, you could use your internal ID/identifier for this transaction. The max length for this field is 36 characters.")
    amount: StrictFloat = Field(..., description="The transaction amount, in USD (e.g. `102.05`)")
    user_present: Optional[StrictBool] = Field(None, description="`true` if the end user is present while initiating the ACH transfer and the endpoint is being called; `false` otherwise (for example, when the ACH transfer is scheduled and the end user is not present, or you call this endpoint after the ACH transfer but before submitting the Nacha file for ACH processing).")
    client_user_id: Optional[constr(strict=True, max_length=36)] = Field(None, description="A unique ID that identifies the end user in your system. This ID is used to correlate requests by a user with multiple Items. The max length for this field is 36 characters. Personally identifiable information, such as an email address or phone number, should not be used in the `client_user_id`.")
    user: Optional[SignalUser] = None
    device: Optional[SignalDevice] = None

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> SignalEvaluateRequest:
        """Create an instance of SignalEvaluateRequest from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)
        # override the default output from pydantic by calling `to_dict()` of user
        if self.user:
            _dict['user'] = self.user.to_dict()
        # override the default output from pydantic by calling `to_dict()` of device
        if self.device:
            _dict['device'] = self.device.to_dict()

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> SignalEvaluateRequest:
        """Create an instance of SignalEvaluateRequest from a dict"""
        if type(obj) is not dict:
            return SignalEvaluateRequest.parse_obj(obj)

        return SignalEvaluateRequest.parse_obj({
            "client_id": obj.get("client_id"),
            "secret": obj.get("secret"),
            "access_token": obj.get("access_token"),
            "account_id": obj.get("account_id"),
            "client_transaction_id": obj.get("client_transaction_id"),
            "amount": obj.get("amount"),
            "user_present": obj.get("user_present"),
            "client_user_id": obj.get("client_user_id"),
            "user": SignalUser.from_dict(obj.get("user")),
            "device": SignalDevice.from_dict(obj.get("device"))
        })


