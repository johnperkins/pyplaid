# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json

from datetime import datetime
from typing import List
from pydantic import BaseModel, Field, StrictStr
from pyplaid.models.account_assets import AccountAssets
from pydantic import ValidationError

class AssetReportItem(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    item_id: StrictStr = Field(..., description="The `item_id` of the Item associated with this webhook, warning, or error")
    institution_name: StrictStr = Field(..., description="The full financial institution name associated with the Item.")
    institution_id: StrictStr = Field(..., description="The id of the financial institution associated with the Item.")
    date_last_updated: datetime = Field(..., description="The date and time when this Item’s data was last retrieved from the financial institution, in [ISO 8601](https://wikipedia.org/wiki/ISO_8601) format.")
    accounts: List[AccountAssets] = Field(..., description="Data about each of the accounts open on the Item.")

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> AssetReportItem:
        """Create an instance of AssetReportItem from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)
        # override the default output from pydantic by calling `to_dict()` of each item in accounts (list)
        _items = []
        if self.accounts:
            for _item in self.accounts:
                if _item:
                    _items.append(_item.to_dict())
            _dict['accounts'] = _items

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> AssetReportItem:
        """Create an instance of AssetReportItem from a dict"""
        if type(obj) is not dict:
            return AssetReportItem.parse_obj(obj)

        return AssetReportItem.parse_obj({
            "item_id": obj.get("item_id"),
            "institution_name": obj.get("institution_name"),
            "institution_id": obj.get("institution_id"),
            "date_last_updated": obj.get("date_last_updated"),
            "accounts": [AccountAssets.from_dict(_item) for _item in obj.get("accounts")]
        })


