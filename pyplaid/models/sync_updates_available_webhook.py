# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json



from pydantic import BaseModel, Field, StrictBool, StrictStr
from pyplaid.models.webhook_environment_values import WebhookEnvironmentValues
from pydantic import ValidationError

class SyncUpdatesAvailableWebhook(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    webhook_type: StrictStr = Field(..., description="`TRANSACTIONS`")
    webhook_code: StrictStr = Field(..., description="`SYNC_UPDATES_AVAILABLE`")
    item_id: StrictStr = Field(..., description="The `item_id` of the Item associated with this webhook, warning, or error")
    initial_update_complete: StrictBool = Field(..., description="Indicates if initial pull information is available.")
    historical_update_complete: StrictBool = Field(..., description="Indicates if historical pull information is available.")
    environment: WebhookEnvironmentValues = ...

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> SyncUpdatesAvailableWebhook:
        """Create an instance of SyncUpdatesAvailableWebhook from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> SyncUpdatesAvailableWebhook:
        """Create an instance of SyncUpdatesAvailableWebhook from a dict"""
        if type(obj) is not dict:
            return SyncUpdatesAvailableWebhook.parse_obj(obj)

        return SyncUpdatesAvailableWebhook.parse_obj({
            "webhook_type": obj.get("webhook_type"),
            "webhook_code": obj.get("webhook_code"),
            "item_id": obj.get("item_id"),
            "initial_update_complete": obj.get("initial_update_complete"),
            "historical_update_complete": obj.get("historical_update_complete"),
            "environment": obj.get("environment")
        })


