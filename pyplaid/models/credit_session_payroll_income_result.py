# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json


from typing import Optional
from pydantic import BaseModel, Field, StrictInt, StrictStr
from pydantic import ValidationError

class CreditSessionPayrollIncomeResult(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    num_paystubs_retrieved: Optional[StrictInt] = Field(None, description="The number of paystubs retrieved from a payroll provider.")
    num_w2s_retrieved: Optional[StrictInt] = Field(None, description="The number of w2s retrieved from a payroll provider.")
    institution_id: Optional[StrictStr] = Field(None, description="The Plaid Institution ID associated with the Item.")
    institution_name: Optional[StrictStr] = Field(None, description="The Institution Name associated with the Item.")

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> CreditSessionPayrollIncomeResult:
        """Create an instance of CreditSessionPayrollIncomeResult from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> CreditSessionPayrollIncomeResult:
        """Create an instance of CreditSessionPayrollIncomeResult from a dict"""
        if type(obj) is not dict:
            return CreditSessionPayrollIncomeResult.parse_obj(obj)

        return CreditSessionPayrollIncomeResult.parse_obj({
            "num_paystubs_retrieved": obj.get("num_paystubs_retrieved"),
            "num_w2s_retrieved": obj.get("num_w2s_retrieved"),
            "institution_id": obj.get("institution_id"),
            "institution_name": obj.get("institution_name")
        })


