# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json


from typing import Optional
from pydantic import BaseModel, Field, StrictBool, StrictInt
from pydantic import ValidationError

class NameMatchScore(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    score: Optional[StrictInt] = Field(None, description="Represents the match score for name. 100 is a perfect score, 85-99 means a strong match, 50-84 is a partial match, less than 50 is a weak match and 0 is a complete mismatch. If the name is missing from either the API or financial institution, this is empty.")
    is_first_name_or_last_name_match: Optional[StrictBool] = Field(None, description="first or last name completely matched")
    is_nickname_match: Optional[StrictBool] = Field(None, description="nickname matched, example Jennifer and Jenn.")
    is_business_name_detected: Optional[StrictBool] = Field(None, description="If the name on either of the names that was matched for the score was a business name, with corp, llc, ltd etc in the name. While this being true confirms business name, false means it was either not a business name or Plaid could not detect it as such, since a lot of business names match owner names and are hard to detect.")

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> NameMatchScore:
        """Create an instance of NameMatchScore from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> NameMatchScore:
        """Create an instance of NameMatchScore from a dict"""
        if type(obj) is not dict:
            return NameMatchScore.parse_obj(obj)

        return NameMatchScore.parse_obj({
            "score": obj.get("score"),
            "is_first_name_or_last_name_match": obj.get("is_first_name_or_last_name_match"),
            "is_nickname_match": obj.get("is_nickname_match"),
            "is_business_name_detected": obj.get("is_business_name_detected")
        })


