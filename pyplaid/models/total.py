# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json


from typing import Optional
from pydantic import BaseModel, Field, StrictStr
from pyplaid.models.pay import Pay
from pyplaid.models.total_canonical_description import TotalCanonicalDescription
from pydantic import ValidationError

class Total(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    canonical_description: Optional[TotalCanonicalDescription] = None
    description: Optional[StrictStr] = Field(None, description="Text of the line item as printed on the paystub.")
    current_pay: Optional[Pay] = None
    ytd_pay: Optional[Pay] = None

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> Total:
        """Create an instance of Total from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)
        # override the default output from pydantic by calling `to_dict()` of current_pay
        if self.current_pay:
            _dict['current_pay'] = self.current_pay.to_dict()
        # override the default output from pydantic by calling `to_dict()` of ytd_pay
        if self.ytd_pay:
            _dict['ytd_pay'] = self.ytd_pay.to_dict()

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> Total:
        """Create an instance of Total from a dict"""
        if type(obj) is not dict:
            return Total.parse_obj(obj)

        return Total.parse_obj({
            "canonical_description": obj.get("canonical_description"),
            "description": obj.get("description"),
            "current_pay": Pay.from_dict(obj.get("current_pay")),
            "ytd_pay": Pay.from_dict(obj.get("ytd_pay"))
        })


