# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json


from typing import List, Optional
from pydantic import BaseModel, Field, StrictStr
from pyplaid.models.country_code import CountryCode
from pyplaid.models.link_token_account_filters import LinkTokenAccountFilters
from pyplaid.models.link_token_create_institution_data import LinkTokenCreateInstitutionData
from pyplaid.models.link_token_create_request_auth import LinkTokenCreateRequestAuth
from pyplaid.models.link_token_create_request_deposit_switch import LinkTokenCreateRequestDepositSwitch
from pyplaid.models.link_token_create_request_identity_verification import LinkTokenCreateRequestIdentityVerification
from pyplaid.models.link_token_create_request_income_verification import LinkTokenCreateRequestIncomeVerification
from pyplaid.models.link_token_create_request_payment_initiation import LinkTokenCreateRequestPaymentInitiation
from pyplaid.models.link_token_create_request_transfer import LinkTokenCreateRequestTransfer
from pyplaid.models.link_token_create_request_update import LinkTokenCreateRequestUpdate
from pyplaid.models.link_token_create_request_user import LinkTokenCreateRequestUser
from pyplaid.models.link_token_eu_config import LinkTokenEUConfig
from pyplaid.models.link_token_investments import LinkTokenInvestments
from pyplaid.models.products import Products
from pydantic import ValidationError

class LinkTokenCreateRequest(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    client_id: Optional[StrictStr] = Field(None, description="Your Plaid API `client_id`. The `client_id` is required and may be provided either in the `PLAID-CLIENT-ID` header or as part of a request body.")
    secret: Optional[StrictStr] = Field(None, description="Your Plaid API `secret`. The `secret` is required and may be provided either in the `PLAID-SECRET` header or as part of a request body.")
    client_name: StrictStr = Field(..., description="The name of your application, as it should be displayed in Link. Maximum length of 30 characters. If a value longer than 30 characters is provided, Link will display \"This Application\" instead.")
    language: StrictStr = Field(..., description="The language that Link should be displayed in.  Supported languages are: - Danish (`'da'`) - Dutch (`'nl'`) - English (`'en'`) - Estonian (`'et'`) - French (`'fr'`) - German (`'de'`) - Italian (`'it'`) - Latvian (`'lv'`) - Lithuanian (`'lt'`) - Norwegian (`'no'`) - Polish (`'po'`) - Romanian (`'ro'`) - Spanish (`'es'`) - Swedish (`'se'`)  When using a Link customization, the language configured here must match the setting in the customization, or the customization will not be applied.")
    country_codes: List[CountryCode] = Field(..., description="Specify an array of Plaid-supported country codes using the ISO-3166-1 alpha-2 country code standard. Institutions from all listed countries will be shown. For a complete mapping of supported products by country, see https://plaid.com/global/.  If Link is launched with multiple country codes, only products that you are enabled for in all countries will be used by Link. Note that while all countries are enabled by default in Sandbox and Development, in Production only US and Canada are enabled by default. To gain access to European institutions in the Production environment, [file a product access Support ticket](https://dashboard.plaid.com/support/new/product-and-development/product-troubleshooting/request-product-access) via the Plaid dashboard. If you initialize with a European country code, your users will see the European consent panel during the Link flow.  If using a Link customization, make sure the country codes in the customization match those specified in `country_codes`. If both `country_codes` and a Link customization are used, the value in `country_codes` may override the value in the customization.  If using the Auth features Instant Match, Same-day Micro-deposits, or Automated Micro-deposits, `country_codes` must be set to `['US']`.")
    user: LinkTokenCreateRequestUser = ...
    products: Optional[List[Products]] = Field(None, description="List of Plaid product(s) you wish to use. If launching Link in update mode, should be omitted; required otherwise.  `balance` is *not* a valid value, the Balance product does not require explicit initialization and will automatically be initialized when any other product is initialized.  The products specified here will determine which institutions will be available to your users in Link. Only institutions that support *all* requested products can be selected; a if a user attempts to select an institution that does not support a listed product, a \"Connectivity not supported\" error message will appear in Link. To maximize the number of institutions available, initialize Link with the minimal product set required for your use case. Additional products can be added after Link initialization by calling the relevant endpoints. For details and exceptions, see [Choosing when to initialize products](https://plaid.com/docs/link/best-practices/#choosing-when-to-initialize-products).  Note that, unless you have opted to disable Instant Match support, institutions that support Instant Match will also be shown in Link if `auth` is specified as a product, even though these institutions do not contain `auth` in their product array.  In Production, you will be billed for each product that you specify when initializing Link. Note that a product cannot be removed from an Item once the Item has been initialized with that product. To stop billing on an Item for subscription-based products, such as Liabilities, Investments, and Transactions, remove the Item via `/item/remove`.")
    additional_consented_products: Optional[List[Products]] = Field(None, description="(Beta) This field has no effect unless you are participating in the Product Scope Transparency beta program. List of additional Plaid product(s) you wish to collect consent for. These products will not be billed until you start using them by calling the relevant endpoints.  `balance` is *not* a valid value, the Balance product does not require explicit initialization and will automatically have consent collected.  Institutions that do not support these products will still be shown in Link")
    webhook: Optional[StrictStr] = Field(None, description="The destination URL to which any webhooks should be sent.")
    access_token: Optional[StrictStr] = Field(None, description="The `access_token` associated with the Item to update or reference, used when updating, modifying, or accessing an existing `access_token`. Used when launching Link in update mode, when completing the Same-day (manual) Micro-deposit flow, or (optionally) when initializing Link for a returning user as part of the Transfer UI flow.")
    link_customization_name: Optional[StrictStr] = Field(None, description="The name of the Link customization from the Plaid Dashboard to be applied to Link. If not specified, the `default` customization will be used. When using a Link customization, the language in the customization must match the language selected via the `language` parameter, and the countries in the customization should match the country codes selected via `country_codes`.")
    redirect_uri: Optional[StrictStr] = Field(None, description="A URI indicating the destination where a user should be forwarded after completing the Link flow; used to support OAuth authentication flows when launching Link in the browser or via a webview. The `redirect_uri` should not contain any query parameters. When used in Production or Development, must be an https URI. To specify any subdomain, use `*` as a wildcard character, e.g. `https://*.example.com/oauth.html`. If `android_package_name` is specified, this field should be left blank.  Note that any redirect URI must also be added to the Allowed redirect URIs list in the [developer dashboard](https://dashboard.plaid.com/team/api).")
    android_package_name: Optional[StrictStr] = Field(None, description="The name of your app's Android package. Required if using the `link_token` to initialize Link on Android. When creating a `link_token` for initializing Link on other platforms, this field must be left blank. Any package name specified here must also be added to the Allowed Android package names setting on the [developer dashboard](https://dashboard.plaid.com/team/api). ")
    institution_data: Optional[LinkTokenCreateInstitutionData] = None
    account_filters: Optional[LinkTokenAccountFilters] = None
    eu_config: Optional[LinkTokenEUConfig] = None
    institution_id: Optional[StrictStr] = Field(None, description="Used for certain Europe-only configurations, as well as certain legacy use cases in other regions.")
    payment_initiation: Optional[LinkTokenCreateRequestPaymentInitiation] = None
    deposit_switch: Optional[LinkTokenCreateRequestDepositSwitch] = None
    income_verification: Optional[LinkTokenCreateRequestIncomeVerification] = None
    auth: Optional[LinkTokenCreateRequestAuth] = None
    transfer: Optional[LinkTokenCreateRequestTransfer] = None
    update: Optional[LinkTokenCreateRequestUpdate] = None
    identity_verification: Optional[LinkTokenCreateRequestIdentityVerification] = None
    user_token: Optional[StrictStr] = Field(None, description="A user token generated using `/user/create`. Any item created during the link session will be associated with the user.")
    investments: Optional[LinkTokenInvestments] = None

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> LinkTokenCreateRequest:
        """Create an instance of LinkTokenCreateRequest from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)
        # override the default output from pydantic by calling `to_dict()` of user
        if self.user:
            _dict['user'] = self.user.to_dict()
        # override the default output from pydantic by calling `to_dict()` of institution_data
        if self.institution_data:
            _dict['institution_data'] = self.institution_data.to_dict()
        # override the default output from pydantic by calling `to_dict()` of account_filters
        if self.account_filters:
            _dict['account_filters'] = self.account_filters.to_dict()
        # override the default output from pydantic by calling `to_dict()` of eu_config
        if self.eu_config:
            _dict['eu_config'] = self.eu_config.to_dict()
        # override the default output from pydantic by calling `to_dict()` of payment_initiation
        if self.payment_initiation:
            _dict['payment_initiation'] = self.payment_initiation.to_dict()
        # override the default output from pydantic by calling `to_dict()` of deposit_switch
        if self.deposit_switch:
            _dict['deposit_switch'] = self.deposit_switch.to_dict()
        # override the default output from pydantic by calling `to_dict()` of income_verification
        if self.income_verification:
            _dict['income_verification'] = self.income_verification.to_dict()
        # override the default output from pydantic by calling `to_dict()` of auth
        if self.auth:
            _dict['auth'] = self.auth.to_dict()
        # override the default output from pydantic by calling `to_dict()` of transfer
        if self.transfer:
            _dict['transfer'] = self.transfer.to_dict()
        # override the default output from pydantic by calling `to_dict()` of update
        if self.update:
            _dict['update'] = self.update.to_dict()
        # override the default output from pydantic by calling `to_dict()` of identity_verification
        if self.identity_verification:
            _dict['identity_verification'] = self.identity_verification.to_dict()
        # override the default output from pydantic by calling `to_dict()` of investments
        if self.investments:
            _dict['investments'] = self.investments.to_dict()

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> LinkTokenCreateRequest:
        """Create an instance of LinkTokenCreateRequest from a dict"""
        if type(obj) is not dict:
            return LinkTokenCreateRequest.parse_obj(obj)

        return LinkTokenCreateRequest.parse_obj({
            "client_id": obj.get("client_id"),
            "secret": obj.get("secret"),
            "client_name": obj.get("client_name"),
            "language": obj.get("language"),
            "country_codes": obj.get("country_codes"),
            "user": LinkTokenCreateRequestUser.from_dict(obj.get("user")),
            "products": obj.get("products"),
            "additional_consented_products": obj.get("additional_consented_products"),
            "webhook": obj.get("webhook"),
            "access_token": obj.get("access_token"),
            "link_customization_name": obj.get("link_customization_name"),
            "redirect_uri": obj.get("redirect_uri"),
            "android_package_name": obj.get("android_package_name"),
            "institution_data": LinkTokenCreateInstitutionData.from_dict(obj.get("institution_data")),
            "account_filters": LinkTokenAccountFilters.from_dict(obj.get("account_filters")),
            "eu_config": LinkTokenEUConfig.from_dict(obj.get("eu_config")),
            "institution_id": obj.get("institution_id"),
            "payment_initiation": LinkTokenCreateRequestPaymentInitiation.from_dict(obj.get("payment_initiation")),
            "deposit_switch": LinkTokenCreateRequestDepositSwitch.from_dict(obj.get("deposit_switch")),
            "income_verification": LinkTokenCreateRequestIncomeVerification.from_dict(obj.get("income_verification")),
            "auth": LinkTokenCreateRequestAuth.from_dict(obj.get("auth")),
            "transfer": LinkTokenCreateRequestTransfer.from_dict(obj.get("transfer")),
            "update": LinkTokenCreateRequestUpdate.from_dict(obj.get("update")),
            "identity_verification": LinkTokenCreateRequestIdentityVerification.from_dict(obj.get("identity_verification")),
            "user_token": obj.get("user_token"),
            "investments": LinkTokenInvestments.from_dict(obj.get("investments"))
        })


