# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json

from datetime import datetime
from typing import Dict, Optional
from pydantic import BaseModel, Field, StrictBool, StrictStr
from pyplaid.models.ach_class import ACHClass
from pyplaid.models.bank_transfer_direction import BankTransferDirection
from pyplaid.models.bank_transfer_failure import BankTransferFailure
from pyplaid.models.bank_transfer_network import BankTransferNetwork
from pyplaid.models.bank_transfer_status import BankTransferStatus
from pyplaid.models.bank_transfer_type import BankTransferType
from pyplaid.models.bank_transfer_user import BankTransferUser
from pydantic import ValidationError

class BankTransfer(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    id: StrictStr = Field(..., description="Plaid’s unique identifier for a bank transfer.")
    ach_class: ACHClass = ...
    account_id: StrictStr = Field(..., description="The account ID that should be credited/debited for this bank transfer.")
    type: BankTransferType = ...
    user: BankTransferUser = ...
    amount: StrictStr = Field(..., description="The amount of the bank transfer (decimal string with two digits of precision e.g. \"10.00\").")
    iso_currency_code: StrictStr = Field(..., description="The currency of the transfer amount, e.g. \"USD\"")
    description: StrictStr = Field(..., description="The description of the transfer.")
    created: datetime = Field(..., description="The datetime when this bank transfer was created. This will be of the form `2006-01-02T15:04:05Z`")
    status: BankTransferStatus = ...
    network: BankTransferNetwork = ...
    cancellable: StrictBool = Field(..., description="When `true`, you can still cancel this bank transfer.")
    failure_reason: Optional[BankTransferFailure] = ...
    custom_tag: Optional[StrictStr] = Field(..., description="A string containing the custom tag provided by the client in the create request. Will be null if not provided.")
    metadata: Optional[Dict[str, StrictStr]] = Field(..., description="The Metadata object is a mapping of client-provided string fields to any string value. The following limitations apply: - The JSON values must be Strings (no nested JSON objects allowed) - Only ASCII characters may be used - Maximum of 50 key/value pairs - Maximum key length of 40 characters - Maximum value length of 500 characters ")
    origination_account_id: StrictStr = Field(..., description="Plaid’s unique identifier for the origination account that was used for this transfer.")
    direction: Optional[BankTransferDirection] = ...

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> BankTransfer:
        """Create an instance of BankTransfer from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)
        # override the default output from pydantic by calling `to_dict()` of user
        if self.user:
            _dict['user'] = self.user.to_dict()
        # override the default output from pydantic by calling `to_dict()` of failure_reason
        if self.failure_reason:
            _dict['failure_reason'] = self.failure_reason.to_dict()

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> BankTransfer:
        """Create an instance of BankTransfer from a dict"""
        if type(obj) is not dict:
            return BankTransfer.parse_obj(obj)

        return BankTransfer.parse_obj({
            "id": obj.get("id"),
            "ach_class": obj.get("ach_class"),
            "account_id": obj.get("account_id"),
            "type": obj.get("type"),
            "user": BankTransferUser.from_dict(obj.get("user")),
            "amount": obj.get("amount"),
            "iso_currency_code": obj.get("iso_currency_code"),
            "description": obj.get("description"),
            "created": obj.get("created"),
            "status": obj.get("status"),
            "network": obj.get("network"),
            "cancellable": obj.get("cancellable"),
            "failure_reason": BankTransferFailure.from_dict(obj.get("failure_reason")),
            "custom_tag": obj.get("custom_tag"),
            "metadata": obj.get("metadata"),
            "origination_account_id": obj.get("origination_account_id"),
            "direction": obj.get("direction")
        })


