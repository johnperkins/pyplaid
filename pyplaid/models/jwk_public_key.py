# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json


from typing import Optional
from pydantic import BaseModel, Field, StrictInt, StrictStr
from pydantic import ValidationError

class JWKPublicKey(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    alg: StrictStr = Field(..., description="The alg member identifies the cryptographic algorithm family used with the key.")
    crv: StrictStr = Field(..., description="The crv member identifies the cryptographic curve used with the key.")
    kid: StrictStr = Field(..., description="The kid (Key ID) member can be used to match a specific key. This can be used, for instance, to choose among a set of keys within the JWK during key rollover.")
    kty: StrictStr = Field(..., description="The kty (key type) parameter identifies the cryptographic algorithm family used with the key, such as RSA or EC.")
    use: StrictStr = Field(..., description="The use (public key use) parameter identifies the intended use of the public key.")
    x: StrictStr = Field(..., description="The x member contains the x coordinate for the elliptic curve point.")
    y: StrictStr = Field(..., description="The y member contains the y coordinate for the elliptic curve point.")
    created_at: StrictInt = Field(..., description="The timestamp when the key was created, in Unix time.")
    expired_at: Optional[StrictInt] = Field(..., description="The timestamp when the key expired, in Unix time.")

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> JWKPublicKey:
        """Create an instance of JWKPublicKey from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> JWKPublicKey:
        """Create an instance of JWKPublicKey from a dict"""
        if type(obj) is not dict:
            return JWKPublicKey.parse_obj(obj)

        return JWKPublicKey.parse_obj({
            "alg": obj.get("alg"),
            "crv": obj.get("crv"),
            "kid": obj.get("kid"),
            "kty": obj.get("kty"),
            "use": obj.get("use"),
            "x": obj.get("x"),
            "y": obj.get("y"),
            "created_at": obj.get("created_at"),
            "expired_at": obj.get("expired_at")
        })


