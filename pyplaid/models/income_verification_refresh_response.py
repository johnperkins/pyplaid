# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json



from pydantic import BaseModel, Field, StrictStr
from pyplaid.models.verification_refresh_status import VerificationRefreshStatus
from pydantic import ValidationError

class IncomeVerificationRefreshResponse(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    request_id: StrictStr = Field(..., description="A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive.")
    verification_refresh_status: VerificationRefreshStatus = ...

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> IncomeVerificationRefreshResponse:
        """Create an instance of IncomeVerificationRefreshResponse from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> IncomeVerificationRefreshResponse:
        """Create an instance of IncomeVerificationRefreshResponse from a dict"""
        if type(obj) is not dict:
            return IncomeVerificationRefreshResponse.parse_obj(obj)

        return IncomeVerificationRefreshResponse.parse_obj({
            "request_id": obj.get("request_id"),
            "verification_refresh_status": obj.get("verification_refresh_status")
        })


