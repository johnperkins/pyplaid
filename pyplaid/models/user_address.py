# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json


from typing import Optional
from pydantic import BaseModel, Field, StrictStr, constr
from pydantic import ValidationError

class UserAddress(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    street: StrictStr = Field(..., description="The primary street portion of an address. If the user has submitted their address, this field will always be filled.")
    street2: Optional[StrictStr] = Field(None, description="Extra street information, like an apartment or suite number.")
    city: StrictStr = Field(..., description="City from the end user's address")
    region: StrictStr = Field(..., description="An ISO 3166-2 subdivision code. Related terms would be \"state\", \"province\", \"prefecture\", \"zone\", \"subdivision\", etc.")
    postal_code: StrictStr = Field(..., description="The postal code for the associated address. Between 2 and 10 alphanumeric characters. For US-based addresses this must be 5 numeric digits.")
    country: constr(strict=True, min_length=2) = Field(..., description="Valid, capitalized, two-letter ISO code representing the country of this object. Must be in ISO 3166-1 alpha-2 form.")

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> UserAddress:
        """Create an instance of UserAddress from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> UserAddress:
        """Create an instance of UserAddress from a dict"""
        if type(obj) is not dict:
            return UserAddress.parse_obj(obj)

        return UserAddress.parse_obj({
            "street": obj.get("street"),
            "street2": obj.get("street2"),
            "city": obj.get("city"),
            "region": obj.get("region"),
            "postal_code": obj.get("postal_code"),
            "country": obj.get("country")
        })


