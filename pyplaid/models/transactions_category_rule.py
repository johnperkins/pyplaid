# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json

from datetime import datetime
from typing import Optional
from pydantic import BaseModel, Field, StrictStr
from pyplaid.models.transactions_rule_details import TransactionsRuleDetails
from pydantic import ValidationError

class TransactionsCategoryRule(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    id: Optional[StrictStr] = Field(None, description="A unique identifier of the rule created")
    item_id: Optional[StrictStr] = Field(None, description="A unique identifier of the item the rule was created for")
    created_at: Optional[datetime] = Field(None, description="Date and time when a rule was created in [ISO 8601](https://wikipedia.org/wiki/ISO_8601) format ( `YYYY-MM-DDTHH:mm:ssZ` ). ")
    personal_finance_category: Optional[StrictStr] = Field(None, description="Personal finance category unique identifier.  In the personal finance category taxonomy, this field is represented by the detailed category field. ")
    rule_details: Optional[TransactionsRuleDetails] = None

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> TransactionsCategoryRule:
        """Create an instance of TransactionsCategoryRule from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)
        # override the default output from pydantic by calling `to_dict()` of rule_details
        if self.rule_details:
            _dict['rule_details'] = self.rule_details.to_dict()

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> TransactionsCategoryRule:
        """Create an instance of TransactionsCategoryRule from a dict"""
        if type(obj) is not dict:
            return TransactionsCategoryRule.parse_obj(obj)

        return TransactionsCategoryRule.parse_obj({
            "id": obj.get("id"),
            "item_id": obj.get("item_id"),
            "created_at": obj.get("created_at"),
            "personal_finance_category": obj.get("personal_finance_category"),
            "rule_details": TransactionsRuleDetails.from_dict(obj.get("rule_details"))
        })


