# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json


from typing import List, Optional
from pydantic import BaseModel, Field, StrictStr
from pyplaid.models.income_verification_precheck_employer import IncomeVerificationPrecheckEmployer
from pyplaid.models.income_verification_precheck_military_info import IncomeVerificationPrecheckMilitaryInfo
from pyplaid.models.income_verification_precheck_payroll_institution import IncomeVerificationPrecheckPayrollInstitution
from pydantic import ValidationError

class CreditPayrollIncomePrecheckRequest(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    client_id: Optional[StrictStr] = Field(None, description="Your Plaid API `client_id`. The `client_id` is required and may be provided either in the `PLAID-CLIENT-ID` header or as part of a request body.")
    secret: Optional[StrictStr] = Field(None, description="Your Plaid API `secret`. The `secret` is required and may be provided either in the `PLAID-SECRET` header or as part of a request body.")
    user_token: Optional[StrictStr] = Field(None, description="The user token associated with the User data is being requested for.")
    access_tokens: Optional[List[StrictStr]] = Field(None, description="An array of access tokens corresponding to Items belonging to the user whose eligibility is being checked. Note that if the Items specified here are not already initialized with `transactions`, providing them in this field will cause these Items to be initialized with (and billed for) the Transactions product.")
    employer: Optional[IncomeVerificationPrecheckEmployer] = None
    us_military_info: Optional[IncomeVerificationPrecheckMilitaryInfo] = None
    payroll_institution: Optional[IncomeVerificationPrecheckPayrollInstitution] = None

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> CreditPayrollIncomePrecheckRequest:
        """Create an instance of CreditPayrollIncomePrecheckRequest from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)
        # override the default output from pydantic by calling `to_dict()` of employer
        if self.employer:
            _dict['employer'] = self.employer.to_dict()
        # override the default output from pydantic by calling `to_dict()` of us_military_info
        if self.us_military_info:
            _dict['us_military_info'] = self.us_military_info.to_dict()
        # override the default output from pydantic by calling `to_dict()` of payroll_institution
        if self.payroll_institution:
            _dict['payroll_institution'] = self.payroll_institution.to_dict()

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> CreditPayrollIncomePrecheckRequest:
        """Create an instance of CreditPayrollIncomePrecheckRequest from a dict"""
        if type(obj) is not dict:
            return CreditPayrollIncomePrecheckRequest.parse_obj(obj)

        return CreditPayrollIncomePrecheckRequest.parse_obj({
            "client_id": obj.get("client_id"),
            "secret": obj.get("secret"),
            "user_token": obj.get("user_token"),
            "access_tokens": obj.get("access_tokens"),
            "employer": IncomeVerificationPrecheckEmployer.from_dict(obj.get("employer")),
            "us_military_info": IncomeVerificationPrecheckMilitaryInfo.from_dict(obj.get("us_military_info")),
            "payroll_institution": IncomeVerificationPrecheckPayrollInstitution.from_dict(obj.get("payroll_institution"))
        })


