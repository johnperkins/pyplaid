# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json

from datetime import date
from typing import Optional
from pydantic import BaseModel, Field, StrictStr
from pyplaid.models.employer_verification import EmployerVerification
from pyplaid.models.employment_verification_status import EmploymentVerificationStatus
from pyplaid.models.platform_ids import PlatformIds
from pydantic import ValidationError

class EmploymentVerification(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    status: Optional[EmploymentVerificationStatus] = None
    start_date: Optional[date] = Field(None, description="Start of employment in ISO 8601 format (YYYY-MM-DD).")
    end_date: Optional[date] = Field(None, description="End of employment, if applicable. Provided in ISO 8601 format (YYY-MM-DD).")
    employer: Optional[EmployerVerification] = None
    title: Optional[StrictStr] = Field(None, description="Current title of employee.")
    platform_ids: Optional[PlatformIds] = None

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> EmploymentVerification:
        """Create an instance of EmploymentVerification from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)
        # override the default output from pydantic by calling `to_dict()` of employer
        if self.employer:
            _dict['employer'] = self.employer.to_dict()
        # override the default output from pydantic by calling `to_dict()` of platform_ids
        if self.platform_ids:
            _dict['platform_ids'] = self.platform_ids.to_dict()

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> EmploymentVerification:
        """Create an instance of EmploymentVerification from a dict"""
        if type(obj) is not dict:
            return EmploymentVerification.parse_obj(obj)

        return EmploymentVerification.parse_obj({
            "status": obj.get("status"),
            "start_date": obj.get("start_date"),
            "end_date": obj.get("end_date"),
            "employer": EmployerVerification.from_dict(obj.get("employer")),
            "title": obj.get("title"),
            "platform_ids": PlatformIds.from_dict(obj.get("platform_ids"))
        })


