# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json


from typing import Optional
from pydantic import BaseModel, Field, StrictFloat, StrictStr
from pyplaid.models.earnings_breakdown_canonical_description import EarningsBreakdownCanonicalDescription
from pydantic import ValidationError

class EarningsBreakdown(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    canonical_description: Optional[EarningsBreakdownCanonicalDescription] = None
    current_amount: Optional[StrictFloat] = Field(None, description="Raw amount of the earning line item.")
    description: Optional[StrictStr] = Field(None, description="Description of the earning line item.")
    hours: Optional[StrictFloat] = Field(None, description="Number of hours applicable for this earning.")
    iso_currency_code: Optional[StrictStr] = Field(None, description="The ISO-4217 currency code of the line item. Always `null` if `unofficial_currency_code` is non-null.")
    rate: Optional[StrictFloat] = Field(None, description="Hourly rate applicable for this earning.")
    unofficial_currency_code: Optional[StrictStr] = Field(None, description="The unofficial currency code associated with the line item. Always `null` if `iso_currency_code` is non-`null`. Unofficial currency codes are used for currencies that do not have official ISO currency codes, such as cryptocurrencies and the currencies of certain countries.  See the [currency code schema](https://plaid.com/docs/api/accounts#currency-code-schema) for a full listing of supported `iso_currency_code`s.")
    ytd_amount: Optional[StrictFloat] = Field(None, description="The year-to-date amount of the deduction.")

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> EarningsBreakdown:
        """Create an instance of EarningsBreakdown from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> EarningsBreakdown:
        """Create an instance of EarningsBreakdown from a dict"""
        if type(obj) is not dict:
            return EarningsBreakdown.parse_obj(obj)

        return EarningsBreakdown.parse_obj({
            "canonical_description": obj.get("canonical_description"),
            "current_amount": obj.get("current_amount"),
            "description": obj.get("description"),
            "hours": obj.get("hours"),
            "iso_currency_code": obj.get("iso_currency_code"),
            "rate": obj.get("rate"),
            "unofficial_currency_code": obj.get("unofficial_currency_code"),
            "ytd_amount": obj.get("ytd_amount")
        })


