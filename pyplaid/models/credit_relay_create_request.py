# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json


from typing import List, Optional
from pydantic import BaseModel, Field, StrictStr
from pydantic import ValidationError

class CreditRelayCreateRequest(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    client_id: Optional[StrictStr] = Field(None, description="Your Plaid API `client_id`. The `client_id` is required and may be provided either in the `PLAID-CLIENT-ID` header or as part of a request body.")
    secret: Optional[StrictStr] = Field(None, description="Your Plaid API `secret`. The `secret` is required and may be provided either in the `PLAID-SECRET` header or as part of a request body.")
    report_tokens: List[StrictStr] = Field(..., description="List of report token strings, with at most one token of each report type. Currently only Asset Report token is supported.")
    secondary_client_id: StrictStr = Field(..., description="The `secondary_client_id` is the client id of the third party with whom you would like to share the Relay Token.")
    webhook: Optional[StrictStr] = Field(None, description="URL to which Plaid will send webhooks when the Secondary Client successfully retrieves an Asset Report by calling `/credit/relay/get`.")

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> CreditRelayCreateRequest:
        """Create an instance of CreditRelayCreateRequest from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> CreditRelayCreateRequest:
        """Create an instance of CreditRelayCreateRequest from a dict"""
        if type(obj) is not dict:
            return CreditRelayCreateRequest.parse_obj(obj)

        return CreditRelayCreateRequest.parse_obj({
            "client_id": obj.get("client_id"),
            "secret": obj.get("secret"),
            "report_tokens": obj.get("report_tokens"),
            "secondary_client_id": obj.get("secondary_client_id"),
            "webhook": obj.get("webhook")
        })


