# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json


from typing import List, Optional
from pydantic import BaseModel, Field, StrictBool, StrictStr
from pyplaid.models.auth_metadata import AuthMetadata
from pyplaid.models.country_code import CountryCode
from pyplaid.models.institution_status import InstitutionStatus
from pyplaid.models.payment_initiation_metadata import PaymentInitiationMetadata
from pyplaid.models.products import Products
from pydantic import ValidationError

class Institution(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    institution_id: StrictStr = Field(..., description="Unique identifier for the institution")
    name: StrictStr = Field(..., description="The official name of the institution")
    products: List[Products] = Field(..., description="A list of the Plaid products supported by the institution. Note that only institutions that support Instant Auth will return `auth` in the product array; institutions that do not list `auth` may still support other Auth methods such as Instant Match or Automated Micro-deposit Verification. To identify institutions that support those methods, use the `auth_metadata` object. For more details, see [Full Auth coverage](https://plaid.com/docs/auth/coverage/).")
    country_codes: List[CountryCode] = Field(..., description="A list of the country codes supported by the institution.")
    url: Optional[StrictStr] = Field(None, description="The URL for the institution's website")
    primary_color: Optional[StrictStr] = Field(None, description="Hexadecimal representation of the primary color used by the institution")
    logo: Optional[StrictStr] = Field(None, description="Base64 encoded representation of the institution's logo")
    routing_numbers: List[StrictStr] = Field(..., description="A partial list of routing numbers associated with the institution. This list is provided for the purpose of looking up institutions by routing number. It is not comprehensive and should never be used as a complete list of routing numbers for an institution.")
    oauth: StrictBool = Field(..., description="Indicates that the institution has a mandatory OAuth login flow. Note that `oauth` may be `false` even for institutions that support OAuth, if the institution is in the process of migrating to OAuth and some active Items still exist that do not use OAuth.")
    status: Optional[InstitutionStatus] = None
    payment_initiation_metadata: Optional[PaymentInitiationMetadata] = None
    auth_metadata: Optional[AuthMetadata] = None

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> Institution:
        """Create an instance of Institution from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)
        # override the default output from pydantic by calling `to_dict()` of status
        if self.status:
            _dict['status'] = self.status.to_dict()
        # override the default output from pydantic by calling `to_dict()` of payment_initiation_metadata
        if self.payment_initiation_metadata:
            _dict['payment_initiation_metadata'] = self.payment_initiation_metadata.to_dict()
        # override the default output from pydantic by calling `to_dict()` of auth_metadata
        if self.auth_metadata:
            _dict['auth_metadata'] = self.auth_metadata.to_dict()

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> Institution:
        """Create an instance of Institution from a dict"""
        if type(obj) is not dict:
            return Institution.parse_obj(obj)

        return Institution.parse_obj({
            "institution_id": obj.get("institution_id"),
            "name": obj.get("name"),
            "products": obj.get("products"),
            "country_codes": obj.get("country_codes"),
            "url": obj.get("url"),
            "primary_color": obj.get("primary_color"),
            "logo": obj.get("logo"),
            "routing_numbers": obj.get("routing_numbers"),
            "oauth": obj.get("oauth"),
            "status": InstitutionStatus.from_dict(obj.get("status")),
            "payment_initiation_metadata": PaymentInitiationMetadata.from_dict(obj.get("payment_initiation_metadata")),
            "auth_metadata": AuthMetadata.from_dict(obj.get("auth_metadata"))
        })


