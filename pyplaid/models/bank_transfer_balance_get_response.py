# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json


from typing import Optional
from pydantic import BaseModel, Field, StrictStr
from pyplaid.models.bank_transfer_balance import BankTransferBalance
from pydantic import ValidationError

class BankTransferBalanceGetResponse(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    balance: BankTransferBalance = ...
    origination_account_id: Optional[StrictStr] = Field(..., description="The ID of the origination account that this balance belongs to.")
    request_id: StrictStr = Field(..., description="A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive.")

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> BankTransferBalanceGetResponse:
        """Create an instance of BankTransferBalanceGetResponse from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)
        # override the default output from pydantic by calling `to_dict()` of balance
        if self.balance:
            _dict['balance'] = self.balance.to_dict()

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> BankTransferBalanceGetResponse:
        """Create an instance of BankTransferBalanceGetResponse from a dict"""
        if type(obj) is not dict:
            return BankTransferBalanceGetResponse.parse_obj(obj)

        return BankTransferBalanceGetResponse.parse_obj({
            "balance": BankTransferBalance.from_dict(obj.get("balance")),
            "origination_account_id": obj.get("origination_account_id"),
            "request_id": obj.get("request_id")
        })


