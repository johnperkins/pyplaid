# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json


from typing import Optional
from pydantic import BaseModel, Field, StrictStr
from pyplaid.models.asset_report_user import AssetReportUser
from pydantic import ValidationError

class AssetReportRefreshRequestOptions(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    client_report_id: Optional[StrictStr] = Field(None, description="Client-generated identifier, which can be used by lenders to track loan applications.")
    webhook: Optional[StrictStr] = Field(None, description="URL to which Plaid will send Assets webhooks, for example when the requested Asset Report is ready.")
    user: Optional[AssetReportUser] = None

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> AssetReportRefreshRequestOptions:
        """Create an instance of AssetReportRefreshRequestOptions from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)
        # override the default output from pydantic by calling `to_dict()` of user
        if self.user:
            _dict['user'] = self.user.to_dict()

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> AssetReportRefreshRequestOptions:
        """Create an instance of AssetReportRefreshRequestOptions from a dict"""
        if type(obj) is not dict:
            return AssetReportRefreshRequestOptions.parse_obj(obj)

        return AssetReportRefreshRequestOptions.parse_obj({
            "client_report_id": obj.get("client_report_id"),
            "webhook": obj.get("webhook"),
            "user": AssetReportUser.from_dict(obj.get("user"))
        })


