# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json

from datetime import date
from typing import Optional
from pydantic import BaseModel, Field, StrictStr
from pyplaid.models.investments_transactions_get_request_options import InvestmentsTransactionsGetRequestOptions
from pydantic import ValidationError

class InvestmentsTransactionsGetRequest(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    client_id: Optional[StrictStr] = Field(None, description="Your Plaid API `client_id`. The `client_id` is required and may be provided either in the `PLAID-CLIENT-ID` header or as part of a request body.")
    secret: Optional[StrictStr] = Field(None, description="Your Plaid API `secret`. The `secret` is required and may be provided either in the `PLAID-SECRET` header or as part of a request body.")
    access_token: StrictStr = Field(..., description="The access token associated with the Item data is being requested for.")
    start_date: date = Field(..., description="The earliest date for which to fetch transaction history. Dates should be formatted as YYYY-MM-DD.")
    end_date: date = Field(..., description="The most recent date for which to fetch transaction history. Dates should be formatted as YYYY-MM-DD.")
    options: Optional[InvestmentsTransactionsGetRequestOptions] = None

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> InvestmentsTransactionsGetRequest:
        """Create an instance of InvestmentsTransactionsGetRequest from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)
        # override the default output from pydantic by calling `to_dict()` of options
        if self.options:
            _dict['options'] = self.options.to_dict()

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> InvestmentsTransactionsGetRequest:
        """Create an instance of InvestmentsTransactionsGetRequest from a dict"""
        if type(obj) is not dict:
            return InvestmentsTransactionsGetRequest.parse_obj(obj)

        return InvestmentsTransactionsGetRequest.parse_obj({
            "client_id": obj.get("client_id"),
            "secret": obj.get("secret"),
            "access_token": obj.get("access_token"),
            "start_date": obj.get("start_date"),
            "end_date": obj.get("end_date"),
            "options": InvestmentsTransactionsGetRequestOptions.from_dict(obj.get("options"))
        })


