# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from inspect import getfullargspec
import pprint
import re  # noqa: F401
from aenum import Enum, no_arg





class DocType(str, Enum):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    allowed enum values
    """

    UNKNOWN = 'UNKNOWN'
    DOCUMENT_TYPE_PAYSTUB = 'DOCUMENT_TYPE_PAYSTUB'
    DOCUMENT_TYPE_BANK_STATEMENT = 'DOCUMENT_TYPE_BANK_STATEMENT'
    DOCUMENT_TYPE_US_TAX_W2 = 'DOCUMENT_TYPE_US_TAX_W2'
    DOCUMENT_TYPE_US_MILITARY_ERAS = 'DOCUMENT_TYPE_US_MILITARY_ERAS'
    DOCUMENT_TYPE_US_MILITARY_LES = 'DOCUMENT_TYPE_US_MILITARY_LES'
    DOCUMENT_TYPE_US_MILITARY_CLES = 'DOCUMENT_TYPE_US_MILITARY_CLES'
    DOCUMENT_TYPE_GIG = 'DOCUMENT_TYPE_GIG'
    DOCUMENT_TYPE_NONE = 'DOCUMENT_TYPE_NONE'
    DOCUMENT_TYPE_US_TAX_1099_MISC = 'DOCUMENT_TYPE_US_TAX_1099_MISC'
    DOCUMENT_TYPE_US_TAX_1099_K = 'DOCUMENT_TYPE_US_TAX_1099_K'
    DOCUMENT_TYPE_PLAID_GENERATED_PAYSTUB_PDF = 'DOCUMENT_TYPE_PLAID_GENERATED_PAYSTUB_PDF'

