# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json



from pydantic import BaseModel, Field, constr
from pyplaid.models.wallet_transaction_counterparty_numbers import WalletTransactionCounterpartyNumbers
from pydantic import ValidationError

class WalletTransactionCounterparty(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    name: constr(strict=True, min_length=1) = Field(..., description="The name of the counterparty")
    numbers: WalletTransactionCounterpartyNumbers = ...

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> WalletTransactionCounterparty:
        """Create an instance of WalletTransactionCounterparty from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)
        # override the default output from pydantic by calling `to_dict()` of numbers
        if self.numbers:
            _dict['numbers'] = self.numbers.to_dict()

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> WalletTransactionCounterparty:
        """Create an instance of WalletTransactionCounterparty from a dict"""
        if type(obj) is not dict:
            return WalletTransactionCounterparty.parse_obj(obj)

        return WalletTransactionCounterparty.parse_obj({
            "name": obj.get("name"),
            "numbers": WalletTransactionCounterpartyNumbers.from_dict(obj.get("numbers"))
        })


