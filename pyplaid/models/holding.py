# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json

from datetime import date, datetime
from typing import Optional
from pydantic import BaseModel, Field, StrictFloat, StrictStr
from pydantic import ValidationError

class Holding(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    account_id: StrictStr = Field(..., description="The Plaid `account_id` associated with the holding.")
    security_id: StrictStr = Field(..., description="The Plaid `security_id` associated with the holding. The `security_id` may change if inherent details of the security change due to a corporate action, for example, in the event of a ticker symbol change or CUSIP change.")
    institution_price: StrictFloat = Field(..., description="The last price given by the institution for this security.")
    institution_price_as_of: Optional[date] = Field(None, description="The date at which `institution_price` was current.")
    institution_price_datetime: Optional[datetime] = Field(None, description="Date and time at which `institution_price` was current, in ISO 8601 format (YYYY-MM-DDTHH:mm:ssZ).  This field is returned for select financial institutions and comes as provided by the institution. It may contain default time values (such as 00:00:00). ")
    institution_value: StrictFloat = Field(..., description="The value of the holding, as reported by the institution.")
    cost_basis: Optional[StrictFloat] = Field(..., description="The original total value of the holding. The accuracy of this field is dependent on the information provided by the institution.")
    quantity: StrictFloat = Field(..., description="The total quantity of the asset held, as reported by the financial institution. If the security is an option, `quantity` will reflect the total number of options (typically the number of contracts multiplied by 100), not the number of contracts.")
    iso_currency_code: Optional[StrictStr] = Field(..., description="The ISO-4217 currency code of the holding. Always `null` if `unofficial_currency_code` is non-`null`.")
    unofficial_currency_code: Optional[StrictStr] = Field(..., description="The unofficial currency code associated with the holding. Always `null` if `iso_currency_code` is non-`null`. Unofficial currency codes are used for currencies that do not have official ISO currency codes, such as cryptocurrencies and the currencies of certain countries.  See the [currency code schema](https://plaid.com/docs/api/accounts#currency-code-schema) for a full listing of supported `iso_currency_code`s. ")

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> Holding:
        """Create an instance of Holding from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> Holding:
        """Create an instance of Holding from a dict"""
        if type(obj) is not dict:
            return Holding.parse_obj(obj)

        return Holding.parse_obj({
            "account_id": obj.get("account_id"),
            "security_id": obj.get("security_id"),
            "institution_price": obj.get("institution_price"),
            "institution_price_as_of": obj.get("institution_price_as_of"),
            "institution_price_datetime": obj.get("institution_price_datetime"),
            "institution_value": obj.get("institution_value"),
            "cost_basis": obj.get("cost_basis"),
            "quantity": obj.get("quantity"),
            "iso_currency_code": obj.get("iso_currency_code"),
            "unofficial_currency_code": obj.get("unofficial_currency_code")
        })


