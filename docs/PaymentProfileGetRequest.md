# PaymentProfileGetRequest

PaymentProfileGetRequest defines the request schema for `/payment_profile/get`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**payment_profile_token** | **str** | A payment profile token associated with the Payment Profile data that is being requested. | 

## Example

```python
from pyplaid.models.payment_profile_get_request import PaymentProfileGetRequest

# TODO update the JSON string below
json = "{}"
# create an instance of PaymentProfileGetRequest from a JSON string
payment_profile_get_request_instance = PaymentProfileGetRequest.from_json(json)
# print the JSON string representation of the object
print PaymentProfileGetRequest.to_json()

# convert the object into a dict
payment_profile_get_request_dict = payment_profile_get_request_instance.to_dict()
# create an instance of PaymentProfileGetRequest from a dict
payment_profile_get_request_form_dict = payment_profile_get_request.from_dict(payment_profile_get_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


