# TransactionsRulesListResponse

TransactionsRulesListResponse defines the response schema for `/beta/transactions/rules/v1/list`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rules** | [**List[TransactionsCategoryRule]**](TransactionsCategoryRule.md) | A list of the Item&#39;s transaction rules | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.transactions_rules_list_response import TransactionsRulesListResponse

# TODO update the JSON string below
json = "{}"
# create an instance of TransactionsRulesListResponse from a JSON string
transactions_rules_list_response_instance = TransactionsRulesListResponse.from_json(json)
# print the JSON string representation of the object
print TransactionsRulesListResponse.to_json()

# convert the object into a dict
transactions_rules_list_response_dict = transactions_rules_list_response_instance.to_dict()
# create an instance of TransactionsRulesListResponse from a dict
transactions_rules_list_response_form_dict = transactions_rules_list_response.from_dict(transactions_rules_list_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


