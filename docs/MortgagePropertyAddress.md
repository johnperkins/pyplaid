# MortgagePropertyAddress

Object containing fields describing property address.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**city** | **str** | The city name. | 
**country** | **str** | The ISO 3166-1 alpha-2 country code. | 
**postal_code** | **str** | The five or nine digit postal code. | 
**region** | **str** | The region or state (example \&quot;NC\&quot;). | 
**street** | **str** | The full street address (example \&quot;564 Main Street, Apt 15\&quot;). | 

## Example

```python
from pyplaid.models.mortgage_property_address import MortgagePropertyAddress

# TODO update the JSON string below
json = "{}"
# create an instance of MortgagePropertyAddress from a JSON string
mortgage_property_address_instance = MortgagePropertyAddress.from_json(json)
# print the JSON string representation of the object
print MortgagePropertyAddress.to_json()

# convert the object into a dict
mortgage_property_address_dict = mortgage_property_address_instance.to_dict()
# create an instance of MortgagePropertyAddress from a dict
mortgage_property_address_form_dict = mortgage_property_address.from_dict(mortgage_property_address_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


