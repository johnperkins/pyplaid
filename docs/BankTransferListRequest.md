# BankTransferListRequest

Defines the request schema for `/bank_transfer/list`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**start_date** | **datetime** | The start datetime of bank transfers to list. This should be in RFC 3339 format (i.e. &#x60;2019-12-06T22:35:49Z&#x60;) | [optional] 
**end_date** | **datetime** | The end datetime of bank transfers to list. This should be in RFC 3339 format (i.e. &#x60;2019-12-06T22:35:49Z&#x60;) | [optional] 
**count** | **int** | The maximum number of bank transfers to return. | [optional] [default to 25]
**offset** | **int** | The number of bank transfers to skip before returning results. | [optional] [default to 0]
**origination_account_id** | **str** | Filter bank transfers to only those originated through the specified origination account. | [optional] 
**direction** | [**BankTransferDirection**](BankTransferDirection.md) |  | [optional] 

## Example

```python
from pyplaid.models.bank_transfer_list_request import BankTransferListRequest

# TODO update the JSON string below
json = "{}"
# create an instance of BankTransferListRequest from a JSON string
bank_transfer_list_request_instance = BankTransferListRequest.from_json(json)
# print the JSON string representation of the object
print BankTransferListRequest.to_json()

# convert the object into a dict
bank_transfer_list_request_dict = bank_transfer_list_request_instance.to_dict()
# create an instance of BankTransferListRequest from a dict
bank_transfer_list_request_form_dict = bank_transfer_list_request.from_dict(bank_transfer_list_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


