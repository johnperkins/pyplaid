# BankTransferEvent

Represents an event in the Bank Transfers API.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**event_id** | **int** | Plaid’s unique identifier for this event. IDs are sequential unsigned 64-bit integers. | 
**timestamp** | **datetime** | The datetime when this event occurred. This will be of the form &#x60;2006-01-02T15:04:05Z&#x60;. | 
**event_type** | [**BankTransferEventType**](BankTransferEventType.md) |  | 
**account_id** | **str** | The account ID associated with the bank transfer. | 
**bank_transfer_id** | **str** | Plaid’s unique identifier for a bank transfer. | 
**origination_account_id** | **str** | The ID of the origination account that this balance belongs to. | 
**bank_transfer_type** | [**BankTransferType**](BankTransferType.md) |  | 
**bank_transfer_amount** | **str** | The bank transfer amount. | 
**bank_transfer_iso_currency_code** | **str** | The currency of the bank transfer amount. | 
**failure_reason** | [**BankTransferFailure**](BankTransferFailure.md) |  | 
**direction** | [**BankTransferDirection**](BankTransferDirection.md) |  | 

## Example

```python
from pyplaid.models.bank_transfer_event import BankTransferEvent

# TODO update the JSON string below
json = "{}"
# create an instance of BankTransferEvent from a JSON string
bank_transfer_event_instance = BankTransferEvent.from_json(json)
# print the JSON string representation of the object
print BankTransferEvent.to_json()

# convert the object into a dict
bank_transfer_event_dict = bank_transfer_event_instance.to_dict()
# create an instance of BankTransferEvent from a dict
bank_transfer_event_form_dict = bank_transfer_event.from_dict(bank_transfer_event_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


