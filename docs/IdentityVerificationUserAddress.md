# IdentityVerificationUserAddress

Even if an address has been collected, some fields may be null depending on the region's addressing system. For example:  Addresses from the United Kingdom will not include a region  Addresses from Hong Kong will not include postal code

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**street** | **str** | The primary street portion of an address. If the user has submitted their address, this field will always be filled. | 
**street2** | **str** | Extra street information, like an apartment or suite number. | 
**city** | **str** | City from the end user&#39;s address | 
**region** | **str** | An ISO 3166-2 subdivision code. Related terms would be \&quot;state\&quot;, \&quot;province\&quot;, \&quot;prefecture\&quot;, \&quot;zone\&quot;, \&quot;subdivision\&quot;, etc. | 
**postal_code** | **str** | The postal code for the associated address. Between 2 and 10 alphanumeric characters. For US-based addresses this must be 5 numeric digits. | 
**country** | **str** | Valid, capitalized, two-letter ISO code representing the country of this object. Must be in ISO 3166-1 alpha-2 form. | 

## Example

```python
from pyplaid.models.identity_verification_user_address import IdentityVerificationUserAddress

# TODO update the JSON string below
json = "{}"
# create an instance of IdentityVerificationUserAddress from a JSON string
identity_verification_user_address_instance = IdentityVerificationUserAddress.from_json(json)
# print the JSON string representation of the object
print IdentityVerificationUserAddress.to_json()

# convert the object into a dict
identity_verification_user_address_dict = identity_verification_user_address_instance.to_dict()
# create an instance of IdentityVerificationUserAddress from a dict
identity_verification_user_address_form_dict = identity_verification_user_address.from_dict(identity_verification_user_address_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


