# PhoneNumberMatchScore

Score found by matching phone number provided by the API with the phone number on the account at the financial institution. If the account contains multiple owners, the maximum match score is filled.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**scores** | **int** | Match score for normalized phone number. 100 is a perfect match and 0 is a no match. If the phone number is missing from either the API or financial institution, this is empty. | [optional] 

## Example

```python
from pyplaid.models.phone_number_match_score import PhoneNumberMatchScore

# TODO update the JSON string below
json = "{}"
# create an instance of PhoneNumberMatchScore from a JSON string
phone_number_match_score_instance = PhoneNumberMatchScore.from_json(json)
# print the JSON string representation of the object
print PhoneNumberMatchScore.to_json()

# convert the object into a dict
phone_number_match_score_dict = phone_number_match_score_instance.to_dict()
# create an instance of PhoneNumberMatchScore from a dict
phone_number_match_score_form_dict = phone_number_match_score.from_dict(phone_number_match_score_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


