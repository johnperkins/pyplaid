# WatchlistScreeningReview

A review submitted by a team member for an individual watchlist screening. A review can be either a comment on the current screening state, actions taken against hits attached to the watchlist screening, or both.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | ID of the associated review. | 
**confirmed_hits** | **List[str]** | Hits marked as a true positive after thorough manual review. These hits will never recur or be updated once dismissed. In most cases, confirmed hits indicate that the customer should be rejected. | 
**dismissed_hits** | **List[str]** | Hits marked as a false positive after thorough manual review. These hits will never recur or be updated once dismissed. | 
**comment** | **str** | A comment submitted by a team member as part of reviewing a watchlist screening. | 
**audit_trail** | [**WatchlistScreeningAuditTrail**](WatchlistScreeningAuditTrail.md) |  | 

## Example

```python
from pyplaid.models.watchlist_screening_review import WatchlistScreeningReview

# TODO update the JSON string below
json = "{}"
# create an instance of WatchlistScreeningReview from a JSON string
watchlist_screening_review_instance = WatchlistScreeningReview.from_json(json)
# print the JSON string representation of the object
print WatchlistScreeningReview.to_json()

# convert the object into a dict
watchlist_screening_review_dict = watchlist_screening_review_instance.to_dict()
# create an instance of WatchlistScreeningReview from a dict
watchlist_screening_review_form_dict = watchlist_screening_review.from_dict(watchlist_screening_review_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


