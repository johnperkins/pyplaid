# DocumentAnalysis

High level descriptions of how the associated document was processed. If a document fails verification, the details in the `analysis` object should help clarify why the document was rejected.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**authenticity** | [**DocumentAuthenticityMatchCode**](DocumentAuthenticityMatchCode.md) |  | 
**image_quality** | [**ImageQuality**](ImageQuality.md) |  | 
**extracted_data** | [**PhysicalDocumentExtractedDataAnalysis**](PhysicalDocumentExtractedDataAnalysis.md) |  | 

## Example

```python
from pyplaid.models.document_analysis import DocumentAnalysis

# TODO update the JSON string below
json = "{}"
# create an instance of DocumentAnalysis from a JSON string
document_analysis_instance = DocumentAnalysis.from_json(json)
# print the JSON string representation of the object
print DocumentAnalysis.to_json()

# convert the object into a dict
document_analysis_dict = document_analysis_instance.to_dict()
# create an instance of DocumentAnalysis from a dict
document_analysis_form_dict = document_analysis.from_dict(document_analysis_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


