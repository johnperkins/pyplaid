# IdentityMatchResponse

IdentityMatchResponse defines the response schema for `/identity/match`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accounts** | [**List[AccountIdentityMatchScore]**](AccountIdentityMatchScore.md) | The accounts for which Identity match has been requested | 
**item** | [**Item**](Item.md) |  | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.identity_match_response import IdentityMatchResponse

# TODO update the JSON string below
json = "{}"
# create an instance of IdentityMatchResponse from a JSON string
identity_match_response_instance = IdentityMatchResponse.from_json(json)
# print the JSON string representation of the object
print IdentityMatchResponse.to_json()

# convert the object into a dict
identity_match_response_dict = identity_match_response_instance.to_dict()
# create an instance of IdentityMatchResponse from a dict
identity_match_response_form_dict = identity_match_response.from_dict(identity_match_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


