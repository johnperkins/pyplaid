# AccountsGetRequest

AccountsGetRequest defines the request schema for `/accounts/get`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**access_token** | **str** | The access token associated with the Item data is being requested for. | 
**options** | [**AccountsGetRequestOptions**](AccountsGetRequestOptions.md) |  | [optional] 

## Example

```python
from pyplaid.models.accounts_get_request import AccountsGetRequest

# TODO update the JSON string below
json = "{}"
# create an instance of AccountsGetRequest from a JSON string
accounts_get_request_instance = AccountsGetRequest.from_json(json)
# print the JSON string representation of the object
print AccountsGetRequest.to_json()

# convert the object into a dict
accounts_get_request_dict = accounts_get_request_instance.to_dict()
# create an instance of AccountsGetRequest from a dict
accounts_get_request_form_dict = accounts_get_request.from_dict(accounts_get_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


