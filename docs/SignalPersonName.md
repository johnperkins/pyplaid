# SignalPersonName

The user's legal name

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**prefix** | **str** | The user&#39;s name prefix (e.g. \&quot;Mr.\&quot;) | [optional] 
**given_name** | **str** | The user&#39;s given name. If the user has a one-word name, it should be provided in this field. | [optional] 
**middle_name** | **str** | The user&#39;s middle name | [optional] 
**family_name** | **str** | The user&#39;s family name / surname | [optional] 
**suffix** | **str** | The user&#39;s name suffix (e.g. \&quot;II\&quot;) | [optional] 

## Example

```python
from pyplaid.models.signal_person_name import SignalPersonName

# TODO update the JSON string below
json = "{}"
# create an instance of SignalPersonName from a JSON string
signal_person_name_instance = SignalPersonName.from_json(json)
# print the JSON string representation of the object
print SignalPersonName.to_json()

# convert the object into a dict
signal_person_name_dict = signal_person_name_instance.to_dict()
# create an instance of SignalPersonName from a dict
signal_person_name_form_dict = signal_person_name.from_dict(signal_person_name_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


