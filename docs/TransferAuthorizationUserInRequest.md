# TransferAuthorizationUserInRequest

The legal name and other information for the account holder.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**legal_name** | **str** | The user&#39;s legal name. | 
**phone_number** | **str** | The user&#39;s phone number. In order to qualify for a guaranteed transfer, at least one of &#x60;phone_number&#x60; or &#x60;email_address&#x60; must be provided. | [optional] 
**email_address** | **str** | The user&#39;s email address. In order to qualify for a guaranteed transfer, at least one of &#x60;phone_number&#x60; or &#x60;email_address&#x60; must be provided. | [optional] 
**address** | [**TransferUserAddressInRequest**](TransferUserAddressInRequest.md) |  | [optional] 

## Example

```python
from pyplaid.models.transfer_authorization_user_in_request import TransferAuthorizationUserInRequest

# TODO update the JSON string below
json = "{}"
# create an instance of TransferAuthorizationUserInRequest from a JSON string
transfer_authorization_user_in_request_instance = TransferAuthorizationUserInRequest.from_json(json)
# print the JSON string representation of the object
print TransferAuthorizationUserInRequest.to_json()

# convert the object into a dict
transfer_authorization_user_in_request_dict = transfer_authorization_user_in_request_instance.to_dict()
# create an instance of TransferAuthorizationUserInRequest from a dict
transfer_authorization_user_in_request_form_dict = transfer_authorization_user_in_request.from_dict(transfer_authorization_user_in_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


