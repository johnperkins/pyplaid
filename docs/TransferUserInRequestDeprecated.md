# TransferUserInRequestDeprecated

The legal name and other information for the account holder.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**legal_name** | **str** | The user&#39;s legal name. | [optional] 
**phone_number** | **str** | The user&#39;s phone number. | [optional] 
**email_address** | **str** | The user&#39;s email address. | [optional] 
**address** | [**TransferUserAddressInRequest**](TransferUserAddressInRequest.md) |  | [optional] 

## Example

```python
from pyplaid.models.transfer_user_in_request_deprecated import TransferUserInRequestDeprecated

# TODO update the JSON string below
json = "{}"
# create an instance of TransferUserInRequestDeprecated from a JSON string
transfer_user_in_request_deprecated_instance = TransferUserInRequestDeprecated.from_json(json)
# print the JSON string representation of the object
print TransferUserInRequestDeprecated.to_json()

# convert the object into a dict
transfer_user_in_request_deprecated_dict = transfer_user_in_request_deprecated_instance.to_dict()
# create an instance of TransferUserInRequestDeprecated from a dict
transfer_user_in_request_deprecated_form_dict = transfer_user_in_request_deprecated.from_dict(transfer_user_in_request_deprecated_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


