# WalletTransactionExecuteResponse

WalletTransactionExecuteResponse defines the response schema for `/wallet/transaction/execute`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**transaction_id** | **str** | A unique ID identifying the transaction | 
**status** | [**WalletTransactionStatus**](WalletTransactionStatus.md) |  | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.wallet_transaction_execute_response import WalletTransactionExecuteResponse

# TODO update the JSON string below
json = "{}"
# create an instance of WalletTransactionExecuteResponse from a JSON string
wallet_transaction_execute_response_instance = WalletTransactionExecuteResponse.from_json(json)
# print the JSON string representation of the object
print WalletTransactionExecuteResponse.to_json()

# convert the object into a dict
wallet_transaction_execute_response_dict = wallet_transaction_execute_response_instance.to_dict()
# create an instance of WalletTransactionExecuteResponse from a dict
wallet_transaction_execute_response_form_dict = wallet_transaction_execute_response.from_dict(wallet_transaction_execute_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


