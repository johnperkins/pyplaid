# LinkTokenCreateRequestIdentityVerification

Specifies option for initializing Link for use with the Identity Verification product.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**template_id** | **str** | ID of the associated Identity Verification template. | 
**consent** | **bool** |  | [optional] 
**gave_consent** | **bool** | A flag specifying whether the end user has already agreed to a privacy policy specifying that their data will be shared with Plaid for verification purposes.  If &#x60;gave_consent&#x60; is set to &#x60;true&#x60;, the &#x60;accept_tos&#x60; step will be marked as &#x60;skipped&#x60; and the end user&#39;s session will start at the next step requirement. | [optional] [default to False]

## Example

```python
from pyplaid.models.link_token_create_request_identity_verification import LinkTokenCreateRequestIdentityVerification

# TODO update the JSON string below
json = "{}"
# create an instance of LinkTokenCreateRequestIdentityVerification from a JSON string
link_token_create_request_identity_verification_instance = LinkTokenCreateRequestIdentityVerification.from_json(json)
# print the JSON string representation of the object
print LinkTokenCreateRequestIdentityVerification.to_json()

# convert the object into a dict
link_token_create_request_identity_verification_dict = link_token_create_request_identity_verification_instance.to_dict()
# create an instance of LinkTokenCreateRequestIdentityVerification from a dict
link_token_create_request_identity_verification_form_dict = link_token_create_request_identity_verification.from_dict(link_token_create_request_identity_verification_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


