# TransferRepaymentReturnListResponse

Defines the response schema for `/transfer/repayments/return/list`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**repayment_returns** | [**List[TransferRepaymentReturn]**](TransferRepaymentReturn.md) |  | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.transfer_repayment_return_list_response import TransferRepaymentReturnListResponse

# TODO update the JSON string below
json = "{}"
# create an instance of TransferRepaymentReturnListResponse from a JSON string
transfer_repayment_return_list_response_instance = TransferRepaymentReturnListResponse.from_json(json)
# print the JSON string representation of the object
print TransferRepaymentReturnListResponse.to_json()

# convert the object into a dict
transfer_repayment_return_list_response_dict = transfer_repayment_return_list_response_instance.to_dict()
# create an instance of TransferRepaymentReturnListResponse from a dict
transfer_repayment_return_list_response_form_dict = transfer_repayment_return_list_response.from_dict(transfer_repayment_return_list_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


