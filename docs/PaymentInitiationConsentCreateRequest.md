# PaymentInitiationConsentCreateRequest

PaymentInitiationConsentCreateRequest defines the request schema for `/payment_initiation/consent/create`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**recipient_id** | **str** | The ID of the recipient the payment consent is for. The created consent can be used to transfer funds to this recipient only. | 
**reference** | **str** | A reference for the payment consent. This must be an alphanumeric string with at most 18 characters and must not contain any special characters. | 
**scopes** | [**List[PaymentInitiationConsentScope]**](PaymentInitiationConsentScope.md) | An array of payment consent scopes. | 
**constraints** | [**PaymentInitiationConsentConstraints**](PaymentInitiationConsentConstraints.md) |  | 
**options** | [**ExternalPaymentInitiationConsentOptions**](ExternalPaymentInitiationConsentOptions.md) |  | [optional] 

## Example

```python
from pyplaid.models.payment_initiation_consent_create_request import PaymentInitiationConsentCreateRequest

# TODO update the JSON string below
json = "{}"
# create an instance of PaymentInitiationConsentCreateRequest from a JSON string
payment_initiation_consent_create_request_instance = PaymentInitiationConsentCreateRequest.from_json(json)
# print the JSON string representation of the object
print PaymentInitiationConsentCreateRequest.to_json()

# convert the object into a dict
payment_initiation_consent_create_request_dict = payment_initiation_consent_create_request_instance.to_dict()
# create an instance of PaymentInitiationConsentCreateRequest from a dict
payment_initiation_consent_create_request_form_dict = payment_initiation_consent_create_request.from_dict(payment_initiation_consent_create_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


