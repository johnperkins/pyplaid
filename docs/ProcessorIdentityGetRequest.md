# ProcessorIdentityGetRequest

ProcessorIdentityGetRequest defines the request schema for `/processor/identity/get`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**processor_token** | **str** | The processor token obtained from the Plaid integration partner. Processor tokens are in the format: &#x60;processor-&lt;environment&gt;-&lt;identifier&gt;&#x60; | 

## Example

```python
from pyplaid.models.processor_identity_get_request import ProcessorIdentityGetRequest

# TODO update the JSON string below
json = "{}"
# create an instance of ProcessorIdentityGetRequest from a JSON string
processor_identity_get_request_instance = ProcessorIdentityGetRequest.from_json(json)
# print the JSON string representation of the object
print ProcessorIdentityGetRequest.to_json()

# convert the object into a dict
processor_identity_get_request_dict = processor_identity_get_request_instance.to_dict()
# create an instance of ProcessorIdentityGetRequest from a dict
processor_identity_get_request_form_dict = processor_identity_get_request.from_dict(processor_identity_get_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


