# TransferSweep

Describes a sweep of funds to / from the sweep account.  A sweep is associated with many sweep events (events of type `swept` or `return_swept`) which can be retrieved by invoking the `/transfer/event/list` endpoint with the corresponding `sweep_id`.  `swept` events occur when the transfer amount is credited or debited from your sweep account, depending on the `type` of the transfer. `return_swept` events occur when a transfer is returned and Plaid undoes the credit or debit.  The total sum of the `swept` and `return_swept` events is equal to the `amount` of the sweep Plaid creates and matches the amount of the entry on your sweep account ledger.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Identifier of the sweep. | 
**created** | **datetime** | The datetime when the sweep occurred, in RFC 3339 format. | 
**amount** | **str** | Signed decimal amount of the sweep as it appears on your sweep account ledger (e.g. \&quot;-10.00\&quot;)  If amount is not present, the sweep was net-settled to zero and outstanding debits and credits between the sweep account and Plaid are balanced. | 
**iso_currency_code** | **str** | The currency of the sweep, e.g. \&quot;USD\&quot;. | 
**settled** | **date** | The date when the sweep settled, in the YYYY-MM-DD format. | 

## Example

```python
from pyplaid.models.transfer_sweep import TransferSweep

# TODO update the JSON string below
json = "{}"
# create an instance of TransferSweep from a JSON string
transfer_sweep_instance = TransferSweep.from_json(json)
# print the JSON string representation of the object
print TransferSweep.to_json()

# convert the object into a dict
transfer_sweep_dict = transfer_sweep_instance.to_dict()
# create an instance of TransferSweep from a dict
transfer_sweep_form_dict = transfer_sweep.from_dict(transfer_sweep_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


