# CreditPayStubNetPay

An object representing information about the net pay amount on the pay stub.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**current_amount** | **float** | Raw amount of the net pay for the pay period. | 
**description** | **str** | Description of the net pay. | 
**iso_currency_code** | **str** | The ISO-4217 currency code of the net pay. Always &#x60;null&#x60; if &#x60;unofficial_currency_code&#x60; is non-null. | 
**unofficial_currency_code** | **str** | The unofficial currency code associated with the net pay. Always &#x60;null&#x60; if &#x60;iso_currency_code&#x60; is non-&#x60;null&#x60;. Unofficial currency codes are used for currencies that do not have official ISO currency codes, such as cryptocurrencies and the currencies of certain countries.  See the [currency code schema](https://plaid.com/docs/api/accounts#currency-code-schema) for a full listing of supported &#x60;iso_currency_code&#x60;s. | 
**ytd_amount** | **float** | The year-to-date amount of the net pay. | 

## Example

```python
from pyplaid.models.credit_pay_stub_net_pay import CreditPayStubNetPay

# TODO update the JSON string below
json = "{}"
# create an instance of CreditPayStubNetPay from a JSON string
credit_pay_stub_net_pay_instance = CreditPayStubNetPay.from_json(json)
# print the JSON string representation of the object
print CreditPayStubNetPay.to_json()

# convert the object into a dict
credit_pay_stub_net_pay_dict = credit_pay_stub_net_pay_instance.to_dict()
# create an instance of CreditPayStubNetPay from a dict
credit_pay_stub_net_pay_form_dict = credit_pay_stub_net_pay.from_dict(credit_pay_stub_net_pay_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


