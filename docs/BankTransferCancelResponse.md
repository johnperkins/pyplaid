# BankTransferCancelResponse

Defines the response schema for `/bank_transfer/cancel`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.bank_transfer_cancel_response import BankTransferCancelResponse

# TODO update the JSON string below
json = "{}"
# create an instance of BankTransferCancelResponse from a JSON string
bank_transfer_cancel_response_instance = BankTransferCancelResponse.from_json(json)
# print the JSON string representation of the object
print BankTransferCancelResponse.to_json()

# convert the object into a dict
bank_transfer_cancel_response_dict = bank_transfer_cancel_response_instance.to_dict()
# create an instance of BankTransferCancelResponse from a dict
bank_transfer_cancel_response_form_dict = bank_transfer_cancel_response.from_dict(bank_transfer_cancel_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


