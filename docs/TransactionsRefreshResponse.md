# TransactionsRefreshResponse

TransactionsRefreshResponse defines the response schema for `/transactions/refresh`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.transactions_refresh_response import TransactionsRefreshResponse

# TODO update the JSON string below
json = "{}"
# create an instance of TransactionsRefreshResponse from a JSON string
transactions_refresh_response_instance = TransactionsRefreshResponse.from_json(json)
# print the JSON string representation of the object
print TransactionsRefreshResponse.to_json()

# convert the object into a dict
transactions_refresh_response_dict = transactions_refresh_response_instance.to_dict()
# create an instance of TransactionsRefreshResponse from a dict
transactions_refresh_response_form_dict = transactions_refresh_response.from_dict(transactions_refresh_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


