# SignalDecisionReportRequest

SignalDecisionReportRequest defines the request schema for `/signal/decision/report`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**client_transaction_id** | **str** | Must be the same as the &#x60;client_transaction_id&#x60; supplied when calling &#x60;/signal/evaluate&#x60; | 
**initiated** | **bool** | &#x60;true&#x60; if the ACH transaction was initiated, &#x60;false&#x60; otherwise.  This field must be returned as a boolean. If formatted incorrectly, this will result in an [&#x60;INVALID_FIELD&#x60;](/docs/errors/invalid-request/#invalid_field) error. | 
**days_funds_on_hold** | **int** | The actual number of days (hold time) since the ACH debit transaction that you wait before making funds available to your customers. The holding time could affect the ACH return rate.  For example, use 0 if you make funds available to your customers instantly or the same day following the debit transaction, or 1 if you make funds available the next day following the debit initialization. | [optional] 

## Example

```python
from pyplaid.models.signal_decision_report_request import SignalDecisionReportRequest

# TODO update the JSON string below
json = "{}"
# create an instance of SignalDecisionReportRequest from a JSON string
signal_decision_report_request_instance = SignalDecisionReportRequest.from_json(json)
# print the JSON string representation of the object
print SignalDecisionReportRequest.to_json()

# convert the object into a dict
signal_decision_report_request_dict = signal_decision_report_request_instance.to_dict()
# create an instance of SignalDecisionReportRequest from a dict
signal_decision_report_request_form_dict = signal_decision_report_request.from_dict(signal_decision_report_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


