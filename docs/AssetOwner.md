# AssetOwner

No documentation available

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**asset_owner_text** | **str** | Account Owner Full Name. | 

## Example

```python
from pyplaid.models.asset_owner import AssetOwner

# TODO update the JSON string below
json = "{}"
# create an instance of AssetOwner from a JSON string
asset_owner_instance = AssetOwner.from_json(json)
# print the JSON string representation of the object
print AssetOwner.to_json()

# convert the object into a dict
asset_owner_dict = asset_owner_instance.to_dict()
# create an instance of AssetOwner from a dict
asset_owner_form_dict = asset_owner.from_dict(asset_owner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


