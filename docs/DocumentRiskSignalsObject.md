# DocumentRiskSignalsObject

Object containing fraud risk data for a set of income documents

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_id** | **str** | ID of the payroll provider account. | 
**single_document_risk_signals** | [**List[SingleDocumentRiskSignal]**](SingleDocumentRiskSignal.md) | Array of document metadata and associated risk signals per document | 
**multi_document_risk_signals** | [**List[MultiDocumentRiskSignal]**](MultiDocumentRiskSignal.md) | Array of risk signals computed from a set of uploaded documents and the associated documents&#39; metadata | 

## Example

```python
from pyplaid.models.document_risk_signals_object import DocumentRiskSignalsObject

# TODO update the JSON string below
json = "{}"
# create an instance of DocumentRiskSignalsObject from a JSON string
document_risk_signals_object_instance = DocumentRiskSignalsObject.from_json(json)
# print the JSON string representation of the object
print DocumentRiskSignalsObject.to_json()

# convert the object into a dict
document_risk_signals_object_dict = document_risk_signals_object_instance.to_dict()
# create an instance of DocumentRiskSignalsObject from a dict
document_risk_signals_object_form_dict = document_risk_signals_object.from_dict(document_risk_signals_object_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


