# CreditPayStub

An object representing an end user's pay stub.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**deductions** | [**CreditPayStubDeductions**](CreditPayStubDeductions.md) |  | 
**document_id** | **str** | An identifier of the document referenced by the document metadata. | 
**document_metadata** | [**CreditDocumentMetadata**](CreditDocumentMetadata.md) |  | 
**earnings** | [**CreditPayStubEarnings**](CreditPayStubEarnings.md) |  | 
**employee** | [**CreditPayStubEmployee**](CreditPayStubEmployee.md) |  | 
**employer** | [**CreditPayStubEmployer**](CreditPayStubEmployer.md) |  | 
**net_pay** | [**CreditPayStubNetPay**](CreditPayStubNetPay.md) |  | 
**pay_period_details** | [**PayStubPayPeriodDetails**](PayStubPayPeriodDetails.md) |  | 

## Example

```python
from pyplaid.models.credit_pay_stub import CreditPayStub

# TODO update the JSON string below
json = "{}"
# create an instance of CreditPayStub from a JSON string
credit_pay_stub_instance = CreditPayStub.from_json(json)
# print the JSON string representation of the object
print CreditPayStub.to_json()

# convert the object into a dict
credit_pay_stub_dict = credit_pay_stub_instance.to_dict()
# create an instance of CreditPayStub from a dict
credit_pay_stub_form_dict = credit_pay_stub.from_dict(credit_pay_stub_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


