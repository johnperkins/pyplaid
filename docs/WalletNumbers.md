# WalletNumbers

An object representing the e-wallet account numbers

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bacs** | [**RecipientBACS**](RecipientBACS.md) |  | [optional] 
**international** | [**NumbersInternationalIBAN**](NumbersInternationalIBAN.md) |  | [optional] 

## Example

```python
from pyplaid.models.wallet_numbers import WalletNumbers

# TODO update the JSON string below
json = "{}"
# create an instance of WalletNumbers from a JSON string
wallet_numbers_instance = WalletNumbers.from_json(json)
# print the JSON string representation of the object
print WalletNumbers.to_json()

# convert the object into a dict
wallet_numbers_dict = wallet_numbers_instance.to_dict()
# create an instance of WalletNumbers from a dict
wallet_numbers_form_dict = wallet_numbers.from_dict(wallet_numbers_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


