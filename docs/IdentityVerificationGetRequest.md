# IdentityVerificationGetRequest

Request input for fetching an identity verification

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**identity_verification_id** | **str** | ID of the associated Identity Verification attempt. | 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 

## Example

```python
from pyplaid.models.identity_verification_get_request import IdentityVerificationGetRequest

# TODO update the JSON string below
json = "{}"
# create an instance of IdentityVerificationGetRequest from a JSON string
identity_verification_get_request_instance = IdentityVerificationGetRequest.from_json(json)
# print the JSON string representation of the object
print IdentityVerificationGetRequest.to_json()

# convert the object into a dict
identity_verification_get_request_dict = identity_verification_get_request_instance.to_dict()
# create an instance of IdentityVerificationGetRequest from a dict
identity_verification_get_request_form_dict = identity_verification_get_request.from_dict(identity_verification_get_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


