# ProcessorAuthGetResponse

ProcessorAuthGetResponse defines the response schema for `/processor/auth/get`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 
**numbers** | [**ProcessorNumber**](ProcessorNumber.md) |  | 
**account** | [**AccountBase**](AccountBase.md) |  | 

## Example

```python
from pyplaid.models.processor_auth_get_response import ProcessorAuthGetResponse

# TODO update the JSON string below
json = "{}"
# create an instance of ProcessorAuthGetResponse from a JSON string
processor_auth_get_response_instance = ProcessorAuthGetResponse.from_json(json)
# print the JSON string representation of the object
print ProcessorAuthGetResponse.to_json()

# convert the object into a dict
processor_auth_get_response_dict = processor_auth_get_response_instance.to_dict()
# create an instance of ProcessorAuthGetResponse from a dict
processor_auth_get_response_form_dict = processor_auth_get_response.from_dict(processor_auth_get_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


