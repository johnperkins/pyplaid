# CreditRelayRefreshRequest

CreditRelayRefreshRequest defines the request schema for `/credit/relay/refresh`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**relay_token** | **str** | The &#x60;relay_token&#x60; granting access to the report you would like to refresh. | 
**report_type** | [**ReportType**](ReportType.md) |  | 
**webhook** | **str** | The URL registered to receive webhooks when the report of a Relay Token has been refreshed. | [optional] 

## Example

```python
from pyplaid.models.credit_relay_refresh_request import CreditRelayRefreshRequest

# TODO update the JSON string below
json = "{}"
# create an instance of CreditRelayRefreshRequest from a JSON string
credit_relay_refresh_request_instance = CreditRelayRefreshRequest.from_json(json)
# print the JSON string representation of the object
print CreditRelayRefreshRequest.to_json()

# convert the object into a dict
credit_relay_refresh_request_dict = credit_relay_refresh_request_instance.to_dict()
# create an instance of CreditRelayRefreshRequest from a dict
credit_relay_refresh_request_form_dict = credit_relay_refresh_request.from_dict(credit_relay_refresh_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


