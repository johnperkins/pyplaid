# CreditRelayGetRequest

CreditRelayGetRequest defines the request schema for `/credit/relay/get`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**relay_token** | **str** | The &#x60;relay_token&#x60; granting access to the report you would like to get. | 
**report_type** | [**ReportType**](ReportType.md) |  | 

## Example

```python
from pyplaid.models.credit_relay_get_request import CreditRelayGetRequest

# TODO update the JSON string below
json = "{}"
# create an instance of CreditRelayGetRequest from a JSON string
credit_relay_get_request_instance = CreditRelayGetRequest.from_json(json)
# print the JSON string representation of the object
print CreditRelayGetRequest.to_json()

# convert the object into a dict
credit_relay_get_request_dict = credit_relay_get_request_instance.to_dict()
# create an instance of CreditRelayGetRequest from a dict
credit_relay_get_request_form_dict = credit_relay_get_request.from_dict(credit_relay_get_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


