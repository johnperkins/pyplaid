# NumbersInternationalIBAN

Account numbers using the International Bank Account Number and BIC/SWIFT code format.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**iban** | **str** | International Bank Account Number (IBAN). | 
**bic** | **str** | The Business Identifier Code, also known as SWIFT code, for this bank account. | 

## Example

```python
from pyplaid.models.numbers_international_iban import NumbersInternationalIBAN

# TODO update the JSON string below
json = "{}"
# create an instance of NumbersInternationalIBAN from a JSON string
numbers_international_iban_instance = NumbersInternationalIBAN.from_json(json)
# print the JSON string representation of the object
print NumbersInternationalIBAN.to_json()

# convert the object into a dict
numbers_international_iban_dict = numbers_international_iban_instance.to_dict()
# create an instance of NumbersInternationalIBAN from a dict
numbers_international_iban_form_dict = numbers_international_iban.from_dict(numbers_international_iban_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


