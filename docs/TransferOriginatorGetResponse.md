# TransferOriginatorGetResponse

Defines the response schema for `/transfer/originator/get`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**originator** | [**Originator**](Originator.md) |  | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.transfer_originator_get_response import TransferOriginatorGetResponse

# TODO update the JSON string below
json = "{}"
# create an instance of TransferOriginatorGetResponse from a JSON string
transfer_originator_get_response_instance = TransferOriginatorGetResponse.from_json(json)
# print the JSON string representation of the object
print TransferOriginatorGetResponse.to_json()

# convert the object into a dict
transfer_originator_get_response_dict = transfer_originator_get_response_instance.to_dict()
# create an instance of TransferOriginatorGetResponse from a dict
transfer_originator_get_response_form_dict = transfer_originator_get_response.from_dict(transfer_originator_get_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


