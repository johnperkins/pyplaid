# TransactionsGetRequestOptions

An optional object to be used with the request. If specified, `options` must not be `null`.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_ids** | **List[str]** | A list of &#x60;account_ids&#x60; to retrieve for the Item  Note: An error will be returned if a provided &#x60;account_id&#x60; is not associated with the Item. | [optional] 
**count** | **int** | The number of transactions to fetch. | [optional] [default to 100]
**offset** | **int** | The number of transactions to skip. The default value is 0. | [optional] [default to 0]
**include_original_description** | **bool** | Include the raw unparsed transaction description from the financial institution. This field is disabled by default. If you need this information in addition to the parsed data provided, contact your Plaid Account Manager. | [optional] [default to False]
**include_personal_finance_category_beta** | **bool** | Please use [&#x60;include_personal_finance_category&#x60;](https://plaid.com/docs/api/products/transactions/#transactions-get-request-options-include-personal-finance-category) instead. | [optional] [default to False]
**include_personal_finance_category** | **bool** | Include the [&#x60;personal_finance_category&#x60;](https://plaid.com/docs/api/products/transactions/#transactions-get-response-transactions-personal-finance-category) object in the response.  See the [&#x60;taxonomy csv file&#x60;](https://plaid.com/documents/transactions-personal-finance-category-taxonomy.csv) for a full list of personal finance categories.  We’re introducing Category Rules - a new beta endpoint that will enable you to change the &#x60;personal_finance_category&#x60; for a transaction based on your users’ needs. When rules are set, the selected category will override the Plaid provided category. To learn more, send a note to transactions-feedback@plaid.com. | [optional] [default to False]

## Example

```python
from pyplaid.models.transactions_get_request_options import TransactionsGetRequestOptions

# TODO update the JSON string below
json = "{}"
# create an instance of TransactionsGetRequestOptions from a JSON string
transactions_get_request_options_instance = TransactionsGetRequestOptions.from_json(json)
# print the JSON string representation of the object
print TransactionsGetRequestOptions.to_json()

# convert the object into a dict
transactions_get_request_options_dict = transactions_get_request_options_instance.to_dict()
# create an instance of TransactionsGetRequestOptions from a dict
transactions_get_request_options_form_dict = transactions_get_request_options.from_dict(transactions_get_request_options_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


