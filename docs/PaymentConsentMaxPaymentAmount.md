# PaymentConsentMaxPaymentAmount

Maximum amount of a single payment initiated using the payment consent.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**currency** | [**PaymentAmountCurrency**](PaymentAmountCurrency.md) |  | 
**value** | **float** | The amount of the payment. Must contain at most two digits of precision e.g. &#x60;1.23&#x60;. Minimum accepted value is &#x60;1&#x60;. | 

## Example

```python
from pyplaid.models.payment_consent_max_payment_amount import PaymentConsentMaxPaymentAmount

# TODO update the JSON string below
json = "{}"
# create an instance of PaymentConsentMaxPaymentAmount from a JSON string
payment_consent_max_payment_amount_instance = PaymentConsentMaxPaymentAmount.from_json(json)
# print the JSON string representation of the object
print PaymentConsentMaxPaymentAmount.to_json()

# convert the object into a dict
payment_consent_max_payment_amount_dict = payment_consent_max_payment_amount_instance.to_dict()
# create an instance of PaymentConsentMaxPaymentAmount from a dict
payment_consent_max_payment_amount_form_dict = payment_consent_max_payment_amount.from_dict(payment_consent_max_payment_amount_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


