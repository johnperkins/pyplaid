# TransferAuthorizationGuaranteeDecisionRationale

The rationale for Plaid's decision to not guarantee a transfer. Will be `null` unless `guarantee_decision` is `NOT_GUARANTEED`.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | [**TransferAuthorizationGuaranteeDecisionRationaleCode**](TransferAuthorizationGuaranteeDecisionRationaleCode.md) |  | 
**description** | **str** | A human-readable description of why the transfer cannot be guaranteed. | 

## Example

```python
from pyplaid.models.transfer_authorization_guarantee_decision_rationale import TransferAuthorizationGuaranteeDecisionRationale

# TODO update the JSON string below
json = "{}"
# create an instance of TransferAuthorizationGuaranteeDecisionRationale from a JSON string
transfer_authorization_guarantee_decision_rationale_instance = TransferAuthorizationGuaranteeDecisionRationale.from_json(json)
# print the JSON string representation of the object
print TransferAuthorizationGuaranteeDecisionRationale.to_json()

# convert the object into a dict
transfer_authorization_guarantee_decision_rationale_dict = transfer_authorization_guarantee_decision_rationale_instance.to_dict()
# create an instance of TransferAuthorizationGuaranteeDecisionRationale from a dict
transfer_authorization_guarantee_decision_rationale_form_dict = transfer_authorization_guarantee_decision_rationale.from_dict(transfer_authorization_guarantee_decision_rationale_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


