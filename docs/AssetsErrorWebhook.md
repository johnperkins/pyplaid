# AssetsErrorWebhook

Fired when Asset Report generation has failed. The resulting `error` will have an `error_type` of `ASSET_REPORT_ERROR`.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**webhook_type** | **str** | &#x60;ASSETS&#x60; | 
**webhook_code** | **str** | &#x60;ERROR&#x60; | 
**error** | [**PlaidError**](PlaidError.md) |  | 
**asset_report_id** | **str** | The ID associated with the Asset Report. | 

## Example

```python
from pyplaid.models.assets_error_webhook import AssetsErrorWebhook

# TODO update the JSON string below
json = "{}"
# create an instance of AssetsErrorWebhook from a JSON string
assets_error_webhook_instance = AssetsErrorWebhook.from_json(json)
# print the JSON string representation of the object
print AssetsErrorWebhook.to_json()

# convert the object into a dict
assets_error_webhook_dict = assets_error_webhook_instance.to_dict()
# create an instance of AssetsErrorWebhook from a dict
assets_error_webhook_form_dict = assets_error_webhook.from_dict(assets_error_webhook_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


