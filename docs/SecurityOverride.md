# SecurityOverride

Specify the security associated with the holding or investment transaction. When inputting custom security data to the Sandbox, Plaid will perform post-data-retrieval normalization and enrichment. These processes may cause the data returned by the Sandbox to be slightly different from the data you input. An ISO-4217 currency code and a security identifier (`ticker_symbol`, `cusip`, `isin`, or `sedol`) are required.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**isin** | **str** | 12-character ISIN, a globally unique securities identifier. | [optional] 
**cusip** | **str** | 9-character CUSIP, an identifier assigned to North American securities. | [optional] 
**sedol** | **str** | 7-character SEDOL, an identifier assigned to securities in the UK. | [optional] 
**name** | **str** | A descriptive name for the security, suitable for display. | [optional] 
**ticker_symbol** | **str** | The security’s trading symbol for publicly traded securities, and otherwise a short identifier if available. | [optional] 
**currency** | **str** | Either a valid &#x60;iso_currency_code&#x60; or &#x60;unofficial_currency_code&#x60; | [optional] 

## Example

```python
from pyplaid.models.security_override import SecurityOverride

# TODO update the JSON string below
json = "{}"
# create an instance of SecurityOverride from a JSON string
security_override_instance = SecurityOverride.from_json(json)
# print the JSON string representation of the object
print SecurityOverride.to_json()

# convert the object into a dict
security_override_dict = security_override_instance.to_dict()
# create an instance of SecurityOverride from a dict
security_override_form_dict = security_override.from_dict(security_override_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


