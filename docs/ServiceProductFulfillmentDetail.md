# ServiceProductFulfillmentDetail

No documentation available

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**vendor_order_identifier** | **str** | A string that uniquely identifies a type of order Verification of Asset. | 
**service_product_fulfillment_identifier** | [**ServiceProductFulfillmentIdentifier**](ServiceProductFulfillmentIdentifier.md) |  | 

## Example

```python
from pyplaid.models.service_product_fulfillment_detail import ServiceProductFulfillmentDetail

# TODO update the JSON string below
json = "{}"
# create an instance of ServiceProductFulfillmentDetail from a JSON string
service_product_fulfillment_detail_instance = ServiceProductFulfillmentDetail.from_json(json)
# print the JSON string representation of the object
print ServiceProductFulfillmentDetail.to_json()

# convert the object into a dict
service_product_fulfillment_detail_dict = service_product_fulfillment_detail_instance.to_dict()
# create an instance of ServiceProductFulfillmentDetail from a dict
service_product_fulfillment_detail_form_dict = service_product_fulfillment_detail.from_dict(service_product_fulfillment_detail_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


