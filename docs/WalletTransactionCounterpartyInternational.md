# WalletTransactionCounterpartyInternational

International Bank Account Number for a Wallet Transaction

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**iban** | **str** | International Bank Account Number (IBAN). | [optional] 

## Example

```python
from pyplaid.models.wallet_transaction_counterparty_international import WalletTransactionCounterpartyInternational

# TODO update the JSON string below
json = "{}"
# create an instance of WalletTransactionCounterpartyInternational from a JSON string
wallet_transaction_counterparty_international_instance = WalletTransactionCounterpartyInternational.from_json(json)
# print the JSON string representation of the object
print WalletTransactionCounterpartyInternational.to_json()

# convert the object into a dict
wallet_transaction_counterparty_international_dict = wallet_transaction_counterparty_international_instance.to_dict()
# create an instance of WalletTransactionCounterpartyInternational from a dict
wallet_transaction_counterparty_international_form_dict = wallet_transaction_counterparty_international.from_dict(wallet_transaction_counterparty_international_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


