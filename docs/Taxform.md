# Taxform

Data about an official document used to report the user's income to the IRS.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**doc_id** | **str** | An identifier of the document referenced by the document metadata. | [optional] 
**document_type** | **str** | The type of tax document. Currently, the only supported value is &#x60;w2&#x60;. | 
**w2** | [**W2**](W2.md) |  | [optional] 

## Example

```python
from pyplaid.models.taxform import Taxform

# TODO update the JSON string below
json = "{}"
# create an instance of Taxform from a JSON string
taxform_instance = Taxform.from_json(json)
# print the JSON string representation of the object
print Taxform.to_json()

# convert the object into a dict
taxform_dict = taxform_instance.to_dict()
# create an instance of Taxform from a dict
taxform_form_dict = taxform.from_dict(taxform_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


