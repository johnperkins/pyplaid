# DepositSwitchCreateRequestOptions

Options to configure the `/deposit_switch/create` request. If provided, cannot be `null`.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**webhook** | **str** | The URL registered to receive webhooks when the status of a deposit switch request has changed.  | [optional] 
**transaction_item_access_tokens** | **List[str]** | An array of access tokens corresponding to transaction items to use when attempting to match the user to their Payroll Provider. These tokens must be created by the same client id as the one creating the switch, and have access to the transactions product. | [optional] 

## Example

```python
from pyplaid.models.deposit_switch_create_request_options import DepositSwitchCreateRequestOptions

# TODO update the JSON string below
json = "{}"
# create an instance of DepositSwitchCreateRequestOptions from a JSON string
deposit_switch_create_request_options_instance = DepositSwitchCreateRequestOptions.from_json(json)
# print the JSON string representation of the object
print DepositSwitchCreateRequestOptions.to_json()

# convert the object into a dict
deposit_switch_create_request_options_dict = deposit_switch_create_request_options_instance.to_dict()
# create an instance of DepositSwitchCreateRequestOptions from a dict
deposit_switch_create_request_options_form_dict = deposit_switch_create_request_options.from_dict(deposit_switch_create_request_options_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


