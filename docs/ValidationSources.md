# ValidationSources

No documentation available.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**validation_source** | [**List[ValidationSource]**](ValidationSource.md) | No documentation available. | 

## Example

```python
from pyplaid.models.validation_sources import ValidationSources

# TODO update the JSON string below
json = "{}"
# create an instance of ValidationSources from a JSON string
validation_sources_instance = ValidationSources.from_json(json)
# print the JSON string representation of the object
print ValidationSources.to_json()

# convert the object into a dict
validation_sources_dict = validation_sources_instance.to_dict()
# create an instance of ValidationSources from a dict
validation_sources_form_dict = validation_sources.from_dict(validation_sources_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


