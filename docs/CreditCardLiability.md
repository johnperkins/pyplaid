# CreditCardLiability

An object representing a credit card account.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_id** | **str** | The ID of the account that this liability belongs to. | 
**aprs** | [**List[APR]**](APR.md) | The various interest rates that apply to the account. APR information is not provided by all card issuers; if APR data is not available, this array will be empty. | 
**is_overdue** | **bool** | true if a payment is currently overdue. Availability for this field is limited. | 
**last_payment_amount** | **float** | The amount of the last payment. | 
**last_payment_date** | **date** | The date of the last payment. Dates are returned in an [ISO 8601](https://wikipedia.org/wiki/ISO_8601) format (YYYY-MM-DD). Availability for this field is limited. | 
**last_statement_issue_date** | **date** | The date of the last statement. Dates are returned in an [ISO 8601](https://wikipedia.org/wiki/ISO_8601) format (YYYY-MM-DD). | 
**last_statement_balance** | **float** | The total amount owed as of the last statement issued | 
**minimum_payment_amount** | **float** | The minimum payment due for the next billing cycle. | 
**next_payment_due_date** | **date** | The due date for the next payment. The due date is &#x60;null&#x60; if a payment is not expected. Dates are returned in an [ISO 8601](https://wikipedia.org/wiki/ISO_8601) format (YYYY-MM-DD). | 

## Example

```python
from pyplaid.models.credit_card_liability import CreditCardLiability

# TODO update the JSON string below
json = "{}"
# create an instance of CreditCardLiability from a JSON string
credit_card_liability_instance = CreditCardLiability.from_json(json)
# print the JSON string representation of the object
print CreditCardLiability.to_json()

# convert the object into a dict
credit_card_liability_dict = credit_card_liability_instance.to_dict()
# create an instance of CreditCardLiability from a dict
credit_card_liability_form_dict = credit_card_liability.from_dict(credit_card_liability_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


