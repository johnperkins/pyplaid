# CreditBankIncomeSummary

Summary for bank income across all income sources and items (max history of 730 days).

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total_amount** | **float** | Total amount of earnings across all the income sources in the end user&#39;s Items for the days requested by the client. | [optional] 
**iso_currency_code** | **str** | The ISO 4217 currency code of the amount or balance. | [optional] 
**unofficial_currency_code** | **str** | The unofficial currency code associated with the amount or balance. Always &#x60;null&#x60; if &#x60;iso_currency_code&#x60; is non-null. Unofficial currency codes are used for currencies that do not have official ISO currency codes, such as cryptocurrencies and the currencies of certain countries. | [optional] 
**start_date** | **date** | The earliest date within the days requested in which all income sources identified by Plaid appear in a user&#39;s account. The date will be returned in an ISO 8601 format (YYYY-MM-DD). | [optional] 
**end_date** | **date** | The latest date in which all income sources identified by Plaid appear in the user&#39;s account. The date will be returned in an ISO 8601 format (YYYY-MM-DD). | [optional] 
**income_sources_count** | **int** | Number of income sources per end user. | [optional] 
**income_categories_count** | **int** | Number of income categories per end user. | [optional] 
**income_transactions_count** | **int** | Number of income transactions per end user. | [optional] 
**historical_summary** | [**List[CreditBankIncomeHistoricalSummary]**](CreditBankIncomeHistoricalSummary.md) |  | [optional] 

## Example

```python
from pyplaid.models.credit_bank_income_summary import CreditBankIncomeSummary

# TODO update the JSON string below
json = "{}"
# create an instance of CreditBankIncomeSummary from a JSON string
credit_bank_income_summary_instance = CreditBankIncomeSummary.from_json(json)
# print the JSON string representation of the object
print CreditBankIncomeSummary.to_json()

# convert the object into a dict
credit_bank_income_summary_dict = credit_bank_income_summary_instance.to_dict()
# create an instance of CreditBankIncomeSummary from a dict
credit_bank_income_summary_form_dict = credit_bank_income_summary.from_dict(credit_bank_income_summary_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


