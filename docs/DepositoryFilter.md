# DepositoryFilter

A filter to apply to `depository`-type accounts

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_subtypes** | [**List[DepositoryAccountSubtype]**](DepositoryAccountSubtype.md) | An array of account subtypes to display in Link. If not specified, all account subtypes will be shown. For a full list of valid types and subtypes, see the [Account schema](https://plaid.com/docs/api/accounts#account-type-schema).  | 

## Example

```python
from pyplaid.models.depository_filter import DepositoryFilter

# TODO update the JSON string below
json = "{}"
# create an instance of DepositoryFilter from a JSON string
depository_filter_instance = DepositoryFilter.from_json(json)
# print the JSON string representation of the object
print DepositoryFilter.to_json()

# convert the object into a dict
depository_filter_dict = depository_filter_instance.to_dict()
# create an instance of DepositoryFilter from a dict
depository_filter_form_dict = depository_filter.from_dict(depository_filter_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


