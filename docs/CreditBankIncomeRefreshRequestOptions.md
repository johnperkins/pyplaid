# CreditBankIncomeRefreshRequestOptions

An optional object for `/credit/bank_income/refresh` request options.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**days_requested** | **int** | How many days of data to include in the refresh. If not specified, this will default to the days requested in the most recently generated bank income report for the user. | [optional] 
**webhook** | **str** | The URL where Plaid will send the bank income webhook. | [optional] 

## Example

```python
from pyplaid.models.credit_bank_income_refresh_request_options import CreditBankIncomeRefreshRequestOptions

# TODO update the JSON string below
json = "{}"
# create an instance of CreditBankIncomeRefreshRequestOptions from a JSON string
credit_bank_income_refresh_request_options_instance = CreditBankIncomeRefreshRequestOptions.from_json(json)
# print the JSON string representation of the object
print CreditBankIncomeRefreshRequestOptions.to_json()

# convert the object into a dict
credit_bank_income_refresh_request_options_dict = credit_bank_income_refresh_request_options_instance.to_dict()
# create an instance of CreditBankIncomeRefreshRequestOptions from a dict
credit_bank_income_refresh_request_options_form_dict = credit_bank_income_refresh_request_options.from_dict(credit_bank_income_refresh_request_options_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


