# AssetReportGetRequestOptions

An optional object to filter or add data to `/asset_report/get` results. If provided, must be non-`null`.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**days_to_include** | **int** | The maximum integer number of days of history to include in the Asset Report. | [optional] 

## Example

```python
from pyplaid.models.asset_report_get_request_options import AssetReportGetRequestOptions

# TODO update the JSON string below
json = "{}"
# create an instance of AssetReportGetRequestOptions from a JSON string
asset_report_get_request_options_instance = AssetReportGetRequestOptions.from_json(json)
# print the JSON string representation of the object
print AssetReportGetRequestOptions.to_json()

# convert the object into a dict
asset_report_get_request_options_dict = asset_report_get_request_options_instance.to_dict()
# create an instance of AssetReportGetRequestOptions from a dict
asset_report_get_request_options_form_dict = asset_report_get_request_options.from_dict(asset_report_get_request_options_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


