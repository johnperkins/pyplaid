# CreditSessionsGetResponse

CreditSessionsGetResponse defines the response schema for `/credit/sessions/get`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sessions** | [**List[CreditSession]**](CreditSession.md) | A list of Link sessions for the user. Sessions will be sorted in reverse chronological order. | [optional] 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.credit_sessions_get_response import CreditSessionsGetResponse

# TODO update the JSON string below
json = "{}"
# create an instance of CreditSessionsGetResponse from a JSON string
credit_sessions_get_response_instance = CreditSessionsGetResponse.from_json(json)
# print the JSON string representation of the object
print CreditSessionsGetResponse.to_json()

# convert the object into a dict
credit_sessions_get_response_dict = credit_sessions_get_response_instance.to_dict()
# create an instance of CreditSessionsGetResponse from a dict
credit_sessions_get_response_form_dict = credit_sessions_get_response.from_dict(credit_sessions_get_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


