# AssetOwners

No documentation available

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**asset_owner** | [**List[AssetOwner]**](AssetOwner.md) | Multiple Occurances of Account Owners Full Name up to 4. | 

## Example

```python
from pyplaid.models.asset_owners import AssetOwners

# TODO update the JSON string below
json = "{}"
# create an instance of AssetOwners from a JSON string
asset_owners_instance = AssetOwners.from_json(json)
# print the JSON string representation of the object
print AssetOwners.to_json()

# convert the object into a dict
asset_owners_dict = asset_owners_instance.to_dict()
# create an instance of AssetOwners from a dict
asset_owners_form_dict = asset_owners.from_dict(asset_owners_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


