# WalletBalance

An object representing the e-wallet balance

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**iso_currency_code** | **str** | The ISO-4217 currency code of the balance | 
**current** | **float** | The total amount of funds in the account | 

## Example

```python
from pyplaid.models.wallet_balance import WalletBalance

# TODO update the JSON string below
json = "{}"
# create an instance of WalletBalance from a JSON string
wallet_balance_instance = WalletBalance.from_json(json)
# print the JSON string representation of the object
print WalletBalance.to_json()

# convert the object into a dict
wallet_balance_dict = wallet_balance_instance.to_dict()
# create an instance of WalletBalance from a dict
wallet_balance_form_dict = wallet_balance.from_dict(wallet_balance_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


