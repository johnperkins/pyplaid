# IncomeOverride

Specify payroll data on the account.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**paystubs** | [**List[PaystubOverride]**](PaystubOverride.md) | A list of paystubs associated with the account. | [optional] 

## Example

```python
from pyplaid.models.income_override import IncomeOverride

# TODO update the JSON string below
json = "{}"
# create an instance of IncomeOverride from a JSON string
income_override_instance = IncomeOverride.from_json(json)
# print the JSON string representation of the object
print IncomeOverride.to_json()

# convert the object into a dict
income_override_dict = income_override_instance.to_dict()
# create an instance of IncomeOverride from a dict
income_override_form_dict = income_override.from_dict(income_override_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


