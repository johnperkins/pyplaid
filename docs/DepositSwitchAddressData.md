# DepositSwitchAddressData

The user's address.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**city** | **str** | The full city name | 
**region** | **str** | The region or state Example: &#x60;\&quot;NC\&quot;&#x60; | 
**street** | **str** | The full street address Example: &#x60;\&quot;564 Main Street, APT 15\&quot;&#x60; | 
**postal_code** | **str** | The postal code | 
**country** | **str** | The ISO 3166-1 alpha-2 country code | 

## Example

```python
from pyplaid.models.deposit_switch_address_data import DepositSwitchAddressData

# TODO update the JSON string below
json = "{}"
# create an instance of DepositSwitchAddressData from a JSON string
deposit_switch_address_data_instance = DepositSwitchAddressData.from_json(json)
# print the JSON string representation of the object
print DepositSwitchAddressData.to_json()

# convert the object into a dict
deposit_switch_address_data_dict = deposit_switch_address_data_instance.to_dict()
# create an instance of DepositSwitchAddressData from a dict
deposit_switch_address_data_form_dict = deposit_switch_address_data.from_dict(deposit_switch_address_data_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


