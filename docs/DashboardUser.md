# DashboardUser

Account information associated with a team member with access to the Plaid dashboard.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | ID of the associated user. | 
**created_at** | **datetime** | An ISO8601 formatted timestamp. | 
**email_address** | **str** | A valid email address. | 
**status** | [**DashboardUserStatus**](DashboardUserStatus.md) |  | 

## Example

```python
from pyplaid.models.dashboard_user import DashboardUser

# TODO update the JSON string below
json = "{}"
# create an instance of DashboardUser from a JSON string
dashboard_user_instance = DashboardUser.from_json(json)
# print the JSON string representation of the object
print DashboardUser.to_json()

# convert the object into a dict
dashboard_user_dict = dashboard_user_instance.to_dict()
# create an instance of DashboardUser from a dict
dashboard_user_form_dict = dashboard_user.from_dict(dashboard_user_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


