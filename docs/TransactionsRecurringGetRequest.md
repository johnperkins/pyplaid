# TransactionsRecurringGetRequest

TransactionsRecurringGetRequest defines the request schema for `/transactions/recurring/get`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**access_token** | **str** | The access token associated with the Item data is being requested for. | 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**options** | [**TransactionsRecurringGetRequestOptions**](TransactionsRecurringGetRequestOptions.md) |  | [optional] 
**account_ids** | **List[str]** | A list of &#x60;account_ids&#x60; to retrieve for the Item  Note: An error will be returned if a provided &#x60;account_id&#x60; is not associated with the Item. | 

## Example

```python
from pyplaid.models.transactions_recurring_get_request import TransactionsRecurringGetRequest

# TODO update the JSON string below
json = "{}"
# create an instance of TransactionsRecurringGetRequest from a JSON string
transactions_recurring_get_request_instance = TransactionsRecurringGetRequest.from_json(json)
# print the JSON string representation of the object
print TransactionsRecurringGetRequest.to_json()

# convert the object into a dict
transactions_recurring_get_request_dict = transactions_recurring_get_request_instance.to_dict()
# create an instance of TransactionsRecurringGetRequest from a dict
transactions_recurring_get_request_form_dict = transactions_recurring_get_request.from_dict(transactions_recurring_get_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


