# SandboxPublicTokenCreateRequestIncomeVerificationBankIncome

Specifies options for Bank Income. This field is required if `income_verification` is included in the `initial_products` array and `bank` is specified in `income_source_types`.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**days_requested** | **int** | The number of days of data to request for the Bank Income product | [optional] 

## Example

```python
from pyplaid.models.sandbox_public_token_create_request_income_verification_bank_income import SandboxPublicTokenCreateRequestIncomeVerificationBankIncome

# TODO update the JSON string below
json = "{}"
# create an instance of SandboxPublicTokenCreateRequestIncomeVerificationBankIncome from a JSON string
sandbox_public_token_create_request_income_verification_bank_income_instance = SandboxPublicTokenCreateRequestIncomeVerificationBankIncome.from_json(json)
# print the JSON string representation of the object
print SandboxPublicTokenCreateRequestIncomeVerificationBankIncome.to_json()

# convert the object into a dict
sandbox_public_token_create_request_income_verification_bank_income_dict = sandbox_public_token_create_request_income_verification_bank_income_instance.to_dict()
# create an instance of SandboxPublicTokenCreateRequestIncomeVerificationBankIncome from a dict
sandbox_public_token_create_request_income_verification_bank_income_form_dict = sandbox_public_token_create_request_income_verification_bank_income.from_dict(sandbox_public_token_create_request_income_verification_bank_income_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


