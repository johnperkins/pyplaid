# CreditSessionResults

The set of results for a Link session.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**item_add_results** | [**List[CreditSessionItemAddResult]**](CreditSessionItemAddResult.md) | The set of item adds for the Link session. | [optional] 
**bank_income_results** | [**List[CreditSessionBankIncomeResult]**](CreditSessionBankIncomeResult.md) | The set of bank income verifications for the Link session. | [optional] 
**payroll_income_results** | [**List[CreditSessionPayrollIncomeResult]**](CreditSessionPayrollIncomeResult.md) | The set of payroll income verifications for the Link session. | [optional] 
**document_income_results** | [**CreditSessionDocumentIncomeResult**](CreditSessionDocumentIncomeResult.md) |  | [optional] 

## Example

```python
from pyplaid.models.credit_session_results import CreditSessionResults

# TODO update the JSON string below
json = "{}"
# create an instance of CreditSessionResults from a JSON string
credit_session_results_instance = CreditSessionResults.from_json(json)
# print the JSON string representation of the object
print CreditSessionResults.to_json()

# convert the object into a dict
credit_session_results_dict = credit_session_results_instance.to_dict()
# create an instance of CreditSessionResults from a dict
credit_session_results_form_dict = credit_session_results.from_dict(credit_session_results_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


