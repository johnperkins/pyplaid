# TransferOriginatorListRequest

Defines the request schema for `/transfer/originator/list`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**count** | **int** | The maximum number of transfers to return. | [optional] [default to 25]
**offset** | **int** | The number of transfers to skip before returning results. | [optional] [default to 0]

## Example

```python
from pyplaid.models.transfer_originator_list_request import TransferOriginatorListRequest

# TODO update the JSON string below
json = "{}"
# create an instance of TransferOriginatorListRequest from a JSON string
transfer_originator_list_request_instance = TransferOriginatorListRequest.from_json(json)
# print the JSON string representation of the object
print TransferOriginatorListRequest.to_json()

# convert the object into a dict
transfer_originator_list_request_dict = transfer_originator_list_request_instance.to_dict()
# create an instance of TransferOriginatorListRequest from a dict
transfer_originator_list_request_form_dict = transfer_originator_list_request.from_dict(transfer_originator_list_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


