# CreditAuditCopyTokenCreateRequest

CreditAuditCopyTokenCreateRequest defines the request schema for `/credit/audit_copy_token/create`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**report_tokens** | **List[str]** | List of report tokens; can include both Asset Report tokens and Income Report tokens. | 

## Example

```python
from pyplaid.models.credit_audit_copy_token_create_request import CreditAuditCopyTokenCreateRequest

# TODO update the JSON string below
json = "{}"
# create an instance of CreditAuditCopyTokenCreateRequest from a JSON string
credit_audit_copy_token_create_request_instance = CreditAuditCopyTokenCreateRequest.from_json(json)
# print the JSON string representation of the object
print CreditAuditCopyTokenCreateRequest.to_json()

# convert the object into a dict
credit_audit_copy_token_create_request_dict = credit_audit_copy_token_create_request_instance.to_dict()
# create an instance of CreditAuditCopyTokenCreateRequest from a dict
credit_audit_copy_token_create_request_form_dict = credit_audit_copy_token_create_request.from_dict(credit_audit_copy_token_create_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


