# AssetReportGetResponse

AssetReportGetResponse defines the response schema for `/asset_report/get`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**report** | [**AssetReport**](AssetReport.md) |  | 
**warnings** | [**List[Warning]**](Warning.md) | If the Asset Report generation was successful but identity information cannot be returned, this array will contain information about the errors causing identity information to be missing | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.asset_report_get_response import AssetReportGetResponse

# TODO update the JSON string below
json = "{}"
# create an instance of AssetReportGetResponse from a JSON string
asset_report_get_response_instance = AssetReportGetResponse.from_json(json)
# print the JSON string representation of the object
print AssetReportGetResponse.to_json()

# convert the object into a dict
asset_report_get_response_dict = asset_report_get_response_instance.to_dict()
# create an instance of AssetReportGetResponse from a dict
asset_report_get_response_form_dict = asset_report_get_response.from_dict(asset_report_get_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


