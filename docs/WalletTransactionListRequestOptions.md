# WalletTransactionListRequestOptions

Additional wallet transaction options

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**start_time** | **datetime** | Timestamp in [ISO 8601](https://wikipedia.org/wiki/ISO_8601) format (YYYY-MM-DDThh:mm:ssZ) for filtering transactions, inclusive of the provided date. | [optional] 
**end_time** | **datetime** | Timestamp in [ISO 8601](https://wikipedia.org/wiki/ISO_8601) format (YYYY-MM-DDThh:mm:ssZ) for filtering transactions, inclusive of the provided date. | [optional] 

## Example

```python
from pyplaid.models.wallet_transaction_list_request_options import WalletTransactionListRequestOptions

# TODO update the JSON string below
json = "{}"
# create an instance of WalletTransactionListRequestOptions from a JSON string
wallet_transaction_list_request_options_instance = WalletTransactionListRequestOptions.from_json(json)
# print the JSON string representation of the object
print WalletTransactionListRequestOptions.to_json()

# convert the object into a dict
wallet_transaction_list_request_options_dict = wallet_transaction_list_request_options_instance.to_dict()
# create an instance of WalletTransactionListRequestOptions from a dict
wallet_transaction_list_request_options_form_dict = wallet_transaction_list_request_options.from_dict(wallet_transaction_list_request_options_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


