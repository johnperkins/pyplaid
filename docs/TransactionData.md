# TransactionData

Information about the matched direct deposit transaction used to verify a user's payroll information.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **str** | The description of the transaction. | 
**amount** | **float** | The amount of the transaction. | 
**var_date** | **date** | The date of the transaction, in [ISO 8601](https://wikipedia.org/wiki/ISO_8601) format (\&quot;yyyy-mm-dd\&quot;). | 
**account_id** | **str** | A unique identifier for the end user&#39;s account. | 
**transaction_id** | **str** | A unique identifier for the transaction. | 

## Example

```python
from pyplaid.models.transaction_data import TransactionData

# TODO update the JSON string below
json = "{}"
# create an instance of TransactionData from a JSON string
transaction_data_instance = TransactionData.from_json(json)
# print the JSON string representation of the object
print TransactionData.to_json()

# convert the object into a dict
transaction_data_dict = transaction_data_instance.to_dict()
# create an instance of TransactionData from a dict
transaction_data_form_dict = transaction_data.from_dict(transaction_data_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


