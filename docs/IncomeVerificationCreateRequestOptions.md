# IncomeVerificationCreateRequestOptions

Optional arguments for `/income/verification/create`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_tokens** | **List[str]** | An array of access tokens corresponding to the Items that will be cross-referenced with the product data. Plaid will attempt to correlate transaction history from these Items with data from the user&#39;s paystub, such as date and amount. The &#x60;verification&#x60; status of the paystub as returned by &#x60;/income/verification/paystubs/get&#x60; will indicate if the verification status was successful, or, if not, why it failed. If the &#x60;transactions&#x60; product was not initialized for the Items during Link, it will be initialized after this Link session. | [optional] 

## Example

```python
from pyplaid.models.income_verification_create_request_options import IncomeVerificationCreateRequestOptions

# TODO update the JSON string below
json = "{}"
# create an instance of IncomeVerificationCreateRequestOptions from a JSON string
income_verification_create_request_options_instance = IncomeVerificationCreateRequestOptions.from_json(json)
# print the JSON string representation of the object
print IncomeVerificationCreateRequestOptions.to_json()

# convert the object into a dict
income_verification_create_request_options_dict = income_verification_create_request_options_instance.to_dict()
# create an instance of IncomeVerificationCreateRequestOptions from a dict
income_verification_create_request_options_form_dict = income_verification_create_request_options.from_dict(income_verification_create_request_options_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


