# TransferUserAddressInRequest

The address associated with the account holder. Providing this data will improve the likelihood that Plaid will be able to guarantee the transfer, if applicable.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**street** | **str** | The street number and name (i.e., \&quot;100 Market St.\&quot;). | [optional] 
**city** | **str** | Ex. \&quot;San Francisco\&quot; | [optional] 
**region** | **str** | The state or province (e.g., \&quot;CA\&quot;). | [optional] 
**postal_code** | **str** | The postal code (e.g., \&quot;94103\&quot;). | [optional] 
**country** | **str** | A two-letter country code (e.g., \&quot;US\&quot;). | [optional] 

## Example

```python
from pyplaid.models.transfer_user_address_in_request import TransferUserAddressInRequest

# TODO update the JSON string below
json = "{}"
# create an instance of TransferUserAddressInRequest from a JSON string
transfer_user_address_in_request_instance = TransferUserAddressInRequest.from_json(json)
# print the JSON string representation of the object
print TransferUserAddressInRequest.to_json()

# convert the object into a dict
transfer_user_address_in_request_dict = transfer_user_address_in_request_instance.to_dict()
# create an instance of TransferUserAddressInRequest from a dict
transfer_user_address_in_request_form_dict = transfer_user_address_in_request.from_dict(transfer_user_address_in_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


