# KYCCheckNameSummary

Result summary object specifying how the `name` field matched.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**summary** | [**MatchSummaryCode**](MatchSummaryCode.md) |  | 

## Example

```python
from pyplaid.models.kyc_check_name_summary import KYCCheckNameSummary

# TODO update the JSON string below
json = "{}"
# create an instance of KYCCheckNameSummary from a JSON string
kyc_check_name_summary_instance = KYCCheckNameSummary.from_json(json)
# print the JSON string representation of the object
print KYCCheckNameSummary.to_json()

# convert the object into a dict
kyc_check_name_summary_dict = kyc_check_name_summary_instance.to_dict()
# create an instance of KYCCheckNameSummary from a dict
kyc_check_name_summary_form_dict = kyc_check_name_summary.from_dict(kyc_check_name_summary_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


