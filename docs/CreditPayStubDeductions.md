# CreditPayStubDeductions

An object with the deduction information found on a pay stub.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**breakdown** | [**List[PayStubDeductionsBreakdown]**](PayStubDeductionsBreakdown.md) |  | 
**total** | [**PayStubDeductionsTotal**](PayStubDeductionsTotal.md) |  | 

## Example

```python
from pyplaid.models.credit_pay_stub_deductions import CreditPayStubDeductions

# TODO update the JSON string below
json = "{}"
# create an instance of CreditPayStubDeductions from a JSON string
credit_pay_stub_deductions_instance = CreditPayStubDeductions.from_json(json)
# print the JSON string representation of the object
print CreditPayStubDeductions.to_json()

# convert the object into a dict
credit_pay_stub_deductions_dict = credit_pay_stub_deductions_instance.to_dict()
# create an instance of CreditPayStubDeductions from a dict
credit_pay_stub_deductions_form_dict = credit_pay_stub_deductions.from_dict(credit_pay_stub_deductions_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


