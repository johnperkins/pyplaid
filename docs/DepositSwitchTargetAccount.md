# DepositSwitchTargetAccount

The deposit switch destination account

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_number** | **str** | Account number for deposit switch destination | 
**routing_number** | **str** | Routing number for deposit switch destination | 
**account_name** | **str** | The name of the deposit switch destination account, as it will be displayed to the end user in the Deposit Switch interface. It is not required to match the name used in online banking. | 
**account_subtype** | **str** | The account subtype of the account, either &#x60;checking&#x60; or &#x60;savings&#x60;. | 

## Example

```python
from pyplaid.models.deposit_switch_target_account import DepositSwitchTargetAccount

# TODO update the JSON string below
json = "{}"
# create an instance of DepositSwitchTargetAccount from a JSON string
deposit_switch_target_account_instance = DepositSwitchTargetAccount.from_json(json)
# print the JSON string representation of the object
print DepositSwitchTargetAccount.to_json()

# convert the object into a dict
deposit_switch_target_account_dict = deposit_switch_target_account_instance.to_dict()
# create an instance of DepositSwitchTargetAccount from a dict
deposit_switch_target_account_form_dict = deposit_switch_target_account.from_dict(deposit_switch_target_account_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


