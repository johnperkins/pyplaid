# AuthSupportedMethods

Metadata specifically related to which auth methods an institution supports.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**instant_auth** | **bool** | Indicates if instant auth is supported. | 
**instant_match** | **bool** | Indicates if instant match is supported. | 
**automated_micro_deposits** | **bool** | Indicates if automated microdeposits are supported. | 

## Example

```python
from pyplaid.models.auth_supported_methods import AuthSupportedMethods

# TODO update the JSON string below
json = "{}"
# create an instance of AuthSupportedMethods from a JSON string
auth_supported_methods_instance = AuthSupportedMethods.from_json(json)
# print the JSON string representation of the object
print AuthSupportedMethods.to_json()

# convert the object into a dict
auth_supported_methods_dict = auth_supported_methods_instance.to_dict()
# create an instance of AuthSupportedMethods from a dict
auth_supported_methods_form_dict = auth_supported_methods.from_dict(auth_supported_methods_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


