# AccountAccess

Allow or disallow product access by account. Unlisted (e.g. missing) accounts will be considered `new_accounts`.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**unique_id** | **str** | The unique account identifier for this account. This value must match that returned by the data access API for this account. | 
**authorized** | **bool** | Allow the application to see this account (and associated details, including balance) in the list of accounts  If unset, defaults to &#x60;true&#x60;. | [optional] [default to True]
**account_product_access** | [**AccountProductAccessNullable**](AccountProductAccessNullable.md) |  | [optional] 

## Example

```python
from pyplaid.models.account_access import AccountAccess

# TODO update the JSON string below
json = "{}"
# create an instance of AccountAccess from a JSON string
account_access_instance = AccountAccess.from_json(json)
# print the JSON string representation of the object
print AccountAccess.to_json()

# convert the object into a dict
account_access_dict = account_access_instance.to_dict()
# create an instance of AccountAccess from a dict
account_access_form_dict = account_access.from_dict(account_access_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


