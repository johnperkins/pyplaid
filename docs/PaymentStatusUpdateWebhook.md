# PaymentStatusUpdateWebhook

Fired when the status of a payment has changed.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**webhook_type** | **str** | &#x60;PAYMENT_INITIATION&#x60; | 
**webhook_code** | **str** | &#x60;PAYMENT_STATUS_UPDATE&#x60; | 
**payment_id** | **str** | The &#x60;payment_id&#x60; for the payment being updated | 
**new_payment_status** | [**PaymentInitiationPaymentStatus**](PaymentInitiationPaymentStatus.md) |  | 
**old_payment_status** | [**PaymentInitiationPaymentStatus**](PaymentInitiationPaymentStatus.md) |  | 
**original_reference** | **str** | The original value of the reference when creating the payment. | 
**adjusted_reference** | **str** | The value of the reference sent to the bank after adjustment to pass bank validation rules. | [optional] 
**original_start_date** | **date** | The original value of the &#x60;start_date&#x60; provided during the creation of a standing order. If the payment is not a standing order, this field will be &#x60;null&#x60;. | 
**adjusted_start_date** | **date** | The start date sent to the bank after adjusting for holidays or weekends.  Will be provided in [ISO 8601](https://wikipedia.org/wiki/ISO_8601) format (YYYY-MM-DD). If the start date did not require adjustment, or if the payment is not a standing order, this field will be &#x60;null&#x60;. | 
**timestamp** | **datetime** | The timestamp of the update, in [ISO 8601](https://wikipedia.org/wiki/ISO_8601) format, e.g. &#x60;\&quot;2017-09-14T14:42:19.350Z\&quot;&#x60; | 
**error** | [**PlaidError**](PlaidError.md) |  | [optional] 
**environment** | [**WebhookEnvironmentValues**](WebhookEnvironmentValues.md) |  | 

## Example

```python
from pyplaid.models.payment_status_update_webhook import PaymentStatusUpdateWebhook

# TODO update the JSON string below
json = "{}"
# create an instance of PaymentStatusUpdateWebhook from a JSON string
payment_status_update_webhook_instance = PaymentStatusUpdateWebhook.from_json(json)
# print the JSON string representation of the object
print PaymentStatusUpdateWebhook.to_json()

# convert the object into a dict
payment_status_update_webhook_dict = payment_status_update_webhook_instance.to_dict()
# create an instance of PaymentStatusUpdateWebhook from a dict
payment_status_update_webhook_form_dict = payment_status_update_webhook.from_dict(payment_status_update_webhook_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


