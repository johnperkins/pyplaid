# LinkTokenCreateInstitutionData

A map containing data used to highlight institutions in Link.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**routing_number** | **str** | The routing number of the bank to highlight. | [optional] 

## Example

```python
from pyplaid.models.link_token_create_institution_data import LinkTokenCreateInstitutionData

# TODO update the JSON string below
json = "{}"
# create an instance of LinkTokenCreateInstitutionData from a JSON string
link_token_create_institution_data_instance = LinkTokenCreateInstitutionData.from_json(json)
# print the JSON string representation of the object
print LinkTokenCreateInstitutionData.to_json()

# convert the object into a dict
link_token_create_institution_data_dict = link_token_create_institution_data_instance.to_dict()
# create an instance of LinkTokenCreateInstitutionData from a dict
link_token_create_institution_data_form_dict = link_token_create_institution_data.from_dict(link_token_create_institution_data_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


