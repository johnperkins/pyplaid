# PaymentInitiationPaymentTokenCreateResponse

PaymentInitiationPaymentTokenCreateResponse defines the response schema for `/payment_initiation/payment/token/create`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**payment_token** | **str** | A &#x60;payment_token&#x60; that can be provided to Link initialization to enter the payment initiation flow | 
**payment_token_expiration_time** | **datetime** | The date and time at which the token will expire, in [ISO 8601](https://wikipedia.org/wiki/ISO_8601) format. A &#x60;payment_token&#x60; expires after 15 minutes. | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.payment_initiation_payment_token_create_response import PaymentInitiationPaymentTokenCreateResponse

# TODO update the JSON string below
json = "{}"
# create an instance of PaymentInitiationPaymentTokenCreateResponse from a JSON string
payment_initiation_payment_token_create_response_instance = PaymentInitiationPaymentTokenCreateResponse.from_json(json)
# print the JSON string representation of the object
print PaymentInitiationPaymentTokenCreateResponse.to_json()

# convert the object into a dict
payment_initiation_payment_token_create_response_dict = payment_initiation_payment_token_create_response_instance.to_dict()
# create an instance of PaymentInitiationPaymentTokenCreateResponse from a dict
payment_initiation_payment_token_create_response_form_dict = payment_initiation_payment_token_create_response.from_dict(payment_initiation_payment_token_create_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


