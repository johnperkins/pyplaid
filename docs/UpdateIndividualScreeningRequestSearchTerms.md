# UpdateIndividualScreeningRequestSearchTerms

Search terms for editing an individual watchlist screening

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**watchlist_program_id** | **str** | ID of the associated program. | [optional] 
**legal_name** | **str** | The legal name of the individual being screened. | [optional] 
**date_of_birth** | **date** | A date in the format YYYY-MM-DD (RFC 3339 Section 5.6). | [optional] 
**document_number** | **str** | The numeric or alphanumeric identifier associated with this document. | [optional] 
**country** | **str** | Valid, capitalized, two-letter ISO code representing the country of this object. Must be in ISO 3166-1 alpha-2 form. | [optional] 

## Example

```python
from pyplaid.models.update_individual_screening_request_search_terms import UpdateIndividualScreeningRequestSearchTerms

# TODO update the JSON string below
json = "{}"
# create an instance of UpdateIndividualScreeningRequestSearchTerms from a JSON string
update_individual_screening_request_search_terms_instance = UpdateIndividualScreeningRequestSearchTerms.from_json(json)
# print the JSON string representation of the object
print UpdateIndividualScreeningRequestSearchTerms.to_json()

# convert the object into a dict
update_individual_screening_request_search_terms_dict = update_individual_screening_request_search_terms_instance.to_dict()
# create an instance of UpdateIndividualScreeningRequestSearchTerms from a dict
update_individual_screening_request_search_terms_form_dict = update_individual_screening_request_search_terms.from_dict(update_individual_screening_request_search_terms_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


