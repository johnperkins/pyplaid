# NetPay

An object representing information about the net pay amount on the paystub.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**current_amount** | **float** | Raw amount of the net pay for the pay period | [optional] 
**description** | **str** | Description of the net pay | [optional] 
**iso_currency_code** | **str** | The ISO-4217 currency code of the net pay. Always &#x60;null&#x60; if &#x60;unofficial_currency_code&#x60; is non-null. | [optional] 
**unofficial_currency_code** | **str** | The unofficial currency code associated with the net pay. Always &#x60;null&#x60; if &#x60;iso_currency_code&#x60; is non-&#x60;null&#x60;. Unofficial currency codes are used for currencies that do not have official ISO currency codes, such as cryptocurrencies and the currencies of certain countries.  See the [currency code schema](https://plaid.com/docs/api/accounts#currency-code-schema) for a full listing of supported &#x60;iso_currency_code&#x60;s. | [optional] 
**ytd_amount** | **float** | The year-to-date amount of the net pay | [optional] 
**total** | [**Total**](Total.md) |  | [optional] 

## Example

```python
from pyplaid.models.net_pay import NetPay

# TODO update the JSON string below
json = "{}"
# create an instance of NetPay from a JSON string
net_pay_instance = NetPay.from_json(json)
# print the JSON string representation of the object
print NetPay.to_json()

# convert the object into a dict
net_pay_dict = net_pay_instance.to_dict()
# create an instance of NetPay from a dict
net_pay_form_dict = net_pay.from_dict(net_pay_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


