# WebhookVerificationKeyGetRequest

WebhookVerificationKeyGetRequest defines the request schema for `/webhook_verification_key/get`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**key_id** | **str** | The key ID ( &#x60;kid&#x60; ) from the JWT header. | 

## Example

```python
from pyplaid.models.webhook_verification_key_get_request import WebhookVerificationKeyGetRequest

# TODO update the JSON string below
json = "{}"
# create an instance of WebhookVerificationKeyGetRequest from a JSON string
webhook_verification_key_get_request_instance = WebhookVerificationKeyGetRequest.from_json(json)
# print the JSON string representation of the object
print WebhookVerificationKeyGetRequest.to_json()

# convert the object into a dict
webhook_verification_key_get_request_dict = webhook_verification_key_get_request_instance.to_dict()
# create an instance of WebhookVerificationKeyGetRequest from a dict
webhook_verification_key_get_request_form_dict = webhook_verification_key_get_request.from_dict(webhook_verification_key_get_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


