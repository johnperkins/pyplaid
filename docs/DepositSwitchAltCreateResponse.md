# DepositSwitchAltCreateResponse

DepositSwitchAltCreateResponse defines the response schema for `/deposit_switch/alt/create`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**deposit_switch_id** | **str** | ID of the deposit switch. This ID is persisted throughout the lifetime of the deposit switch. | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.deposit_switch_alt_create_response import DepositSwitchAltCreateResponse

# TODO update the JSON string below
json = "{}"
# create an instance of DepositSwitchAltCreateResponse from a JSON string
deposit_switch_alt_create_response_instance = DepositSwitchAltCreateResponse.from_json(json)
# print the JSON string representation of the object
print DepositSwitchAltCreateResponse.to_json()

# convert the object into a dict
deposit_switch_alt_create_response_dict = deposit_switch_alt_create_response_instance.to_dict()
# create an instance of DepositSwitchAltCreateResponse from a dict
deposit_switch_alt_create_response_form_dict = deposit_switch_alt_create_response.from_dict(deposit_switch_alt_create_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


