# Location

A representation of where a transaction took place

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address** | **str** | The street address where the transaction occurred. | 
**city** | **str** | The city where the transaction occurred. | 
**region** | **str** | The region or state where the transaction occurred. In API versions 2018-05-22 and earlier, this field is called &#x60;state&#x60;. | 
**postal_code** | **str** | The postal code where the transaction occurred. In API versions 2018-05-22 and earlier, this field is called &#x60;zip&#x60;. | 
**country** | **str** | The ISO 3166-1 alpha-2 country code where the transaction occurred. | 
**lat** | **float** | The latitude where the transaction occurred. | 
**lon** | **float** | The longitude where the transaction occurred. | 
**store_number** | **str** | The merchant defined store number where the transaction occurred. | 

## Example

```python
from pyplaid.models.location import Location

# TODO update the JSON string below
json = "{}"
# create an instance of Location from a JSON string
location_instance = Location.from_json(json)
# print the JSON string representation of the object
print Location.to_json()

# convert the object into a dict
location_dict = location_instance.to_dict()
# create an instance of Location from a dict
location_form_dict = location.from_dict(location_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


