# CreditPayStubEarnings

An object representing both a breakdown of earnings on a pay stub and the total earnings.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**breakdown** | [**List[PayStubEarningsBreakdown]**](PayStubEarningsBreakdown.md) |  | 
**total** | [**PayStubEarningsTotal**](PayStubEarningsTotal.md) |  | 

## Example

```python
from pyplaid.models.credit_pay_stub_earnings import CreditPayStubEarnings

# TODO update the JSON string below
json = "{}"
# create an instance of CreditPayStubEarnings from a JSON string
credit_pay_stub_earnings_instance = CreditPayStubEarnings.from_json(json)
# print the JSON string representation of the object
print CreditPayStubEarnings.to_json()

# convert the object into a dict
credit_pay_stub_earnings_dict = credit_pay_stub_earnings_instance.to_dict()
# create an instance of CreditPayStubEarnings from a dict
credit_pay_stub_earnings_form_dict = credit_pay_stub_earnings.from_dict(credit_pay_stub_earnings_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


