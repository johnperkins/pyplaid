# WatchlistScreeningIndividualProgramListRequest

Request input for listing watchlist screening programs for individuals

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**cursor** | **str** | An identifier that determines which page of results you receive. | [optional] 

## Example

```python
from pyplaid.models.watchlist_screening_individual_program_list_request import WatchlistScreeningIndividualProgramListRequest

# TODO update the JSON string below
json = "{}"
# create an instance of WatchlistScreeningIndividualProgramListRequest from a JSON string
watchlist_screening_individual_program_list_request_instance = WatchlistScreeningIndividualProgramListRequest.from_json(json)
# print the JSON string representation of the object
print WatchlistScreeningIndividualProgramListRequest.to_json()

# convert the object into a dict
watchlist_screening_individual_program_list_request_dict = watchlist_screening_individual_program_list_request_instance.to_dict()
# create an instance of WatchlistScreeningIndividualProgramListRequest from a dict
watchlist_screening_individual_program_list_request_form_dict = watchlist_screening_individual_program_list_request.from_dict(watchlist_screening_individual_program_list_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


