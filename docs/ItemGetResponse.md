# ItemGetResponse

ItemGetResponse defines the response schema for `/item/get` and `/item/webhook/update`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**item** | [**Item**](Item.md) |  | 
**status** | [**ItemStatusNullable**](ItemStatusNullable.md) |  | [optional] 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.item_get_response import ItemGetResponse

# TODO update the JSON string below
json = "{}"
# create an instance of ItemGetResponse from a JSON string
item_get_response_instance = ItemGetResponse.from_json(json)
# print the JSON string representation of the object
print ItemGetResponse.to_json()

# convert the object into a dict
item_get_response_dict = item_get_response_instance.to_dict()
# create an instance of ItemGetResponse from a dict
item_get_response_form_dict = item_get_response.from_dict(item_get_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


