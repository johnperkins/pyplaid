# ItemPublicTokenExchangeRequest

ItemPublicTokenExchangeRequest defines the request schema for `/item/public_token/exchange`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**public_token** | **str** | Your &#x60;public_token&#x60;, obtained from the Link &#x60;onSuccess&#x60; callback or &#x60;/sandbox/item/public_token/create&#x60;. | 

## Example

```python
from pyplaid.models.item_public_token_exchange_request import ItemPublicTokenExchangeRequest

# TODO update the JSON string below
json = "{}"
# create an instance of ItemPublicTokenExchangeRequest from a JSON string
item_public_token_exchange_request_instance = ItemPublicTokenExchangeRequest.from_json(json)
# print the JSON string representation of the object
print ItemPublicTokenExchangeRequest.to_json()

# convert the object into a dict
item_public_token_exchange_request_dict = item_public_token_exchange_request_instance.to_dict()
# create an instance of ItemPublicTokenExchangeRequest from a dict
item_public_token_exchange_request_form_dict = item_public_token_exchange_request.from_dict(item_public_token_exchange_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


