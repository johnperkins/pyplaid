# PaymentInitiationConsent

PaymentInitiationConsent defines a payment initiation consent.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**consent_id** | **str** | The consent ID. | 
**status** | [**PaymentInitiationConsentStatus**](PaymentInitiationConsentStatus.md) |  | 
**created_at** | **datetime** | Consent creation timestamp, in [ISO 8601](https://wikipedia.org/wiki/ISO_8601) format. | 
**recipient_id** | **str** | The ID of the recipient the payment consent is for. | 
**reference** | **str** | A reference for the payment consent. | 
**constraints** | [**PaymentInitiationConsentConstraints**](PaymentInitiationConsentConstraints.md) |  | 
**scopes** | [**List[PaymentInitiationConsentScope]**](PaymentInitiationConsentScope.md) | An array of payment consent scopes. | 

## Example

```python
from pyplaid.models.payment_initiation_consent import PaymentInitiationConsent

# TODO update the JSON string below
json = "{}"
# create an instance of PaymentInitiationConsent from a JSON string
payment_initiation_consent_instance = PaymentInitiationConsent.from_json(json)
# print the JSON string representation of the object
print PaymentInitiationConsent.to_json()

# convert the object into a dict
payment_initiation_consent_dict = payment_initiation_consent_instance.to_dict()
# create an instance of PaymentInitiationConsent from a dict
payment_initiation_consent_form_dict = payment_initiation_consent.from_dict(payment_initiation_consent_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


