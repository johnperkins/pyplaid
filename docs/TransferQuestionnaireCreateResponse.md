# TransferQuestionnaireCreateResponse

Defines the response schema for `/transfer/questionnaire/create`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**onboarding_url** | **str** | Plaid-hosted onboarding URL that you will redirect the end customer to. | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.transfer_questionnaire_create_response import TransferQuestionnaireCreateResponse

# TODO update the JSON string below
json = "{}"
# create an instance of TransferQuestionnaireCreateResponse from a JSON string
transfer_questionnaire_create_response_instance = TransferQuestionnaireCreateResponse.from_json(json)
# print the JSON string representation of the object
print TransferQuestionnaireCreateResponse.to_json()

# convert the object into a dict
transfer_questionnaire_create_response_dict = transfer_questionnaire_create_response_instance.to_dict()
# create an instance of TransferQuestionnaireCreateResponse from a dict
transfer_questionnaire_create_response_form_dict = transfer_questionnaire_create_response.from_dict(transfer_questionnaire_create_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


