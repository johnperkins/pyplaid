# AssetHolder

No documentation available

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | [**AssetHolderName**](AssetHolderName.md) |  | 

## Example

```python
from pyplaid.models.asset_holder import AssetHolder

# TODO update the JSON string below
json = "{}"
# create an instance of AssetHolder from a JSON string
asset_holder_instance = AssetHolder.from_json(json)
# print the JSON string representation of the object
print AssetHolder.to_json()

# convert the object into a dict
asset_holder_dict = asset_holder_instance.to_dict()
# create an instance of AssetHolder from a dict
asset_holder_form_dict = asset_holder.from_dict(asset_holder_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


