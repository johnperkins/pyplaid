# PaymentInitiationRecipientGetResponseAllOf


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | [optional] 

## Example

```python
from pyplaid.models.payment_initiation_recipient_get_response_all_of import PaymentInitiationRecipientGetResponseAllOf

# TODO update the JSON string below
json = "{}"
# create an instance of PaymentInitiationRecipientGetResponseAllOf from a JSON string
payment_initiation_recipient_get_response_all_of_instance = PaymentInitiationRecipientGetResponseAllOf.from_json(json)
# print the JSON string representation of the object
print PaymentInitiationRecipientGetResponseAllOf.to_json()

# convert the object into a dict
payment_initiation_recipient_get_response_all_of_dict = payment_initiation_recipient_get_response_all_of_instance.to_dict()
# create an instance of PaymentInitiationRecipientGetResponseAllOf from a dict
payment_initiation_recipient_get_response_all_of_form_dict = payment_initiation_recipient_get_response_all_of.from_dict(payment_initiation_recipient_get_response_all_of_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


