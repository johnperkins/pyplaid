# FDXFiAttribute

Financial Institution provider-specific attribute

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | Name of attribute | [optional] 
**value** | **str** | Value of attribute | [optional] 

## Example

```python
from pyplaid.models.fdxfi_attribute import FDXFiAttribute

# TODO update the JSON string below
json = "{}"
# create an instance of FDXFiAttribute from a JSON string
fdxfi_attribute_instance = FDXFiAttribute.from_json(json)
# print the JSON string representation of the object
print FDXFiAttribute.to_json()

# convert the object into a dict
fdxfi_attribute_dict = fdxfi_attribute_instance.to_dict()
# create an instance of FDXFiAttribute from a dict
fdxfi_attribute_form_dict = fdxfi_attribute.from_dict(fdxfi_attribute_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


