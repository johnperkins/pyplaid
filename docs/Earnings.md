# Earnings

An object representing both a breakdown of earnings on a paystub and the total earnings.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**subtotals** | [**List[EarningsTotal]**](EarningsTotal.md) |  | [optional] 
**totals** | [**List[EarningsTotal]**](EarningsTotal.md) |  | [optional] 
**breakdown** | [**List[EarningsBreakdown]**](EarningsBreakdown.md) |  | [optional] 
**total** | [**EarningsTotal**](EarningsTotal.md) |  | [optional] 

## Example

```python
from pyplaid.models.earnings import Earnings

# TODO update the JSON string below
json = "{}"
# create an instance of Earnings from a JSON string
earnings_instance = Earnings.from_json(json)
# print the JSON string representation of the object
print Earnings.to_json()

# convert the object into a dict
earnings_dict = earnings_instance.to_dict()
# create an instance of Earnings from a dict
earnings_form_dict = earnings.from_dict(earnings_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


