# WatchlistScreeningDocument

An official document, usually issued by a governing body or institution, with an associated identifier.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | [**WatchlistScreeningDocumentType**](WatchlistScreeningDocumentType.md) |  | 
**number** | **str** | The numeric or alphanumeric identifier associated with this document. | 

## Example

```python
from pyplaid.models.watchlist_screening_document import WatchlistScreeningDocument

# TODO update the JSON string below
json = "{}"
# create an instance of WatchlistScreeningDocument from a JSON string
watchlist_screening_document_instance = WatchlistScreeningDocument.from_json(json)
# print the JSON string representation of the object
print WatchlistScreeningDocument.to_json()

# convert the object into a dict
watchlist_screening_document_dict = watchlist_screening_document_instance.to_dict()
# create an instance of WatchlistScreeningDocument from a dict
watchlist_screening_document_form_dict = watchlist_screening_document.from_dict(watchlist_screening_document_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


