# Cause

An error object and associated `item_id` used to identify a specific Item and error when a batch operation operating on multiple Items has encountered an error in one of the Items.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**error_type** | [**PlaidErrorType**](PlaidErrorType.md) |  | 
**error_code** | **str** | The particular error code. Safe for programmatic use. | 
**error_message** | **str** | A developer-friendly representation of the error code. This may change over time and is not safe for programmatic use. | 
**display_message** | **str** | A user-friendly representation of the error code. &#x60;null&#x60; if the error is not related to user action.  This may change over time and is not safe for programmatic use. | 
**request_id** | **str** | A unique ID identifying the request, to be used for troubleshooting purposes. This field will be omitted in errors provided by webhooks. | [optional] 
**causes** | **List[object]** | In the Assets product, a request can pertain to more than one Item. If an error is returned for such a request, &#x60;causes&#x60; will return an array of errors containing a breakdown of these errors on the individual Item level, if any can be identified.  &#x60;causes&#x60; will only be provided for the &#x60;error_type&#x60; &#x60;ASSET_REPORT_ERROR&#x60;. &#x60;causes&#x60; will also not be populated inside an error nested within a &#x60;warning&#x60; object. | [optional] 
**status** | **float** | The HTTP status code associated with the error. This will only be returned in the response body when the error information is provided via a webhook. | [optional] 
**documentation_url** | **str** | The URL of a Plaid documentation page with more information about the error | [optional] 
**suggested_action** | **str** | Suggested steps for resolving the error | [optional] 
**item_id** | **str** | The &#x60;item_id&#x60; of the Item associated with this webhook, warning, or error | 

## Example

```python
from pyplaid.models.cause import Cause

# TODO update the JSON string below
json = "{}"
# create an instance of Cause from a JSON string
cause_instance = Cause.from_json(json)
# print the JSON string representation of the object
print Cause.to_json()

# convert the object into a dict
cause_dict = cause_instance.to_dict()
# create an instance of Cause from a dict
cause_form_dict = cause.from_dict(cause_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


