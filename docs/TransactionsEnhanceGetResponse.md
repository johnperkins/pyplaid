# TransactionsEnhanceGetResponse

TransactionsEnhanceGetResponse defines the response schema for `/transactions/enhance`.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enhanced_transactions** | [**List[ClientProvidedEnhancedTransaction]**](ClientProvidedEnhancedTransaction.md) | An array of enhanced transactions. | 

## Example

```python
from pyplaid.models.transactions_enhance_get_response import TransactionsEnhanceGetResponse

# TODO update the JSON string below
json = "{}"
# create an instance of TransactionsEnhanceGetResponse from a JSON string
transactions_enhance_get_response_instance = TransactionsEnhanceGetResponse.from_json(json)
# print the JSON string representation of the object
print TransactionsEnhanceGetResponse.to_json()

# convert the object into a dict
transactions_enhance_get_response_dict = transactions_enhance_get_response_instance.to_dict()
# create an instance of TransactionsEnhanceGetResponse from a dict
transactions_enhance_get_response_form_dict = transactions_enhance_get_response.from_dict(transactions_enhance_get_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


