# PaymentConsentPeriodicAmount

Defines consent payments limitations per period.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amount** | [**PaymentConsentPeriodicAmountAmount**](PaymentConsentPeriodicAmountAmount.md) |  | 
**interval** | [**PaymentConsentPeriodicInterval**](PaymentConsentPeriodicInterval.md) |  | 
**alignment** | [**PaymentConsentPeriodicAlignment**](PaymentConsentPeriodicAlignment.md) |  | 

## Example

```python
from pyplaid.models.payment_consent_periodic_amount import PaymentConsentPeriodicAmount

# TODO update the JSON string below
json = "{}"
# create an instance of PaymentConsentPeriodicAmount from a JSON string
payment_consent_periodic_amount_instance = PaymentConsentPeriodicAmount.from_json(json)
# print the JSON string representation of the object
print PaymentConsentPeriodicAmount.to_json()

# convert the object into a dict
payment_consent_periodic_amount_dict = payment_consent_periodic_amount_instance.to_dict()
# create an instance of PaymentConsentPeriodicAmount from a dict
payment_consent_periodic_amount_form_dict = payment_consent_periodic_amount.from_dict(payment_consent_periodic_amount_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


