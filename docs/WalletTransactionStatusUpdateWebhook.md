# WalletTransactionStatusUpdateWebhook

Fired when the status of a wallet transaction has changed.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**webhook_type** | **str** | &#x60;WALLET&#x60; | 
**webhook_code** | **str** | &#x60;WALLET_TRANSACTION_STATUS_UPDATE&#x60; | 
**transaction_id** | **str** | The &#x60;transaction_id&#x60; for the wallet transaction being updated | 
**new_status** | [**WalletTransactionStatus**](WalletTransactionStatus.md) |  | 
**old_status** | [**WalletTransactionStatus**](WalletTransactionStatus.md) |  | 
**timestamp** | **datetime** | The timestamp of the update, in [ISO 8601](https://wikipedia.org/wiki/ISO_8601) format, e.g. &#x60;\&quot;2017-09-14T14:42:19.350Z\&quot;&#x60; | 
**environment** | [**WebhookEnvironmentValues**](WebhookEnvironmentValues.md) |  | 

## Example

```python
from pyplaid.models.wallet_transaction_status_update_webhook import WalletTransactionStatusUpdateWebhook

# TODO update the JSON string below
json = "{}"
# create an instance of WalletTransactionStatusUpdateWebhook from a JSON string
wallet_transaction_status_update_webhook_instance = WalletTransactionStatusUpdateWebhook.from_json(json)
# print the JSON string representation of the object
print WalletTransactionStatusUpdateWebhook.to_json()

# convert the object into a dict
wallet_transaction_status_update_webhook_dict = wallet_transaction_status_update_webhook_instance.to_dict()
# create an instance of WalletTransactionStatusUpdateWebhook from a dict
wallet_transaction_status_update_webhook_form_dict = wallet_transaction_status_update_webhook.from_dict(wallet_transaction_status_update_webhook_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


