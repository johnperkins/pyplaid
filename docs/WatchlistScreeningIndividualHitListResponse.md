# WatchlistScreeningIndividualHitListResponse

Paginated list of individual watchlist screening hits

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**watchlist_screening_hits** | [**List[WatchlistScreeningHit]**](WatchlistScreeningHit.md) | List of individual watchlist screening hits | 
**next_cursor** | **str** | An identifier that determines which page of results you receive. | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.watchlist_screening_individual_hit_list_response import WatchlistScreeningIndividualHitListResponse

# TODO update the JSON string below
json = "{}"
# create an instance of WatchlistScreeningIndividualHitListResponse from a JSON string
watchlist_screening_individual_hit_list_response_instance = WatchlistScreeningIndividualHitListResponse.from_json(json)
# print the JSON string representation of the object
print WatchlistScreeningIndividualHitListResponse.to_json()

# convert the object into a dict
watchlist_screening_individual_hit_list_response_dict = watchlist_screening_individual_hit_list_response_instance.to_dict()
# create an instance of WatchlistScreeningIndividualHitListResponse from a dict
watchlist_screening_individual_hit_list_response_form_dict = watchlist_screening_individual_hit_list_response.from_dict(watchlist_screening_individual_hit_list_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


