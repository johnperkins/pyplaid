# CreditPayStubAddress

Address on the pay stub.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**city** | **str** | The full city name. | 
**country** | **str** | The ISO 3166-1 alpha-2 country code. | 
**postal_code** | **str** | The postal code of the address. | 
**region** | **str** | The region or state. Example: &#x60;\&quot;NC\&quot;&#x60; | 
**street** | **str** | The full street address. | 

## Example

```python
from pyplaid.models.credit_pay_stub_address import CreditPayStubAddress

# TODO update the JSON string below
json = "{}"
# create an instance of CreditPayStubAddress from a JSON string
credit_pay_stub_address_instance = CreditPayStubAddress.from_json(json)
# print the JSON string representation of the object
print CreditPayStubAddress.to_json()

# convert the object into a dict
credit_pay_stub_address_dict = credit_pay_stub_address_instance.to_dict()
# create an instance of CreditPayStubAddress from a dict
credit_pay_stub_address_form_dict = credit_pay_stub_address.from_dict(credit_pay_stub_address_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


