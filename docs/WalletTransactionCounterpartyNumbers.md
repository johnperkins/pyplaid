# WalletTransactionCounterpartyNumbers

The counterparty's bank account numbers. Exactly one of IBAN or BACS data is required.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bacs** | [**WalletTransactionCounterpartyBACS**](WalletTransactionCounterpartyBACS.md) |  | [optional] 
**international** | [**WalletTransactionCounterpartyInternational**](WalletTransactionCounterpartyInternational.md) |  | [optional] 

## Example

```python
from pyplaid.models.wallet_transaction_counterparty_numbers import WalletTransactionCounterpartyNumbers

# TODO update the JSON string below
json = "{}"
# create an instance of WalletTransactionCounterpartyNumbers from a JSON string
wallet_transaction_counterparty_numbers_instance = WalletTransactionCounterpartyNumbers.from_json(json)
# print the JSON string representation of the object
print WalletTransactionCounterpartyNumbers.to_json()

# convert the object into a dict
wallet_transaction_counterparty_numbers_dict = wallet_transaction_counterparty_numbers_instance.to_dict()
# create an instance of WalletTransactionCounterpartyNumbers from a dict
wallet_transaction_counterparty_numbers_form_dict = wallet_transaction_counterparty_numbers.from_dict(wallet_transaction_counterparty_numbers_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


