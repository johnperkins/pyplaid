# CreditRelayRefreshResponse

CreditRelayRefreshResponse defines the response schema for `/credit/relay/refresh`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**relay_token** | **str** |  | 
**asset_report_id** | **str** | A unique ID identifying an Asset Report. Like all Plaid identifiers, this ID is case sensitive. | [optional] 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.credit_relay_refresh_response import CreditRelayRefreshResponse

# TODO update the JSON string below
json = "{}"
# create an instance of CreditRelayRefreshResponse from a JSON string
credit_relay_refresh_response_instance = CreditRelayRefreshResponse.from_json(json)
# print the JSON string representation of the object
print CreditRelayRefreshResponse.to_json()

# convert the object into a dict
credit_relay_refresh_response_dict = credit_relay_refresh_response_instance.to_dict()
# create an instance of CreditRelayRefreshResponse from a dict
credit_relay_refresh_response_form_dict = credit_relay_refresh_response.from_dict(credit_relay_refresh_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


