# EntityScreeningHitAnalysis

Analysis information describing why a screening hit matched the provided entity information

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**documents** | [**MatchSummaryCode**](MatchSummaryCode.md) |  | [optional] 
**email_addresses** | [**MatchSummaryCode**](MatchSummaryCode.md) |  | [optional] 
**locations** | [**MatchSummaryCode**](MatchSummaryCode.md) |  | [optional] 
**names** | [**MatchSummaryCode**](MatchSummaryCode.md) |  | [optional] 
**phone_numbers** | [**MatchSummaryCode**](MatchSummaryCode.md) |  | [optional] 
**urls** | [**MatchSummaryCode**](MatchSummaryCode.md) |  | [optional] 
**search_terms_version** | **float** | The version of the entity screening&#39;s &#x60;search_terms&#x60; that were compared when the entity screening hit was added. entity screening hits are immutable once they have been reviewed. If changes are detected due to updates to the entity screening&#39;s &#x60;search_terms&#x60;, the associated entity program, or the list&#39;s source data prior to review, the entity screening hit will be updated to reflect those changes. | 

## Example

```python
from pyplaid.models.entity_screening_hit_analysis import EntityScreeningHitAnalysis

# TODO update the JSON string below
json = "{}"
# create an instance of EntityScreeningHitAnalysis from a JSON string
entity_screening_hit_analysis_instance = EntityScreeningHitAnalysis.from_json(json)
# print the JSON string representation of the object
print EntityScreeningHitAnalysis.to_json()

# convert the object into a dict
entity_screening_hit_analysis_dict = entity_screening_hit_analysis_instance.to_dict()
# create an instance of EntityScreeningHitAnalysis from a dict
entity_screening_hit_analysis_form_dict = entity_screening_hit_analysis.from_dict(entity_screening_hit_analysis_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


