# PaystubEmployer

Information about the employer on the paystub

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address** | [**PaystubAddress**](PaystubAddress.md) |  | [optional] 
**name** | **str** | The name of the employer on the paystub. | 

## Example

```python
from pyplaid.models.paystub_employer import PaystubEmployer

# TODO update the JSON string below
json = "{}"
# create an instance of PaystubEmployer from a JSON string
paystub_employer_instance = PaystubEmployer.from_json(json)
# print the JSON string representation of the object
print PaystubEmployer.to_json()

# convert the object into a dict
paystub_employer_dict = paystub_employer_instance.to_dict()
# create an instance of PaystubEmployer from a dict
paystub_employer_form_dict = paystub_employer.from_dict(paystub_employer_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


