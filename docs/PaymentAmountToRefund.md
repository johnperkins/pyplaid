# PaymentAmountToRefund

An amount to refund the payment partially. If this amount is not specified, the payment is refunded fully for the remaining amount.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**currency** | [**PaymentAmountCurrency**](PaymentAmountCurrency.md) |  | 
**value** | **float** | The amount of the payment. Must contain at most two digits of precision e.g. &#x60;1.23&#x60;. Minimum accepted value is &#x60;1&#x60;. | 

## Example

```python
from pyplaid.models.payment_amount_to_refund import PaymentAmountToRefund

# TODO update the JSON string below
json = "{}"
# create an instance of PaymentAmountToRefund from a JSON string
payment_amount_to_refund_instance = PaymentAmountToRefund.from_json(json)
# print the JSON string representation of the object
print PaymentAmountToRefund.to_json()

# convert the object into a dict
payment_amount_to_refund_dict = payment_amount_to_refund_instance.to_dict()
# create an instance of PaymentAmountToRefund from a dict
payment_amount_to_refund_form_dict = payment_amount_to_refund.from_dict(payment_amount_to_refund_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


