# PartnerCustomerCreateRequest

Request schema for `/partner/customer/create`.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**company_name** | **str** | The company name of the end customer being created. | 
**is_diligence_attested** | **bool** | Denotes whether or not the partner has completed attestation of diligence for the end customer to be created. | 
**products** | [**List[Products]**](Products.md) | The products to be enabled for the end customer. | 
**create_link_customization** | **bool** | If true, the end customer&#39;s default Link customization will be set to match the partner&#39;s. | [optional] 
**logo** | **str** | Base64-encoded representation of the end customer&#39;s logo. Must be a PNG of size 1024x1024 under 4MB. The logo will be shared with financial institutions and shown to the end user during Link flows. A logo is required if &#x60;create_link_customization&#x60; is &#x60;true&#x60;. If &#x60;create_link_customization&#x60; is &#x60;false&#x60; and the logo is omitted, a stock logo will be used. | [optional] 
**legal_entity_name** | **str** | The end customer&#39;s legal name. | 
**website** | **str** | The end customer&#39;s website. | 
**application_name** | **str** | The name of the end customer&#39;s application. | 
**technical_contact** | [**PartnerEndCustomerTechnicalContact**](PartnerEndCustomerTechnicalContact.md) |  | [optional] 
**billing_contact** | [**PartnerEndCustomerBillingContact**](PartnerEndCustomerBillingContact.md) |  | [optional] 
**address** | [**PartnerEndCustomerAddress**](PartnerEndCustomerAddress.md) |  | 
**is_bank_addendum_completed** | **bool** | Denotes whether the partner has forwarded the Plaid bank addendum to the end customer. | 
**assets_under_management** | [**PartnerEndCustomerAssetsUnderManagement**](PartnerEndCustomerAssetsUnderManagement.md) |  | [optional] 

## Example

```python
from pyplaid.models.partner_customer_create_request import PartnerCustomerCreateRequest

# TODO update the JSON string below
json = "{}"
# create an instance of PartnerCustomerCreateRequest from a JSON string
partner_customer_create_request_instance = PartnerCustomerCreateRequest.from_json(json)
# print the JSON string representation of the object
print PartnerCustomerCreateRequest.to_json()

# convert the object into a dict
partner_customer_create_request_dict = partner_customer_create_request_instance.to_dict()
# create an instance of PartnerCustomerCreateRequest from a dict
partner_customer_create_request_form_dict = partner_customer_create_request.from_dict(partner_customer_create_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


