# AddressNullable

A physical mailing address.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**AddressData**](AddressData.md) |  | 
**primary** | **bool** | When &#x60;true&#x60;, identifies the address as the primary address on an account. | [optional] 

## Example

```python
from pyplaid.models.address_nullable import AddressNullable

# TODO update the JSON string below
json = "{}"
# create an instance of AddressNullable from a JSON string
address_nullable_instance = AddressNullable.from_json(json)
# print the JSON string representation of the object
print AddressNullable.to_json()

# convert the object into a dict
address_nullable_dict = address_nullable_instance.to_dict()
# create an instance of AddressNullable from a dict
address_nullable_form_dict = address_nullable.from_dict(address_nullable_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


