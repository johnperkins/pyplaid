# ProcessorIdentityGetResponse

ProcessorIdentityGetResponse defines the response schema for `/processor/identity/get`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account** | [**AccountIdentity**](AccountIdentity.md) |  | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.processor_identity_get_response import ProcessorIdentityGetResponse

# TODO update the JSON string below
json = "{}"
# create an instance of ProcessorIdentityGetResponse from a JSON string
processor_identity_get_response_instance = ProcessorIdentityGetResponse.from_json(json)
# print the JSON string representation of the object
print ProcessorIdentityGetResponse.to_json()

# convert the object into a dict
processor_identity_get_response_dict = processor_identity_get_response_instance.to_dict()
# create an instance of ProcessorIdentityGetResponse from a dict
processor_identity_get_response_form_dict = processor_identity_get_response.from_dict(processor_identity_get_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


