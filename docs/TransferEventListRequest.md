# TransferEventListRequest

Defines the request schema for `/transfer/event/list`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**start_date** | **datetime** | The start datetime of transfers to list. This should be in RFC 3339 format (i.e. &#x60;2019-12-06T22:35:49Z&#x60;) | [optional] 
**end_date** | **datetime** | The end datetime of transfers to list. This should be in RFC 3339 format (i.e. &#x60;2019-12-06T22:35:49Z&#x60;) | [optional] 
**transfer_id** | **str** | Plaid’s unique identifier for a transfer. | [optional] 
**account_id** | **str** | The account ID to get events for all transactions to/from an account. | [optional] 
**transfer_type** | [**TransferEventListTransferType**](TransferEventListTransferType.md) |  | [optional] 
**event_types** | [**List[TransferEventType]**](TransferEventType.md) | Filter events by event type. | [optional] 
**sweep_id** | **str** | Plaid’s unique identifier for a sweep. | [optional] 
**count** | **int** | The maximum number of transfer events to return. If the number of events matching the above parameters is greater than &#x60;count&#x60;, the most recent events will be returned. | [optional] [default to 25]
**offset** | **int** | The offset into the list of transfer events. When &#x60;count&#x60;&#x3D;25 and &#x60;offset&#x60;&#x3D;0, the first 25 events will be returned. When &#x60;count&#x60;&#x3D;25 and &#x60;offset&#x60;&#x3D;25, the next 25 events will be returned. | [optional] [default to 0]
**origination_account_id** | **str** | The origination account ID to get events for transfers from a specific origination account. | [optional] 
**originator_client_id** | **str** | Filter transfer events to only those with the specified originator client. | [optional] 

## Example

```python
from pyplaid.models.transfer_event_list_request import TransferEventListRequest

# TODO update the JSON string below
json = "{}"
# create an instance of TransferEventListRequest from a JSON string
transfer_event_list_request_instance = TransferEventListRequest.from_json(json)
# print the JSON string representation of the object
print TransferEventListRequest.to_json()

# convert the object into a dict
transfer_event_list_request_dict = transfer_event_list_request_instance.to_dict()
# create an instance of TransferEventListRequest from a dict
transfer_event_list_request_form_dict = transfer_event_list_request.from_dict(transfer_event_list_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


