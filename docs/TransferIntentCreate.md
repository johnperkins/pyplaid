# TransferIntentCreate

Represents a transfer intent within Transfer UI.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Plaid&#39;s unique identifier for the transfer intent object. | 
**created** | **datetime** | The datetime the transfer was created. This will be of the form &#x60;2006-01-02T15:04:05Z&#x60;. | 
**status** | [**TransferIntentStatus**](TransferIntentStatus.md) |  | 
**account_id** | **str** | The Plaid &#x60;account_id&#x60; for the account that will be debited or credited. Returned only if &#x60;account_id&#x60; was set on intent creation. | [optional] 
**origination_account_id** | **str** | Plaid’s unique identifier for the origination account for the intent. If not provided, the default account will be used. | 
**amount** | **str** | The amount of the transfer (decimal string with two digits of precision e.g. \&quot;10.00\&quot;). | 
**mode** | [**TransferIntentCreateMode**](TransferIntentCreateMode.md) |  | 
**ach_class** | [**ACHClass**](ACHClass.md) |  | [optional] 
**user** | [**TransferUserInResponse**](TransferUserInResponse.md) |  | 
**description** | **str** | A description for the underlying transfer. Maximum of 8 characters. | 
**metadata** | **Dict[str, str]** | The Metadata object is a mapping of client-provided string fields to any string value. The following limitations apply: - The JSON values must be Strings (no nested JSON objects allowed) - Only ASCII characters may be used - Maximum of 50 key/value pairs - Maximum key length of 40 characters - Maximum value length of 500 characters  | [optional] 
**iso_currency_code** | **str** | The currency of the transfer amount, e.g. \&quot;USD\&quot; | 
**require_guarantee** | **bool** | When &#x60;true&#x60;, the transfer requires a &#x60;GUARANTEED&#x60; decision by Plaid to proceed (Guarantee customers only). | [optional] 

## Example

```python
from pyplaid.models.transfer_intent_create import TransferIntentCreate

# TODO update the JSON string below
json = "{}"
# create an instance of TransferIntentCreate from a JSON string
transfer_intent_create_instance = TransferIntentCreate.from_json(json)
# print the JSON string representation of the object
print TransferIntentCreate.to_json()

# convert the object into a dict
transfer_intent_create_dict = transfer_intent_create_instance.to_dict()
# create an instance of TransferIntentCreate from a dict
transfer_intent_create_form_dict = transfer_intent_create.from_dict(transfer_intent_create_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


