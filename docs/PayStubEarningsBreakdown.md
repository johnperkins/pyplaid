# PayStubEarningsBreakdown

An object representing the earnings line items for the pay period.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**canonical_description** | **str** | Commonly used term to describe the earning line item. | 
**current_amount** | **float** | Raw amount of the earning line item. | 
**description** | **str** | Description of the earning line item. | 
**hours** | **float** | Number of hours applicable for this earning. | 
**iso_currency_code** | **str** | The ISO-4217 currency code of the line item. Always &#x60;null&#x60; if &#x60;unofficial_currency_code&#x60; is non-null. | 
**rate** | **float** | Hourly rate applicable for this earning. | 
**unofficial_currency_code** | **str** | The unofficial currency code associated with the line item. Always &#x60;null&#x60; if &#x60;iso_currency_code&#x60; is non-&#x60;null&#x60;. Unofficial currency codes are used for currencies that do not have official ISO currency codes, such as cryptocurrencies and the currencies of certain countries.  See the [currency code schema](https://plaid.com/docs/api/accounts#currency-code-schema) for a full listing of supported &#x60;iso_currency_code&#x60;s. | 
**ytd_amount** | **float** | The year-to-date amount of the deduction. | 

## Example

```python
from pyplaid.models.pay_stub_earnings_breakdown import PayStubEarningsBreakdown

# TODO update the JSON string below
json = "{}"
# create an instance of PayStubEarningsBreakdown from a JSON string
pay_stub_earnings_breakdown_instance = PayStubEarningsBreakdown.from_json(json)
# print the JSON string representation of the object
print PayStubEarningsBreakdown.to_json()

# convert the object into a dict
pay_stub_earnings_breakdown_dict = pay_stub_earnings_breakdown_instance.to_dict()
# create an instance of PayStubEarningsBreakdown from a dict
pay_stub_earnings_breakdown_form_dict = pay_stub_earnings_breakdown.from_dict(pay_stub_earnings_breakdown_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


