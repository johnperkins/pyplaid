# IncomeSummaryFieldNumber

Field number for income summary

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value** | **float** | The value of the field. | 
**verification_status** | [**VerificationStatus**](VerificationStatus.md) |  | 

## Example

```python
from pyplaid.models.income_summary_field_number import IncomeSummaryFieldNumber

# TODO update the JSON string below
json = "{}"
# create an instance of IncomeSummaryFieldNumber from a JSON string
income_summary_field_number_instance = IncomeSummaryFieldNumber.from_json(json)
# print the JSON string representation of the object
print IncomeSummaryFieldNumber.to_json()

# convert the object into a dict
income_summary_field_number_dict = income_summary_field_number_instance.to_dict()
# create an instance of IncomeSummaryFieldNumber from a dict
income_summary_field_number_form_dict = income_summary_field_number.from_dict(income_summary_field_number_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


