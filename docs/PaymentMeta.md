# PaymentMeta

Transaction information specific to inter-bank transfers. If the transaction was not an inter-bank transfer, all fields will be `null`.  If the `transactions` object was returned by a Transactions endpoint such as `/transactions/get`, the `payment_meta` key will always appear, but no data elements are guaranteed. If the `transactions` object was returned by an Assets endpoint such as `/asset_report/get/` or `/asset_report/pdf/get`, this field will only appear in an Asset Report with Insights.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reference_number** | **str** | The transaction reference number supplied by the financial institution. | 
**ppd_id** | **str** | The ACH PPD ID for the payer. | 
**payee** | **str** | For transfers, the party that is receiving the transaction. | 
**by_order_of** | **str** | The party initiating a wire transfer. Will be &#x60;null&#x60; if the transaction is not a wire transfer. | 
**payer** | **str** | For transfers, the party that is paying the transaction. | 
**payment_method** | **str** | The type of transfer, e.g. &#39;ACH&#39; | 
**payment_processor** | **str** | The name of the payment processor | 
**reason** | **str** | The payer-supplied description of the transfer. | 

## Example

```python
from pyplaid.models.payment_meta import PaymentMeta

# TODO update the JSON string below
json = "{}"
# create an instance of PaymentMeta from a JSON string
payment_meta_instance = PaymentMeta.from_json(json)
# print the JSON string representation of the object
print PaymentMeta.to_json()

# convert the object into a dict
payment_meta_dict = payment_meta_instance.to_dict()
# create an instance of PaymentMeta from a dict
payment_meta_form_dict = payment_meta.from_dict(payment_meta_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


