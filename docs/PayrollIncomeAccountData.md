# PayrollIncomeAccountData

An object containing account level data.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_id** | **str** | ID of the payroll provider account. | 
**rate_of_pay** | [**PayrollIncomeRateOfPay**](PayrollIncomeRateOfPay.md) |  | 
**pay_frequency** | **str** | The frequency at which an individual is paid. | 

## Example

```python
from pyplaid.models.payroll_income_account_data import PayrollIncomeAccountData

# TODO update the JSON string below
json = "{}"
# create an instance of PayrollIncomeAccountData from a JSON string
payroll_income_account_data_instance = PayrollIncomeAccountData.from_json(json)
# print the JSON string representation of the object
print PayrollIncomeAccountData.to_json()

# convert the object into a dict
payroll_income_account_data_dict = payroll_income_account_data_instance.to_dict()
# create an instance of PayrollIncomeAccountData from a dict
payroll_income_account_data_form_dict = payroll_income_account_data.from_dict(payroll_income_account_data_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


