# ItemStatusLastWebhook

Information about the last webhook fired for the Item.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sent_at** | **datetime** | [ISO 8601](https://wikipedia.org/wiki/ISO_8601) timestamp of when the webhook was fired.  | [optional] 
**code_sent** | **str** | The last webhook code sent. | [optional] 

## Example

```python
from pyplaid.models.item_status_last_webhook import ItemStatusLastWebhook

# TODO update the JSON string below
json = "{}"
# create an instance of ItemStatusLastWebhook from a JSON string
item_status_last_webhook_instance = ItemStatusLastWebhook.from_json(json)
# print the JSON string representation of the object
print ItemStatusLastWebhook.to_json()

# convert the object into a dict
item_status_last_webhook_dict = item_status_last_webhook_instance.to_dict()
# create an instance of ItemStatusLastWebhook from a dict
item_status_last_webhook_form_dict = item_status_last_webhook.from_dict(item_status_last_webhook_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


