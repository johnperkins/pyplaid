# Enhancements

A grouping of the Plaid produced transaction enhancement fields.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**merchant_name** | **str** | The name of the primary counterparty, such as the merchant or the financial institution, as extracted by Plaid from the raw description. | [optional] 
**website** | **str** | The website associated with this transaction. | [optional] 
**logo_url** | **str** | A link to the logo associated with this transaction. The logo will always be 100x100 resolution. | [optional] 
**check_number** | **str** | The check number of the transaction. This field is only populated for check transactions. | [optional] 
**payment_channel** | [**PaymentChannel**](PaymentChannel.md) |  | 
**category_id** | **str** | The ID of the category to which this transaction belongs. For a full list of categories, see [&#x60;/categories/get&#x60;](https://plaid.com/docs/api/products/transactions/#categoriesget). | 
**category** | **List[str]** | A hierarchical array of the categories to which this transaction belongs. For a full list of categories, see [&#x60;/categories/get&#x60;](https://plaid.com/docs/api/products/transactions/#categoriesget). | 
**location** | [**Location**](Location.md) |  | 
**personal_finance_category** | [**PersonalFinanceCategory**](PersonalFinanceCategory.md) |  | [optional] 
**personal_finance_category_icon_url** | **str** | A link to the icon associated with the primary personal finance category. The logo will always be 100x100 resolution. | [optional] 
**counterparties** | [**List[Counterparty]**](Counterparty.md) | The counterparties present in the transaction. Counterparties, such as the merchant or the financial institution, are extracted by Plaid from the raw description. | [optional] 

## Example

```python
from pyplaid.models.enhancements import Enhancements

# TODO update the JSON string below
json = "{}"
# create an instance of Enhancements from a JSON string
enhancements_instance = Enhancements.from_json(json)
# print the JSON string representation of the object
print Enhancements.to_json()

# convert the object into a dict
enhancements_dict = enhancements_instance.to_dict()
# create an instance of Enhancements from a dict
enhancements_form_dict = enhancements.from_dict(enhancements_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


