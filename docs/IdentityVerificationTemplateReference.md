# IdentityVerificationTemplateReference

The resource ID and version number of the template configuring the behavior of a given identity verification.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | ID of the associated Identity Verification template. | 
**version** | **float** | Version of the associated Identity Verification template. | 

## Example

```python
from pyplaid.models.identity_verification_template_reference import IdentityVerificationTemplateReference

# TODO update the JSON string below
json = "{}"
# create an instance of IdentityVerificationTemplateReference from a JSON string
identity_verification_template_reference_instance = IdentityVerificationTemplateReference.from_json(json)
# print the JSON string representation of the object
print IdentityVerificationTemplateReference.to_json()

# convert the object into a dict
identity_verification_template_reference_dict = identity_verification_template_reference_instance.to_dict()
# create an instance of IdentityVerificationTemplateReference from a dict
identity_verification_template_reference_form_dict = identity_verification_template_reference.from_dict(identity_verification_template_reference_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


