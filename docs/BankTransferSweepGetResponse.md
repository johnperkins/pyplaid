# BankTransferSweepGetResponse

BankTransferSweepGetResponse defines the response schema for `/bank_transfer/sweep/get`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sweep** | [**BankTransferSweep**](BankTransferSweep.md) |  | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.bank_transfer_sweep_get_response import BankTransferSweepGetResponse

# TODO update the JSON string below
json = "{}"
# create an instance of BankTransferSweepGetResponse from a JSON string
bank_transfer_sweep_get_response_instance = BankTransferSweepGetResponse.from_json(json)
# print the JSON string representation of the object
print BankTransferSweepGetResponse.to_json()

# convert the object into a dict
bank_transfer_sweep_get_response_dict = bank_transfer_sweep_get_response_instance.to_dict()
# create an instance of BankTransferSweepGetResponse from a dict
bank_transfer_sweep_get_response_form_dict = bank_transfer_sweep_get_response.from_dict(bank_transfer_sweep_get_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


