# DocumentNameMatchCode

A match summary describing the cross comparison between the subject's name, extracted from the document image, and the name they separately provided to identity verification attempt.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


