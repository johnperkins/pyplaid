# HistoricalUpdateWebhook

Fired when an Item's historical transaction pull is completed and Plaid has prepared as much historical transaction data as possible for the Item. Once this webhook has been fired, transaction data beyond the most recent 30 days can be fetched for the Item. If [Account Select v2](https://plaid.com/docs/link/customization/#account-select) is enabled, this webhook will also be fired if account selections for the Item are updated, with `new_transactions` set to the number of net new transactions pulled after the account selection update.  This webhook is intended for use with `/transactions/get`; if you are using the newer `/transactions/sync` endpoint, this webhook will still be fired to maintain backwards compatibility, but it is recommended to listen for and respond to the `SYNC_UPDATES_AVAILABLE` webhook instead.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**webhook_type** | **str** | &#x60;TRANSACTIONS&#x60; | 
**webhook_code** | **str** | &#x60;HISTORICAL_UPDATE&#x60; | 
**error** | [**PlaidError**](PlaidError.md) |  | [optional] 
**new_transactions** | **float** | The number of new, unfetched transactions available | 
**item_id** | **str** | The &#x60;item_id&#x60; of the Item associated with this webhook, warning, or error | 
**environment** | [**WebhookEnvironmentValues**](WebhookEnvironmentValues.md) |  | 

## Example

```python
from pyplaid.models.historical_update_webhook import HistoricalUpdateWebhook

# TODO update the JSON string below
json = "{}"
# create an instance of HistoricalUpdateWebhook from a JSON string
historical_update_webhook_instance = HistoricalUpdateWebhook.from_json(json)
# print the JSON string representation of the object
print HistoricalUpdateWebhook.to_json()

# convert the object into a dict
historical_update_webhook_dict = historical_update_webhook_instance.to_dict()
# create an instance of HistoricalUpdateWebhook from a dict
historical_update_webhook_form_dict = historical_update_webhook.from_dict(historical_update_webhook_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


