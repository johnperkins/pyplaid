# UserPermissionRevokedWebhook

The `USER_PERMISSION_REVOKED` webhook is fired when an end user has used either the [my.plaid.com portal](https://my.plaid.com) or the financial institution’s consent portal to revoke the permission that they previously granted to access an Item. Once access to an Item has been revoked, it cannot be restored. If the user subsequently returns to your application, a new Item must be created for the user.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**webhook_type** | **str** | &#x60;ITEM&#x60; | 
**webhook_code** | **str** | &#x60;USER_PERMISSION_REVOKED&#x60; | 
**item_id** | **str** | The &#x60;item_id&#x60; of the Item associated with this webhook, warning, or error | 
**error** | [**PlaidError**](PlaidError.md) |  | [optional] 
**environment** | [**WebhookEnvironmentValues**](WebhookEnvironmentValues.md) |  | 

## Example

```python
from pyplaid.models.user_permission_revoked_webhook import UserPermissionRevokedWebhook

# TODO update the JSON string below
json = "{}"
# create an instance of UserPermissionRevokedWebhook from a JSON string
user_permission_revoked_webhook_instance = UserPermissionRevokedWebhook.from_json(json)
# print the JSON string representation of the object
print UserPermissionRevokedWebhook.to_json()

# convert the object into a dict
user_permission_revoked_webhook_dict = user_permission_revoked_webhook_instance.to_dict()
# create an instance of UserPermissionRevokedWebhook from a dict
user_permission_revoked_webhook_form_dict = user_permission_revoked_webhook.from_dict(user_permission_revoked_webhook_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


