# AssetReport

An object representing an Asset Report

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**asset_report_id** | **str** | A unique ID identifying an Asset Report. Like all Plaid identifiers, this ID is case sensitive. | 
**client_report_id** | **str** | An identifier you determine and submit for the Asset Report. | 
**date_generated** | **datetime** | The date and time when the Asset Report was created, in [ISO 8601](https://wikipedia.org/wiki/ISO_8601) format (e.g. \&quot;2018-04-12T03:32:11Z\&quot;). | 
**days_requested** | **float** | The duration of transaction history you requested | 
**user** | [**AssetReportUser**](AssetReportUser.md) |  | 
**items** | [**List[AssetReportItem]**](AssetReportItem.md) | Data returned by Plaid about each of the Items included in the Asset Report. | 

## Example

```python
from pyplaid.models.asset_report import AssetReport

# TODO update the JSON string below
json = "{}"
# create an instance of AssetReport from a JSON string
asset_report_instance = AssetReport.from_json(json)
# print the JSON string representation of the object
print AssetReport.to_json()

# convert the object into a dict
asset_report_dict = asset_report_instance.to_dict()
# create an instance of AssetReport from a dict
asset_report_form_dict = asset_report.from_dict(asset_report_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


