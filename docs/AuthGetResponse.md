# AuthGetResponse

AuthGetResponse defines the response schema for `/auth/get`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accounts** | [**List[AccountBase]**](AccountBase.md) | The &#x60;accounts&#x60; for which numbers are being retrieved. | 
**numbers** | [**AuthGetNumbers**](AuthGetNumbers.md) |  | 
**item** | [**Item**](Item.md) |  | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.auth_get_response import AuthGetResponse

# TODO update the JSON string below
json = "{}"
# create an instance of AuthGetResponse from a JSON string
auth_get_response_instance = AuthGetResponse.from_json(json)
# print the JSON string representation of the object
print AuthGetResponse.to_json()

# convert the object into a dict
auth_get_response_dict = auth_get_response_instance.to_dict()
# create an instance of AuthGetResponse from a dict
auth_get_response_form_dict = auth_get_response.from_dict(auth_get_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


