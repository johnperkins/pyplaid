# Credit1099Filer

An object representing a filer used by 1099-K tax documents.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address** | [**CreditPayStubAddress**](CreditPayStubAddress.md) |  | [optional] 
**name** | **str** | Name of filer. | [optional] 
**tin** | **str** | Tax identification number of filer. | [optional] 
**type** | **str** | One of the following values will be provided: Payment Settlement Entity (PSE), Electronic Payment Facilitator (EPF), Other Third Party | [optional] 

## Example

```python
from pyplaid.models.credit1099_filer import Credit1099Filer

# TODO update the JSON string below
json = "{}"
# create an instance of Credit1099Filer from a JSON string
credit1099_filer_instance = Credit1099Filer.from_json(json)
# print the JSON string representation of the object
print Credit1099Filer.to_json()

# convert the object into a dict
credit1099_filer_dict = credit1099_filer_instance.to_dict()
# create an instance of Credit1099Filer from a dict
credit1099_filer_form_dict = credit1099_filer.from_dict(credit1099_filer_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


