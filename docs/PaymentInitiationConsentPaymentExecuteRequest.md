# PaymentInitiationConsentPaymentExecuteRequest

PaymentInitiationConsentPaymentExecuteRequest defines the request schema for `/payment_initiation/consent/payment/execute`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**consent_id** | **str** | The consent ID. | 
**amount** | [**PaymentAmount**](PaymentAmount.md) |  | 
**idempotency_key** | **str** | A random key provided by the client, per unique consent payment. Maximum of 128 characters.  The API supports idempotency for safely retrying requests without accidentally performing the same operation twice. If a request to execute a consent payment fails due to a network connection error, you can retry the request with the same idempotency key to guarantee that only a single payment is created. If the request was successfully processed, it will prevent any payment that uses the same idempotency key, and was received within 24 hours of the first request, from being processed. | 

## Example

```python
from pyplaid.models.payment_initiation_consent_payment_execute_request import PaymentInitiationConsentPaymentExecuteRequest

# TODO update the JSON string below
json = "{}"
# create an instance of PaymentInitiationConsentPaymentExecuteRequest from a JSON string
payment_initiation_consent_payment_execute_request_instance = PaymentInitiationConsentPaymentExecuteRequest.from_json(json)
# print the JSON string representation of the object
print PaymentInitiationConsentPaymentExecuteRequest.to_json()

# convert the object into a dict
payment_initiation_consent_payment_execute_request_dict = payment_initiation_consent_payment_execute_request_instance.to_dict()
# create an instance of PaymentInitiationConsentPaymentExecuteRequest from a dict
payment_initiation_consent_payment_execute_request_form_dict = payment_initiation_consent_payment_execute_request.from_dict(payment_initiation_consent_payment_execute_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


