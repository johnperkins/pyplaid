# TransferAuthorizationCreateRequest

Defines the request schema for `/transfer/authorization/create`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**access_token** | **str** | The Plaid &#x60;access_token&#x60; for the account that will be debited or credited. Required if not using &#x60;payment_profile_token&#x60;. | [optional] 
**account_id** | **str** | The Plaid &#x60;account_id&#x60; for the account that will be debited or credited. Required if not using &#x60;payment_profile_token&#x60;. | [optional] 
**payment_profile_token** | **str** | The payment profile token associated with the Payment Profile that will be debited or credited. Required if not using &#x60;access_token&#x60;. | [optional] 
**type** | [**TransferType**](TransferType.md) |  | 
**network** | [**TransferNetwork**](TransferNetwork.md) |  | 
**amount** | **str** | The amount of the transfer (decimal string with two digits of precision e.g. \&quot;10.00\&quot;). | 
**ach_class** | [**ACHClass**](ACHClass.md) |  | [optional] 
**user** | [**TransferAuthorizationUserInRequest**](TransferAuthorizationUserInRequest.md) |  | 
**device** | [**TransferAuthorizationDevice**](TransferAuthorizationDevice.md) |  | [optional] 
**origination_account_id** | **str** | Plaid&#39;s unique identifier for the origination account for this authorization. If not specified, the default account will be used. | [optional] 
**iso_currency_code** | **str** | The currency of the transfer amount. The default value is \&quot;USD\&quot;. | [optional] 
**idempotency_key** | **str** | A random key provided by the client, per unique authorization. Maximum of 50 characters.  The API supports idempotency for safely retrying requests without accidentally performing the same operation twice. For example, if a request to create an authorization fails due to a network connection error, you can retry the request with the same idempotency key to guarantee that only a single authorization is created.  Failure to provide this key may result in duplicate charges.  Required for guaranteed ACH customers. | [optional] 
**user_present** | **bool** | Required for Guarantee. If the end user is initiating the specific transfer themselves via an interactive UI, this should be &#x60;true&#x60;; for automatic recurring payments where the end user is not actually initiating each individual transfer, it should be &#x60;false&#x60;. | [optional] 
**with_guarantee** | **bool** | If set to &#x60;false&#x60;, Plaid will not offer a &#x60;guarantee_decision&#x60; for this request(Guarantee customers only). | [optional] [default to True]
**beacon_session_id** | **str** | The unique identifier returned by Plaid&#39;s [beacon](https://plaid.com/docs/transfer/guarantee/#using-a-beacon) when it is run on your webpage. Required for Guarantee customers who are not using [Transfer UI](https://plaid.com/docs/transfer/using-transfer-ui/) and have a web checkout experience. | [optional] 
**originator_client_id** | **str** | The Plaid client ID that is the originator of this transfer. Only needed if creating transfers on behalf of another client as a third-party sender (TPS). | [optional] 

## Example

```python
from pyplaid.models.transfer_authorization_create_request import TransferAuthorizationCreateRequest

# TODO update the JSON string below
json = "{}"
# create an instance of TransferAuthorizationCreateRequest from a JSON string
transfer_authorization_create_request_instance = TransferAuthorizationCreateRequest.from_json(json)
# print the JSON string representation of the object
print TransferAuthorizationCreateRequest.to_json()

# convert the object into a dict
transfer_authorization_create_request_dict = transfer_authorization_create_request_instance.to_dict()
# create an instance of TransferAuthorizationCreateRequest from a dict
transfer_authorization_create_request_form_dict = transfer_authorization_create_request.from_dict(transfer_authorization_create_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


