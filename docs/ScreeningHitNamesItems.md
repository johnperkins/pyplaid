# ScreeningHitNamesItems

Analyzed name information for the associated hit

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**analysis** | [**MatchSummary**](MatchSummary.md) |  | [optional] 
**data** | [**IndividualScreeningHitNames**](IndividualScreeningHitNames.md) |  | [optional] 

## Example

```python
from pyplaid.models.screening_hit_names_items import ScreeningHitNamesItems

# TODO update the JSON string below
json = "{}"
# create an instance of ScreeningHitNamesItems from a JSON string
screening_hit_names_items_instance = ScreeningHitNamesItems.from_json(json)
# print the JSON string representation of the object
print ScreeningHitNamesItems.to_json()

# convert the object into a dict
screening_hit_names_items_dict = screening_hit_names_items_instance.to_dict()
# create an instance of ScreeningHitNamesItems from a dict
screening_hit_names_items_form_dict = screening_hit_names_items.from_dict(screening_hit_names_items_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


