# DepositSwitchTargetUser

The deposit switch target user

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**given_name** | **str** | The given name (first name) of the user. | 
**family_name** | **str** | The family name (last name) of the user. | 
**phone** | **str** | The phone number of the user. The endpoint can accept a variety of phone number formats, including E.164. | 
**email** | **str** | The email address of the user. | 
**address** | [**DepositSwitchAddressData**](DepositSwitchAddressData.md) |  | [optional] 
**tax_payer_id** | **str** | The taxpayer ID of the user, generally their SSN, EIN, or TIN. | [optional] 

## Example

```python
from pyplaid.models.deposit_switch_target_user import DepositSwitchTargetUser

# TODO update the JSON string below
json = "{}"
# create an instance of DepositSwitchTargetUser from a JSON string
deposit_switch_target_user_instance = DepositSwitchTargetUser.from_json(json)
# print the JSON string representation of the object
print DepositSwitchTargetUser.to_json()

# convert the object into a dict
deposit_switch_target_user_dict = deposit_switch_target_user_instance.to_dict()
# create an instance of DepositSwitchTargetUser from a dict
deposit_switch_target_user_form_dict = deposit_switch_target_user.from_dict(deposit_switch_target_user_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


