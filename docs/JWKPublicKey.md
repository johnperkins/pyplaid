# JWKPublicKey

A JSON Web Key (JWK) that can be used in conjunction with [JWT libraries](https://jwt.io/#libraries-io) to verify Plaid webhooks

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**alg** | **str** | The alg member identifies the cryptographic algorithm family used with the key. | 
**crv** | **str** | The crv member identifies the cryptographic curve used with the key. | 
**kid** | **str** | The kid (Key ID) member can be used to match a specific key. This can be used, for instance, to choose among a set of keys within the JWK during key rollover. | 
**kty** | **str** | The kty (key type) parameter identifies the cryptographic algorithm family used with the key, such as RSA or EC. | 
**use** | **str** | The use (public key use) parameter identifies the intended use of the public key. | 
**x** | **str** | The x member contains the x coordinate for the elliptic curve point. | 
**y** | **str** | The y member contains the y coordinate for the elliptic curve point. | 
**created_at** | **int** | The timestamp when the key was created, in Unix time. | 
**expired_at** | **int** | The timestamp when the key expired, in Unix time. | 

## Example

```python
from pyplaid.models.jwk_public_key import JWKPublicKey

# TODO update the JSON string below
json = "{}"
# create an instance of JWKPublicKey from a JSON string
jwk_public_key_instance = JWKPublicKey.from_json(json)
# print the JSON string representation of the object
print JWKPublicKey.to_json()

# convert the object into a dict
jwk_public_key_dict = jwk_public_key_instance.to_dict()
# create an instance of JWKPublicKey from a dict
jwk_public_key_form_dict = jwk_public_key.from_dict(jwk_public_key_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


