# HealthIncident

A status health incident

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**start_date** | **datetime** | The start date of the incident, in [ISO 8601](https://wikipedia.org/wiki/ISO_8601) format, e.g. &#x60;\&quot;2020-10-30T15:26:48Z\&quot;&#x60;. | 
**end_date** | **datetime** | The end date of the incident, in [ISO 8601](https://wikipedia.org/wiki/ISO_8601) format, e.g. &#x60;\&quot;2020-10-30T15:26:48Z\&quot;&#x60;. | [optional] 
**title** | **str** | The title of the incident | 
**incident_updates** | [**List[IncidentUpdate]**](IncidentUpdate.md) | Updates on the health incident. | 

## Example

```python
from pyplaid.models.health_incident import HealthIncident

# TODO update the JSON string below
json = "{}"
# create an instance of HealthIncident from a JSON string
health_incident_instance = HealthIncident.from_json(json)
# print the JSON string representation of the object
print HealthIncident.to_json()

# convert the object into a dict
health_incident_dict = health_incident_instance.to_dict()
# create an instance of HealthIncident from a dict
health_incident_form_dict = health_incident.from_dict(health_incident_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


