# TransactionsRemovedWebhook

Fired when transaction(s) for an Item are deleted. The deleted transaction IDs are included in the webhook payload. Plaid will typically check for deleted transaction data several times a day.  This webhook is intended for use with `/transactions/get`; if you are using the newer `/transactions/sync` endpoint, this webhook will still be fired to maintain backwards compatibility, but it is recommended to listen for and respond to the `SYNC_UPDATES_AVAILABLE` webhook instead.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**webhook_type** | **str** | &#x60;TRANSACTIONS&#x60; | 
**webhook_code** | **str** | &#x60;TRANSACTIONS_REMOVED&#x60; | 
**error** | [**PlaidError**](PlaidError.md) |  | [optional] 
**removed_transactions** | **List[str]** | An array of &#x60;transaction_ids&#x60; corresponding to the removed transactions | 
**item_id** | **str** | The &#x60;item_id&#x60; of the Item associated with this webhook, warning, or error | 
**environment** | [**WebhookEnvironmentValues**](WebhookEnvironmentValues.md) |  | 

## Example

```python
from pyplaid.models.transactions_removed_webhook import TransactionsRemovedWebhook

# TODO update the JSON string below
json = "{}"
# create an instance of TransactionsRemovedWebhook from a JSON string
transactions_removed_webhook_instance = TransactionsRemovedWebhook.from_json(json)
# print the JSON string representation of the object
print TransactionsRemovedWebhook.to_json()

# convert the object into a dict
transactions_removed_webhook_dict = transactions_removed_webhook_instance.to_dict()
# create an instance of TransactionsRemovedWebhook from a dict
transactions_removed_webhook_form_dict = transactions_removed_webhook.from_dict(transactions_removed_webhook_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


