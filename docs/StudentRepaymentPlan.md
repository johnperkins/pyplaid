# StudentRepaymentPlan

An object representing the repayment plan for the student loan

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **str** | The description of the repayment plan as provided by the servicer. | 
**type** | **str** | The type of the repayment plan. | 

## Example

```python
from pyplaid.models.student_repayment_plan import StudentRepaymentPlan

# TODO update the JSON string below
json = "{}"
# create an instance of StudentRepaymentPlan from a JSON string
student_repayment_plan_instance = StudentRepaymentPlan.from_json(json)
# print the JSON string representation of the object
print StudentRepaymentPlan.to_json()

# convert the object into a dict
student_repayment_plan_dict = student_repayment_plan_instance.to_dict()
# create an instance of StudentRepaymentPlan from a dict
student_repayment_plan_form_dict = student_repayment_plan.from_dict(student_repayment_plan_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


