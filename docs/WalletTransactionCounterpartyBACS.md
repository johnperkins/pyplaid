# WalletTransactionCounterpartyBACS

The account number and sort code of the counterparty's account

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account** | **str** | The account number of the account. Maximum of 10 characters. | [optional] 
**sort_code** | **str** | The 6-character sort code of the account. | [optional] 

## Example

```python
from pyplaid.models.wallet_transaction_counterparty_bacs import WalletTransactionCounterpartyBACS

# TODO update the JSON string below
json = "{}"
# create an instance of WalletTransactionCounterpartyBACS from a JSON string
wallet_transaction_counterparty_bacs_instance = WalletTransactionCounterpartyBACS.from_json(json)
# print the JSON string representation of the object
print WalletTransactionCounterpartyBACS.to_json()

# convert the object into a dict
wallet_transaction_counterparty_bacs_dict = wallet_transaction_counterparty_bacs_instance.to_dict()
# create an instance of WalletTransactionCounterpartyBACS from a dict
wallet_transaction_counterparty_bacs_form_dict = wallet_transaction_counterparty_bacs.from_dict(wallet_transaction_counterparty_bacs_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


