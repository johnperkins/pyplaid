# EntityScreeningHitUrlsItems

Analyzed URLs for the associated hit

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**analysis** | [**MatchSummary**](MatchSummary.md) |  | [optional] 
**data** | [**EntityScreeningHitUrls**](EntityScreeningHitUrls.md) |  | [optional] 

## Example

```python
from pyplaid.models.entity_screening_hit_urls_items import EntityScreeningHitUrlsItems

# TODO update the JSON string below
json = "{}"
# create an instance of EntityScreeningHitUrlsItems from a JSON string
entity_screening_hit_urls_items_instance = EntityScreeningHitUrlsItems.from_json(json)
# print the JSON string representation of the object
print EntityScreeningHitUrlsItems.to_json()

# convert the object into a dict
entity_screening_hit_urls_items_dict = entity_screening_hit_urls_items_instance.to_dict()
# create an instance of EntityScreeningHitUrlsItems from a dict
entity_screening_hit_urls_items_form_dict = entity_screening_hit_urls_items.from_dict(entity_screening_hit_urls_items_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


