# ItemRemoveResponse

ItemRemoveResponse defines the response schema for `/item/remove`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.item_remove_response import ItemRemoveResponse

# TODO update the JSON string below
json = "{}"
# create an instance of ItemRemoveResponse from a JSON string
item_remove_response_instance = ItemRemoveResponse.from_json(json)
# print the JSON string representation of the object
print ItemRemoveResponse.to_json()

# convert the object into a dict
item_remove_response_dict = item_remove_response_instance.to_dict()
# create an instance of ItemRemoveResponse from a dict
item_remove_response_form_dict = item_remove_response.from_dict(item_remove_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


