# DocumentRiskSummary

A summary across all risk signals associated with a document

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**risk_score** | **float** | A number between 0 and 100, inclusive, where a score closer to 0 indicates a document is likely to be trustworthy and a score closer to 100 indicates a document is likely to be fraudulent | 

## Example

```python
from pyplaid.models.document_risk_summary import DocumentRiskSummary

# TODO update the JSON string below
json = "{}"
# create an instance of DocumentRiskSummary from a JSON string
document_risk_summary_instance = DocumentRiskSummary.from_json(json)
# print the JSON string representation of the object
print DocumentRiskSummary.to_json()

# convert the object into a dict
document_risk_summary_dict = document_risk_summary_instance.to_dict()
# create an instance of DocumentRiskSummary from a dict
document_risk_summary_form_dict = document_risk_summary.from_dict(document_risk_summary_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


