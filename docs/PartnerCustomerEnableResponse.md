# PartnerCustomerEnableResponse

Response schema for `/partner/customer/enable`.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**production_secret** | **str** | The end customer&#39;s secret key for the Production environment. | [optional] 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | [optional] 

## Example

```python
from pyplaid.models.partner_customer_enable_response import PartnerCustomerEnableResponse

# TODO update the JSON string below
json = "{}"
# create an instance of PartnerCustomerEnableResponse from a JSON string
partner_customer_enable_response_instance = PartnerCustomerEnableResponse.from_json(json)
# print the JSON string representation of the object
print PartnerCustomerEnableResponse.to_json()

# convert the object into a dict
partner_customer_enable_response_dict = partner_customer_enable_response_instance.to_dict()
# create an instance of PartnerCustomerEnableResponse from a dict
partner_customer_enable_response_form_dict = partner_customer_enable_response.from_dict(partner_customer_enable_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


