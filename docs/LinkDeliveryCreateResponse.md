# LinkDeliveryCreateResponse

LinkDeliveryCreateResponse defines the response schema for `/link_delivery/create`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**link_delivery_url** | **str** | The URL to the Link Delivery session, which will be delivered by the specified delivery method. | 
**link_delivery_session_id** | **str** | The ID for the link delivery session. Same as the link token string excluding the \&quot;link-{env}-\&quot; prefix | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.link_delivery_create_response import LinkDeliveryCreateResponse

# TODO update the JSON string below
json = "{}"
# create an instance of LinkDeliveryCreateResponse from a JSON string
link_delivery_create_response_instance = LinkDeliveryCreateResponse.from_json(json)
# print the JSON string representation of the object
print LinkDeliveryCreateResponse.to_json()

# convert the object into a dict
link_delivery_create_response_dict = link_delivery_create_response_instance.to_dict()
# create an instance of LinkDeliveryCreateResponse from a dict
link_delivery_create_response_form_dict = link_delivery_create_response.from_dict(link_delivery_create_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


