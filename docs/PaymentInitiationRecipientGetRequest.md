# PaymentInitiationRecipientGetRequest

PaymentInitiationRecipientGetRequest defines the request schema for `/payment_initiation/recipient/get`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**recipient_id** | **str** | The ID of the recipient | 

## Example

```python
from pyplaid.models.payment_initiation_recipient_get_request import PaymentInitiationRecipientGetRequest

# TODO update the JSON string below
json = "{}"
# create an instance of PaymentInitiationRecipientGetRequest from a JSON string
payment_initiation_recipient_get_request_instance = PaymentInitiationRecipientGetRequest.from_json(json)
# print the JSON string representation of the object
print PaymentInitiationRecipientGetRequest.to_json()

# convert the object into a dict
payment_initiation_recipient_get_request_dict = payment_initiation_recipient_get_request_instance.to_dict()
# create an instance of PaymentInitiationRecipientGetRequest from a dict
payment_initiation_recipient_get_request_form_dict = payment_initiation_recipient_get_request.from_dict(payment_initiation_recipient_get_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


