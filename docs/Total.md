# Total

An object representing both the current pay period and year to date amount for a category.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**canonical_description** | [**TotalCanonicalDescription**](TotalCanonicalDescription.md) |  | [optional] 
**description** | **str** | Text of the line item as printed on the paystub. | [optional] 
**current_pay** | [**Pay**](Pay.md) |  | [optional] 
**ytd_pay** | [**Pay**](Pay.md) |  | [optional] 

## Example

```python
from pyplaid.models.total import Total

# TODO update the JSON string below
json = "{}"
# create an instance of Total from a JSON string
total_instance = Total.from_json(json)
# print the JSON string representation of the object
print Total.to_json()

# convert the object into a dict
total_dict = total_instance.to_dict()
# create an instance of Total from a dict
total_form_dict = total.from_dict(total_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


