# ExternalPaymentRefundDetails

Details about external payment refund

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The name of the account holder. | 
**iban** | **str** | The International Bank Account Number (IBAN) for the account. | 
**bacs** | [**RecipientBACSNullable**](RecipientBACSNullable.md) |  | 

## Example

```python
from pyplaid.models.external_payment_refund_details import ExternalPaymentRefundDetails

# TODO update the JSON string below
json = "{}"
# create an instance of ExternalPaymentRefundDetails from a JSON string
external_payment_refund_details_instance = ExternalPaymentRefundDetails.from_json(json)
# print the JSON string representation of the object
print ExternalPaymentRefundDetails.to_json()

# convert the object into a dict
external_payment_refund_details_dict = external_payment_refund_details_instance.to_dict()
# create an instance of ExternalPaymentRefundDetails from a dict
external_payment_refund_details_form_dict = external_payment_refund_details.from_dict(external_payment_refund_details_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


