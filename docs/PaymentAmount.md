# PaymentAmount

The amount and currency of a payment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**currency** | [**PaymentAmountCurrency**](PaymentAmountCurrency.md) |  | 
**value** | **float** | The amount of the payment. Must contain at most two digits of precision e.g. &#x60;1.23&#x60;. Minimum accepted value is &#x60;1&#x60;. | 

## Example

```python
from pyplaid.models.payment_amount import PaymentAmount

# TODO update the JSON string below
json = "{}"
# create an instance of PaymentAmount from a JSON string
payment_amount_instance = PaymentAmount.from_json(json)
# print the JSON string representation of the object
print PaymentAmount.to_json()

# convert the object into a dict
payment_amount_dict = payment_amount_instance.to_dict()
# create an instance of PaymentAmount from a dict
payment_amount_form_dict = payment_amount.from_dict(payment_amount_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


