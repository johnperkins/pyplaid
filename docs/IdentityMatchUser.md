# IdentityMatchUser

The user's legal name, phone number, email address and address used to perform fuzzy match.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**legal_name** | **str** | The user&#39;s full legal name. | [optional] 
**phone_number** | **str** | The user&#39;s phone number, in E.164 format: +{countrycode}{number}. For example: \&quot;+14151234567\&quot;. Phone numbers provided in other formats will be parsed on a best-effort basis. | [optional] 
**email_address** | **str** | The user&#39;s email address. | [optional] 
**address** | [**AddressDataNullable**](AddressDataNullable.md) |  | [optional] 

## Example

```python
from pyplaid.models.identity_match_user import IdentityMatchUser

# TODO update the JSON string below
json = "{}"
# create an instance of IdentityMatchUser from a JSON string
identity_match_user_instance = IdentityMatchUser.from_json(json)
# print the JSON string representation of the object
print IdentityMatchUser.to_json()

# convert the object into a dict
identity_match_user_dict = identity_match_user_instance.to_dict()
# create an instance of IdentityMatchUser from a dict
identity_match_user_form_dict = identity_match_user.from_dict(identity_match_user_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


