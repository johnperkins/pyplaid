# LinkTokenGetMetadataResponse

An object specifying the arguments originally provided to the `/link/token/create` call.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**initial_products** | [**List[Products]**](Products.md) | The &#x60;products&#x60; specified in the &#x60;/link/token/create&#x60; call. | 
**webhook** | **str** | The &#x60;webhook&#x60; specified in the &#x60;/link/token/create&#x60; call. | 
**country_codes** | [**List[CountryCode]**](CountryCode.md) | The &#x60;country_codes&#x60; specified in the &#x60;/link/token/create&#x60; call. | 
**language** | **str** | The &#x60;language&#x60; specified in the &#x60;/link/token/create&#x60; call. | 
**institution_data** | [**LinkTokenCreateInstitutionData**](LinkTokenCreateInstitutionData.md) |  | [optional] 
**account_filters** | [**AccountFiltersResponse**](AccountFiltersResponse.md) |  | [optional] 
**redirect_uri** | **str** | The &#x60;redirect_uri&#x60; specified in the &#x60;/link/token/create&#x60; call. | 
**client_name** | **str** | The &#x60;client_name&#x60; specified in the &#x60;/link/token/create&#x60; call. | 

## Example

```python
from pyplaid.models.link_token_get_metadata_response import LinkTokenGetMetadataResponse

# TODO update the JSON string below
json = "{}"
# create an instance of LinkTokenGetMetadataResponse from a JSON string
link_token_get_metadata_response_instance = LinkTokenGetMetadataResponse.from_json(json)
# print the JSON string representation of the object
print LinkTokenGetMetadataResponse.to_json()

# convert the object into a dict
link_token_get_metadata_response_dict = link_token_get_metadata_response_instance.to_dict()
# create an instance of LinkTokenGetMetadataResponse from a dict
link_token_get_metadata_response_form_dict = link_token_get_metadata_response.from_dict(link_token_get_metadata_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


