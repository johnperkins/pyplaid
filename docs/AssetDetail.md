# AssetDetail

Details about an asset.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**asset_unique_identifier** | **str** | A vendor creadted unique Identifier. | 
**asset_account_identifier** | **str** | A unique alphanumeric string identifying an asset. | 
**asset_as_of_date** | **str** | Account Report As of Date / Create Date. Format YYYY-MM-DD | 
**asset_description** | **str** | A text description that further defines the Asset. This could be used to describe the shares associated with the stocks, bonds or mutual funds, retirement funds or business owned that the borrower has disclosed (named) as an asset. | 
**asset_available_balance_amount** | **float** | Asset Account Available Balance. | 
**asset_current_balance_amount** | **float** | A vendor creadted unique Identifier | 
**asset_type** | [**AssetType**](AssetType.md) |  | 
**asset_type_additional_description** | **str** | Additional Asset Decription some examples are Investment Tax-Deferred , Loan, 401K, 403B, Checking, Money Market, Credit Card,ROTH,529,Biller,ROLLOVER,CD,Savings,Investment Taxable, IRA, Mortgage, Line Of Credit. | 
**asset_days_requested_count** | **int** | The Number of days requested made to the Financial Institution. Example When looking for 3 months of data from the FI, pass in 90 days. | 
**asset_ownership_type** | **str** | Ownership type of the asset account. | 

## Example

```python
from pyplaid.models.asset_detail import AssetDetail

# TODO update the JSON string below
json = "{}"
# create an instance of AssetDetail from a JSON string
asset_detail_instance = AssetDetail.from_json(json)
# print the JSON string representation of the object
print AssetDetail.to_json()

# convert the object into a dict
asset_detail_dict = asset_detail_instance.to_dict()
# create an instance of AssetDetail from a dict
asset_detail_form_dict = asset_detail.from_dict(asset_detail_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


