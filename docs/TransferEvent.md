# TransferEvent

Represents an event in the Transfers API.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**event_id** | **int** | Plaid’s unique identifier for this event. IDs are sequential unsigned 64-bit integers. | 
**timestamp** | **datetime** | The datetime when this event occurred. This will be of the form &#x60;2006-01-02T15:04:05Z&#x60;. | 
**event_type** | [**TransferEventType**](TransferEventType.md) |  | 
**account_id** | **str** | The account ID associated with the transfer. | 
**transfer_id** | **str** | Plaid’s unique identifier for a transfer. | 
**origination_account_id** | **str** | The ID of the origination account that this balance belongs to. | 
**transfer_type** | [**TransferType**](TransferType.md) |  | 
**transfer_amount** | **str** | The amount of the transfer (decimal string with two digits of precision e.g. \&quot;10.00\&quot;). | 
**failure_reason** | [**TransferFailure**](TransferFailure.md) |  | 
**sweep_id** | **str** | Plaid’s unique identifier for a sweep. | 
**sweep_amount** | **str** | A signed amount of how much was &#x60;swept&#x60; or &#x60;return_swept&#x60; (decimal string with two digits of precision e.g. \&quot;-5.50\&quot;). | 
**refund_id** | **str** | Plaid’s unique identifier for a refund. A non-null value indicates the event is for the associated refund of the transfer. | 
**originator_client_id** | **str** | The Plaid client ID that is the originator of the transfer that this event applies to. Only present if the transfer was created on behalf of another client as a third-party sender (TPS). | 

## Example

```python
from pyplaid.models.transfer_event import TransferEvent

# TODO update the JSON string below
json = "{}"
# create an instance of TransferEvent from a JSON string
transfer_event_instance = TransferEvent.from_json(json)
# print the JSON string representation of the object
print TransferEvent.to_json()

# convert the object into a dict
transfer_event_dict = transfer_event_instance.to_dict()
# create an instance of TransferEvent from a dict
transfer_event_form_dict = transfer_event.from_dict(transfer_event_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


