# CreditBankIncome

The report of the Bank Income data for an end user.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bank_income_id** | **str** | The unique identifier associated with the Bank Income Report. | [optional] 
**generated_time** | **datetime** | The time when the Bank Income Report was generated. | [optional] 
**days_requested** | **int** | The number of days requested by the customer for the Bank Income Report. | [optional] 
**items** | [**List[CreditBankIncomeItem]**](CreditBankIncomeItem.md) | The list of Items in the report along with the associated metadata about the Item. | [optional] 
**bank_income_summary** | [**CreditBankIncomeSummary**](CreditBankIncomeSummary.md) |  | [optional] 
**warnings** | [**List[CreditBankIncomeWarning]**](CreditBankIncomeWarning.md) | If data from the Bank Income report was unable to be retrieved, the warnings will contain information about the error that caused the data to be incomplete. | [optional] 

## Example

```python
from pyplaid.models.credit_bank_income import CreditBankIncome

# TODO update the JSON string below
json = "{}"
# create an instance of CreditBankIncome from a JSON string
credit_bank_income_instance = CreditBankIncome.from_json(json)
# print the JSON string representation of the object
print CreditBankIncome.to_json()

# convert the object into a dict
credit_bank_income_dict = credit_bank_income_instance.to_dict()
# create an instance of CreditBankIncome from a dict
credit_bank_income_form_dict = credit_bank_income.from_dict(credit_bank_income_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


