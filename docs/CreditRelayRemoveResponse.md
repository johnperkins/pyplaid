# CreditRelayRemoveResponse

CreditRelayRemoveResponse defines the response schema for `/credit/relay/remove`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**removed** | **bool** | &#x60;true&#x60; if the Relay token was successfully removed. | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.credit_relay_remove_response import CreditRelayRemoveResponse

# TODO update the JSON string below
json = "{}"
# create an instance of CreditRelayRemoveResponse from a JSON string
credit_relay_remove_response_instance = CreditRelayRemoveResponse.from_json(json)
# print the JSON string representation of the object
print CreditRelayRemoveResponse.to_json()

# convert the object into a dict
credit_relay_remove_response_dict = credit_relay_remove_response_instance.to_dict()
# create an instance of CreditRelayRemoveResponse from a dict
credit_relay_remove_response_form_dict = credit_relay_remove_response.from_dict(credit_relay_remove_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


