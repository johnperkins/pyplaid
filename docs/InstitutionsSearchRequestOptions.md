# InstitutionsSearchRequestOptions

An optional object to filter `/institutions/search` results.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**oauth** | **bool** | Limit results to institutions with or without mandatory OAuth login flows. Note that institutions will only have &#x60;oauth&#x60; set to &#x60;true&#x60; if *all* Items associated with that institution are required to use OAuth flows; institutions in a state of migration to OAuth may have the &#x60;oauth&#x60; attribute set to &#x60;false&#x60; even if they support OAuth. | [optional] 
**include_optional_metadata** | **bool** | When true, return the institution&#39;s homepage URL, logo and primary brand color. | [optional] 
**include_auth_metadata** | **bool** | When &#x60;true&#x60;, returns metadata related to the Auth product indicating which auth methods are supported. | [optional] [default to False]
**include_payment_initiation_metadata** | **bool** | When &#x60;true&#x60;, returns metadata related to the Payment Initiation product indicating which payment configurations are supported. | [optional] [default to False]
**payment_initiation** | [**InstitutionsSearchPaymentInitiationOptions**](InstitutionsSearchPaymentInitiationOptions.md) |  | [optional] 

## Example

```python
from pyplaid.models.institutions_search_request_options import InstitutionsSearchRequestOptions

# TODO update the JSON string below
json = "{}"
# create an instance of InstitutionsSearchRequestOptions from a JSON string
institutions_search_request_options_instance = InstitutionsSearchRequestOptions.from_json(json)
# print the JSON string representation of the object
print InstitutionsSearchRequestOptions.to_json()

# convert the object into a dict
institutions_search_request_options_dict = institutions_search_request_options_instance.to_dict()
# create an instance of InstitutionsSearchRequestOptions from a dict
institutions_search_request_options_form_dict = institutions_search_request_options.from_dict(institutions_search_request_options_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


