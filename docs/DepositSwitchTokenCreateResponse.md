# DepositSwitchTokenCreateResponse

DepositSwitchTokenCreateResponse defines the response schema for `/deposit_switch/token/create`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**deposit_switch_token** | **str** | Deposit switch token, used to initialize Link for the Deposit Switch product | 
**deposit_switch_token_expiration_time** | **str** | Expiration time of the token, in [ISO 8601](https://wikipedia.org/wiki/ISO_8601) format | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.deposit_switch_token_create_response import DepositSwitchTokenCreateResponse

# TODO update the JSON string below
json = "{}"
# create an instance of DepositSwitchTokenCreateResponse from a JSON string
deposit_switch_token_create_response_instance = DepositSwitchTokenCreateResponse.from_json(json)
# print the JSON string representation of the object
print DepositSwitchTokenCreateResponse.to_json()

# convert the object into a dict
deposit_switch_token_create_response_dict = deposit_switch_token_create_response_instance.to_dict()
# create an instance of DepositSwitchTokenCreateResponse from a dict
deposit_switch_token_create_response_form_dict = deposit_switch_token_create_response.from_dict(deposit_switch_token_create_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


