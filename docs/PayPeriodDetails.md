# PayPeriodDetails

Details about the pay period.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**check_amount** | **float** | The amount of the paycheck. | [optional] 
**distribution_breakdown** | [**List[DistributionBreakdown]**](DistributionBreakdown.md) |  | [optional] 
**end_date** | **date** | The pay period end date, in [ISO 8601](https://wikipedia.org/wiki/ISO_8601) format: \&quot;yyyy-mm-dd\&quot;. | [optional] 
**gross_earnings** | **float** | Total earnings before tax/deductions. | [optional] 
**pay_date** | **date** | The date on which the paystub was issued, in [ISO 8601](https://wikipedia.org/wiki/ISO_8601) format (\&quot;yyyy-mm-dd\&quot;). | [optional] 
**pay_frequency** | **str** | The frequency at which an individual is paid. | [optional] 
**pay_day** | **date** | The date on which the paystub was issued, in [ISO 8601](https://wikipedia.org/wiki/ISO_8601) format (\&quot;yyyy-mm-dd\&quot;). | [optional] 
**start_date** | **date** | The pay period start date, in [ISO 8601](https://wikipedia.org/wiki/ISO_8601) format: \&quot;yyyy-mm-dd\&quot;. | [optional] 

## Example

```python
from pyplaid.models.pay_period_details import PayPeriodDetails

# TODO update the JSON string below
json = "{}"
# create an instance of PayPeriodDetails from a JSON string
pay_period_details_instance = PayPeriodDetails.from_json(json)
# print the JSON string representation of the object
print PayPeriodDetails.to_json()

# convert the object into a dict
pay_period_details_dict = pay_period_details_instance.to_dict()
# create an instance of PayPeriodDetails from a dict
pay_period_details_form_dict = pay_period_details.from_dict(pay_period_details_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


