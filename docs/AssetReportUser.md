# AssetReportUser

The user object allows you to provide additional information about the user to be appended to the Asset Report. All fields are optional. The `first_name`, `last_name`, and `ssn` fields are required if you would like the Report to be eligible for Fannie Mae’s Day 1 Certainty™ program.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_user_id** | **str** | An identifier you determine and submit for the user. | [optional] 
**first_name** | **str** | The user&#39;s first name. Required for the Fannie Mae Day 1 Certainty™ program. | [optional] 
**middle_name** | **str** | The user&#39;s middle name | [optional] 
**last_name** | **str** | The user&#39;s last name.  Required for the Fannie Mae Day 1 Certainty™ program. | [optional] 
**ssn** | **str** | The user&#39;s Social Security Number. Required for the Fannie Mae Day 1 Certainty™ program.  Format: \&quot;ddd-dd-dddd\&quot; | [optional] 
**phone_number** | **str** | The user&#39;s phone number, in E.164 format: +{countrycode}{number}. For example: \&quot;+14151234567\&quot;. Phone numbers provided in other formats will be parsed on a best-effort basis. | [optional] 
**email** | **str** | The user&#39;s email address. | [optional] 

## Example

```python
from pyplaid.models.asset_report_user import AssetReportUser

# TODO update the JSON string below
json = "{}"
# create an instance of AssetReportUser from a JSON string
asset_report_user_instance = AssetReportUser.from_json(json)
# print the JSON string representation of the object
print AssetReportUser.to_json()

# convert the object into a dict
asset_report_user_dict = asset_report_user_instance.to_dict()
# create an instance of AssetReportUser from a dict
asset_report_user_form_dict = asset_report_user.from_dict(asset_report_user_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


