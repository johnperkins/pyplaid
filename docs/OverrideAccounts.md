# OverrideAccounts

Data to use to set values of test accounts. Some values cannot be specified in the schema and will instead will be calculated from other test data in order to achieve more consistent, realistic test data.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | [**OverrideAccountType**](OverrideAccountType.md) |  | 
**subtype** | [**AccountSubtype**](AccountSubtype.md) |  | 
**starting_balance** | **float** | If provided, the account will start with this amount as the current balance.  | 
**force_available_balance** | **float** | If provided, the account will always have this amount as its  available balance, regardless of current balance or changes in transactions over time. | 
**currency** | **str** | ISO-4217 currency code. If provided, the account will be denominated in the given currency. Transactions will also be in this currency by default. | 
**meta** | [**Meta**](Meta.md) |  | 
**numbers** | [**Numbers**](Numbers.md) |  | 
**transactions** | [**List[TransactionOverride]**](TransactionOverride.md) | Specify the list of transactions on the account. | 
**holdings** | [**HoldingsOverride**](HoldingsOverride.md) |  | [optional] 
**investment_transactions** | [**InvestmentsTransactionsOverride**](InvestmentsTransactionsOverride.md) |  | [optional] 
**identity** | [**OwnerOverride**](OwnerOverride.md) |  | 
**liability** | [**LiabilityOverride**](LiabilityOverride.md) |  | 
**inflow_model** | [**InflowModel**](InflowModel.md) |  | 
**income** | [**IncomeOverride**](IncomeOverride.md) |  | [optional] 

## Example

```python
from pyplaid.models.override_accounts import OverrideAccounts

# TODO update the JSON string below
json = "{}"
# create an instance of OverrideAccounts from a JSON string
override_accounts_instance = OverrideAccounts.from_json(json)
# print the JSON string representation of the object
print OverrideAccounts.to_json()

# convert the object into a dict
override_accounts_dict = override_accounts_instance.to_dict()
# create an instance of OverrideAccounts from a dict
override_accounts_form_dict = override_accounts.from_dict(override_accounts_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


