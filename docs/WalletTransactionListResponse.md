# WalletTransactionListResponse

WalletTransactionListResponse defines the response schema for `/wallet/transaction/list`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**transactions** | [**List[WalletTransaction]**](WalletTransaction.md) | An array of transactions of an e-wallet, associated with the given &#x60;wallet_id&#x60; | 
**next_cursor** | **str** | Cursor used for fetching transactions created before the latest transaction provided in this response | [optional] 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.wallet_transaction_list_response import WalletTransactionListResponse

# TODO update the JSON string below
json = "{}"
# create an instance of WalletTransactionListResponse from a JSON string
wallet_transaction_list_response_instance = WalletTransactionListResponse.from_json(json)
# print the JSON string representation of the object
print WalletTransactionListResponse.to_json()

# convert the object into a dict
wallet_transaction_list_response_dict = wallet_transaction_list_response_instance.to_dict()
# create an instance of WalletTransactionListResponse from a dict
wallet_transaction_list_response_form_dict = wallet_transaction_list_response.from_dict(wallet_transaction_list_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


