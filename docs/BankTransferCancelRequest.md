# BankTransferCancelRequest

Defines the request schema for `/bank_transfer/cancel`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**bank_transfer_id** | **str** | Plaid’s unique identifier for a bank transfer. | 

## Example

```python
from pyplaid.models.bank_transfer_cancel_request import BankTransferCancelRequest

# TODO update the JSON string below
json = "{}"
# create an instance of BankTransferCancelRequest from a JSON string
bank_transfer_cancel_request_instance = BankTransferCancelRequest.from_json(json)
# print the JSON string representation of the object
print BankTransferCancelRequest.to_json()

# convert the object into a dict
bank_transfer_cancel_request_dict = bank_transfer_cancel_request_instance.to_dict()
# create an instance of BankTransferCancelRequest from a dict
bank_transfer_cancel_request_form_dict = bank_transfer_cancel_request.from_dict(bank_transfer_cancel_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


