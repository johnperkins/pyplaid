# EntityScreeningStatusUpdatedWebhook

Fired when an entity screening status has changed, which can occur manually via the dashboard or during ongoing monitoring.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**webhook_type** | **str** | &#x60;ENTITY_SCREENING&#x60; | 
**webhook_code** | **str** | &#x60;STATUS_UPDATED&#x60; | 
**screening_id** | **object** | The ID of the associated screening. | 
**environment** | [**WebhookEnvironmentValues**](WebhookEnvironmentValues.md) |  | 

## Example

```python
from pyplaid.models.entity_screening_status_updated_webhook import EntityScreeningStatusUpdatedWebhook

# TODO update the JSON string below
json = "{}"
# create an instance of EntityScreeningStatusUpdatedWebhook from a JSON string
entity_screening_status_updated_webhook_instance = EntityScreeningStatusUpdatedWebhook.from_json(json)
# print the JSON string representation of the object
print EntityScreeningStatusUpdatedWebhook.to_json()

# convert the object into a dict
entity_screening_status_updated_webhook_dict = entity_screening_status_updated_webhook_instance.to_dict()
# create an instance of EntityScreeningStatusUpdatedWebhook from a dict
entity_screening_status_updated_webhook_form_dict = entity_screening_status_updated_webhook.from_dict(entity_screening_status_updated_webhook_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


