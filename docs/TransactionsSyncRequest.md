# TransactionsSyncRequest

TransactionsSyncRequest defines the request schema for `/transactions/sync`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**access_token** | **str** | The access token associated with the Item data is being requested for. | 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**cursor** | **str** | The cursor value represents the last update requested. Providing it will cause the response to only return changes after this update. If omitted, the entire history of updates will be returned, starting with the first-added transactions on the item. Note: The upper-bound length of this cursor is 256 characters of base64. | [optional] 
**count** | **int** | The number of transaction updates to fetch. | [optional] [default to 100]
**options** | [**TransactionsSyncRequestOptions**](TransactionsSyncRequestOptions.md) |  | [optional] 

## Example

```python
from pyplaid.models.transactions_sync_request import TransactionsSyncRequest

# TODO update the JSON string below
json = "{}"
# create an instance of TransactionsSyncRequest from a JSON string
transactions_sync_request_instance = TransactionsSyncRequest.from_json(json)
# print the JSON string representation of the object
print TransactionsSyncRequest.to_json()

# convert the object into a dict
transactions_sync_request_dict = transactions_sync_request_instance.to_dict()
# create an instance of TransactionsSyncRequest from a dict
transactions_sync_request_form_dict = transactions_sync_request.from_dict(transactions_sync_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


