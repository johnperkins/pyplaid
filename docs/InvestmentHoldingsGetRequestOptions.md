# InvestmentHoldingsGetRequestOptions

An optional object to filter `/investments/holdings/get` results. If provided, must not be `null`.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_ids** | **List[str]** | An array of &#x60;account_id&#x60;s to retrieve for the Item. An error will be returned if a provided &#x60;account_id&#x60; is not associated with the Item. | [optional] 

## Example

```python
from pyplaid.models.investment_holdings_get_request_options import InvestmentHoldingsGetRequestOptions

# TODO update the JSON string below
json = "{}"
# create an instance of InvestmentHoldingsGetRequestOptions from a JSON string
investment_holdings_get_request_options_instance = InvestmentHoldingsGetRequestOptions.from_json(json)
# print the JSON string representation of the object
print InvestmentHoldingsGetRequestOptions.to_json()

# convert the object into a dict
investment_holdings_get_request_options_dict = investment_holdings_get_request_options_instance.to_dict()
# create an instance of InvestmentHoldingsGetRequestOptions from a dict
investment_holdings_get_request_options_form_dict = investment_holdings_get_request_options.from_dict(investment_holdings_get_request_options_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


