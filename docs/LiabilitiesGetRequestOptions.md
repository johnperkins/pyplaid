# LiabilitiesGetRequestOptions

An optional object to filter `/liabilities/get` results. If provided, `options` cannot be null.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_ids** | **List[str]** | A list of accounts to retrieve for the Item.  An error will be returned if a provided &#x60;account_id&#x60; is not associated with the Item | [optional] 

## Example

```python
from pyplaid.models.liabilities_get_request_options import LiabilitiesGetRequestOptions

# TODO update the JSON string below
json = "{}"
# create an instance of LiabilitiesGetRequestOptions from a JSON string
liabilities_get_request_options_instance = LiabilitiesGetRequestOptions.from_json(json)
# print the JSON string representation of the object
print LiabilitiesGetRequestOptions.to_json()

# convert the object into a dict
liabilities_get_request_options_dict = liabilities_get_request_options_instance.to_dict()
# create an instance of LiabilitiesGetRequestOptions from a dict
liabilities_get_request_options_form_dict = liabilities_get_request_options.from_dict(liabilities_get_request_options_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


