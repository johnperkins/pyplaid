# LinkTokenCreateRequestDepositSwitch

Specifies options for initializing Link for use with the Deposit Switch (beta) product. This field is required if `deposit_switch` is included in the `products` array.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**deposit_switch_id** | **str** | The &#x60;deposit_switch_id&#x60; provided by the &#x60;/deposit_switch/create&#x60; endpoint. | 

## Example

```python
from pyplaid.models.link_token_create_request_deposit_switch import LinkTokenCreateRequestDepositSwitch

# TODO update the JSON string below
json = "{}"
# create an instance of LinkTokenCreateRequestDepositSwitch from a JSON string
link_token_create_request_deposit_switch_instance = LinkTokenCreateRequestDepositSwitch.from_json(json)
# print the JSON string representation of the object
print LinkTokenCreateRequestDepositSwitch.to_json()

# convert the object into a dict
link_token_create_request_deposit_switch_dict = link_token_create_request_deposit_switch_instance.to_dict()
# create an instance of LinkTokenCreateRequestDepositSwitch from a dict
link_token_create_request_deposit_switch_form_dict = link_token_create_request_deposit_switch.from_dict(link_token_create_request_deposit_switch_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


