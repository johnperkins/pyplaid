# TransferUserAddressInResponse

The address associated with the account holder.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**street** | **str** | The street number and name (i.e., \&quot;100 Market St.\&quot;). | 
**city** | **str** | Ex. \&quot;San Francisco\&quot; | 
**region** | **str** | The state or province (e.g., \&quot;CA\&quot;). | 
**postal_code** | **str** | The postal code (e.g., \&quot;94103\&quot;). | 
**country** | **str** | A two-letter country code (e.g., \&quot;US\&quot;). | 

## Example

```python
from pyplaid.models.transfer_user_address_in_response import TransferUserAddressInResponse

# TODO update the JSON string below
json = "{}"
# create an instance of TransferUserAddressInResponse from a JSON string
transfer_user_address_in_response_instance = TransferUserAddressInResponse.from_json(json)
# print the JSON string representation of the object
print TransferUserAddressInResponse.to_json()

# convert the object into a dict
transfer_user_address_in_response_dict = transfer_user_address_in_response_instance.to_dict()
# create an instance of TransferUserAddressInResponse from a dict
transfer_user_address_in_response_form_dict = transfer_user_address_in_response.from_dict(transfer_user_address_in_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


