# LinkOAuthCorrelationIdExchangeRequest

LinkOAuthCorrelationIdExchangeRequest defines the request schema for `/link/oauth/correlation_id/exchange`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**link_correlation_id** | **str** | A &#x60;link_correlation_id&#x60; from a received OAuth redirect URI callback | 

## Example

```python
from pyplaid.models.link_o_auth_correlation_id_exchange_request import LinkOAuthCorrelationIdExchangeRequest

# TODO update the JSON string below
json = "{}"
# create an instance of LinkOAuthCorrelationIdExchangeRequest from a JSON string
link_o_auth_correlation_id_exchange_request_instance = LinkOAuthCorrelationIdExchangeRequest.from_json(json)
# print the JSON string representation of the object
print LinkOAuthCorrelationIdExchangeRequest.to_json()

# convert the object into a dict
link_o_auth_correlation_id_exchange_request_dict = link_o_auth_correlation_id_exchange_request_instance.to_dict()
# create an instance of LinkOAuthCorrelationIdExchangeRequest from a dict
link_o_auth_correlation_id_exchange_request_form_dict = link_o_auth_correlation_id_exchange_request.from_dict(link_o_auth_correlation_id_exchange_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


