# LinkTokenCreateRequestTransfer

Specifies options for initializing Link for use with the Transfer product.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**intent_id** | **str** | The &#x60;id&#x60; returned by the &#x60;/transfer/intent/create&#x60; endpoint. | [optional] 
**payment_profile_token** | **str** | The &#x60;payment_profile_token&#x60; returned by the &#x60;/payment_profile/create&#x60; endpoint. | [optional] 

## Example

```python
from pyplaid.models.link_token_create_request_transfer import LinkTokenCreateRequestTransfer

# TODO update the JSON string below
json = "{}"
# create an instance of LinkTokenCreateRequestTransfer from a JSON string
link_token_create_request_transfer_instance = LinkTokenCreateRequestTransfer.from_json(json)
# print the JSON string representation of the object
print LinkTokenCreateRequestTransfer.to_json()

# convert the object into a dict
link_token_create_request_transfer_dict = link_token_create_request_transfer_instance.to_dict()
# create an instance of LinkTokenCreateRequestTransfer from a dict
link_token_create_request_transfer_form_dict = link_token_create_request_transfer.from_dict(link_token_create_request_transfer_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


