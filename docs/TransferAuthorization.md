# TransferAuthorization

Contains the authorization decision for a proposed transfer

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Plaid’s unique identifier for a transfer authorization. | 
**created** | **datetime** | The datetime representing when the authorization was created, in the format &#x60;2006-01-02T15:04:05Z&#x60;. | 
**decision** | [**TransferAuthorizationDecision**](TransferAuthorizationDecision.md) |  | 
**decision_rationale** | [**TransferAuthorizationDecisionRationale**](TransferAuthorizationDecisionRationale.md) |  | 
**guarantee_decision** | [**TransferAuthorizationGuaranteeDecision**](TransferAuthorizationGuaranteeDecision.md) |  | 
**guarantee_decision_rationale** | [**TransferAuthorizationGuaranteeDecisionRationale**](TransferAuthorizationGuaranteeDecisionRationale.md) |  | 
**proposed_transfer** | [**TransferAuthorizationProposedTransfer**](TransferAuthorizationProposedTransfer.md) |  | 

## Example

```python
from pyplaid.models.transfer_authorization import TransferAuthorization

# TODO update the JSON string below
json = "{}"
# create an instance of TransferAuthorization from a JSON string
transfer_authorization_instance = TransferAuthorization.from_json(json)
# print the JSON string representation of the object
print TransferAuthorization.to_json()

# convert the object into a dict
transfer_authorization_dict = transfer_authorization_instance.to_dict()
# create an instance of TransferAuthorization from a dict
transfer_authorization_form_dict = transfer_authorization.from_dict(transfer_authorization_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


