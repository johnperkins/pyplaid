# EntityScreeningHitEmailsItems

Analyzed emails for the associated hit

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**analysis** | [**MatchSummary**](MatchSummary.md) |  | [optional] 
**data** | [**EntityScreeningHitEmails**](EntityScreeningHitEmails.md) |  | [optional] 

## Example

```python
from pyplaid.models.entity_screening_hit_emails_items import EntityScreeningHitEmailsItems

# TODO update the JSON string below
json = "{}"
# create an instance of EntityScreeningHitEmailsItems from a JSON string
entity_screening_hit_emails_items_instance = EntityScreeningHitEmailsItems.from_json(json)
# print the JSON string representation of the object
print EntityScreeningHitEmailsItems.to_json()

# convert the object into a dict
entity_screening_hit_emails_items_dict = entity_screening_hit_emails_items_instance.to_dict()
# create an instance of EntityScreeningHitEmailsItems from a dict
entity_screening_hit_emails_items_form_dict = entity_screening_hit_emails_items.from_dict(entity_screening_hit_emails_items_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


