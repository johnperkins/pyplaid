# SignalDecisionReportResponse

SignalDecisionReportResponse defines the response schema for `/signal/decision/report`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.signal_decision_report_response import SignalDecisionReportResponse

# TODO update the JSON string below
json = "{}"
# create an instance of SignalDecisionReportResponse from a JSON string
signal_decision_report_response_instance = SignalDecisionReportResponse.from_json(json)
# print the JSON string representation of the object
print SignalDecisionReportResponse.to_json()

# convert the object into a dict
signal_decision_report_response_dict = signal_decision_report_response_instance.to_dict()
# create an instance of SignalDecisionReportResponse from a dict
signal_decision_report_response_form_dict = signal_decision_report_response.from_dict(signal_decision_report_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


