# LinkTokenCreateRequestUser

An object specifying information about the end user who will be linking their account.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_user_id** | **str** | A unique ID representing the end user. Typically this will be a user ID number from your application. Personally identifiable information, such as an email address or phone number, should not be used in the &#x60;client_user_id&#x60;. It is currently used as a means of searching logs for the given user in the Plaid Dashboard. | 
**legal_name** | **str** | The user&#39;s full legal name. Currently used only to support certain legacy flows. | [optional] 
**name** | [**LinkTokenCreateRequestUserName**](LinkTokenCreateRequestUserName.md) |  | [optional] 
**phone_number** | **str** | The user&#39;s phone number in [E.164](https://en.wikipedia.org/wiki/E.164) format. This field is optional, but required to enable the [returning user experience](https://plaid.com/docs/link/returning-user). | [optional] 
**phone_number_verified_time** | **datetime** | The date and time the phone number was verified in [ISO 8601](https://wikipedia.org/wiki/ISO_8601) format (&#x60;YYYY-MM-DDThh:mm:ssZ&#x60;). This field is optional, but required to enable any [returning user experience](https://plaid.com/docs/link/returning-user).   Only pass a verification time for a phone number that you have verified. If you have performed verification but don’t have the time, you may supply a signal value of the start of the UNIX epoch.   Example: &#x60;2020-01-01T00:00:00Z&#x60;  | [optional] 
**email_address** | **str** | The user&#39;s email address. This field is optional, but required to enable the [pre-authenticated returning user flow](https://plaid.com/docs/link/returning-user/#enabling-the-returning-user-experience). | [optional] 
**email_address_verified_time** | **datetime** | The date and time the email address was verified in [ISO 8601](https://wikipedia.org/wiki/ISO_8601) format (&#x60;YYYY-MM-DDThh:mm:ssZ&#x60;). This is an optional field used in the [returning user experience](https://plaid.com/docs/link/returning-user).   Only pass a verification time for an email address that you have verified. If you have performed verification but don’t have the time, you may supply a signal value of the start of the UNIX epoch.   Example: &#x60;2020-01-01T00:00:00Z&#x60; | [optional] 
**ssn** | **str** | To be provided in the format \&quot;ddd-dd-dddd\&quot;. Not currently used. | [optional] 
**date_of_birth** | **date** | To be provided in the format \&quot;yyyy-mm-dd\&quot;. Not currently used. | [optional] 
**address** | [**UserAddress**](UserAddress.md) |  | [optional] 
**id_number** | [**UserIDNumber**](UserIDNumber.md) |  | [optional] 

## Example

```python
from pyplaid.models.link_token_create_request_user import LinkTokenCreateRequestUser

# TODO update the JSON string below
json = "{}"
# create an instance of LinkTokenCreateRequestUser from a JSON string
link_token_create_request_user_instance = LinkTokenCreateRequestUser.from_json(json)
# print the JSON string representation of the object
print LinkTokenCreateRequestUser.to_json()

# convert the object into a dict
link_token_create_request_user_dict = link_token_create_request_user_instance.to_dict()
# create an instance of LinkTokenCreateRequestUser from a dict
link_token_create_request_user_form_dict = link_token_create_request_user.from_dict(link_token_create_request_user_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


