# NumbersEFTNullable

Identifying information for transferring money to or from a Canadian bank account via EFT.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_id** | **str** | The Plaid account ID associated with the account numbers | 
**account** | **str** | The EFT account number for the account | 
**institution** | **str** | The EFT institution number for the account | 
**branch** | **str** | The EFT branch number for the account | 

## Example

```python
from pyplaid.models.numbers_eft_nullable import NumbersEFTNullable

# TODO update the JSON string below
json = "{}"
# create an instance of NumbersEFTNullable from a JSON string
numbers_eft_nullable_instance = NumbersEFTNullable.from_json(json)
# print the JSON string representation of the object
print NumbersEFTNullable.to_json()

# convert the object into a dict
numbers_eft_nullable_dict = numbers_eft_nullable_instance.to_dict()
# create an instance of NumbersEFTNullable from a dict
numbers_eft_nullable_form_dict = numbers_eft_nullable.from_dict(numbers_eft_nullable_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


