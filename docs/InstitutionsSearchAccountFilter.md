# InstitutionsSearchAccountFilter

An account filter to apply to institutions search requests

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**loan** | [**List[AccountSubtype]**](AccountSubtype.md) |  | [optional] 
**depository** | [**List[AccountSubtype]**](AccountSubtype.md) |  | [optional] 
**credit** | [**List[AccountSubtype]**](AccountSubtype.md) |  | [optional] 
**investment** | [**List[AccountSubtype]**](AccountSubtype.md) |  | [optional] 

## Example

```python
from pyplaid.models.institutions_search_account_filter import InstitutionsSearchAccountFilter

# TODO update the JSON string below
json = "{}"
# create an instance of InstitutionsSearchAccountFilter from a JSON string
institutions_search_account_filter_instance = InstitutionsSearchAccountFilter.from_json(json)
# print the JSON string representation of the object
print InstitutionsSearchAccountFilter.to_json()

# convert the object into a dict
institutions_search_account_filter_dict = institutions_search_account_filter_instance.to_dict()
# create an instance of InstitutionsSearchAccountFilter from a dict
institutions_search_account_filter_form_dict = institutions_search_account_filter.from_dict(institutions_search_account_filter_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


