# InstitutionsSearchPaymentInitiationOptions

Additional options that will be used to filter institutions by various Payment Initiation configurations.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**payment_id** | **str** | A unique ID identifying the payment | [optional] 
**consent_id** | **str** | A unique ID identifying the payment consent | [optional] 

## Example

```python
from pyplaid.models.institutions_search_payment_initiation_options import InstitutionsSearchPaymentInitiationOptions

# TODO update the JSON string below
json = "{}"
# create an instance of InstitutionsSearchPaymentInitiationOptions from a JSON string
institutions_search_payment_initiation_options_instance = InstitutionsSearchPaymentInitiationOptions.from_json(json)
# print the JSON string representation of the object
print InstitutionsSearchPaymentInitiationOptions.to_json()

# convert the object into a dict
institutions_search_payment_initiation_options_dict = institutions_search_payment_initiation_options_instance.to_dict()
# create an instance of InstitutionsSearchPaymentInitiationOptions from a dict
institutions_search_payment_initiation_options_form_dict = institutions_search_payment_initiation_options.from_dict(institutions_search_payment_initiation_options_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


