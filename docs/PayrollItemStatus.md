# PayrollItemStatus

Details about the status of the payroll item.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**processing_status** | **str** | Denotes the processing status for the verification.  &#x60;UNKNOWN&#x60;: The processing status could not be determined.  &#x60;PROCESSING_COMPLETE&#x60;: The processing has completed and the user has approved for sharing. The data is available to be retrieved.  &#x60;PROCESSING&#x60;: The verification is still processing. The data is not available yet.  &#x60;FAILED&#x60;: The processing failed to complete successfully.  &#x60;APPROVAL_STATUS_PENDING&#x60;: The processing has completed but the user has not yet approved the sharing of the data. | [optional] 

## Example

```python
from pyplaid.models.payroll_item_status import PayrollItemStatus

# TODO update the JSON string below
json = "{}"
# create an instance of PayrollItemStatus from a JSON string
payroll_item_status_instance = PayrollItemStatus.from_json(json)
# print the JSON string representation of the object
print PayrollItemStatus.to_json()

# convert the object into a dict
payroll_item_status_dict = payroll_item_status_instance.to_dict()
# create an instance of PayrollItemStatus from a dict
payroll_item_status_form_dict = payroll_item_status.from_dict(payroll_item_status_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


