# TransferAuthorizationProposedTransfer

Details regarding the proposed transfer.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ach_class** | [**ACHClass**](ACHClass.md) |  | [optional] 
**account_id** | **str** | The Plaid &#x60;account_id&#x60; for the account that will be debited or credited. | [optional] 
**type** | [**TransferType**](TransferType.md) |  | 
**user** | [**TransferUserInResponse**](TransferUserInResponse.md) |  | 
**amount** | **str** | The amount of the transfer (decimal string with two digits of precision e.g. \&quot;10.00\&quot;). | 
**network** | **str** | The network or rails used for the transfer. | 
**origination_account_id** | **str** | Plaid&#39;s unique identifier for the origination account that was used for this transfer. | 
**iso_currency_code** | **str** | The currency of the transfer amount. The default value is \&quot;USD\&quot;. | 
**originator_client_id** | **str** | The Plaid client ID that is the originator of this transfer. Only present if created on behalf of another client as a third-party sender (TPS). | 

## Example

```python
from pyplaid.models.transfer_authorization_proposed_transfer import TransferAuthorizationProposedTransfer

# TODO update the JSON string below
json = "{}"
# create an instance of TransferAuthorizationProposedTransfer from a JSON string
transfer_authorization_proposed_transfer_instance = TransferAuthorizationProposedTransfer.from_json(json)
# print the JSON string representation of the object
print TransferAuthorizationProposedTransfer.to_json()

# convert the object into a dict
transfer_authorization_proposed_transfer_dict = transfer_authorization_proposed_transfer_instance.to_dict()
# create an instance of TransferAuthorizationProposedTransfer from a dict
transfer_authorization_proposed_transfer_form_dict = transfer_authorization_proposed_transfer.from_dict(transfer_authorization_proposed_transfer_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


