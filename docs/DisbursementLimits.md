# DisbursementLimits

Requested disbursement (i.e. sending money) limits for the end customer.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**average_amount** | **int** | Average disbursement amount in a single transaction, in dollars. | 
**maximum_amount** | **int** | Maximum disbursement amount in a single transaction, in dollars. | 
**monthly_amount** | **int** | Monthly disbursement amount, in dollars. | 

## Example

```python
from pyplaid.models.disbursement_limits import DisbursementLimits

# TODO update the JSON string below
json = "{}"
# create an instance of DisbursementLimits from a JSON string
disbursement_limits_instance = DisbursementLimits.from_json(json)
# print the JSON string representation of the object
print DisbursementLimits.to_json()

# convert the object into a dict
disbursement_limits_dict = disbursement_limits_instance.to_dict()
# create an instance of DisbursementLimits from a dict
disbursement_limits_form_dict = disbursement_limits.from_dict(disbursement_limits_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


