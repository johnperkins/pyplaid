# EmployerIncomeSummaryFieldString

The name of the employer, as reported on the paystub.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value** | **str** | The value of the field. | 
**verification_status** | [**VerificationStatus**](VerificationStatus.md) |  | 

## Example

```python
from pyplaid.models.employer_income_summary_field_string import EmployerIncomeSummaryFieldString

# TODO update the JSON string below
json = "{}"
# create an instance of EmployerIncomeSummaryFieldString from a JSON string
employer_income_summary_field_string_instance = EmployerIncomeSummaryFieldString.from_json(json)
# print the JSON string representation of the object
print EmployerIncomeSummaryFieldString.to_json()

# convert the object into a dict
employer_income_summary_field_string_dict = employer_income_summary_field_string_instance.to_dict()
# create an instance of EmployerIncomeSummaryFieldString from a dict
employer_income_summary_field_string_form_dict = employer_income_summary_field_string.from_dict(employer_income_summary_field_string_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


