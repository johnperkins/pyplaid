# Loan

Information specific to a mortgage loan agreement between one or more borrowers and a mortgage lender.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**loan_identifiers** | [**LoanIdentifiers**](LoanIdentifiers.md) |  | 

## Example

```python
from pyplaid.models.loan import Loan

# TODO update the JSON string below
json = "{}"
# create an instance of Loan from a JSON string
loan_instance = Loan.from_json(json)
# print the JSON string representation of the object
print Loan.to_json()

# convert the object into a dict
loan_dict = loan_instance.to_dict()
# create an instance of Loan from a dict
loan_form_dict = loan.from_dict(loan_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


