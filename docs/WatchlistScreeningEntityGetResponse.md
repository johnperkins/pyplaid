# WatchlistScreeningEntityGetResponse

The entity screening object allows you to represent an entity in your system, update its profile, and search for it on various watchlists. Note: Rejected entity screenings will not receive new hits, regardless of entity program configuration.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | ID of the associated entity screening. | 
**search_terms** | [**EntityWatchlistScreeningSearchTerms**](EntityWatchlistScreeningSearchTerms.md) |  | 
**assignee** | **str** | ID of the associated user. | 
**status** | [**WatchlistScreeningStatus**](WatchlistScreeningStatus.md) |  | 
**client_user_id** | **str** | An identifier to help you connect this object to your internal systems. For example, your database ID corresponding to this object. | 
**audit_trail** | [**WatchlistScreeningAuditTrail**](WatchlistScreeningAuditTrail.md) |  | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.watchlist_screening_entity_get_response import WatchlistScreeningEntityGetResponse

# TODO update the JSON string below
json = "{}"
# create an instance of WatchlistScreeningEntityGetResponse from a JSON string
watchlist_screening_entity_get_response_instance = WatchlistScreeningEntityGetResponse.from_json(json)
# print the JSON string representation of the object
print WatchlistScreeningEntityGetResponse.to_json()

# convert the object into a dict
watchlist_screening_entity_get_response_dict = watchlist_screening_entity_get_response_instance.to_dict()
# create an instance of WatchlistScreeningEntityGetResponse from a dict
watchlist_screening_entity_get_response_form_dict = watchlist_screening_entity_get_response.from_dict(watchlist_screening_entity_get_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


