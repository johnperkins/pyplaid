# CreditBankIncomePDFGetRequest

CreditBankIncomePDFGetRequest defines the request schema for `/credit/bank_income/pdf/get`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**user_token** | **str** | The user token associated with the User data is being requested for. | 

## Example

```python
from pyplaid.models.credit_bank_income_pdf_get_request import CreditBankIncomePDFGetRequest

# TODO update the JSON string below
json = "{}"
# create an instance of CreditBankIncomePDFGetRequest from a JSON string
credit_bank_income_pdf_get_request_instance = CreditBankIncomePDFGetRequest.from_json(json)
# print the JSON string representation of the object
print CreditBankIncomePDFGetRequest.to_json()

# convert the object into a dict
credit_bank_income_pdf_get_request_dict = credit_bank_income_pdf_get_request_instance.to_dict()
# create an instance of CreditBankIncomePDFGetRequest from a dict
credit_bank_income_pdf_get_request_form_dict = credit_bank_income_pdf_get_request.from_dict(credit_bank_income_pdf_get_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


