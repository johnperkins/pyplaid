# PaymentAmountRefunded

The amount that has been refunded already. Subtract this from the payment amount to calculate the amount still available to refund.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**currency** | [**PaymentAmountCurrency**](PaymentAmountCurrency.md) |  | 
**value** | **float** | The amount of the payment. Must contain at most two digits of precision e.g. &#x60;1.23&#x60;. Minimum accepted value is &#x60;1&#x60;. | 

## Example

```python
from pyplaid.models.payment_amount_refunded import PaymentAmountRefunded

# TODO update the JSON string below
json = "{}"
# create an instance of PaymentAmountRefunded from a JSON string
payment_amount_refunded_instance = PaymentAmountRefunded.from_json(json)
# print the JSON string representation of the object
print PaymentAmountRefunded.to_json()

# convert the object into a dict
payment_amount_refunded_dict = payment_amount_refunded_instance.to_dict()
# create an instance of PaymentAmountRefunded from a dict
payment_amount_refunded_form_dict = payment_amount_refunded.from_dict(payment_amount_refunded_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


