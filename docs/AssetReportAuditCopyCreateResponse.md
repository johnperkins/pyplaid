# AssetReportAuditCopyCreateResponse

AssetReportAuditCopyCreateResponse defines the response schema for `/asset_report/audit_copy/get`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**audit_copy_token** | **str** | A token that can be shared with a third party auditor to allow them to obtain access to the Asset Report. This token should be stored securely. | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.asset_report_audit_copy_create_response import AssetReportAuditCopyCreateResponse

# TODO update the JSON string below
json = "{}"
# create an instance of AssetReportAuditCopyCreateResponse from a JSON string
asset_report_audit_copy_create_response_instance = AssetReportAuditCopyCreateResponse.from_json(json)
# print the JSON string representation of the object
print AssetReportAuditCopyCreateResponse.to_json()

# convert the object into a dict
asset_report_audit_copy_create_response_dict = asset_report_audit_copy_create_response_instance.to_dict()
# create an instance of AssetReportAuditCopyCreateResponse from a dict
asset_report_audit_copy_create_response_form_dict = asset_report_audit_copy_create_response.from_dict(asset_report_audit_copy_create_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


