# IdentityDefaultUpdateWebhook

Fired when a change to identity data has been detected on an Item.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**webhook_type** | **str** | &#x60;IDENTITY&#x60; | 
**webhook_code** | **str** | &#x60;DEFAULT_UPDATE&#x60; | 
**item_id** | **str** | The &#x60;item_id&#x60; of the Item associated with this webhook, warning, or error | 
**account_ids_with_updated_identity** | **Dict[str, List[IdentityUpdateTypes]]** | An object with keys of &#x60;account_id&#x60;&#39;s that are mapped to their respective identity attributes that changed.  Example: &#x60;{ \&quot;XMBvvyMGQ1UoLbKByoMqH3nXMj84ALSdE5B58\&quot;: [\&quot;PHONES\&quot;] }&#x60;  | 
**error** | [**PlaidError**](PlaidError.md) |  | 
**environment** | [**WebhookEnvironmentValues**](WebhookEnvironmentValues.md) |  | 

## Example

```python
from pyplaid.models.identity_default_update_webhook import IdentityDefaultUpdateWebhook

# TODO update the JSON string below
json = "{}"
# create an instance of IdentityDefaultUpdateWebhook from a JSON string
identity_default_update_webhook_instance = IdentityDefaultUpdateWebhook.from_json(json)
# print the JSON string representation of the object
print IdentityDefaultUpdateWebhook.to_json()

# convert the object into a dict
identity_default_update_webhook_dict = identity_default_update_webhook_instance.to_dict()
# create an instance of IdentityDefaultUpdateWebhook from a dict
identity_default_update_webhook_form_dict = identity_default_update_webhook.from_dict(identity_default_update_webhook_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


