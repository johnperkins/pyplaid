# PartnerCustomerRemoveRequest

Request schema for `/partner/customer/remove`.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**end_customer_client_id** | **str** |  | 

## Example

```python
from pyplaid.models.partner_customer_remove_request import PartnerCustomerRemoveRequest

# TODO update the JSON string below
json = "{}"
# create an instance of PartnerCustomerRemoveRequest from a JSON string
partner_customer_remove_request_instance = PartnerCustomerRemoveRequest.from_json(json)
# print the JSON string representation of the object
print PartnerCustomerRemoveRequest.to_json()

# convert the object into a dict
partner_customer_remove_request_dict = partner_customer_remove_request_instance.to_dict()
# create an instance of PartnerCustomerRemoveRequest from a dict
partner_customer_remove_request_form_dict = partner_customer_remove_request.from_dict(partner_customer_remove_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


