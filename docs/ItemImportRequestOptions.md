# ItemImportRequestOptions

An optional object to configure `/item/import` request.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**webhook** | **str** | Specifies a webhook URL to associate with an Item. Plaid fires a webhook if credentials fail.  | [optional] 

## Example

```python
from pyplaid.models.item_import_request_options import ItemImportRequestOptions

# TODO update the JSON string below
json = "{}"
# create an instance of ItemImportRequestOptions from a JSON string
item_import_request_options_instance = ItemImportRequestOptions.from_json(json)
# print the JSON string representation of the object
print ItemImportRequestOptions.to_json()

# convert the object into a dict
item_import_request_options_dict = item_import_request_options_instance.to_dict()
# create an instance of ItemImportRequestOptions from a dict
item_import_request_options_form_dict = item_import_request_options.from_dict(item_import_request_options_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


