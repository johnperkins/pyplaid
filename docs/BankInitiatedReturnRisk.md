# BankInitiatedReturnRisk

The object contains a risk score and a risk tier that evaluate the transaction return risk because an account is overdrawn or because an ineligible account is used. Common return codes in this category include: \"R01\", \"R02\", \"R03\", \"R04\", \"R06\", \"R08\",  \"R09\", \"R13\", \"R16\", \"R17\", \"R20\", \"R23\". These returns have a turnaround time of 2 banking days.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**score** | **int** | A score from 1-99 that indicates the transaction return risk: a higher risk score suggests a higher return likelihood. | 
**risk_tier** | **int** | In the &#x60;bank_initiated_return_risk&#x60; object, there are eight risk tiers corresponding to the scores:   1: Predicted bank-initiated return incidence rate between 0.0% - 0.5%   2: Predicted bank-initiated return incidence rate between 0.5% - 1.5%   3: Predicted bank-initiated return incidence rate between 1.5% - 3%   4: Predicted bank-initiated return incidence rate between 3% - 5%   5: Predicted bank-initiated return incidence rate between 5% - 10%   6: Predicted bank-initiated return incidence rate between 10% - 15%   7: Predicted bank-initiated return incidence rate between 15% and 50%   8: Predicted bank-initiated return incidence rate greater than 50%  | 

## Example

```python
from pyplaid.models.bank_initiated_return_risk import BankInitiatedReturnRisk

# TODO update the JSON string below
json = "{}"
# create an instance of BankInitiatedReturnRisk from a JSON string
bank_initiated_return_risk_instance = BankInitiatedReturnRisk.from_json(json)
# print the JSON string representation of the object
print BankInitiatedReturnRisk.to_json()

# convert the object into a dict
bank_initiated_return_risk_dict = bank_initiated_return_risk_instance.to_dict()
# create an instance of BankInitiatedReturnRisk from a dict
bank_initiated_return_risk_form_dict = bank_initiated_return_risk.from_dict(bank_initiated_return_risk_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


