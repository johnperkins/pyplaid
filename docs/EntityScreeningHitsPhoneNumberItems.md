# EntityScreeningHitsPhoneNumberItems

Analyzed phone numbers for the associated hit

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**analysis** | [**MatchSummary**](MatchSummary.md) |  | [optional] 
**data** | [**EntityScreeningHitPhoneNumbers**](EntityScreeningHitPhoneNumbers.md) |  | [optional] 

## Example

```python
from pyplaid.models.entity_screening_hits_phone_number_items import EntityScreeningHitsPhoneNumberItems

# TODO update the JSON string below
json = "{}"
# create an instance of EntityScreeningHitsPhoneNumberItems from a JSON string
entity_screening_hits_phone_number_items_instance = EntityScreeningHitsPhoneNumberItems.from_json(json)
# print the JSON string representation of the object
print EntityScreeningHitsPhoneNumberItems.to_json()

# convert the object into a dict
entity_screening_hits_phone_number_items_dict = entity_screening_hits_phone_number_items_instance.to_dict()
# create an instance of EntityScreeningHitsPhoneNumberItems from a dict
entity_screening_hits_phone_number_items_form_dict = entity_screening_hits_phone_number_items.from_dict(entity_screening_hits_phone_number_items_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


