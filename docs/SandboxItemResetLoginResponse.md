# SandboxItemResetLoginResponse

SandboxItemResetLoginResponse defines the response schema for `/sandbox/item/reset_login`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reset_login** | **bool** | &#x60;true&#x60; if the call succeeded | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.sandbox_item_reset_login_response import SandboxItemResetLoginResponse

# TODO update the JSON string below
json = "{}"
# create an instance of SandboxItemResetLoginResponse from a JSON string
sandbox_item_reset_login_response_instance = SandboxItemResetLoginResponse.from_json(json)
# print the JSON string representation of the object
print SandboxItemResetLoginResponse.to_json()

# convert the object into a dict
sandbox_item_reset_login_response_dict = sandbox_item_reset_login_response_instance.to_dict()
# create an instance of SandboxItemResetLoginResponse from a dict
sandbox_item_reset_login_response_form_dict = sandbox_item_reset_login_response.from_dict(sandbox_item_reset_login_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


