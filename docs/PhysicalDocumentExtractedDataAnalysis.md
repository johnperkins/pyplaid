# PhysicalDocumentExtractedDataAnalysis

Analysis of the data extracted from the submitted document.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | [**DocumentNameMatchCode**](DocumentNameMatchCode.md) |  | 
**date_of_birth** | [**DocumentDateOfBirthMatchCode**](DocumentDateOfBirthMatchCode.md) |  | 
**expiration_date** | [**ExpirationDate**](ExpirationDate.md) |  | 
**issuing_country** | [**IssuingCountry**](IssuingCountry.md) |  | 

## Example

```python
from pyplaid.models.physical_document_extracted_data_analysis import PhysicalDocumentExtractedDataAnalysis

# TODO update the JSON string below
json = "{}"
# create an instance of PhysicalDocumentExtractedDataAnalysis from a JSON string
physical_document_extracted_data_analysis_instance = PhysicalDocumentExtractedDataAnalysis.from_json(json)
# print the JSON string representation of the object
print PhysicalDocumentExtractedDataAnalysis.to_json()

# convert the object into a dict
physical_document_extracted_data_analysis_dict = physical_document_extracted_data_analysis_instance.to_dict()
# create an instance of PhysicalDocumentExtractedDataAnalysis from a dict
physical_document_extracted_data_analysis_form_dict = physical_document_extracted_data_analysis.from_dict(physical_document_extracted_data_analysis_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


