# Credit1099Recipient

An object representing a recipient used in both 1099-K and 1099-MISC tax documents.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address** | [**CreditPayStubAddress**](CreditPayStubAddress.md) |  | [optional] 
**name** | **str** | Name of recipient. | [optional] 
**tin** | **str** | Tax identification number of recipient. | [optional] 
**account_number** | **str** | Account number number of recipient. | [optional] 
**facta_filing_requirement** | **str** | Checked if FACTA is a filing requirement. | [optional] 
**second_tin_exists** | **str** | Checked if 2nd TIN exists. | [optional] 

## Example

```python
from pyplaid.models.credit1099_recipient import Credit1099Recipient

# TODO update the JSON string below
json = "{}"
# create an instance of Credit1099Recipient from a JSON string
credit1099_recipient_instance = Credit1099Recipient.from_json(json)
# print the JSON string representation of the object
print Credit1099Recipient.to_json()

# convert the object into a dict
credit1099_recipient_dict = credit1099_recipient_instance.to_dict()
# create an instance of Credit1099Recipient from a dict
credit1099_recipient_form_dict = credit1099_recipient.from_dict(credit1099_recipient_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


