# AssetTransaction

An object representing...

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**asset_transaction_detail** | [**AssetTransactionDetail**](AssetTransactionDetail.md) |  | 
**asset_transaction_descripton** | [**List[AssetTransactionDescription]**](AssetTransactionDescription.md) | No documentation available | 

## Example

```python
from pyplaid.models.asset_transaction import AssetTransaction

# TODO update the JSON string below
json = "{}"
# create an instance of AssetTransaction from a JSON string
asset_transaction_instance = AssetTransaction.from_json(json)
# print the JSON string representation of the object
print AssetTransaction.to_json()

# convert the object into a dict
asset_transaction_dict = asset_transaction_instance.to_dict()
# create an instance of AssetTransaction from a dict
asset_transaction_form_dict = asset_transaction.from_dict(asset_transaction_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


