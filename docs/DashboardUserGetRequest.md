# DashboardUserGetRequest

Request input for fetching a dashboard user

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dashboard_user_id** | **str** | ID of the associated user. | 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 

## Example

```python
from pyplaid.models.dashboard_user_get_request import DashboardUserGetRequest

# TODO update the JSON string below
json = "{}"
# create an instance of DashboardUserGetRequest from a JSON string
dashboard_user_get_request_instance = DashboardUserGetRequest.from_json(json)
# print the JSON string representation of the object
print DashboardUserGetRequest.to_json()

# convert the object into a dict
dashboard_user_get_request_dict = dashboard_user_get_request_instance.to_dict()
# create an instance of DashboardUserGetRequest from a dict
dashboard_user_get_request_form_dict = dashboard_user_get_request.from_dict(dashboard_user_get_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


