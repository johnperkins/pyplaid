# PayFrequency

The frequency of the pay period.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value** | [**PayFrequencyValue**](PayFrequencyValue.md) |  | 
**verification_status** | [**VerificationStatus**](VerificationStatus.md) |  | 

## Example

```python
from pyplaid.models.pay_frequency import PayFrequency

# TODO update the JSON string below
json = "{}"
# create an instance of PayFrequency from a JSON string
pay_frequency_instance = PayFrequency.from_json(json)
# print the JSON string representation of the object
print PayFrequency.to_json()

# convert the object into a dict
pay_frequency_dict = pay_frequency_instance.to_dict()
# create an instance of PayFrequency from a dict
pay_frequency_form_dict = pay_frequency.from_dict(pay_frequency_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


