# SandboxBankTransferFireWebhookResponse

Defines the response schema for `/sandbox/bank_transfer/fire_webhook`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.sandbox_bank_transfer_fire_webhook_response import SandboxBankTransferFireWebhookResponse

# TODO update the JSON string below
json = "{}"
# create an instance of SandboxBankTransferFireWebhookResponse from a JSON string
sandbox_bank_transfer_fire_webhook_response_instance = SandboxBankTransferFireWebhookResponse.from_json(json)
# print the JSON string representation of the object
print SandboxBankTransferFireWebhookResponse.to_json()

# convert the object into a dict
sandbox_bank_transfer_fire_webhook_response_dict = sandbox_bank_transfer_fire_webhook_response_instance.to_dict()
# create an instance of SandboxBankTransferFireWebhookResponse from a dict
sandbox_bank_transfer_fire_webhook_response_form_dict = sandbox_bank_transfer_fire_webhook_response.from_dict(sandbox_bank_transfer_fire_webhook_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


