# ItemStatusInvestments

Information about the last successful and failed investments update for the Item.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**last_successful_update** | **datetime** | [ISO 8601](https://wikipedia.org/wiki/ISO_8601) timestamp of the last successful investments update for the Item. The status will update each time Plaid successfully connects with the institution, regardless of whether any new data is available in the update. | [optional] 
**last_failed_update** | **datetime** | [ISO 8601](https://wikipedia.org/wiki/ISO_8601) timestamp of the last failed investments update for the Item. The status will update each time Plaid fails an attempt to connect with the institution, regardless of whether any new data is available in the update. | [optional] 

## Example

```python
from pyplaid.models.item_status_investments import ItemStatusInvestments

# TODO update the JSON string below
json = "{}"
# create an instance of ItemStatusInvestments from a JSON string
item_status_investments_instance = ItemStatusInvestments.from_json(json)
# print the JSON string representation of the object
print ItemStatusInvestments.to_json()

# convert the object into a dict
item_status_investments_dict = item_status_investments_instance.to_dict()
# create an instance of ItemStatusInvestments from a dict
item_status_investments_form_dict = item_status_investments.from_dict(item_status_investments_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


