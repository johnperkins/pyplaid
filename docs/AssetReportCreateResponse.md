# AssetReportCreateResponse

AssetReportCreateResponse defines the response schema for `/asset_report/create`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**asset_report_token** | **str** | A token that can be provided to endpoints such as &#x60;/asset_report/get&#x60; or &#x60;/asset_report/pdf/get&#x60; to fetch or update an Asset Report. | 
**asset_report_id** | **str** | A unique ID identifying an Asset Report. Like all Plaid identifiers, this ID is case sensitive. | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.asset_report_create_response import AssetReportCreateResponse

# TODO update the JSON string below
json = "{}"
# create an instance of AssetReportCreateResponse from a JSON string
asset_report_create_response_instance = AssetReportCreateResponse.from_json(json)
# print the JSON string representation of the object
print AssetReportCreateResponse.to_json()

# convert the object into a dict
asset_report_create_response_dict = asset_report_create_response_instance.to_dict()
# create an instance of AssetReportCreateResponse from a dict
asset_report_create_response_form_dict = asset_report_create_response.from_dict(asset_report_create_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


