# WalletTransactionListRequest

WalletTransactionListRequest defines the request schema for `/wallet/transaction/list`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**wallet_id** | **str** | The ID of the e-wallet to fetch transactions from | 
**cursor** | **str** | A base64 value representing the latest transaction that has already been requested. Set this to &#x60;next_cursor&#x60; received from the previous &#x60;/wallet/transaction/list&#x60; request. If provided, the response will only contain transactions created before that transaction. If omitted, the response will contain transactions starting from the most recent, and in descending order by the &#x60;created_at&#x60; time. | [optional] 
**count** | **int** | The number of transactions to fetch | [optional] [default to 10]
**options** | [**WalletTransactionListRequestOptions**](WalletTransactionListRequestOptions.md) |  | [optional] 

## Example

```python
from pyplaid.models.wallet_transaction_list_request import WalletTransactionListRequest

# TODO update the JSON string below
json = "{}"
# create an instance of WalletTransactionListRequest from a JSON string
wallet_transaction_list_request_instance = WalletTransactionListRequest.from_json(json)
# print the JSON string representation of the object
print WalletTransactionListRequest.to_json()

# convert the object into a dict
wallet_transaction_list_request_dict = wallet_transaction_list_request_instance.to_dict()
# create an instance of WalletTransactionListRequest from a dict
wallet_transaction_list_request_form_dict = wallet_transaction_list_request.from_dict(wallet_transaction_list_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


