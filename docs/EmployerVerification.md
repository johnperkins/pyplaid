# EmployerVerification

An object containing employer data.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | Name of employer. | [optional] 

## Example

```python
from pyplaid.models.employer_verification import EmployerVerification

# TODO update the JSON string below
json = "{}"
# create an instance of EmployerVerification from a JSON string
employer_verification_instance = EmployerVerification.from_json(json)
# print the JSON string representation of the object
print EmployerVerification.to_json()

# convert the object into a dict
employer_verification_dict = employer_verification_instance.to_dict()
# create an instance of EmployerVerification from a dict
employer_verification_form_dict = employer_verification.from_dict(employer_verification_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


