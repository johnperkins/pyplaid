# StandaloneCurrencyCodeList

The following currency codes are supported by Plaid.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**iso_currency_code** | **str** | Plaid supports all ISO 4217 currency codes. | 
**unofficial_currency_code** | **str** | List of unofficial currency codes | 

## Example

```python
from pyplaid.models.standalone_currency_code_list import StandaloneCurrencyCodeList

# TODO update the JSON string below
json = "{}"
# create an instance of StandaloneCurrencyCodeList from a JSON string
standalone_currency_code_list_instance = StandaloneCurrencyCodeList.from_json(json)
# print the JSON string representation of the object
print StandaloneCurrencyCodeList.to_json()

# convert the object into a dict
standalone_currency_code_list_dict = standalone_currency_code_list_instance.to_dict()
# create an instance of StandaloneCurrencyCodeList from a dict
standalone_currency_code_list_form_dict = standalone_currency_code_list.from_dict(standalone_currency_code_list_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


