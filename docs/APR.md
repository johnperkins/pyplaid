# APR

Information about the APR on the account.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**apr_percentage** | **float** | Annual Percentage Rate applied.  | 
**apr_type** | **str** | The type of balance to which the APR applies. | 
**balance_subject_to_apr** | **float** | Amount of money that is subjected to the APR if a balance was carried beyond payment due date. How it is calculated can vary by card issuer. It is often calculated as an average daily balance. | 
**interest_charge_amount** | **float** | Amount of money charged due to interest from last statement. | 

## Example

```python
from pyplaid.models.apr import APR

# TODO update the JSON string below
json = "{}"
# create an instance of APR from a JSON string
apr_instance = APR.from_json(json)
# print the JSON string representation of the object
print APR.to_json()

# convert the object into a dict
apr_dict = apr_instance.to_dict()
# create an instance of APR from a dict
apr_form_dict = apr.from_dict(apr_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


