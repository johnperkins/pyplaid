# EmployersSearchRequest

EmployersSearchRequest defines the request schema for `/employers/search`.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**query** | **str** | The employer name to be searched for. | 
**products** | **List[str]** | The Plaid products the returned employers should support. Currently, this field must be set to &#x60;\&quot;deposit_switch\&quot;&#x60;. | 

## Example

```python
from pyplaid.models.employers_search_request import EmployersSearchRequest

# TODO update the JSON string below
json = "{}"
# create an instance of EmployersSearchRequest from a JSON string
employers_search_request_instance = EmployersSearchRequest.from_json(json)
# print the JSON string representation of the object
print EmployersSearchRequest.to_json()

# convert the object into a dict
employers_search_request_dict = employers_search_request_instance.to_dict()
# create an instance of EmployersSearchRequest from a dict
employers_search_request_form_dict = employers_search_request.from_dict(employers_search_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


