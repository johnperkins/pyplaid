# AssetReportItem

A representation of an Item within an Asset Report.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**item_id** | **str** | The &#x60;item_id&#x60; of the Item associated with this webhook, warning, or error | 
**institution_name** | **str** | The full financial institution name associated with the Item. | 
**institution_id** | **str** | The id of the financial institution associated with the Item. | 
**date_last_updated** | **datetime** | The date and time when this Item’s data was last retrieved from the financial institution, in [ISO 8601](https://wikipedia.org/wiki/ISO_8601) format. | 
**accounts** | [**List[AccountAssets]**](AccountAssets.md) | Data about each of the accounts open on the Item. | 

## Example

```python
from pyplaid.models.asset_report_item import AssetReportItem

# TODO update the JSON string below
json = "{}"
# create an instance of AssetReportItem from a JSON string
asset_report_item_instance = AssetReportItem.from_json(json)
# print the JSON string representation of the object
print AssetReportItem.to_json()

# convert the object into a dict
asset_report_item_dict = asset_report_item_instance.to_dict()
# create an instance of AssetReportItem from a dict
asset_report_item_form_dict = asset_report_item.from_dict(asset_report_item_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


