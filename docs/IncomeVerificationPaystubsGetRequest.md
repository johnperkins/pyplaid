# IncomeVerificationPaystubsGetRequest

IncomeVerificationPaystubsGetRequest defines the request schema for `/income/verification/paystubs/get`.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**income_verification_id** | **str** | The ID of the verification for which to get paystub information. | [optional] 
**access_token** | **str** | The access token associated with the Item data is being requested for. | [optional] 

## Example

```python
from pyplaid.models.income_verification_paystubs_get_request import IncomeVerificationPaystubsGetRequest

# TODO update the JSON string below
json = "{}"
# create an instance of IncomeVerificationPaystubsGetRequest from a JSON string
income_verification_paystubs_get_request_instance = IncomeVerificationPaystubsGetRequest.from_json(json)
# print the JSON string representation of the object
print IncomeVerificationPaystubsGetRequest.to_json()

# convert the object into a dict
income_verification_paystubs_get_request_dict = income_verification_paystubs_get_request_instance.to_dict()
# create an instance of IncomeVerificationPaystubsGetRequest from a dict
income_verification_paystubs_get_request_form_dict = income_verification_paystubs_get_request.from_dict(income_verification_paystubs_get_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


