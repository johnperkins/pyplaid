# StandaloneInvestmentTransactionType

Valid values for investment transaction types and subtypes. Note that transactions representing inflow of cash will appear as negative amounts, outflow of cash will appear as positive amounts.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**buy** | **str** | Buying an investment | 
**sell** | **str** | Selling an investment | 
**cancel** | **str** | A cancellation of a pending transaction | 
**cash** | **str** | Activity that modifies a cash position | 
**fee** | **str** | Fees on the account, e.g. commission, bookkeeping, options-related. | 
**transfer** | **str** | Activity that modifies a position, but not through buy/sell activity e.g. options exercise, portfolio transfer | 

## Example

```python
from pyplaid.models.standalone_investment_transaction_type import StandaloneInvestmentTransactionType

# TODO update the JSON string below
json = "{}"
# create an instance of StandaloneInvestmentTransactionType from a JSON string
standalone_investment_transaction_type_instance = StandaloneInvestmentTransactionType.from_json(json)
# print the JSON string representation of the object
print StandaloneInvestmentTransactionType.to_json()

# convert the object into a dict
standalone_investment_transaction_type_dict = standalone_investment_transaction_type_instance.to_dict()
# create an instance of StandaloneInvestmentTransactionType from a dict
standalone_investment_transaction_type_form_dict = standalone_investment_transaction_type.from_dict(standalone_investment_transaction_type_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


