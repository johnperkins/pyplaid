# AutomaticallyVerifiedWebhook

Fired when an Item is verified via automated micro-deposits. We recommend communicating to your users when this event is received to notify them that their account is verified and ready for use.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**webhook_type** | **str** | &#x60;AUTH&#x60; | 
**webhook_code** | **str** | &#x60;AUTOMATICALLY_VERIFIED&#x60; | 
**account_id** | **str** | The &#x60;account_id&#x60; of the account associated with the webhook | 
**item_id** | **str** | The &#x60;item_id&#x60; of the Item associated with this webhook, warning, or error | 
**environment** | [**WebhookEnvironmentValues**](WebhookEnvironmentValues.md) |  | 

## Example

```python
from pyplaid.models.automatically_verified_webhook import AutomaticallyVerifiedWebhook

# TODO update the JSON string below
json = "{}"
# create an instance of AutomaticallyVerifiedWebhook from a JSON string
automatically_verified_webhook_instance = AutomaticallyVerifiedWebhook.from_json(json)
# print the JSON string representation of the object
print AutomaticallyVerifiedWebhook.to_json()

# convert the object into a dict
automatically_verified_webhook_dict = automatically_verified_webhook_instance.to_dict()
# create an instance of AutomaticallyVerifiedWebhook from a dict
automatically_verified_webhook_form_dict = automatically_verified_webhook.from_dict(automatically_verified_webhook_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


