# CreditSessionPayrollIncomeResult

The details of a digital payroll income verification in Link

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**num_paystubs_retrieved** | **int** | The number of paystubs retrieved from a payroll provider. | [optional] 
**num_w2s_retrieved** | **int** | The number of w2s retrieved from a payroll provider. | [optional] 
**institution_id** | **str** | The Plaid Institution ID associated with the Item. | [optional] 
**institution_name** | **str** | The Institution Name associated with the Item. | [optional] 

## Example

```python
from pyplaid.models.credit_session_payroll_income_result import CreditSessionPayrollIncomeResult

# TODO update the JSON string below
json = "{}"
# create an instance of CreditSessionPayrollIncomeResult from a JSON string
credit_session_payroll_income_result_instance = CreditSessionPayrollIncomeResult.from_json(json)
# print the JSON string representation of the object
print CreditSessionPayrollIncomeResult.to_json()

# convert the object into a dict
credit_session_payroll_income_result_dict = credit_session_payroll_income_result_instance.to_dict()
# create an instance of CreditSessionPayrollIncomeResult from a dict
credit_session_payroll_income_result_form_dict = credit_session_payroll_income_result.from_dict(credit_session_payroll_income_result_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


