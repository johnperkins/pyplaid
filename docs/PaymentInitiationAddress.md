# PaymentInitiationAddress

The optional address of the payment recipient.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**street** | **List[str]** | An array of length 1-2 representing the street address where the recipient is located. Maximum of 70 characters. | 
**city** | **str** | The city where the recipient is located. Maximum of 35 characters. | 
**postal_code** | **str** | The postal code where the recipient is located. Maximum of 16 characters. | 
**country** | **str** | The ISO 3166-1 alpha-2 country code where the recipient is located. | 

## Example

```python
from pyplaid.models.payment_initiation_address import PaymentInitiationAddress

# TODO update the JSON string below
json = "{}"
# create an instance of PaymentInitiationAddress from a JSON string
payment_initiation_address_instance = PaymentInitiationAddress.from_json(json)
# print the JSON string representation of the object
print PaymentInitiationAddress.to_json()

# convert the object into a dict
payment_initiation_address_dict = payment_initiation_address_instance.to_dict()
# create an instance of PaymentInitiationAddress from a dict
payment_initiation_address_form_dict = payment_initiation_address.from_dict(payment_initiation_address_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


