# TransactionsRecurringGetResponse

TransactionsRecurringGetResponse defines the response schema for `/transactions/recurring/get`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**inflow_streams** | [**List[TransactionStream]**](TransactionStream.md) | An array of depository transaction streams. | 
**outflow_streams** | [**List[TransactionStream]**](TransactionStream.md) | An array of expense transaction streams. | 
**updated_datetime** | **datetime** | Timestamp in [ISO 8601](https://wikipedia.org/wiki/ISO_8601) format (&#x60;YYYY-MM-DDTHH:mm:ssZ&#x60;) indicating the last time transaction streams for the given account were updated on | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.transactions_recurring_get_response import TransactionsRecurringGetResponse

# TODO update the JSON string below
json = "{}"
# create an instance of TransactionsRecurringGetResponse from a JSON string
transactions_recurring_get_response_instance = TransactionsRecurringGetResponse.from_json(json)
# print the JSON string representation of the object
print TransactionsRecurringGetResponse.to_json()

# convert the object into a dict
transactions_recurring_get_response_dict = transactions_recurring_get_response_instance.to_dict()
# create an instance of TransactionsRecurringGetResponse from a dict
transactions_recurring_get_response_form_dict = transactions_recurring_get_response.from_dict(transactions_recurring_get_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


