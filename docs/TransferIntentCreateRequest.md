# TransferIntentCreateRequest

Defines the request schema for `/transfer/intent/create`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**account_id** | **str** | The Plaid &#x60;account_id&#x60; for the account that will be debited or credited. | [optional] 
**mode** | [**TransferIntentCreateMode**](TransferIntentCreateMode.md) |  | 
**amount** | **str** | The amount of the transfer (decimal string with two digits of precision e.g. \&quot;10.00\&quot;). | 
**description** | **str** | A description for the underlying transfer. Maximum of 8 characters. | 
**ach_class** | [**ACHClass**](ACHClass.md) |  | [optional] 
**origination_account_id** | **str** | Plaid’s unique identifier for the origination account for the intent. If not provided, the default account will be used. | [optional] 
**user** | [**TransferUserInRequest**](TransferUserInRequest.md) |  | 
**metadata** | **Dict[str, str]** | The Metadata object is a mapping of client-provided string fields to any string value. The following limitations apply: - The JSON values must be Strings (no nested JSON objects allowed) - Only ASCII characters may be used - Maximum of 50 key/value pairs - Maximum key length of 40 characters - Maximum value length of 500 characters  | [optional] 
**iso_currency_code** | **str** | The currency of the transfer amount, e.g. \&quot;USD\&quot; | [optional] 
**require_guarantee** | **bool** | When &#x60;true&#x60;, the transfer requires a &#x60;GUARANTEED&#x60; decision by Plaid to proceed (Guarantee customers only). | [optional] [default to False]

## Example

```python
from pyplaid.models.transfer_intent_create_request import TransferIntentCreateRequest

# TODO update the JSON string below
json = "{}"
# create an instance of TransferIntentCreateRequest from a JSON string
transfer_intent_create_request_instance = TransferIntentCreateRequest.from_json(json)
# print the JSON string representation of the object
print TransferIntentCreateRequest.to_json()

# convert the object into a dict
transfer_intent_create_request_dict = transfer_intent_create_request_instance.to_dict()
# create an instance of TransferIntentCreateRequest from a dict
transfer_intent_create_request_form_dict = transfer_intent_create_request.from_dict(transfer_intent_create_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


