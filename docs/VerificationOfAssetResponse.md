# VerificationOfAssetResponse

No documentation available

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**assets** | [**Assets**](Assets.md) |  | 

## Example

```python
from pyplaid.models.verification_of_asset_response import VerificationOfAssetResponse

# TODO update the JSON string below
json = "{}"
# create an instance of VerificationOfAssetResponse from a JSON string
verification_of_asset_response_instance = VerificationOfAssetResponse.from_json(json)
# print the JSON string representation of the object
print VerificationOfAssetResponse.to_json()

# convert the object into a dict
verification_of_asset_response_dict = verification_of_asset_response_instance.to_dict()
# create an instance of VerificationOfAssetResponse from a dict
verification_of_asset_response_form_dict = verification_of_asset_response.from_dict(verification_of_asset_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


