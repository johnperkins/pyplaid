# SandboxProcessorTokenCreateRequest

SandboxProcessorTokenCreateRequest defines the request schema for `/sandbox/processor_token/create`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**institution_id** | **str** | The ID of the institution the Item will be associated with | 
**options** | [**SandboxProcessorTokenCreateRequestOptions**](SandboxProcessorTokenCreateRequestOptions.md) |  | [optional] 

## Example

```python
from pyplaid.models.sandbox_processor_token_create_request import SandboxProcessorTokenCreateRequest

# TODO update the JSON string below
json = "{}"
# create an instance of SandboxProcessorTokenCreateRequest from a JSON string
sandbox_processor_token_create_request_instance = SandboxProcessorTokenCreateRequest.from_json(json)
# print the JSON string representation of the object
print SandboxProcessorTokenCreateRequest.to_json()

# convert the object into a dict
sandbox_processor_token_create_request_dict = sandbox_processor_token_create_request_instance.to_dict()
# create an instance of SandboxProcessorTokenCreateRequest from a dict
sandbox_processor_token_create_request_form_dict = sandbox_processor_token_create_request.from_dict(sandbox_processor_token_create_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


