# EntityScreeningHitNamesItems

Analyzed names for the associated hit

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**analysis** | [**MatchSummary**](MatchSummary.md) |  | [optional] 
**data** | [**EntityScreeningHitNames**](EntityScreeningHitNames.md) |  | [optional] 

## Example

```python
from pyplaid.models.entity_screening_hit_names_items import EntityScreeningHitNamesItems

# TODO update the JSON string below
json = "{}"
# create an instance of EntityScreeningHitNamesItems from a JSON string
entity_screening_hit_names_items_instance = EntityScreeningHitNamesItems.from_json(json)
# print the JSON string representation of the object
print EntityScreeningHitNamesItems.to_json()

# convert the object into a dict
entity_screening_hit_names_items_dict = entity_screening_hit_names_items_instance.to_dict()
# create an instance of EntityScreeningHitNamesItems from a dict
entity_screening_hit_names_items_form_dict = entity_screening_hit_names_items.from_dict(entity_screening_hit_names_items_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


