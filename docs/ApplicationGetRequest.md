# ApplicationGetRequest

ApplicationGetRequest defines the schema for `/application/get`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | 
**application_id** | **str** | This field will map to the application ID that is returned from /item/applications/list, or provided to the institution in an oauth redirect. | 

## Example

```python
from pyplaid.models.application_get_request import ApplicationGetRequest

# TODO update the JSON string below
json = "{}"
# create an instance of ApplicationGetRequest from a JSON string
application_get_request_instance = ApplicationGetRequest.from_json(json)
# print the JSON string representation of the object
print ApplicationGetRequest.to_json()

# convert the object into a dict
application_get_request_dict = application_get_request_instance.to_dict()
# create an instance of ApplicationGetRequest from a dict
application_get_request_form_dict = application_get_request.from_dict(application_get_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


