# AssetReportRefreshRequestOptions

An optional object to filter `/asset_report/refresh` results. If provided, cannot be `null`. If not specified, the `options` from the original call to `/asset_report/create` will be used.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_report_id** | **str** | Client-generated identifier, which can be used by lenders to track loan applications. | [optional] 
**webhook** | **str** | URL to which Plaid will send Assets webhooks, for example when the requested Asset Report is ready. | [optional] 
**user** | [**AssetReportUser**](AssetReportUser.md) |  | [optional] 

## Example

```python
from pyplaid.models.asset_report_refresh_request_options import AssetReportRefreshRequestOptions

# TODO update the JSON string below
json = "{}"
# create an instance of AssetReportRefreshRequestOptions from a JSON string
asset_report_refresh_request_options_instance = AssetReportRefreshRequestOptions.from_json(json)
# print the JSON string representation of the object
print AssetReportRefreshRequestOptions.to_json()

# convert the object into a dict
asset_report_refresh_request_options_dict = asset_report_refresh_request_options_instance.to_dict()
# create an instance of AssetReportRefreshRequestOptions from a dict
asset_report_refresh_request_options_form_dict = asset_report_refresh_request_options.from_dict(asset_report_refresh_request_options_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


