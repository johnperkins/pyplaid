# IdentityVerificationRetriedWebhook

Fired when identity verification has been retried, which can be triggered via the dashboard or the API.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**webhook_type** | **str** | &#x60;IDENTITY_VERIFICATION&#x60; | 
**webhook_code** | **str** | &#x60;RETRIED&#x60; | 
**identity_verification_id** | **object** | The ID of the associated Identity Verification attempt. | 
**environment** | [**WebhookEnvironmentValues**](WebhookEnvironmentValues.md) |  | 

## Example

```python
from pyplaid.models.identity_verification_retried_webhook import IdentityVerificationRetriedWebhook

# TODO update the JSON string below
json = "{}"
# create an instance of IdentityVerificationRetriedWebhook from a JSON string
identity_verification_retried_webhook_instance = IdentityVerificationRetriedWebhook.from_json(json)
# print the JSON string representation of the object
print IdentityVerificationRetriedWebhook.to_json()

# convert the object into a dict
identity_verification_retried_webhook_dict = identity_verification_retried_webhook_instance.to_dict()
# create an instance of IdentityVerificationRetriedWebhook from a dict
identity_verification_retried_webhook_form_dict = identity_verification_retried_webhook.from_dict(identity_verification_retried_webhook_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


