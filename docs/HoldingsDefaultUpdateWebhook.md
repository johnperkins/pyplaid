# HoldingsDefaultUpdateWebhook

Fired when new or updated holdings have been detected on an investment account. The webhook typically fires in response to any newly added holdings or price changes to existing holdings, most commonly after market close.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**webhook_type** | **str** | &#x60;HOLDINGS&#x60; | 
**webhook_code** | **str** | &#x60;DEFAULT_UPDATE&#x60; | 
**item_id** | **str** | The &#x60;item_id&#x60; of the Item associated with this webhook, warning, or error | 
**error** | [**PlaidError**](PlaidError.md) |  | [optional] 
**new_holdings** | **float** | The number of new holdings reported since the last time this webhook was fired. | 
**updated_holdings** | **float** | The number of updated holdings reported since the last time this webhook was fired. | 
**environment** | [**WebhookEnvironmentValues**](WebhookEnvironmentValues.md) |  | 

## Example

```python
from pyplaid.models.holdings_default_update_webhook import HoldingsDefaultUpdateWebhook

# TODO update the JSON string below
json = "{}"
# create an instance of HoldingsDefaultUpdateWebhook from a JSON string
holdings_default_update_webhook_instance = HoldingsDefaultUpdateWebhook.from_json(json)
# print the JSON string representation of the object
print HoldingsDefaultUpdateWebhook.to_json()

# convert the object into a dict
holdings_default_update_webhook_dict = holdings_default_update_webhook_instance.to_dict()
# create an instance of HoldingsDefaultUpdateWebhook from a dict
holdings_default_update_webhook_form_dict = holdings_default_update_webhook.from_dict(holdings_default_update_webhook_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


