# VerificationExpiredWebhook

Fired when an Item was not verified via automated micro-deposits after seven days since the automated micro-deposit was made.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**webhook_type** | **str** | &#x60;AUTH&#x60; | 
**webhook_code** | **str** | &#x60;VERIFICATION_EXPIRED&#x60; | 
**item_id** | **str** | The &#x60;item_id&#x60; of the Item associated with this webhook, warning, or error | 
**account_id** | **str** | The &#x60;account_id&#x60; of the account associated with the webhook | 
**environment** | [**WebhookEnvironmentValues**](WebhookEnvironmentValues.md) |  | 

## Example

```python
from pyplaid.models.verification_expired_webhook import VerificationExpiredWebhook

# TODO update the JSON string below
json = "{}"
# create an instance of VerificationExpiredWebhook from a JSON string
verification_expired_webhook_instance = VerificationExpiredWebhook.from_json(json)
# print the JSON string representation of the object
print VerificationExpiredWebhook.to_json()

# convert the object into a dict
verification_expired_webhook_dict = verification_expired_webhook_instance.to_dict()
# create an instance of VerificationExpiredWebhook from a dict
verification_expired_webhook_form_dict = verification_expired_webhook.from_dict(verification_expired_webhook_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


