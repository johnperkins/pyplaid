# CreditBankIncomeHistoricalSummary

The end user's monthly summary for the income source(s).

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total_amount** | **float** | Total amount of earnings for the income source(s) of the user for the month in the summary. | [optional] 
**iso_currency_code** | **str** | The ISO 4217 currency code of the amount or balance. | [optional] 
**unofficial_currency_code** | **str** | The unofficial currency code associated with the amount or balance. Always &#x60;null&#x60; if &#x60;iso_currency_code&#x60; is non-null. Unofficial currency codes are used for currencies that do not have official ISO currency codes, such as cryptocurrencies and the currencies of certain countries. | [optional] 
**start_date** | **date** | The start date of the period covered in this monthly summary. This date will be the first day of the month, unless the month being covered is a partial month because it is the first month included in the summary and the date range being requested does not begin with the first day of the month. The date will be returned in an ISO 8601 format (YYYY-MM-DD). | [optional] 
**end_date** | **date** | The end date of the period included in this monthly summary. This date will be the last day of the month, unless the month being covered is a partial month because it is the last month included in the summary and the date range being requested does not end with the last day of the month. The date will be returned in an ISO 8601 format (YYYY-MM-DD). | [optional] 
**transactions** | [**List[CreditBankIncomeTransaction]**](CreditBankIncomeTransaction.md) |  | [optional] 

## Example

```python
from pyplaid.models.credit_bank_income_historical_summary import CreditBankIncomeHistoricalSummary

# TODO update the JSON string below
json = "{}"
# create an instance of CreditBankIncomeHistoricalSummary from a JSON string
credit_bank_income_historical_summary_instance = CreditBankIncomeHistoricalSummary.from_json(json)
# print the JSON string representation of the object
print CreditBankIncomeHistoricalSummary.to_json()

# convert the object into a dict
credit_bank_income_historical_summary_dict = credit_bank_income_historical_summary_instance.to_dict()
# create an instance of CreditBankIncomeHistoricalSummary from a dict
credit_bank_income_historical_summary_form_dict = credit_bank_income_historical_summary.from_dict(credit_bank_income_historical_summary_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


