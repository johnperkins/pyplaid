# PayrollIncomeRateOfPay

An object representing the rate at which an individual is paid.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pay_rate** | **str** | The rate at which an employee is paid. | [optional] 
**pay_amount** | **float** | The amount at which an employee is paid. | [optional] 

## Example

```python
from pyplaid.models.payroll_income_rate_of_pay import PayrollIncomeRateOfPay

# TODO update the JSON string below
json = "{}"
# create an instance of PayrollIncomeRateOfPay from a JSON string
payroll_income_rate_of_pay_instance = PayrollIncomeRateOfPay.from_json(json)
# print the JSON string representation of the object
print PayrollIncomeRateOfPay.to_json()

# convert the object into a dict
payroll_income_rate_of_pay_dict = payroll_income_rate_of_pay_instance.to_dict()
# create an instance of PayrollIncomeRateOfPay from a dict
payroll_income_rate_of_pay_form_dict = payroll_income_rate_of_pay.from_dict(payroll_income_rate_of_pay_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


