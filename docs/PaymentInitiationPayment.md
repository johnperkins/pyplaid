# PaymentInitiationPayment

PaymentInitiationPayment defines a payment initiation payment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**payment_id** | **str** | The ID of the payment. Like all Plaid identifiers, the &#x60;payment_id&#x60; is case sensitive. | 
**amount** | [**PaymentAmount**](PaymentAmount.md) |  | 
**status** | [**PaymentInitiationPaymentStatus**](PaymentInitiationPaymentStatus.md) |  | 
**recipient_id** | **str** | The ID of the recipient | 
**reference** | **str** | A reference for the payment. | 
**adjusted_reference** | **str** | The value of the reference sent to the bank after adjustment to pass bank validation rules. | [optional] 
**last_status_update** | **datetime** | The date and time of the last time the &#x60;status&#x60; was updated, in IS0 8601 format | 
**schedule** | [**ExternalPaymentScheduleGet**](ExternalPaymentScheduleGet.md) |  | [optional] 
**refund_details** | [**ExternalPaymentRefundDetails**](ExternalPaymentRefundDetails.md) |  | [optional] 
**bacs** | [**SenderBACSNullable**](SenderBACSNullable.md) |  | 
**iban** | **str** | The International Bank Account Number (IBAN) for the sender, if specified in the &#x60;/payment_initiation/payment/create&#x60; call. | 
**refund_ids** | **List[str]** | Refund IDs associated with the payment. | [optional] 
**amount_refunded** | [**PaymentAmountRefunded**](PaymentAmountRefunded.md) |  | [optional] 
**wallet_id** | **str** | The EMI (E-Money Institution) wallet that this payment is associated with, if any. This wallet is used as an intermediary account to enable Plaid to reconcile the settlement of funds for Payment Initiation requests. | [optional] 
**scheme** | [**PaymentScheme**](PaymentScheme.md) |  | [optional] 
**adjusted_scheme** | [**PaymentScheme**](PaymentScheme.md) |  | [optional] 
**consent_id** | **str** | The payment consent ID that this payment was initiated with. Is present only when payment was initiated using the payment consent. | [optional] 
**transaction_id** | **str** | The transaction ID that this payment is associated with, if any. This is present only when a payment was initiated using virtual accounts. | [optional] 

## Example

```python
from pyplaid.models.payment_initiation_payment import PaymentInitiationPayment

# TODO update the JSON string below
json = "{}"
# create an instance of PaymentInitiationPayment from a JSON string
payment_initiation_payment_instance = PaymentInitiationPayment.from_json(json)
# print the JSON string representation of the object
print PaymentInitiationPayment.to_json()

# convert the object into a dict
payment_initiation_payment_dict = payment_initiation_payment_instance.to_dict()
# create an instance of PaymentInitiationPayment from a dict
payment_initiation_payment_form_dict = payment_initiation_payment.from_dict(payment_initiation_payment_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


