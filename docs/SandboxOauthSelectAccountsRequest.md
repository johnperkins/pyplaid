# SandboxOauthSelectAccountsRequest

Defines the request schema for `sandbox/oauth/select_accounts`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**oauth_state_id** | **str** |  | 
**accounts** | **List[str]** |  | 

## Example

```python
from pyplaid.models.sandbox_oauth_select_accounts_request import SandboxOauthSelectAccountsRequest

# TODO update the JSON string below
json = "{}"
# create an instance of SandboxOauthSelectAccountsRequest from a JSON string
sandbox_oauth_select_accounts_request_instance = SandboxOauthSelectAccountsRequest.from_json(json)
# print the JSON string representation of the object
print SandboxOauthSelectAccountsRequest.to_json()

# convert the object into a dict
sandbox_oauth_select_accounts_request_dict = sandbox_oauth_select_accounts_request_instance.to_dict()
# create an instance of SandboxOauthSelectAccountsRequest from a dict
sandbox_oauth_select_accounts_request_form_dict = sandbox_oauth_select_accounts_request.from_dict(sandbox_oauth_select_accounts_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


