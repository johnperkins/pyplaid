# PayrollIncomeObject

An object representing payroll data.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_id** | **str** | ID of the payroll provider account. | 
**pay_stubs** | [**List[CreditPayStub]**](CreditPayStub.md) | Array of pay stubs for the user. | 
**w2s** | [**List[CreditW2]**](CreditW2.md) | Array of tax form W-2s. | 
**form1099s** | [**List[Credit1099]**](Credit1099.md) | Array of tax form 1099s. | 

## Example

```python
from pyplaid.models.payroll_income_object import PayrollIncomeObject

# TODO update the JSON string below
json = "{}"
# create an instance of PayrollIncomeObject from a JSON string
payroll_income_object_instance = PayrollIncomeObject.from_json(json)
# print the JSON string representation of the object
print PayrollIncomeObject.to_json()

# convert the object into a dict
payroll_income_object_dict = payroll_income_object_instance.to_dict()
# create an instance of PayrollIncomeObject from a dict
payroll_income_object_form_dict = payroll_income_object.from_dict(payroll_income_object_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


