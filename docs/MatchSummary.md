# MatchSummary

Summary object reflecting the match result of the associated data

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**summary** | [**MatchSummaryCode**](MatchSummaryCode.md) |  | 

## Example

```python
from pyplaid.models.match_summary import MatchSummary

# TODO update the JSON string below
json = "{}"
# create an instance of MatchSummary from a JSON string
match_summary_instance = MatchSummary.from_json(json)
# print the JSON string representation of the object
print MatchSummary.to_json()

# convert the object into a dict
match_summary_dict = match_summary_instance.to_dict()
# create an instance of MatchSummary from a dict
match_summary_form_dict = match_summary.from_dict(match_summary_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


