# IdentityVerificationListResponse

Paginated list of Plaid sessions.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**identity_verifications** | [**List[IdentityVerification]**](IdentityVerification.md) | List of Plaid sessions | 
**next_cursor** | **str** | An identifier that determines which page of results you receive. | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.identity_verification_list_response import IdentityVerificationListResponse

# TODO update the JSON string below
json = "{}"
# create an instance of IdentityVerificationListResponse from a JSON string
identity_verification_list_response_instance = IdentityVerificationListResponse.from_json(json)
# print the JSON string representation of the object
print IdentityVerificationListResponse.to_json()

# convert the object into a dict
identity_verification_list_response_dict = identity_verification_list_response_instance.to_dict()
# create an instance of IdentityVerificationListResponse from a dict
identity_verification_list_response_form_dict = identity_verification_list_response.from_dict(identity_verification_list_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


