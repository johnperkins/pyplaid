# ProcessorBalanceGetRequest

ProcessorBalanceGetRequest defines the request schema for `/processor/balance/get`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**processor_token** | **str** | The processor token obtained from the Plaid integration partner. Processor tokens are in the format: &#x60;processor-&lt;environment&gt;-&lt;identifier&gt;&#x60; | 
**options** | [**ProcessorBalanceGetRequestOptions**](ProcessorBalanceGetRequestOptions.md) |  | [optional] 

## Example

```python
from pyplaid.models.processor_balance_get_request import ProcessorBalanceGetRequest

# TODO update the JSON string below
json = "{}"
# create an instance of ProcessorBalanceGetRequest from a JSON string
processor_balance_get_request_instance = ProcessorBalanceGetRequest.from_json(json)
# print the JSON string representation of the object
print ProcessorBalanceGetRequest.to_json()

# convert the object into a dict
processor_balance_get_request_dict = processor_balance_get_request_instance.to_dict()
# create an instance of ProcessorBalanceGetRequest from a dict
processor_balance_get_request_form_dict = processor_balance_get_request.from_dict(processor_balance_get_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


