# DateRange

A date range with a start and end date

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**beginning** | **date** | A date in the format YYYY-MM-DD (RFC 3339 Section 5.6). | 
**ending** | **date** | A date in the format YYYY-MM-DD (RFC 3339 Section 5.6). | 

## Example

```python
from pyplaid.models.date_range import DateRange

# TODO update the JSON string below
json = "{}"
# create an instance of DateRange from a JSON string
date_range_instance = DateRange.from_json(json)
# print the JSON string representation of the object
print DateRange.to_json()

# convert the object into a dict
date_range_dict = date_range_instance.to_dict()
# create an instance of DateRange from a dict
date_range_form_dict = date_range.from_dict(date_range_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


