# Deductions

An object with the deduction information found on a paystub.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**subtotals** | [**List[Total]**](Total.md) |  | [optional] 
**breakdown** | [**List[DeductionsBreakdown]**](DeductionsBreakdown.md) |  | 
**totals** | [**List[Total]**](Total.md) |  | [optional] 
**total** | [**DeductionsTotal**](DeductionsTotal.md) |  | 

## Example

```python
from pyplaid.models.deductions import Deductions

# TODO update the JSON string below
json = "{}"
# create an instance of Deductions from a JSON string
deductions_instance = Deductions.from_json(json)
# print the JSON string representation of the object
print Deductions.to_json()

# convert the object into a dict
deductions_dict = deductions_instance.to_dict()
# create an instance of Deductions from a dict
deductions_form_dict = deductions.from_dict(deductions_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


