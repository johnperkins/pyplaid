# ScreeningHitDateOfBirthItem

Analyzed date of birth for the associated hit

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**analysis** | [**MatchSummary**](MatchSummary.md) |  | [optional] 
**data** | [**DateRange**](DateRange.md) |  | [optional] 

## Example

```python
from pyplaid.models.screening_hit_date_of_birth_item import ScreeningHitDateOfBirthItem

# TODO update the JSON string below
json = "{}"
# create an instance of ScreeningHitDateOfBirthItem from a JSON string
screening_hit_date_of_birth_item_instance = ScreeningHitDateOfBirthItem.from_json(json)
# print the JSON string representation of the object
print ScreeningHitDateOfBirthItem.to_json()

# convert the object into a dict
screening_hit_date_of_birth_item_dict = screening_hit_date_of_birth_item_instance.to_dict()
# create an instance of ScreeningHitDateOfBirthItem from a dict
screening_hit_date_of_birth_item_form_dict = screening_hit_date_of_birth_item.from_dict(screening_hit_date_of_birth_item_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


