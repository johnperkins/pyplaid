# TransferRefundCreateResponse

Defines the response schema for `/transfer/refund/create`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**refund** | [**TransferRefund**](TransferRefund.md) |  | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.transfer_refund_create_response import TransferRefundCreateResponse

# TODO update the JSON string below
json = "{}"
# create an instance of TransferRefundCreateResponse from a JSON string
transfer_refund_create_response_instance = TransferRefundCreateResponse.from_json(json)
# print the JSON string representation of the object
print TransferRefundCreateResponse.to_json()

# convert the object into a dict
transfer_refund_create_response_dict = transfer_refund_create_response_instance.to_dict()
# create an instance of TransferRefundCreateResponse from a dict
transfer_refund_create_response_form_dict = transfer_refund_create_response.from_dict(transfer_refund_create_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


