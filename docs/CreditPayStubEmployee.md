# CreditPayStubEmployee

Data about the employee.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address** | [**CreditPayStubAddress**](CreditPayStubAddress.md) |  | 
**name** | **str** | The name of the employee. | 
**marital_status** | **str** | Marital status of the employee - either &#x60;SINGLE&#x60; or &#x60;MARRIED&#x60;. | 
**taxpayer_id** | [**PayStubTaxpayerID**](PayStubTaxpayerID.md) |  | 

## Example

```python
from pyplaid.models.credit_pay_stub_employee import CreditPayStubEmployee

# TODO update the JSON string below
json = "{}"
# create an instance of CreditPayStubEmployee from a JSON string
credit_pay_stub_employee_instance = CreditPayStubEmployee.from_json(json)
# print the JSON string representation of the object
print CreditPayStubEmployee.to_json()

# convert the object into a dict
credit_pay_stub_employee_dict = credit_pay_stub_employee_instance.to_dict()
# create an instance of CreditPayStubEmployee from a dict
credit_pay_stub_employee_form_dict = credit_pay_stub_employee.from_dict(credit_pay_stub_employee_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


