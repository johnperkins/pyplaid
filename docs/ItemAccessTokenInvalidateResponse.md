# ItemAccessTokenInvalidateResponse

ItemAccessTokenInvalidateResponse defines the response schema for `/item/access_token/invalidate`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**new_access_token** | **str** | The access token associated with the Item data is being requested for. | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.item_access_token_invalidate_response import ItemAccessTokenInvalidateResponse

# TODO update the JSON string below
json = "{}"
# create an instance of ItemAccessTokenInvalidateResponse from a JSON string
item_access_token_invalidate_response_instance = ItemAccessTokenInvalidateResponse.from_json(json)
# print the JSON string representation of the object
print ItemAccessTokenInvalidateResponse.to_json()

# convert the object into a dict
item_access_token_invalidate_response_dict = item_access_token_invalidate_response_instance.to_dict()
# create an instance of ItemAccessTokenInvalidateResponse from a dict
item_access_token_invalidate_response_form_dict = item_access_token_invalidate_response.from_dict(item_access_token_invalidate_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


