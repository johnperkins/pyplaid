# BankTransfersEventsUpdateWebhook

Fired when new bank transfer events are available. Receiving this webhook indicates you should fetch the new events from `/bank_transfer/event/sync`.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**webhook_type** | **str** | &#x60;BANK_TRANSFERS&#x60; | 
**webhook_code** | **str** | &#x60;BANK_TRANSFERS_EVENTS_UPDATE&#x60; | 
**environment** | [**WebhookEnvironmentValues**](WebhookEnvironmentValues.md) |  | 

## Example

```python
from pyplaid.models.bank_transfers_events_update_webhook import BankTransfersEventsUpdateWebhook

# TODO update the JSON string below
json = "{}"
# create an instance of BankTransfersEventsUpdateWebhook from a JSON string
bank_transfers_events_update_webhook_instance = BankTransfersEventsUpdateWebhook.from_json(json)
# print the JSON string representation of the object
print BankTransfersEventsUpdateWebhook.to_json()

# convert the object into a dict
bank_transfers_events_update_webhook_dict = bank_transfers_events_update_webhook_instance.to_dict()
# create an instance of BankTransfersEventsUpdateWebhook from a dict
bank_transfers_events_update_webhook_form_dict = bank_transfers_events_update_webhook.from_dict(bank_transfers_events_update_webhook_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


