# TransactionsRuleType

Transaction rule's match type. For TRANSACTION_ID field, EXACT_MATCH is available. Matches are case sensitive. 

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


