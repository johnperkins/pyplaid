# PartnerEndCustomerAssetsUnderManagement

Assets under management for the given end customer. Required for end customers with monthly service commitments.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amount** | **float** |  | 
**iso_currency_code** | **str** |  | 

## Example

```python
from pyplaid.models.partner_end_customer_assets_under_management import PartnerEndCustomerAssetsUnderManagement

# TODO update the JSON string below
json = "{}"
# create an instance of PartnerEndCustomerAssetsUnderManagement from a JSON string
partner_end_customer_assets_under_management_instance = PartnerEndCustomerAssetsUnderManagement.from_json(json)
# print the JSON string representation of the object
print PartnerEndCustomerAssetsUnderManagement.to_json()

# convert the object into a dict
partner_end_customer_assets_under_management_dict = partner_end_customer_assets_under_management_instance.to_dict()
# create an instance of PartnerEndCustomerAssetsUnderManagement from a dict
partner_end_customer_assets_under_management_form_dict = partner_end_customer_assets_under_management.from_dict(partner_end_customer_assets_under_management_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


