# IdentityVerificationRetryRequestStepsObject

Instructions for the `custom` retry strategy specifying which steps should be required or skipped.   Note:   This field must be provided when the retry strategy is `custom` and must be omitted otherwise.  Custom retries override settings in your Plaid Template. For example, if your Plaid Template has `verify_sms` disabled, a custom retry with `verify_sms` enabled will still require the step.  The `selfie_check` step is currently not supported on the sandbox server. Sandbox requests will silently disable the `selfie_check` step when provided.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**verify_sms** | **bool** | A boolean field specifying whether the new session should require or skip the &#x60;verify_sms&#x60; step. | 
**kyc_check** | **bool** | A boolean field specifying whether the new session should require or skip the &#x60;kyc_check&#x60; step. | 
**documentary_verification** | **bool** | A boolean field specifying whether the new session should require or skip the &#x60;documentary_verification&#x60; step. | 
**selfie_check** | **bool** | A boolean field specifying whether the new session should require or skip the &#x60;selfie_check&#x60; step. | 

## Example

```python
from pyplaid.models.identity_verification_retry_request_steps_object import IdentityVerificationRetryRequestStepsObject

# TODO update the JSON string below
json = "{}"
# create an instance of IdentityVerificationRetryRequestStepsObject from a JSON string
identity_verification_retry_request_steps_object_instance = IdentityVerificationRetryRequestStepsObject.from_json(json)
# print the JSON string representation of the object
print IdentityVerificationRetryRequestStepsObject.to_json()

# convert the object into a dict
identity_verification_retry_request_steps_object_dict = identity_verification_retry_request_steps_object_instance.to_dict()
# create an instance of IdentityVerificationRetryRequestStepsObject from a dict
identity_verification_retry_request_steps_object_form_dict = identity_verification_retry_request_steps_object.from_dict(identity_verification_retry_request_steps_object_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


