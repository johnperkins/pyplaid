# CounterpartyType

The counterparty type.  merchant: a provider of goods or services for purchase financial_institution: a financial entity (bank, credit union, BNPL, fintech) payment_app: a transfer or P2P app (e.g. Zelle) delivery_marketplace: a food delivery marketplace (e.g DoorDash)

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


