# CreditW2

W2 is an object that represents income data taken from a W2 tax document.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**document_metadata** | [**CreditDocumentMetadata**](CreditDocumentMetadata.md) |  | 
**document_id** | **str** | An identifier of the document referenced by the document metadata. | 
**employer** | [**CreditPayStubEmployer**](CreditPayStubEmployer.md) |  | 
**employee** | [**CreditPayStubEmployee**](CreditPayStubEmployee.md) |  | 
**tax_year** | **str** | The tax year of the W2 document. | 
**employer_id_number** | **str** | An employee identification number or EIN. | 
**wages_tips_other_comp** | **str** | Wages from tips and other compensation. | 
**federal_income_tax_withheld** | **str** | Federal income tax withheld for the tax year. | 
**social_security_wages** | **str** | Wages from social security. | 
**social_security_tax_withheld** | **str** | Social security tax withheld for the tax year. | 
**medicare_wages_and_tips** | **str** | Wages and tips from medicare. | 
**medicare_tax_withheld** | **str** | Medicare tax withheld for the tax year. | 
**social_security_tips** | **str** | Tips from social security. | 
**allocated_tips** | **str** | Allocated tips. | 
**box_9** | **str** | Contents from box 9 on the W2. | 
**dependent_care_benefits** | **str** | Dependent care benefits. | 
**nonqualified_plans** | **str** | Nonqualified plans. | 
**box_12** | [**List[W2Box12]**](W2Box12.md) |  | 
**statutory_employee** | **str** | Statutory employee. | 
**retirement_plan** | **str** | Retirement plan. | 
**third_party_sick_pay** | **str** | Third party sick pay. | 
**other** | **str** | Other. | 
**state_and_local_wages** | [**List[W2StateAndLocalWages]**](W2StateAndLocalWages.md) |  | 

## Example

```python
from pyplaid.models.credit_w2 import CreditW2

# TODO update the JSON string below
json = "{}"
# create an instance of CreditW2 from a JSON string
credit_w2_instance = CreditW2.from_json(json)
# print the JSON string representation of the object
print CreditW2.to_json()

# convert the object into a dict
credit_w2_dict = credit_w2_instance.to_dict()
# create an instance of CreditW2 from a dict
credit_w2_form_dict = credit_w2.from_dict(credit_w2_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


