# StudentLoanStatus

An object representing the status of the student loan

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**end_date** | **date** | The date until which the loan will be in its current status. Dates are returned in an [ISO 8601](https://wikipedia.org/wiki/ISO_8601) format (YYYY-MM-DD).  | 
**type** | **str** | The status type of the student loan | 

## Example

```python
from pyplaid.models.student_loan_status import StudentLoanStatus

# TODO update the JSON string below
json = "{}"
# create an instance of StudentLoanStatus from a JSON string
student_loan_status_instance = StudentLoanStatus.from_json(json)
# print the JSON string representation of the object
print StudentLoanStatus.to_json()

# convert the object into a dict
student_loan_status_dict = student_loan_status_instance.to_dict()
# create an instance of StudentLoanStatus from a dict
student_loan_status_form_dict = student_loan_status.from_dict(student_loan_status_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


