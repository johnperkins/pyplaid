# LinkTokenAccountFilters

By default, Link will provide limited account filtering: it will only display Institutions that are compatible with all products supplied in the `products` parameter of `/link/token/create`, and, if `auth` is specified in the `products` array, will also filter out accounts other than `checking` and `savings` accounts on the Account Select pane. You can further limit the accounts shown in Link by using `account_filters` to specify the account subtypes to be shown in Link. Only the specified subtypes will be shown. This filtering applies to both the Account Select view (if enabled) and the Institution Select view. Institutions that do not support the selected subtypes will be omitted from Link. To indicate that all subtypes should be shown, use the value `\"all\"`. If the `account_filters` filter is used, any account type for which a filter is not specified will be entirely omitted from Link. For a full list of valid types and subtypes, see the [Account schema](https://plaid.com/docs/api/accounts#account-type-schema).  For institutions using OAuth, the filter will not affect the list of accounts shown by the bank in the OAuth window. 

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**depository** | [**DepositoryFilter**](DepositoryFilter.md) |  | [optional] 
**credit** | [**CreditFilter**](CreditFilter.md) |  | [optional] 
**loan** | [**LoanFilter**](LoanFilter.md) |  | [optional] 
**investment** | [**InvestmentFilter**](InvestmentFilter.md) |  | [optional] 

## Example

```python
from pyplaid.models.link_token_account_filters import LinkTokenAccountFilters

# TODO update the JSON string below
json = "{}"
# create an instance of LinkTokenAccountFilters from a JSON string
link_token_account_filters_instance = LinkTokenAccountFilters.from_json(json)
# print the JSON string representation of the object
print LinkTokenAccountFilters.to_json()

# convert the object into a dict
link_token_account_filters_dict = link_token_account_filters_instance.to_dict()
# create an instance of LinkTokenAccountFilters from a dict
link_token_account_filters_form_dict = link_token_account_filters.from_dict(link_token_account_filters_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


