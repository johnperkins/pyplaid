# BankTransferSweep

BankTransferSweep describes a sweep transfer.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Identifier of the sweep. | 
**created_at** | **datetime** | The datetime when the sweep occurred, in RFC 3339 format. | 
**amount** | **str** | The amount of the sweep. | 
**iso_currency_code** | **str** | The currency of the sweep, e.g. \&quot;USD\&quot;. | 

## Example

```python
from pyplaid.models.bank_transfer_sweep import BankTransferSweep

# TODO update the JSON string below
json = "{}"
# create an instance of BankTransferSweep from a JSON string
bank_transfer_sweep_instance = BankTransferSweep.from_json(json)
# print the JSON string representation of the object
print BankTransferSweep.to_json()

# convert the object into a dict
bank_transfer_sweep_dict = bank_transfer_sweep_instance.to_dict()
# create an instance of BankTransferSweep from a dict
bank_transfer_sweep_form_dict = bank_transfer_sweep.from_dict(bank_transfer_sweep_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


