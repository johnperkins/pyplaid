# RiskSignalDocumentReference

Object containing metadata for the document

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**document_id** | **str** | An identifier of the document referenced by the document metadata. | [optional] 
**document_name** | **str** | The name of the document | [optional] 

## Example

```python
from pyplaid.models.risk_signal_document_reference import RiskSignalDocumentReference

# TODO update the JSON string below
json = "{}"
# create an instance of RiskSignalDocumentReference from a JSON string
risk_signal_document_reference_instance = RiskSignalDocumentReference.from_json(json)
# print the JSON string representation of the object
print RiskSignalDocumentReference.to_json()

# convert the object into a dict
risk_signal_document_reference_dict = risk_signal_document_reference_instance.to_dict()
# create an instance of RiskSignalDocumentReference from a dict
risk_signal_document_reference_form_dict = risk_signal_document_reference.from_dict(risk_signal_document_reference_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


