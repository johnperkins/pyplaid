# NumbersInternational

Identifying information for transferring money to or from an international bank account via wire transfer.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_id** | **str** | The Plaid account ID associated with the account numbers | 
**iban** | **str** | The International Bank Account Number (IBAN) for the account | 
**bic** | **str** | The Bank Identifier Code (BIC) for the account | 

## Example

```python
from pyplaid.models.numbers_international import NumbersInternational

# TODO update the JSON string below
json = "{}"
# create an instance of NumbersInternational from a JSON string
numbers_international_instance = NumbersInternational.from_json(json)
# print the JSON string representation of the object
print NumbersInternational.to_json()

# convert the object into a dict
numbers_international_dict = numbers_international_instance.to_dict()
# create an instance of NumbersInternational from a dict
numbers_international_form_dict = numbers_international.from_dict(numbers_international_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


