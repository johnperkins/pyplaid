# ItemPublicTokenCreateResponse

ItemPublicTokenCreateResponse defines the response schema for `/item/public_token/create`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**public_token** | **str** | A &#x60;public_token&#x60; for the particular Item corresponding to the specified &#x60;access_token&#x60; | 
**expiration** | **datetime** |  | [optional] 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.item_public_token_create_response import ItemPublicTokenCreateResponse

# TODO update the JSON string below
json = "{}"
# create an instance of ItemPublicTokenCreateResponse from a JSON string
item_public_token_create_response_instance = ItemPublicTokenCreateResponse.from_json(json)
# print the JSON string representation of the object
print ItemPublicTokenCreateResponse.to_json()

# convert the object into a dict
item_public_token_create_response_dict = item_public_token_create_response_instance.to_dict()
# create an instance of ItemPublicTokenCreateResponse from a dict
item_public_token_create_response_form_dict = item_public_token_create_response.from_dict(item_public_token_create_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


