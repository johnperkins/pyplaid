# GenericScreeningHitLocationItems

Analyzed location information for the associated hit

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**analysis** | [**MatchSummary**](MatchSummary.md) |  | [optional] 
**data** | [**WatchlistScreeningHitLocations**](WatchlistScreeningHitLocations.md) |  | [optional] 

## Example

```python
from pyplaid.models.generic_screening_hit_location_items import GenericScreeningHitLocationItems

# TODO update the JSON string below
json = "{}"
# create an instance of GenericScreeningHitLocationItems from a JSON string
generic_screening_hit_location_items_instance = GenericScreeningHitLocationItems.from_json(json)
# print the JSON string representation of the object
print GenericScreeningHitLocationItems.to_json()

# convert the object into a dict
generic_screening_hit_location_items_dict = generic_screening_hit_location_items_instance.to_dict()
# create an instance of GenericScreeningHitLocationItems from a dict
generic_screening_hit_location_items_form_dict = generic_screening_hit_location_items.from_dict(generic_screening_hit_location_items_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


