# NewAccountsAvailableWebhook

Fired when Plaid detects a new account for Items created or updated with [Account Select v2](https://plaid.com/docs/link/customization/#account-select). Upon receiving this webhook, you can prompt your users to share new accounts with you through [Account Select v2 update mode](https://plaid.com/docs/link/update-mode/#using-update-mode-to-request-new-accounts).

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**webhook_type** | **str** | &#x60;ITEM&#x60; | [optional] 
**webhook_code** | **str** | &#x60;NEW_ACCOUNTS_AVAILABLE&#x60; | [optional] 
**item_id** | **str** | The &#x60;item_id&#x60; of the Item associated with this webhook, warning, or error | [optional] 
**error** | [**PlaidError**](PlaidError.md) |  | [optional] 
**environment** | [**WebhookEnvironmentValues**](WebhookEnvironmentValues.md) |  | [optional] 

## Example

```python
from pyplaid.models.new_accounts_available_webhook import NewAccountsAvailableWebhook

# TODO update the JSON string below
json = "{}"
# create an instance of NewAccountsAvailableWebhook from a JSON string
new_accounts_available_webhook_instance = NewAccountsAvailableWebhook.from_json(json)
# print the JSON string representation of the object
print NewAccountsAvailableWebhook.to_json()

# convert the object into a dict
new_accounts_available_webhook_dict = new_accounts_available_webhook_instance.to_dict()
# create an instance of NewAccountsAvailableWebhook from a dict
new_accounts_available_webhook_form_dict = new_accounts_available_webhook.from_dict(new_accounts_available_webhook_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


