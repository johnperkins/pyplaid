# PaymentInitiationRecipientGetResponse

PaymentInitiationRecipientGetResponse defines the response schema for `/payment_initiation/recipient/get`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**recipient_id** | **str** | The ID of the recipient. | 
**name** | **str** | The name of the recipient. | 
**address** | [**PaymentInitiationAddress**](PaymentInitiationAddress.md) |  | [optional] 
**iban** | **str** | The International Bank Account Number (IBAN) for the recipient. | [optional] 
**bacs** | [**RecipientBACSNullable**](RecipientBACSNullable.md) |  | [optional] 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.payment_initiation_recipient_get_response import PaymentInitiationRecipientGetResponse

# TODO update the JSON string below
json = "{}"
# create an instance of PaymentInitiationRecipientGetResponse from a JSON string
payment_initiation_recipient_get_response_instance = PaymentInitiationRecipientGetResponse.from_json(json)
# print the JSON string representation of the object
print PaymentInitiationRecipientGetResponse.to_json()

# convert the object into a dict
payment_initiation_recipient_get_response_dict = payment_initiation_recipient_get_response_instance.to_dict()
# create an instance of PaymentInitiationRecipientGetResponse from a dict
payment_initiation_recipient_get_response_form_dict = payment_initiation_recipient_get_response.from_dict(payment_initiation_recipient_get_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


