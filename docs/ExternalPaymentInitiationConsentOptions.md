# ExternalPaymentInitiationConsentOptions

Additional payment consent options

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**wallet_id** | **str** | The EMI (E-Money Institution) wallet that this payment consent is associated with, if any. This wallet is used as an intermediary account to enable Plaid to reconcile the settlement of funds for Payment Initiation requests. | [optional] 
**request_refund_details** | **bool** | When &#x60;true&#x60;, Plaid will attempt to request refund details from the payee&#39;s financial institution.  Support varies between financial institutions and will not always be available.  If refund details could be retrieved, they will be available in the &#x60;/payment_initiation/payment/get&#x60; response. | [optional] 
**iban** | **str** | The International Bank Account Number (IBAN) for the payer&#39;s account. If provided, the end user will be able to set up payment consent using only the specified bank account. | [optional] 
**bacs** | [**PaymentInitiationOptionalRestrictionBacs**](PaymentInitiationOptionalRestrictionBacs.md) |  | [optional] 

## Example

```python
from pyplaid.models.external_payment_initiation_consent_options import ExternalPaymentInitiationConsentOptions

# TODO update the JSON string below
json = "{}"
# create an instance of ExternalPaymentInitiationConsentOptions from a JSON string
external_payment_initiation_consent_options_instance = ExternalPaymentInitiationConsentOptions.from_json(json)
# print the JSON string representation of the object
print ExternalPaymentInitiationConsentOptions.to_json()

# convert the object into a dict
external_payment_initiation_consent_options_dict = external_payment_initiation_consent_options_instance.to_dict()
# create an instance of ExternalPaymentInitiationConsentOptions from a dict
external_payment_initiation_consent_options_form_dict = external_payment_initiation_consent_options.from_dict(external_payment_initiation_consent_options_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


