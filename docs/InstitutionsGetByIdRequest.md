# InstitutionsGetByIdRequest

InstitutionsGetByIdRequest defines the request schema for `/institutions/get_by_id`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**institution_id** | **str** | The ID of the institution to get details about | 
**country_codes** | [**List[CountryCode]**](CountryCode.md) | Specify an array of Plaid-supported country codes this institution supports, using the ISO-3166-1 alpha-2 country code standard. In API versions 2019-05-29 and earlier, the &#x60;country_codes&#x60; parameter is an optional parameter within the &#x60;options&#x60; object and will default to &#x60;[US]&#x60; if it is not supplied.  | 
**options** | [**InstitutionsGetByIdRequestOptions**](InstitutionsGetByIdRequestOptions.md) |  | [optional] 

## Example

```python
from pyplaid.models.institutions_get_by_id_request import InstitutionsGetByIdRequest

# TODO update the JSON string below
json = "{}"
# create an instance of InstitutionsGetByIdRequest from a JSON string
institutions_get_by_id_request_instance = InstitutionsGetByIdRequest.from_json(json)
# print the JSON string representation of the object
print InstitutionsGetByIdRequest.to_json()

# convert the object into a dict
institutions_get_by_id_request_dict = institutions_get_by_id_request_instance.to_dict()
# create an instance of InstitutionsGetByIdRequest from a dict
institutions_get_by_id_request_form_dict = institutions_get_by_id_request.from_dict(institutions_get_by_id_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


