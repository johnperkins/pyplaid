# Asset

No documentation available

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**asset_detail** | [**AssetDetail**](AssetDetail.md) |  | 
**asset_owners** | [**AssetOwners**](AssetOwners.md) |  | 
**asset_holder** | [**AssetHolder**](AssetHolder.md) |  | 
**asset_transactions** | [**AssetTransactions**](AssetTransactions.md) |  | 
**validation_sources** | [**ValidationSources**](ValidationSources.md) |  | 

## Example

```python
from pyplaid.models.asset import Asset

# TODO update the JSON string below
json = "{}"
# create an instance of Asset from a JSON string
asset_instance = Asset.from_json(json)
# print the JSON string representation of the object
print Asset.to_json()

# convert the object into a dict
asset_dict = asset_instance.to_dict()
# create an instance of Asset from a dict
asset_form_dict = asset.from_dict(asset_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


