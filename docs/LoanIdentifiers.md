# LoanIdentifiers

Collection of current and previous identifiers for this loan.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**loan_identifier** | [**LoanIdentifier**](LoanIdentifier.md) |  | 

## Example

```python
from pyplaid.models.loan_identifiers import LoanIdentifiers

# TODO update the JSON string below
json = "{}"
# create an instance of LoanIdentifiers from a JSON string
loan_identifiers_instance = LoanIdentifiers.from_json(json)
# print the JSON string representation of the object
print LoanIdentifiers.to_json()

# convert the object into a dict
loan_identifiers_dict = loan_identifiers_instance.to_dict()
# create an instance of LoanIdentifiers from a dict
loan_identifiers_form_dict = loan_identifiers.from_dict(loan_identifiers_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


