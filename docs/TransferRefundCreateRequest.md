# TransferRefundCreateRequest

Defines the request schema for `/transfer/refund/create`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**transfer_id** | **str** | The ID of the transfer to refund. | 
**amount** | **str** | The amount of the refund (decimal string with two digits of precision e.g. \&quot;10.00\&quot;). | 
**idempotency_key** | **str** | A random key provided by the client, per unique refund. Maximum of 50 characters.  The API supports idempotency for safely retrying requests without accidentally performing the same operation twice. For example, if a request to create a refund fails due to a network connection error, you can retry the request with the same idempotency key to guarantee that only a single refund is created. | 

## Example

```python
from pyplaid.models.transfer_refund_create_request import TransferRefundCreateRequest

# TODO update the JSON string below
json = "{}"
# create an instance of TransferRefundCreateRequest from a JSON string
transfer_refund_create_request_instance = TransferRefundCreateRequest.from_json(json)
# print the JSON string representation of the object
print TransferRefundCreateRequest.to_json()

# convert the object into a dict
transfer_refund_create_request_dict = transfer_refund_create_request_instance.to_dict()
# create an instance of TransferRefundCreateRequest from a dict
transfer_refund_create_request_form_dict = transfer_refund_create_request.from_dict(transfer_refund_create_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


