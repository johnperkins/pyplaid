# AccountsGetResponse

AccountsGetResponse defines the response schema for `/accounts/get` and `/accounts/balance/get`.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accounts** | [**List[AccountBase]**](AccountBase.md) | An array of financial institution accounts associated with the Item. If &#x60;/accounts/balance/get&#x60; was called, each account will include real-time balance information. | 
**item** | [**Item**](Item.md) |  | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.accounts_get_response import AccountsGetResponse

# TODO update the JSON string below
json = "{}"
# create an instance of AccountsGetResponse from a JSON string
accounts_get_response_instance = AccountsGetResponse.from_json(json)
# print the JSON string representation of the object
print AccountsGetResponse.to_json()

# convert the object into a dict
accounts_get_response_dict = accounts_get_response_instance.to_dict()
# create an instance of AccountsGetResponse from a dict
accounts_get_response_form_dict = accounts_get_response.from_dict(accounts_get_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


