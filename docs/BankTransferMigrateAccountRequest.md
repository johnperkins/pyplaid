# BankTransferMigrateAccountRequest

Defines the request schema for `/bank_transfer/migrate_account`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**account_number** | **str** | The user&#39;s account number. | 
**routing_number** | **str** | The user&#39;s routing number. | 
**wire_routing_number** | **str** | The user&#39;s wire transfer routing number. This is the ABA number; for some institutions, this may differ from the ACH number used in &#x60;routing_number&#x60;. | [optional] 
**account_type** | **str** | The type of the bank account (&#x60;checking&#x60; or &#x60;savings&#x60;). | 

## Example

```python
from pyplaid.models.bank_transfer_migrate_account_request import BankTransferMigrateAccountRequest

# TODO update the JSON string below
json = "{}"
# create an instance of BankTransferMigrateAccountRequest from a JSON string
bank_transfer_migrate_account_request_instance = BankTransferMigrateAccountRequest.from_json(json)
# print the JSON string representation of the object
print BankTransferMigrateAccountRequest.to_json()

# convert the object into a dict
bank_transfer_migrate_account_request_dict = bank_transfer_migrate_account_request_instance.to_dict()
# create an instance of BankTransferMigrateAccountRequest from a dict
bank_transfer_migrate_account_request_form_dict = bank_transfer_migrate_account_request.from_dict(bank_transfer_migrate_account_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


