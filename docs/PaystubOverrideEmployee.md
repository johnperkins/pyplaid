# PaystubOverrideEmployee

The employee on the paystub.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The name of the employee. | [optional] 
**address** | [**PaystubOverrideEmployeeAddress**](PaystubOverrideEmployeeAddress.md) |  | [optional] 

## Example

```python
from pyplaid.models.paystub_override_employee import PaystubOverrideEmployee

# TODO update the JSON string below
json = "{}"
# create an instance of PaystubOverrideEmployee from a JSON string
paystub_override_employee_instance = PaystubOverrideEmployee.from_json(json)
# print the JSON string representation of the object
print PaystubOverrideEmployee.to_json()

# convert the object into a dict
paystub_override_employee_dict = paystub_override_employee_instance.to_dict()
# create an instance of PaystubOverrideEmployee from a dict
paystub_override_employee_form_dict = paystub_override_employee.from_dict(paystub_override_employee_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


