# PhysicalDocumentExtractedData

Data extracted from a user-submitted document.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id_number** | **str** | Alpha-numeric ID number extracted via OCR from the user&#39;s document image. | 
**category** | [**PhysicalDocumentCategory**](PhysicalDocumentCategory.md) |  | 
**expiration_date** | **date** | A date in the format YYYY-MM-DD (RFC 3339 Section 5.6). | 
**issuing_country** | **str** | Valid, capitalized, two-letter ISO code representing the country of this object. Must be in ISO 3166-1 alpha-2 form. | 
**issuing_region** | **str** | An ISO 3166-2 subdivision code. Related terms would be \&quot;state\&quot;, \&quot;province\&quot;, \&quot;prefecture\&quot;, \&quot;zone\&quot;, \&quot;subdivision\&quot;, etc. | 

## Example

```python
from pyplaid.models.physical_document_extracted_data import PhysicalDocumentExtractedData

# TODO update the JSON string below
json = "{}"
# create an instance of PhysicalDocumentExtractedData from a JSON string
physical_document_extracted_data_instance = PhysicalDocumentExtractedData.from_json(json)
# print the JSON string representation of the object
print PhysicalDocumentExtractedData.to_json()

# convert the object into a dict
physical_document_extracted_data_dict = physical_document_extracted_data_instance.to_dict()
# create an instance of PhysicalDocumentExtractedData from a dict
physical_document_extracted_data_form_dict = physical_document_extracted_data.from_dict(physical_document_extracted_data_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


