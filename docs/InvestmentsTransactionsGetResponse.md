# InvestmentsTransactionsGetResponse

InvestmentsTransactionsGetResponse defines the response schema for `/investments/transactions/get`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**item** | [**Item**](Item.md) |  | 
**accounts** | [**List[AccountBase]**](AccountBase.md) | The accounts for which transaction history is being fetched. | 
**securities** | [**List[Security]**](Security.md) | All securities for which there is a corresponding transaction being fetched. | 
**investment_transactions** | [**List[InvestmentTransaction]**](InvestmentTransaction.md) | The transactions being fetched | 
**total_investment_transactions** | **int** | The total number of transactions available within the date range specified. If &#x60;total_investment_transactions&#x60; is larger than the size of the &#x60;transactions&#x60; array, more transactions are available and can be fetched via manipulating the &#x60;offset&#x60; parameter.&#39; | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.investments_transactions_get_response import InvestmentsTransactionsGetResponse

# TODO update the JSON string below
json = "{}"
# create an instance of InvestmentsTransactionsGetResponse from a JSON string
investments_transactions_get_response_instance = InvestmentsTransactionsGetResponse.from_json(json)
# print the JSON string representation of the object
print InvestmentsTransactionsGetResponse.to_json()

# convert the object into a dict
investments_transactions_get_response_dict = investments_transactions_get_response_instance.to_dict()
# create an instance of InvestmentsTransactionsGetResponse from a dict
investments_transactions_get_response_form_dict = investments_transactions_get_response.from_dict(investments_transactions_get_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


