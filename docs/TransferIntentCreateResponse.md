# TransferIntentCreateResponse

Defines the response schema for `/transfer/intent/create`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**transfer_intent** | [**TransferIntentCreate**](TransferIntentCreate.md) |  | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.transfer_intent_create_response import TransferIntentCreateResponse

# TODO update the JSON string below
json = "{}"
# create an instance of TransferIntentCreateResponse from a JSON string
transfer_intent_create_response_instance = TransferIntentCreateResponse.from_json(json)
# print the JSON string representation of the object
print TransferIntentCreateResponse.to_json()

# convert the object into a dict
transfer_intent_create_response_dict = transfer_intent_create_response_instance.to_dict()
# create an instance of TransferIntentCreateResponse from a dict
transfer_intent_create_response_form_dict = transfer_intent_create_response.from_dict(transfer_intent_create_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


