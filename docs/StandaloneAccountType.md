# StandaloneAccountType

The schema below describes the various `types` and corresponding `subtypes` that Plaid recognizes and reports for financial institution accounts.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**depository** | **str** | An account type holding cash, in which funds are deposited. Supported products for &#x60;depository&#x60; accounts are: Auth (&#x60;checking&#x60; and &#x60;savings&#x60; types only), Balance, Transactions, Identity, Payment Initiation, and Assets. | 
**credit** | **str** | A credit card type account. Supported products for &#x60;credit&#x60; accounts are: Balance, Transactions, Identity, and Liabilities. | 
**loan** | **str** | A loan type account. Supported products for &#x60;loan&#x60; accounts are: Balance, Liabilities, and Transactions. | 
**investment** | **str** | An investment account. Supported products for &#x60;investment&#x60; accounts are: Balance and Investments. In API versions 2018-05-22 and earlier, this type is called &#x60;brokerage&#x60;. | 
**other** | **str** | Other or unknown account type. Supported products for &#x60;other&#x60; accounts are: Balance, Transactions, Identity, and Assets. | 

## Example

```python
from pyplaid.models.standalone_account_type import StandaloneAccountType

# TODO update the JSON string below
json = "{}"
# create an instance of StandaloneAccountType from a JSON string
standalone_account_type_instance = StandaloneAccountType.from_json(json)
# print the JSON string representation of the object
print StandaloneAccountType.to_json()

# convert the object into a dict
standalone_account_type_dict = standalone_account_type_instance.to_dict()
# create an instance of StandaloneAccountType from a dict
standalone_account_type_form_dict = standalone_account_type.from_dict(standalone_account_type_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


