# InstitutionsGetByIdResponse

InstitutionsGetByIdResponse defines the response schema for `/institutions/get_by_id`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**institution** | [**Institution**](Institution.md) |  | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.institutions_get_by_id_response import InstitutionsGetByIdResponse

# TODO update the JSON string below
json = "{}"
# create an instance of InstitutionsGetByIdResponse from a JSON string
institutions_get_by_id_response_instance = InstitutionsGetByIdResponse.from_json(json)
# print the JSON string representation of the object
print InstitutionsGetByIdResponse.to_json()

# convert the object into a dict
institutions_get_by_id_response_dict = institutions_get_by_id_response_instance.to_dict()
# create an instance of InstitutionsGetByIdResponse from a dict
institutions_get_by_id_response_form_dict = institutions_get_by_id_response.from_dict(institutions_get_by_id_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


