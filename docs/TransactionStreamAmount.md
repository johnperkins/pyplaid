# TransactionStreamAmount

Object with data pertaining to an amount on the transaction stream.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amount** | **float** | Represents the numerical value of an amount. | [optional] 
**iso_currency_code** | **str** | The ISO-4217 currency code of the amount. Always &#x60;null&#x60; if &#x60;unofficial_currency_code&#x60; is non-&#x60;null&#x60;.  See the [currency code schema](https://plaid.com/docs/api/accounts#currency-code-schema) for a full listing of supported &#x60;iso_currency_code&#x60;s. | [optional] 
**unofficial_currency_code** | **str** | The unofficial currency code of the amount. Always &#x60;null&#x60; if &#x60;iso_currency_code&#x60; is non-&#x60;null&#x60;. Unofficial currency codes are used for currencies that do not have official ISO currency codes, such as cryptocurrencies and the currencies of certain countries. | [optional] 

## Example

```python
from pyplaid.models.transaction_stream_amount import TransactionStreamAmount

# TODO update the JSON string below
json = "{}"
# create an instance of TransactionStreamAmount from a JSON string
transaction_stream_amount_instance = TransactionStreamAmount.from_json(json)
# print the JSON string representation of the object
print TransactionStreamAmount.to_json()

# convert the object into a dict
transaction_stream_amount_dict = transaction_stream_amount_instance.to_dict()
# create an instance of TransactionStreamAmount from a dict
transaction_stream_amount_form_dict = transaction_stream_amount.from_dict(transaction_stream_amount_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


