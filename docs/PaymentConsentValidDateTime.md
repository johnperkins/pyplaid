# PaymentConsentValidDateTime

Life span for the payment consent. After the `to` date the payment consent expires and can no longer be used for payment initiation.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**var_from** | **datetime** | The date and time from which the consent should be active, in [ISO 8601](https://wikipedia.org/wiki/ISO_8601) format. | [optional] 
**to** | **datetime** | The date and time at which the consent expires, in [ISO 8601](https://wikipedia.org/wiki/ISO_8601) format. | [optional] 

## Example

```python
from pyplaid.models.payment_consent_valid_date_time import PaymentConsentValidDateTime

# TODO update the JSON string below
json = "{}"
# create an instance of PaymentConsentValidDateTime from a JSON string
payment_consent_valid_date_time_instance = PaymentConsentValidDateTime.from_json(json)
# print the JSON string representation of the object
print PaymentConsentValidDateTime.to_json()

# convert the object into a dict
payment_consent_valid_date_time_dict = payment_consent_valid_date_time_instance.to_dict()
# create an instance of PaymentConsentValidDateTime from a dict
payment_consent_valid_date_time_form_dict = payment_consent_valid_date_time.from_dict(payment_consent_valid_date_time_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


