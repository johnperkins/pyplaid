# IncomeBreakdown

An object representing a breakdown of the different income types on the paystub.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | [**IncomeBreakdownType**](IncomeBreakdownType.md) |  | 
**rate** | **float** | The hourly rate at which the income is paid. | 
**hours** | **float** | The number of hours logged for this income for this pay period. | 
**total** | **float** | The total pay for this pay period. | 

## Example

```python
from pyplaid.models.income_breakdown import IncomeBreakdown

# TODO update the JSON string below
json = "{}"
# create an instance of IncomeBreakdown from a JSON string
income_breakdown_instance = IncomeBreakdown.from_json(json)
# print the JSON string representation of the object
print IncomeBreakdown.to_json()

# convert the object into a dict
income_breakdown_dict = income_breakdown_instance.to_dict()
# create an instance of IncomeBreakdown from a dict
income_breakdown_form_dict = income_breakdown.from_dict(income_breakdown_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


