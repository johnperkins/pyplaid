# CategoriesGetResponse

CategoriesGetResponse defines the response schema for `/categories/get`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**categories** | [**List[Category]**](Category.md) | An array of all of the transaction categories used by Plaid. | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.categories_get_response import CategoriesGetResponse

# TODO update the JSON string below
json = "{}"
# create an instance of CategoriesGetResponse from a JSON string
categories_get_response_instance = CategoriesGetResponse.from_json(json)
# print the JSON string representation of the object
print CategoriesGetResponse.to_json()

# convert the object into a dict
categories_get_response_dict = categories_get_response_instance.to_dict()
# create an instance of CategoriesGetResponse from a dict
categories_get_response_form_dict = categories_get_response.from_dict(categories_get_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


