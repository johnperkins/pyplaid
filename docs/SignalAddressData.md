# SignalAddressData

Data about the components comprising an address.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**city** | **str** | The full city name | [optional] 
**region** | **str** | The region or state Example: &#x60;\&quot;NC\&quot;&#x60; | [optional] 
**street** | **str** | The full street address Example: &#x60;\&quot;564 Main Street, APT 15\&quot;&#x60; | [optional] 
**postal_code** | **str** | The postal code | [optional] 
**country** | **str** | The ISO 3166-1 alpha-2 country code | [optional] 

## Example

```python
from pyplaid.models.signal_address_data import SignalAddressData

# TODO update the JSON string below
json = "{}"
# create an instance of SignalAddressData from a JSON string
signal_address_data_instance = SignalAddressData.from_json(json)
# print the JSON string representation of the object
print SignalAddressData.to_json()

# convert the object into a dict
signal_address_data_dict = signal_address_data_instance.to_dict()
# create an instance of SignalAddressData from a dict
signal_address_data_form_dict = signal_address_data.from_dict(signal_address_data_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


