# TransferCancelResponse

Defines the response schema for `/transfer/cancel`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.transfer_cancel_response import TransferCancelResponse

# TODO update the JSON string below
json = "{}"
# create an instance of TransferCancelResponse from a JSON string
transfer_cancel_response_instance = TransferCancelResponse.from_json(json)
# print the JSON string representation of the object
print TransferCancelResponse.to_json()

# convert the object into a dict
transfer_cancel_response_dict = transfer_cancel_response_instance.to_dict()
# create an instance of TransferCancelResponse from a dict
transfer_cancel_response_form_dict = transfer_cancel_response.from_dict(transfer_cancel_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


