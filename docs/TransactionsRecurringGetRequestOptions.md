# TransactionsRecurringGetRequestOptions

An optional object to be used with the request. If specified, `options` must not be `null`.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**include_personal_finance_category** | **bool** | Include the [&#x60;personal_finance_category&#x60;](https://plaid.com/docs/api/products/transactions/#transactions-get-response-transactions-personal-finance-category) object for each transaction stream in the response.  See the [&#x60;taxonomy csv file&#x60;](https://plaid.com/documents/transactions-personal-finance-category-taxonomy.csv) for a full list of personal finance categories. | [optional] [default to False]

## Example

```python
from pyplaid.models.transactions_recurring_get_request_options import TransactionsRecurringGetRequestOptions

# TODO update the JSON string below
json = "{}"
# create an instance of TransactionsRecurringGetRequestOptions from a JSON string
transactions_recurring_get_request_options_instance = TransactionsRecurringGetRequestOptions.from_json(json)
# print the JSON string representation of the object
print TransactionsRecurringGetRequestOptions.to_json()

# convert the object into a dict
transactions_recurring_get_request_options_dict = transactions_recurring_get_request_options_instance.to_dict()
# create an instance of TransactionsRecurringGetRequestOptions from a dict
transactions_recurring_get_request_options_form_dict = transactions_recurring_get_request_options.from_dict(transactions_recurring_get_request_options_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


