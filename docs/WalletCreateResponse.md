# WalletCreateResponse

WalletCreateResponse defines the response schema for `/wallet/create`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**wallet_id** | **str** | A unique ID identifying the e-wallet | 
**balance** | [**WalletBalance**](WalletBalance.md) |  | 
**numbers** | [**WalletNumbers**](WalletNumbers.md) |  | 
**recipient_id** | **str** | The ID of the recipient that corresponds to the e-wallet account numbers | [optional] 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.wallet_create_response import WalletCreateResponse

# TODO update the JSON string below
json = "{}"
# create an instance of WalletCreateResponse from a JSON string
wallet_create_response_instance = WalletCreateResponse.from_json(json)
# print the JSON string representation of the object
print WalletCreateResponse.to_json()

# convert the object into a dict
wallet_create_response_dict = wallet_create_response_instance.to_dict()
# create an instance of WalletCreateResponse from a dict
wallet_create_response_form_dict = wallet_create_response.from_dict(wallet_create_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


