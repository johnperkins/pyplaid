# YTDNetIncomeSummaryFieldNumber

Year-to-date earnings after any tax withholdings, benefit payments or deductions, as reported on the paystub.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value** | **float** | The value of the field. | 
**verification_status** | [**VerificationStatus**](VerificationStatus.md) |  | 

## Example

```python
from pyplaid.models.ytd_net_income_summary_field_number import YTDNetIncomeSummaryFieldNumber

# TODO update the JSON string below
json = "{}"
# create an instance of YTDNetIncomeSummaryFieldNumber from a JSON string
ytd_net_income_summary_field_number_instance = YTDNetIncomeSummaryFieldNumber.from_json(json)
# print the JSON string representation of the object
print YTDNetIncomeSummaryFieldNumber.to_json()

# convert the object into a dict
ytd_net_income_summary_field_number_dict = ytd_net_income_summary_field_number_instance.to_dict()
# create an instance of YTDNetIncomeSummaryFieldNumber from a dict
ytd_net_income_summary_field_number_form_dict = ytd_net_income_summary_field_number.from_dict(ytd_net_income_summary_field_number_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


