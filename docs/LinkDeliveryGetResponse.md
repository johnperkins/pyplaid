# LinkDeliveryGetResponse

LinkDeliveryGetRequest defines the response schema for `/link_delivery/get`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | [**LinkDeliverySessionStatus**](LinkDeliverySessionStatus.md) |  | 
**created_at** | **datetime** | Timestamp in [ISO 8601](https://wikipedia.org/wiki/ISO_8601) format (&#x60;YYYY-MM-DDTHH:mm:ssZ&#x60;) indicating the time the given Link Delivery Session was created at | 
**public_tokens** | **List[str]** | The public tokens returned by the Link session upon completion | [optional] 
**completed_at** | **datetime** | Timestamp in [ISO 8601](https://wikipedia.org/wiki/ISO_8601) format (&#x60;YYYY-MM-DDTHH:mm:ssZ&#x60;) indicating the time the given Link Delivery Session was completed at | [optional] 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.link_delivery_get_response import LinkDeliveryGetResponse

# TODO update the JSON string below
json = "{}"
# create an instance of LinkDeliveryGetResponse from a JSON string
link_delivery_get_response_instance = LinkDeliveryGetResponse.from_json(json)
# print the JSON string representation of the object
print LinkDeliveryGetResponse.to_json()

# convert the object into a dict
link_delivery_get_response_dict = link_delivery_get_response_instance.to_dict()
# create an instance of LinkDeliveryGetResponse from a dict
link_delivery_get_response_form_dict = link_delivery_get_response.from_dict(link_delivery_get_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


