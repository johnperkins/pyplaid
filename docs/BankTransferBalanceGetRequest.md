# BankTransferBalanceGetRequest

Defines the request schema for `/bank_transfer/balance/get`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**origination_account_id** | **str** | If multiple origination accounts are available, &#x60;origination_account_id&#x60; must be used to specify the account for which balance will be returned. | [optional] 

## Example

```python
from pyplaid.models.bank_transfer_balance_get_request import BankTransferBalanceGetRequest

# TODO update the JSON string below
json = "{}"
# create an instance of BankTransferBalanceGetRequest from a JSON string
bank_transfer_balance_get_request_instance = BankTransferBalanceGetRequest.from_json(json)
# print the JSON string representation of the object
print BankTransferBalanceGetRequest.to_json()

# convert the object into a dict
bank_transfer_balance_get_request_dict = bank_transfer_balance_get_request_instance.to_dict()
# create an instance of BankTransferBalanceGetRequest from a dict
bank_transfer_balance_get_request_form_dict = bank_transfer_balance_get_request.from_dict(bank_transfer_balance_get_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


