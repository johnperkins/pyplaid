# DocumentaryVerificationDocument

Images, extracted data, and analysis from a user's identity document

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | [**DocumentStatus**](DocumentStatus.md) |  | 
**attempt** | **float** | The &#x60;attempt&#x60; field begins with 1 and increments with each subsequent document upload. | 
**images** | [**PhysicalDocumentImages**](PhysicalDocumentImages.md) |  | 
**extracted_data** | [**PhysicalDocumentExtractedData**](PhysicalDocumentExtractedData.md) |  | 
**analysis** | [**DocumentAnalysis**](DocumentAnalysis.md) |  | 

## Example

```python
from pyplaid.models.documentary_verification_document import DocumentaryVerificationDocument

# TODO update the JSON string below
json = "{}"
# create an instance of DocumentaryVerificationDocument from a JSON string
documentary_verification_document_instance = DocumentaryVerificationDocument.from_json(json)
# print the JSON string representation of the object
print DocumentaryVerificationDocument.to_json()

# convert the object into a dict
documentary_verification_document_dict = documentary_verification_document_instance.to_dict()
# create an instance of DocumentaryVerificationDocument from a dict
documentary_verification_document_form_dict = documentary_verification_document.from_dict(documentary_verification_document_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


