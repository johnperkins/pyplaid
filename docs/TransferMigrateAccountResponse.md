# TransferMigrateAccountResponse

Defines the response schema for `/transfer/migrate_account`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_token** | **str** | The Plaid &#x60;access_token&#x60; for the newly created Item. | 
**account_id** | **str** | The Plaid &#x60;account_id&#x60; for the newly created Item. | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.transfer_migrate_account_response import TransferMigrateAccountResponse

# TODO update the JSON string below
json = "{}"
# create an instance of TransferMigrateAccountResponse from a JSON string
transfer_migrate_account_response_instance = TransferMigrateAccountResponse.from_json(json)
# print the JSON string representation of the object
print TransferMigrateAccountResponse.to_json()

# convert the object into a dict
transfer_migrate_account_response_dict = transfer_migrate_account_response_instance.to_dict()
# create an instance of TransferMigrateAccountResponse from a dict
transfer_migrate_account_response_form_dict = transfer_migrate_account_response.from_dict(transfer_migrate_account_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


