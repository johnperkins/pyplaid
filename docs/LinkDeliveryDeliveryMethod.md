# LinkDeliveryDeliveryMethod

The delivery method to be used to deliver the Link Delivery URL.  `SMS`: The URL will be delivered through SMS  `EMAIL`: The URL will be delivered through email

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


