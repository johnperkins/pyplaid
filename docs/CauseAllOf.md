# CauseAllOf


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**item_id** | **str** | The &#x60;item_id&#x60; of the Item associated with this webhook, warning, or error | [optional] 

## Example

```python
from pyplaid.models.cause_all_of import CauseAllOf

# TODO update the JSON string below
json = "{}"
# create an instance of CauseAllOf from a JSON string
cause_all_of_instance = CauseAllOf.from_json(json)
# print the JSON string representation of the object
print CauseAllOf.to_json()

# convert the object into a dict
cause_all_of_dict = cause_all_of_instance.to_dict()
# create an instance of CauseAllOf from a dict
cause_all_of_form_dict = cause_all_of.from_dict(cause_all_of_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


