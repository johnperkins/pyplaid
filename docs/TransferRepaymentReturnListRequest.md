# TransferRepaymentReturnListRequest

Defines the request schema for `/transfer/repayment/return/list`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**repayment_id** | **str** | Identifier of the repayment to query. | 
**count** | **int** | The maximum number of repayments to return. | [optional] [default to 25]
**offset** | **int** | The number of repayments to skip before returning results. | [optional] [default to 0]

## Example

```python
from pyplaid.models.transfer_repayment_return_list_request import TransferRepaymentReturnListRequest

# TODO update the JSON string below
json = "{}"
# create an instance of TransferRepaymentReturnListRequest from a JSON string
transfer_repayment_return_list_request_instance = TransferRepaymentReturnListRequest.from_json(json)
# print the JSON string representation of the object
print TransferRepaymentReturnListRequest.to_json()

# convert the object into a dict
transfer_repayment_return_list_request_dict = transfer_repayment_return_list_request_instance.to_dict()
# create an instance of TransferRepaymentReturnListRequest from a dict
transfer_repayment_return_list_request_form_dict = transfer_repayment_return_list_request.from_dict(transfer_repayment_return_list_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


