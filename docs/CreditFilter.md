# CreditFilter

A filter to apply to `credit`-type accounts

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_subtypes** | [**List[CreditAccountSubtype]**](CreditAccountSubtype.md) | An array of account subtypes to display in Link. If not specified, all account subtypes will be shown. For a full list of valid types and subtypes, see the [Account schema](https://plaid.com/docs/api/accounts#account-type-schema).  | 

## Example

```python
from pyplaid.models.credit_filter import CreditFilter

# TODO update the JSON string below
json = "{}"
# create an instance of CreditFilter from a JSON string
credit_filter_instance = CreditFilter.from_json(json)
# print the JSON string representation of the object
print CreditFilter.to_json()

# convert the object into a dict
credit_filter_dict = credit_filter_instance.to_dict()
# create an instance of CreditFilter from a dict
credit_filter_form_dict = credit_filter.from_dict(credit_filter_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


