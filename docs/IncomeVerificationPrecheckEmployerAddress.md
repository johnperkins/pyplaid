# IncomeVerificationPrecheckEmployerAddress

The address of the employer

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**city** | **str** | The full city name | [optional] 
**country** | **str** | The ISO 3166-1 alpha-2 country code | [optional] 
**postal_code** | **str** | The postal code. In API versions 2018-05-22 and earlier, this field is called &#x60;zip&#x60;. | [optional] 
**region** | **str** | The region or state. In API versions 2018-05-22 and earlier, this field is called &#x60;state&#x60;. Example: &#x60;\&quot;NC\&quot;&#x60; | [optional] 
**street** | **str** | The full street address Example: &#x60;\&quot;564 Main Street, APT 15\&quot;&#x60; | [optional] 

## Example

```python
from pyplaid.models.income_verification_precheck_employer_address import IncomeVerificationPrecheckEmployerAddress

# TODO update the JSON string below
json = "{}"
# create an instance of IncomeVerificationPrecheckEmployerAddress from a JSON string
income_verification_precheck_employer_address_instance = IncomeVerificationPrecheckEmployerAddress.from_json(json)
# print the JSON string representation of the object
print IncomeVerificationPrecheckEmployerAddress.to_json()

# convert the object into a dict
income_verification_precheck_employer_address_dict = income_verification_precheck_employer_address_instance.to_dict()
# create an instance of IncomeVerificationPrecheckEmployerAddress from a dict
income_verification_precheck_employer_address_form_dict = income_verification_precheck_employer_address.from_dict(income_verification_precheck_employer_address_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


