# PaymentProfileRemoveResponse

PaymentProfileRemoveResponse defines the response schema for `/payment_profile/remove`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.payment_profile_remove_response import PaymentProfileRemoveResponse

# TODO update the JSON string below
json = "{}"
# create an instance of PaymentProfileRemoveResponse from a JSON string
payment_profile_remove_response_instance = PaymentProfileRemoveResponse.from_json(json)
# print the JSON string representation of the object
print PaymentProfileRemoveResponse.to_json()

# convert the object into a dict
payment_profile_remove_response_dict = payment_profile_remove_response_instance.to_dict()
# create an instance of PaymentProfileRemoveResponse from a dict
payment_profile_remove_response_form_dict = payment_profile_remove_response.from_dict(payment_profile_remove_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


