# CreditBankIncomeItem

The details and metadata for an end user's Item.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bank_income_accounts** | [**List[CreditBankIncomeAccount]**](CreditBankIncomeAccount.md) | The Item&#39;s accounts that have Bank Income data. | [optional] 
**bank_income_sources** | [**List[CreditBankIncomeSource]**](CreditBankIncomeSource.md) | The income sources for this Item. Each entry in the array is a single income source. | [optional] 
**last_updated_time** | **datetime** | The time when this Item&#39;s data was last retrieved from the financial institution. | [optional] 
**institution_id** | **str** | The unique identifier of the institution associated with the Item. | [optional] 
**institution_name** | **str** | The name of the institution associated with the Item. | [optional] 
**item_id** | **str** | The unique identifier for the Item. | [optional] 

## Example

```python
from pyplaid.models.credit_bank_income_item import CreditBankIncomeItem

# TODO update the JSON string below
json = "{}"
# create an instance of CreditBankIncomeItem from a JSON string
credit_bank_income_item_instance = CreditBankIncomeItem.from_json(json)
# print the JSON string representation of the object
print CreditBankIncomeItem.to_json()

# convert the object into a dict
credit_bank_income_item_dict = credit_bank_income_item_instance.to_dict()
# create an instance of CreditBankIncomeItem from a dict
credit_bank_income_item_form_dict = credit_bank_income_item.from_dict(credit_bank_income_item_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


