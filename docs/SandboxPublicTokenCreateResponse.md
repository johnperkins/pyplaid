# SandboxPublicTokenCreateResponse

SandboxPublicTokenCreateResponse defines the response schema for `/sandbox/public_token/create`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**public_token** | **str** | A public token that can be exchanged for an access token using &#x60;/item/public_token/exchange&#x60; | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.sandbox_public_token_create_response import SandboxPublicTokenCreateResponse

# TODO update the JSON string below
json = "{}"
# create an instance of SandboxPublicTokenCreateResponse from a JSON string
sandbox_public_token_create_response_instance = SandboxPublicTokenCreateResponse.from_json(json)
# print the JSON string representation of the object
print SandboxPublicTokenCreateResponse.to_json()

# convert the object into a dict
sandbox_public_token_create_response_dict = sandbox_public_token_create_response_instance.to_dict()
# create an instance of SandboxPublicTokenCreateResponse from a dict
sandbox_public_token_create_response_form_dict = sandbox_public_token_create_response.from_dict(sandbox_public_token_create_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


