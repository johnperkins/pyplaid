# TaxpayerIdentifier

Information about the Taxpayer identification values assigned to the individual or legal entity.Information about the Taxpayer identification values assigned to the individual or legal entity.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**taxpayer_identifier_type** | [**TaxpayerIdentifierType**](TaxpayerIdentifierType.md) |  | 
**taxpayer_identifier_value** | **str** | The value of the taxpayer identifier as assigned by the IRS to the individual or legal entity. | 

## Example

```python
from pyplaid.models.taxpayer_identifier import TaxpayerIdentifier

# TODO update the JSON string below
json = "{}"
# create an instance of TaxpayerIdentifier from a JSON string
taxpayer_identifier_instance = TaxpayerIdentifier.from_json(json)
# print the JSON string representation of the object
print TaxpayerIdentifier.to_json()

# convert the object into a dict
taxpayer_identifier_dict = taxpayer_identifier_instance.to_dict()
# create an instance of TaxpayerIdentifier from a dict
taxpayer_identifier_form_dict = taxpayer_identifier.from_dict(taxpayer_identifier_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


