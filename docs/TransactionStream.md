# TransactionStream

A grouping of related transactions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_id** | **str** | The ID of the account to which the stream belongs | 
**stream_id** | **str** | A unique id for the stream | 
**category_id** | **str** | The ID of the category to which this transaction belongs. See [Categories](https://plaid.com/docs/#category-overview). | 
**category** | **List[str]** | A hierarchical array of the categories to which this transaction belongs. See [Categories](https://plaid.com/docs/#category-overview). | 
**description** | **str** | A description of the transaction stream. | 
**merchant_name** | **str** | The merchant associated with the transaction stream. | 
**first_date** | **date** | The posted date of the earliest transaction in the stream. | 
**last_date** | **date** | The posted date of the latest transaction in the stream. | 
**frequency** | [**RecurringTransactionFrequency**](RecurringTransactionFrequency.md) |  | 
**transaction_ids** | **List[str]** | An array of Plaid transaction IDs belonging to the stream, sorted by posted date. | 
**average_amount** | [**TransactionStreamAmount**](TransactionStreamAmount.md) |  | 
**last_amount** | [**TransactionStreamAmount**](TransactionStreamAmount.md) |  | 
**is_active** | **bool** | Indicates whether the transaction stream is still live. | 
**status** | [**TransactionStreamStatus**](TransactionStreamStatus.md) |  | 
**personal_finance_category** | [**PersonalFinanceCategory**](PersonalFinanceCategory.md) |  | [optional] 

## Example

```python
from pyplaid.models.transaction_stream import TransactionStream

# TODO update the JSON string below
json = "{}"
# create an instance of TransactionStream from a JSON string
transaction_stream_instance = TransactionStream.from_json(json)
# print the JSON string representation of the object
print TransactionStream.to_json()

# convert the object into a dict
transaction_stream_dict = transaction_stream_instance.to_dict()
# create an instance of TransactionStream from a dict
transaction_stream_form_dict = transaction_stream.from_dict(transaction_stream_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


