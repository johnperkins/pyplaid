# PaymentInitiationPaymentReverseResponse

PaymentInitiationPaymentReverseResponse defines the response schema for `/payment_initation/payment/reverse`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**refund_id** | **str** | A unique ID identifying the refund | 
**status** | [**WalletTransactionStatus**](WalletTransactionStatus.md) |  | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.payment_initiation_payment_reverse_response import PaymentInitiationPaymentReverseResponse

# TODO update the JSON string below
json = "{}"
# create an instance of PaymentInitiationPaymentReverseResponse from a JSON string
payment_initiation_payment_reverse_response_instance = PaymentInitiationPaymentReverseResponse.from_json(json)
# print the JSON string representation of the object
print PaymentInitiationPaymentReverseResponse.to_json()

# convert the object into a dict
payment_initiation_payment_reverse_response_dict = payment_initiation_payment_reverse_response_instance.to_dict()
# create an instance of PaymentInitiationPaymentReverseResponse from a dict
payment_initiation_payment_reverse_response_form_dict = payment_initiation_payment_reverse_response.from_dict(payment_initiation_payment_reverse_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


