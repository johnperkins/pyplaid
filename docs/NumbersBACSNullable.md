# NumbersBACSNullable

Identifying information for transferring money to or from a UK bank account via BACS.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_id** | **str** | The Plaid account ID associated with the account numbers | 
**account** | **str** | The BACS account number for the account | 
**sort_code** | **str** | The BACS sort code for the account | 

## Example

```python
from pyplaid.models.numbers_bacs_nullable import NumbersBACSNullable

# TODO update the JSON string below
json = "{}"
# create an instance of NumbersBACSNullable from a JSON string
numbers_bacs_nullable_instance = NumbersBACSNullable.from_json(json)
# print the JSON string representation of the object
print NumbersBACSNullable.to_json()

# convert the object into a dict
numbers_bacs_nullable_dict = numbers_bacs_nullable_instance.to_dict()
# create an instance of NumbersBACSNullable from a dict
numbers_bacs_nullable_form_dict = numbers_bacs_nullable.from_dict(numbers_bacs_nullable_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


