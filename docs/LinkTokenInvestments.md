# LinkTokenInvestments

Configuration parameters for the Investments product

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**allow_unverified_crypto_wallets** | **bool** | If &#x60;true&#x60;, allow self-custody crypto wallets to be added without requiring signature verification. Defaults to &#x60;false&#x60;. | [optional] 

## Example

```python
from pyplaid.models.link_token_investments import LinkTokenInvestments

# TODO update the JSON string below
json = "{}"
# create an instance of LinkTokenInvestments from a JSON string
link_token_investments_instance = LinkTokenInvestments.from_json(json)
# print the JSON string representation of the object
print LinkTokenInvestments.to_json()

# convert the object into a dict
link_token_investments_dict = link_token_investments_instance.to_dict()
# create an instance of LinkTokenInvestments from a dict
link_token_investments_form_dict = link_token_investments.from_dict(link_token_investments_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


