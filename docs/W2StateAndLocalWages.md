# W2StateAndLocalWages

W2 state and local wages

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**state** | **str** | State associated with the wage. | [optional] 
**employer_state_id_number** | **str** | State identification number of the employer. | [optional] 
**state_wages_tips** | **str** | Wages and tips from the specified state. | [optional] 
**state_income_tax** | **str** | Income tax from the specified state. | [optional] 
**local_wages_tips** | **str** | Wages and tips from the locality. | [optional] 
**local_income_tax** | **str** | Income tax from the locality. | [optional] 
**locality_name** | **str** | Name of the locality. | [optional] 

## Example

```python
from pyplaid.models.w2_state_and_local_wages import W2StateAndLocalWages

# TODO update the JSON string below
json = "{}"
# create an instance of W2StateAndLocalWages from a JSON string
w2_state_and_local_wages_instance = W2StateAndLocalWages.from_json(json)
# print the JSON string representation of the object
print W2StateAndLocalWages.to_json()

# convert the object into a dict
w2_state_and_local_wages_dict = w2_state_and_local_wages_instance.to_dict()
# create an instance of W2StateAndLocalWages from a dict
w2_state_and_local_wages_form_dict = w2_state_and_local_wages.from_dict(w2_state_and_local_wages_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


