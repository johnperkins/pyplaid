# AssetReportAuditCopyRemoveRequest

AssetReportAuditCopyRemoveRequest defines the request schema for `/asset_report/audit_copy/remove`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**audit_copy_token** | **str** | The &#x60;audit_copy_token&#x60; granting access to the Audit Copy you would like to revoke. | 

## Example

```python
from pyplaid.models.asset_report_audit_copy_remove_request import AssetReportAuditCopyRemoveRequest

# TODO update the JSON string below
json = "{}"
# create an instance of AssetReportAuditCopyRemoveRequest from a JSON string
asset_report_audit_copy_remove_request_instance = AssetReportAuditCopyRemoveRequest.from_json(json)
# print the JSON string representation of the object
print AssetReportAuditCopyRemoveRequest.to_json()

# convert the object into a dict
asset_report_audit_copy_remove_request_dict = asset_report_audit_copy_remove_request_instance.to_dict()
# create an instance of AssetReportAuditCopyRemoveRequest from a dict
asset_report_audit_copy_remove_request_form_dict = asset_report_audit_copy_remove_request.from_dict(asset_report_audit_copy_remove_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


