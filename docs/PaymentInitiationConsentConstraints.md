# PaymentInitiationConsentConstraints

Limitations that will be applied to payments initiated using the payment consent.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**valid_date_time** | [**PaymentConsentValidDateTime**](PaymentConsentValidDateTime.md) |  | [optional] 
**max_payment_amount** | [**PaymentConsentMaxPaymentAmount**](PaymentConsentMaxPaymentAmount.md) |  | 
**periodic_amounts** | [**List[PaymentConsentPeriodicAmount]**](PaymentConsentPeriodicAmount.md) | A list of amount limitations per period of time. | 

## Example

```python
from pyplaid.models.payment_initiation_consent_constraints import PaymentInitiationConsentConstraints

# TODO update the JSON string below
json = "{}"
# create an instance of PaymentInitiationConsentConstraints from a JSON string
payment_initiation_consent_constraints_instance = PaymentInitiationConsentConstraints.from_json(json)
# print the JSON string representation of the object
print PaymentInitiationConsentConstraints.to_json()

# convert the object into a dict
payment_initiation_consent_constraints_dict = payment_initiation_consent_constraints_instance.to_dict()
# create an instance of PaymentInitiationConsentConstraints from a dict
payment_initiation_consent_constraints_form_dict = payment_initiation_consent_constraints.from_dict(payment_initiation_consent_constraints_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


