# PartnerEndCustomerWithSecretsAllOf


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**secrets** | [**PartnerEndCustomerSecrets**](PartnerEndCustomerSecrets.md) |  | [optional] 

## Example

```python
from pyplaid.models.partner_end_customer_with_secrets_all_of import PartnerEndCustomerWithSecretsAllOf

# TODO update the JSON string below
json = "{}"
# create an instance of PartnerEndCustomerWithSecretsAllOf from a JSON string
partner_end_customer_with_secrets_all_of_instance = PartnerEndCustomerWithSecretsAllOf.from_json(json)
# print the JSON string representation of the object
print PartnerEndCustomerWithSecretsAllOf.to_json()

# convert the object into a dict
partner_end_customer_with_secrets_all_of_dict = partner_end_customer_with_secrets_all_of_instance.to_dict()
# create an instance of PartnerEndCustomerWithSecretsAllOf from a dict
partner_end_customer_with_secrets_all_of_form_dict = partner_end_customer_with_secrets_all_of.from_dict(partner_end_customer_with_secrets_all_of_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


