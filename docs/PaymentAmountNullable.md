# PaymentAmountNullable

The amount and currency of a payment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**currency** | [**PaymentAmountCurrency**](PaymentAmountCurrency.md) |  | 
**value** | **float** | The amount of the payment. Must contain at most two digits of precision e.g. &#x60;1.23&#x60;. Minimum accepted value is &#x60;1&#x60;. | 

## Example

```python
from pyplaid.models.payment_amount_nullable import PaymentAmountNullable

# TODO update the JSON string below
json = "{}"
# create an instance of PaymentAmountNullable from a JSON string
payment_amount_nullable_instance = PaymentAmountNullable.from_json(json)
# print the JSON string representation of the object
print PaymentAmountNullable.to_json()

# convert the object into a dict
payment_amount_nullable_dict = payment_amount_nullable_instance.to_dict()
# create an instance of PaymentAmountNullable from a dict
payment_amount_nullable_form_dict = payment_amount_nullable.from_dict(payment_amount_nullable_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


