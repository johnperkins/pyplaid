# ItemWebhookUpdateResponse

ItemWebhookUpdateResponse defines the response schema for `/item/webhook/update`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**item** | [**Item**](Item.md) |  | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.item_webhook_update_response import ItemWebhookUpdateResponse

# TODO update the JSON string below
json = "{}"
# create an instance of ItemWebhookUpdateResponse from a JSON string
item_webhook_update_response_instance = ItemWebhookUpdateResponse.from_json(json)
# print the JSON string representation of the object
print ItemWebhookUpdateResponse.to_json()

# convert the object into a dict
item_webhook_update_response_dict = item_webhook_update_response_instance.to_dict()
# create an instance of ItemWebhookUpdateResponse from a dict
item_webhook_update_response_form_dict = item_webhook_update_response.from_dict(item_webhook_update_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


