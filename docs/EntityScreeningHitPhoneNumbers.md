# EntityScreeningHitPhoneNumbers

Phone number information associated with the entity screening hit

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | [**PhoneType**](PhoneType.md) |  | 
**phone_number** | **str** | A phone number in E.164 format. | 

## Example

```python
from pyplaid.models.entity_screening_hit_phone_numbers import EntityScreeningHitPhoneNumbers

# TODO update the JSON string below
json = "{}"
# create an instance of EntityScreeningHitPhoneNumbers from a JSON string
entity_screening_hit_phone_numbers_instance = EntityScreeningHitPhoneNumbers.from_json(json)
# print the JSON string representation of the object
print EntityScreeningHitPhoneNumbers.to_json()

# convert the object into a dict
entity_screening_hit_phone_numbers_dict = entity_screening_hit_phone_numbers_instance.to_dict()
# create an instance of EntityScreeningHitPhoneNumbers from a dict
entity_screening_hit_phone_numbers_form_dict = entity_screening_hit_phone_numbers.from_dict(entity_screening_hit_phone_numbers_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


