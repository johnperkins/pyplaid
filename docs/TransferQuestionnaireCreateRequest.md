# TransferQuestionnaireCreateRequest

Defines the request schema for `/transfer/questionnaire/create`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**originator_client_id** | **str** | Client ID of the end customer. | 
**redirect_uri** | **str** | URL the end customer will be redirected to after completing questions in Plaid-hosted onboarding flow. | 
**disbursement_limits** | [**DisbursementLimits**](DisbursementLimits.md) |  | 
**payment_limits** | [**PaymentLimits**](PaymentLimits.md) |  | 
**transaction_frequency** | [**TransactionFrequency**](TransactionFrequency.md) |  | 

## Example

```python
from pyplaid.models.transfer_questionnaire_create_request import TransferQuestionnaireCreateRequest

# TODO update the JSON string below
json = "{}"
# create an instance of TransferQuestionnaireCreateRequest from a JSON string
transfer_questionnaire_create_request_instance = TransferQuestionnaireCreateRequest.from_json(json)
# print the JSON string representation of the object
print TransferQuestionnaireCreateRequest.to_json()

# convert the object into a dict
transfer_questionnaire_create_request_dict = transfer_questionnaire_create_request_instance.to_dict()
# create an instance of TransferQuestionnaireCreateRequest from a dict
transfer_questionnaire_create_request_form_dict = transfer_questionnaire_create_request.from_dict(transfer_questionnaire_create_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


