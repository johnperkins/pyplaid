# EntityWatchlistScreening

The entity screening object allows you to represent an entity in your system, update its profile, and search for it on various watchlists. Note: Rejected entity screenings will not receive new hits, regardless of entity program configuration.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | ID of the associated entity screening. | 
**search_terms** | [**EntityWatchlistScreeningSearchTerms**](EntityWatchlistScreeningSearchTerms.md) |  | 
**assignee** | **str** | ID of the associated user. | 
**status** | [**WatchlistScreeningStatus**](WatchlistScreeningStatus.md) |  | 
**client_user_id** | **str** | An identifier to help you connect this object to your internal systems. For example, your database ID corresponding to this object. | 
**audit_trail** | [**WatchlistScreeningAuditTrail**](WatchlistScreeningAuditTrail.md) |  | 

## Example

```python
from pyplaid.models.entity_watchlist_screening import EntityWatchlistScreening

# TODO update the JSON string below
json = "{}"
# create an instance of EntityWatchlistScreening from a JSON string
entity_watchlist_screening_instance = EntityWatchlistScreening.from_json(json)
# print the JSON string representation of the object
print EntityWatchlistScreening.to_json()

# convert the object into a dict
entity_watchlist_screening_dict = entity_watchlist_screening_instance.to_dict()
# create an instance of EntityWatchlistScreening from a dict
entity_watchlist_screening_form_dict = entity_watchlist_screening.from_dict(entity_watchlist_screening_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


