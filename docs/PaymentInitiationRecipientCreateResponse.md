# PaymentInitiationRecipientCreateResponse

PaymentInitiationRecipientCreateResponse defines the response schema for `/payment_initation/recipient/create`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**recipient_id** | **str** | A unique ID identifying the recipient | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.payment_initiation_recipient_create_response import PaymentInitiationRecipientCreateResponse

# TODO update the JSON string below
json = "{}"
# create an instance of PaymentInitiationRecipientCreateResponse from a JSON string
payment_initiation_recipient_create_response_instance = PaymentInitiationRecipientCreateResponse.from_json(json)
# print the JSON string representation of the object
print PaymentInitiationRecipientCreateResponse.to_json()

# convert the object into a dict
payment_initiation_recipient_create_response_dict = payment_initiation_recipient_create_response_instance.to_dict()
# create an instance of PaymentInitiationRecipientCreateResponse from a dict
payment_initiation_recipient_create_response_form_dict = payment_initiation_recipient_create_response.from_dict(payment_initiation_recipient_create_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


