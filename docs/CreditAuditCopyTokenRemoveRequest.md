# CreditAuditCopyTokenRemoveRequest

CreditAuditCopyTokenRemoveRequest defines the request schema for `/credit/audit_copy_token/remove`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**audit_copy_token** | **str** | The &#x60;audit_copy_token&#x60; granting access to the Audit Copy you would like to revoke. | 

## Example

```python
from pyplaid.models.credit_audit_copy_token_remove_request import CreditAuditCopyTokenRemoveRequest

# TODO update the JSON string below
json = "{}"
# create an instance of CreditAuditCopyTokenRemoveRequest from a JSON string
credit_audit_copy_token_remove_request_instance = CreditAuditCopyTokenRemoveRequest.from_json(json)
# print the JSON string representation of the object
print CreditAuditCopyTokenRemoveRequest.to_json()

# convert the object into a dict
credit_audit_copy_token_remove_request_dict = credit_audit_copy_token_remove_request_instance.to_dict()
# create an instance of CreditAuditCopyTokenRemoveRequest from a dict
credit_audit_copy_token_remove_request_form_dict = credit_audit_copy_token_remove_request.from_dict(credit_audit_copy_token_remove_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


