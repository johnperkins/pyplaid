# CreditBankIncomeGetResponse

CreditBankIncomeGetResponse defines the response schema for `/credit/bank_income/get`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bank_income** | [**List[CreditBankIncome]**](CreditBankIncome.md) |  | [optional] 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.credit_bank_income_get_response import CreditBankIncomeGetResponse

# TODO update the JSON string below
json = "{}"
# create an instance of CreditBankIncomeGetResponse from a JSON string
credit_bank_income_get_response_instance = CreditBankIncomeGetResponse.from_json(json)
# print the JSON string representation of the object
print CreditBankIncomeGetResponse.to_json()

# convert the object into a dict
credit_bank_income_get_response_dict = credit_bank_income_get_response_instance.to_dict()
# create an instance of CreditBankIncomeGetResponse from a dict
credit_bank_income_get_response_form_dict = credit_bank_income_get_response.from_dict(credit_bank_income_get_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


