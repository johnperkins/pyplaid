# IncomeVerificationPrecheckResponse

IncomeVerificationPrecheckResponse defines the response schema for `/income/verification/precheck`.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**precheck_id** | **str** | ID of the precheck. Provide this value when calling &#x60;/link/token/create&#x60; in order to optimize Link conversion. | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 
**confidence** | [**IncomeVerificationPrecheckConfidence**](IncomeVerificationPrecheckConfidence.md) |  | 

## Example

```python
from pyplaid.models.income_verification_precheck_response import IncomeVerificationPrecheckResponse

# TODO update the JSON string below
json = "{}"
# create an instance of IncomeVerificationPrecheckResponse from a JSON string
income_verification_precheck_response_instance = IncomeVerificationPrecheckResponse.from_json(json)
# print the JSON string representation of the object
print IncomeVerificationPrecheckResponse.to_json()

# convert the object into a dict
income_verification_precheck_response_dict = income_verification_precheck_response_instance.to_dict()
# create an instance of IncomeVerificationPrecheckResponse from a dict
income_verification_precheck_response_form_dict = income_verification_precheck_response.from_dict(income_verification_precheck_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


