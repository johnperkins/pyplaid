# PaymentInitiationConsentCreateResponse

PaymentInitiationConsentCreateResponse defines the response schema for `/payment_initiation/consent/create`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**consent_id** | **str** | A unique ID identifying the payment consent. | 
**status** | [**PaymentInitiationConsentStatus**](PaymentInitiationConsentStatus.md) |  | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.payment_initiation_consent_create_response import PaymentInitiationConsentCreateResponse

# TODO update the JSON string below
json = "{}"
# create an instance of PaymentInitiationConsentCreateResponse from a JSON string
payment_initiation_consent_create_response_instance = PaymentInitiationConsentCreateResponse.from_json(json)
# print the JSON string representation of the object
print PaymentInitiationConsentCreateResponse.to_json()

# convert the object into a dict
payment_initiation_consent_create_response_dict = payment_initiation_consent_create_response_instance.to_dict()
# create an instance of PaymentInitiationConsentCreateResponse from a dict
payment_initiation_consent_create_response_form_dict = payment_initiation_consent_create_response.from_dict(payment_initiation_consent_create_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


