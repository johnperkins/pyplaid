# BankTransferBalance

Information about the balance of a bank transfer

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**available** | **str** | The total available balance - the sum of all successful debit transfer amounts minus all credit transfer amounts. | 
**transactable** | **str** | The transactable balance shows the amount in your account that you are able to use for transfers, and is essentially your available balance minus your minimum balance. | 

## Example

```python
from pyplaid.models.bank_transfer_balance import BankTransferBalance

# TODO update the JSON string below
json = "{}"
# create an instance of BankTransferBalance from a JSON string
bank_transfer_balance_instance = BankTransferBalance.from_json(json)
# print the JSON string representation of the object
print BankTransferBalance.to_json()

# convert the object into a dict
bank_transfer_balance_dict = bank_transfer_balance_instance.to_dict()
# create an instance of BankTransferBalance from a dict
bank_transfer_balance_form_dict = bank_transfer_balance.from_dict(bank_transfer_balance_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


