# CustomerInitiatedReturnRisk

The object contains a risk score and a risk tier that evaluate the transaction return risk of an unauthorized debit. Common return codes in this category include: \"R05\", \"R07\", \"R10\", \"R11\", \"R29\". These returns typically have a return time frame of up to 60 calendar days. During this period, the customer of financial institutions can dispute a transaction as unauthorized.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**score** | **int** | A score from 1-99 that indicates the transaction return risk: a higher risk score suggests a higher return likelihood. | 
**risk_tier** | **int** | A tier corresponding to the projected likelihood that the transaction, if initiated, will be subject to a return.  In the &#x60;customer_initiated_return_risk&#x60; object, there are five risk tiers corresponding to the scores:   1: Predicted customer-initiated return incidence rate between 0.00% - 0.02%   2: Predicted customer-initiated return incidence rate between 0.02% - 0.05%   3: Predicted customer-initiated return incidence rate between 0.05% - 0.1%   4: Predicted customer-initiated return incidence rate between 0.1% - 0.5%   5: Predicted customer-initiated return incidence rate greater than 0.5%  | 

## Example

```python
from pyplaid.models.customer_initiated_return_risk import CustomerInitiatedReturnRisk

# TODO update the JSON string below
json = "{}"
# create an instance of CustomerInitiatedReturnRisk from a JSON string
customer_initiated_return_risk_instance = CustomerInitiatedReturnRisk.from_json(json)
# print the JSON string representation of the object
print CustomerInitiatedReturnRisk.to_json()

# convert the object into a dict
customer_initiated_return_risk_dict = customer_initiated_return_risk_instance.to_dict()
# create an instance of CustomerInitiatedReturnRisk from a dict
customer_initiated_return_risk_form_dict = customer_initiated_return_risk.from_dict(customer_initiated_return_risk_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


