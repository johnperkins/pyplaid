# PartnerEndCustomerWithSecrets

The details for the newly created end customer, including secrets for non-Production environments.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** |  | [optional] 
**company_name** | **str** |  | [optional] 
**status** | [**PartnerEndCustomerStatus**](PartnerEndCustomerStatus.md) |  | [optional] 
**secrets** | [**PartnerEndCustomerSecrets**](PartnerEndCustomerSecrets.md) |  | [optional] 

## Example

```python
from pyplaid.models.partner_end_customer_with_secrets import PartnerEndCustomerWithSecrets

# TODO update the JSON string below
json = "{}"
# create an instance of PartnerEndCustomerWithSecrets from a JSON string
partner_end_customer_with_secrets_instance = PartnerEndCustomerWithSecrets.from_json(json)
# print the JSON string representation of the object
print PartnerEndCustomerWithSecrets.to_json()

# convert the object into a dict
partner_end_customer_with_secrets_dict = partner_end_customer_with_secrets_instance.to_dict()
# create an instance of PartnerEndCustomerWithSecrets from a dict
partner_end_customer_with_secrets_form_dict = partner_end_customer_with_secrets.from_dict(partner_end_customer_with_secrets_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


