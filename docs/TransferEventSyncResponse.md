# TransferEventSyncResponse

Defines the response schema for `/transfer/event/sync`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**transfer_events** | [**List[TransferEvent]**](TransferEvent.md) |  | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.transfer_event_sync_response import TransferEventSyncResponse

# TODO update the JSON string below
json = "{}"
# create an instance of TransferEventSyncResponse from a JSON string
transfer_event_sync_response_instance = TransferEventSyncResponse.from_json(json)
# print the JSON string representation of the object
print TransferEventSyncResponse.to_json()

# convert the object into a dict
transfer_event_sync_response_dict = transfer_event_sync_response_instance.to_dict()
# create an instance of TransferEventSyncResponse from a dict
transfer_event_sync_response_form_dict = transfer_event_sync_response.from_dict(transfer_event_sync_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


