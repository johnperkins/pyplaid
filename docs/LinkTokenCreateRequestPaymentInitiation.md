# LinkTokenCreateRequestPaymentInitiation

Specifies options for initializing Link for use with the Payment Initiation (Europe) product. This field is required if `payment_initiation` is included in the `products` array. Either `payment_id` or `consent_id` must be provided.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**payment_id** | **str** | The &#x60;payment_id&#x60; provided by the &#x60;/payment_initiation/payment/create&#x60; endpoint. | [optional] 
**consent_id** | **str** | The &#x60;consent_id&#x60; provided by the &#x60;/payment_initiation/consent/create&#x60; endpoint. | [optional] 

## Example

```python
from pyplaid.models.link_token_create_request_payment_initiation import LinkTokenCreateRequestPaymentInitiation

# TODO update the JSON string below
json = "{}"
# create an instance of LinkTokenCreateRequestPaymentInitiation from a JSON string
link_token_create_request_payment_initiation_instance = LinkTokenCreateRequestPaymentInitiation.from_json(json)
# print the JSON string representation of the object
print LinkTokenCreateRequestPaymentInitiation.to_json()

# convert the object into a dict
link_token_create_request_payment_initiation_dict = link_token_create_request_payment_initiation_instance.to_dict()
# create an instance of LinkTokenCreateRequestPaymentInitiation from a dict
link_token_create_request_payment_initiation_form_dict = link_token_create_request_payment_initiation.from_dict(link_token_create_request_payment_initiation_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


