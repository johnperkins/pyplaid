# AddressMatchScore

Score found by matching address provided by the API with the address on the account at the financial institution. If the account contains multiple owners, the maximum match score is filled.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**score** | **int** | Match score for address. 100 is a perfect match and 0 is a no match. If the address is missing from either the API or financial institution, this is empty. | [optional] 
**is_postal_code_match** | **bool** | postal code was provided for both and was a match | [optional] 

## Example

```python
from pyplaid.models.address_match_score import AddressMatchScore

# TODO update the JSON string below
json = "{}"
# create an instance of AddressMatchScore from a JSON string
address_match_score_instance = AddressMatchScore.from_json(json)
# print the JSON string representation of the object
print AddressMatchScore.to_json()

# convert the object into a dict
address_match_score_dict = address_match_score_instance.to_dict()
# create an instance of AddressMatchScore from a dict
address_match_score_form_dict = address_match_score.from_dict(address_match_score_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


