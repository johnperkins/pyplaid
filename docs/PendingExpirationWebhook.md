# PendingExpirationWebhook

Fired when an Item’s access consent is expiring in 7 days. Some Items have explicit expiration times and we try to relay this when possible to reduce service disruption. This can be resolved by having the user go through Link’s update mode.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**webhook_type** | **str** | &#x60;ITEM&#x60; | 
**webhook_code** | **str** | &#x60;PENDING_EXPIRATION&#x60; | 
**item_id** | **str** | The &#x60;item_id&#x60; of the Item associated with this webhook, warning, or error | 
**consent_expiration_time** | **datetime** | The date and time at which the Item&#39;s access consent will expire, in [ISO 8601](https://wikipedia.org/wiki/ISO_8601) format | 
**environment** | [**WebhookEnvironmentValues**](WebhookEnvironmentValues.md) |  | 

## Example

```python
from pyplaid.models.pending_expiration_webhook import PendingExpirationWebhook

# TODO update the JSON string below
json = "{}"
# create an instance of PendingExpirationWebhook from a JSON string
pending_expiration_webhook_instance = PendingExpirationWebhook.from_json(json)
# print the JSON string representation of the object
print PendingExpirationWebhook.to_json()

# convert the object into a dict
pending_expiration_webhook_dict = pending_expiration_webhook_instance.to_dict()
# create an instance of PendingExpirationWebhook from a dict
pending_expiration_webhook_form_dict = pending_expiration_webhook.from_dict(pending_expiration_webhook_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


