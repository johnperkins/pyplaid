# SandboxBankTransferSimulateRequest

Defines the request schema for `/sandbox/bank_transfer/simulate`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**bank_transfer_id** | **str** | Plaid’s unique identifier for a bank transfer. | 
**event_type** | **str** | The asynchronous event to be simulated. May be: &#x60;posted&#x60;, &#x60;failed&#x60;, or &#x60;reversed&#x60;.  An error will be returned if the event type is incompatible with the current transfer status. Compatible status --&gt; event type transitions include:  &#x60;pending&#x60; --&gt; &#x60;failed&#x60;  &#x60;pending&#x60; --&gt; &#x60;posted&#x60;  &#x60;posted&#x60; --&gt; &#x60;reversed&#x60;  | 
**failure_reason** | [**BankTransferFailure**](BankTransferFailure.md) |  | [optional] 

## Example

```python
from pyplaid.models.sandbox_bank_transfer_simulate_request import SandboxBankTransferSimulateRequest

# TODO update the JSON string below
json = "{}"
# create an instance of SandboxBankTransferSimulateRequest from a JSON string
sandbox_bank_transfer_simulate_request_instance = SandboxBankTransferSimulateRequest.from_json(json)
# print the JSON string representation of the object
print SandboxBankTransferSimulateRequest.to_json()

# convert the object into a dict
sandbox_bank_transfer_simulate_request_dict = sandbox_bank_transfer_simulate_request_instance.to_dict()
# create an instance of SandboxBankTransferSimulateRequest from a dict
sandbox_bank_transfer_simulate_request_form_dict = sandbox_bank_transfer_simulate_request.from_dict(sandbox_bank_transfer_simulate_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


