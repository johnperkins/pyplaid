# WatchlistScreeningIndividualReviewCreateRequest

Request input for creating a screening review

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**confirmed_hits** | **List[str]** | Hits to mark as a true positive after thorough manual review. These hits will never recur or be updated once dismissed. In most cases, confirmed hits indicate that the customer should be rejected. | 
**dismissed_hits** | **List[str]** | Hits to mark as a false positive after thorough manual review. These hits will never recur or be updated once dismissed. | 
**comment** | **str** | A comment submitted by a team member as part of reviewing a watchlist screening. | [optional] 
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**watchlist_screening_id** | **str** | ID of the associated screening. | 

## Example

```python
from pyplaid.models.watchlist_screening_individual_review_create_request import WatchlistScreeningIndividualReviewCreateRequest

# TODO update the JSON string below
json = "{}"
# create an instance of WatchlistScreeningIndividualReviewCreateRequest from a JSON string
watchlist_screening_individual_review_create_request_instance = WatchlistScreeningIndividualReviewCreateRequest.from_json(json)
# print the JSON string representation of the object
print WatchlistScreeningIndividualReviewCreateRequest.to_json()

# convert the object into a dict
watchlist_screening_individual_review_create_request_dict = watchlist_screening_individual_review_create_request_instance.to_dict()
# create an instance of WatchlistScreeningIndividualReviewCreateRequest from a dict
watchlist_screening_individual_review_create_request_form_dict = watchlist_screening_individual_review_create_request.from_dict(watchlist_screening_individual_review_create_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


