# Holding

A securities holding at an institution.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_id** | **str** | The Plaid &#x60;account_id&#x60; associated with the holding. | 
**security_id** | **str** | The Plaid &#x60;security_id&#x60; associated with the holding. The &#x60;security_id&#x60; may change if inherent details of the security change due to a corporate action, for example, in the event of a ticker symbol change or CUSIP change. | 
**institution_price** | **float** | The last price given by the institution for this security. | 
**institution_price_as_of** | **date** | The date at which &#x60;institution_price&#x60; was current. | [optional] 
**institution_price_datetime** | **datetime** | Date and time at which &#x60;institution_price&#x60; was current, in ISO 8601 format (YYYY-MM-DDTHH:mm:ssZ).  This field is returned for select financial institutions and comes as provided by the institution. It may contain default time values (such as 00:00:00).  | [optional] 
**institution_value** | **float** | The value of the holding, as reported by the institution. | 
**cost_basis** | **float** | The original total value of the holding. The accuracy of this field is dependent on the information provided by the institution. | 
**quantity** | **float** | The total quantity of the asset held, as reported by the financial institution. If the security is an option, &#x60;quantity&#x60; will reflect the total number of options (typically the number of contracts multiplied by 100), not the number of contracts. | 
**iso_currency_code** | **str** | The ISO-4217 currency code of the holding. Always &#x60;null&#x60; if &#x60;unofficial_currency_code&#x60; is non-&#x60;null&#x60;. | 
**unofficial_currency_code** | **str** | The unofficial currency code associated with the holding. Always &#x60;null&#x60; if &#x60;iso_currency_code&#x60; is non-&#x60;null&#x60;. Unofficial currency codes are used for currencies that do not have official ISO currency codes, such as cryptocurrencies and the currencies of certain countries.  See the [currency code schema](https://plaid.com/docs/api/accounts#currency-code-schema) for a full listing of supported &#x60;iso_currency_code&#x60;s.  | 

## Example

```python
from pyplaid.models.holding import Holding

# TODO update the JSON string below
json = "{}"
# create an instance of Holding from a JSON string
holding_instance = Holding.from_json(json)
# print the JSON string representation of the object
print Holding.to_json()

# convert the object into a dict
holding_dict = holding_instance.to_dict()
# create an instance of Holding from a dict
holding_form_dict = holding.from_dict(holding_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


