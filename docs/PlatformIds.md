# PlatformIds

An object containing a set of ids related to an employee

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**employee_id** | **str** | The ID of an employee as given by their employer | [optional] 
**payroll_id** | **str** | The ID of an employee as given by their payroll | [optional] 
**position_id** | **str** | The ID of the position of the employee | [optional] 

## Example

```python
from pyplaid.models.platform_ids import PlatformIds

# TODO update the JSON string below
json = "{}"
# create an instance of PlatformIds from a JSON string
platform_ids_instance = PlatformIds.from_json(json)
# print the JSON string representation of the object
print PlatformIds.to_json()

# convert the object into a dict
platform_ids_dict = platform_ids_instance.to_dict()
# create an instance of PlatformIds from a dict
platform_ids_form_dict = platform_ids.from_dict(platform_ids_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


