# LiabilitiesDefaultUpdateWebhook

The webhook of type `LIABILITIES` and code `DEFAULT_UPDATE` will be fired when new or updated liabilities have been detected on a liabilities item.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**webhook_type** | **str** | &#x60;LIABILITIES&#x60; | 
**webhook_code** | **str** | &#x60;DEFAULT_UPDATE&#x60; | 
**item_id** | **str** | The &#x60;item_id&#x60; of the Item associated with this webhook, warning, or error | 
**error** | [**PlaidError**](PlaidError.md) |  | 
**account_ids_with_new_liabilities** | **List[str]** | An array of &#x60;account_id&#x60;&#39;s for accounts that contain new liabilities.&#39; | 
**account_ids_with_updated_liabilities** | **Dict[str, List[str]]** | An object with keys of &#x60;account_id&#x60;&#39;s that are mapped to their respective liabilities fields that changed.  Example: &#x60;{ \&quot;XMBvvyMGQ1UoLbKByoMqH3nXMj84ALSdE5B58\&quot;: [\&quot;past_amount_due\&quot;] }&#x60;  | 
**environment** | [**WebhookEnvironmentValues**](WebhookEnvironmentValues.md) |  | 

## Example

```python
from pyplaid.models.liabilities_default_update_webhook import LiabilitiesDefaultUpdateWebhook

# TODO update the JSON string below
json = "{}"
# create an instance of LiabilitiesDefaultUpdateWebhook from a JSON string
liabilities_default_update_webhook_instance = LiabilitiesDefaultUpdateWebhook.from_json(json)
# print the JSON string representation of the object
print LiabilitiesDefaultUpdateWebhook.to_json()

# convert the object into a dict
liabilities_default_update_webhook_dict = liabilities_default_update_webhook_instance.to_dict()
# create an instance of LiabilitiesDefaultUpdateWebhook from a dict
liabilities_default_update_webhook_form_dict = liabilities_default_update_webhook.from_dict(liabilities_default_update_webhook_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


