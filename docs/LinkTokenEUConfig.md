# LinkTokenEUConfig

Configuration parameters for EU flows

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**headless** | **bool** | If &#x60;true&#x60;, open Link without an initial UI. Defaults to &#x60;false&#x60;. | [optional] 

## Example

```python
from pyplaid.models.link_token_eu_config import LinkTokenEUConfig

# TODO update the JSON string below
json = "{}"
# create an instance of LinkTokenEUConfig from a JSON string
link_token_eu_config_instance = LinkTokenEUConfig.from_json(json)
# print the JSON string representation of the object
print LinkTokenEUConfig.to_json()

# convert the object into a dict
link_token_eu_config_dict = link_token_eu_config_instance.to_dict()
# create an instance of LinkTokenEUConfig from a dict
link_token_eu_config_form_dict = link_token_eu_config.from_dict(link_token_eu_config_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


