# AccountIdentityAllOf


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**owners** | [**List[Owner]**](Owner.md) | Data returned by the financial institution about the account owner or owners. Only returned by Identity or Assets endpoints. For business accounts, the name reported may be either the name of the individual or the name of the business, depending on the institution. Multiple owners on a single account will be represented in the same &#x60;owner&#x60; object, not in multiple owner objects within the array. In API versions 2018-05-22 and earlier, the &#x60;owners&#x60; object is not returned, and instead identity information is returned in the top level &#x60;identity&#x60; object. For more details, see [Plaid API versioning](https://plaid.com/docs/api/versioning/#version-2019-05-29) | 

## Example

```python
from pyplaid.models.account_identity_all_of import AccountIdentityAllOf

# TODO update the JSON string below
json = "{}"
# create an instance of AccountIdentityAllOf from a JSON string
account_identity_all_of_instance = AccountIdentityAllOf.from_json(json)
# print the JSON string representation of the object
print AccountIdentityAllOf.to_json()

# convert the object into a dict
account_identity_all_of_dict = account_identity_all_of_instance.to_dict()
# create an instance of AccountIdentityAllOf from a dict
account_identity_all_of_form_dict = account_identity_all_of.from_dict(account_identity_all_of_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


