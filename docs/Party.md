# Party

A collection of information about a single party to a transaction. Included direct participants like the borrower and seller as well as indirect participants such as the flood certificate provider.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**individual** | [**PartyIndividual**](PartyIndividual.md) |  | 
**roles** | [**Roles**](Roles.md) |  | 
**taxpayer_identifiers** | [**TaxpayerIdentifiers**](TaxpayerIdentifiers.md) |  | 

## Example

```python
from pyplaid.models.party import Party

# TODO update the JSON string below
json = "{}"
# create an instance of Party from a JSON string
party_instance = Party.from_json(json)
# print the JSON string representation of the object
print Party.to_json()

# convert the object into a dict
party_dict = party_instance.to_dict()
# create an instance of Party from a dict
party_form_dict = party.from_dict(party_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


