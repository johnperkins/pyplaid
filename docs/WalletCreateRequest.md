# WalletCreateRequest

WalletCreateRequest defines the request schema for `/wallet/create`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**iso_currency_code** | [**WalletISOCurrencyCode**](WalletISOCurrencyCode.md) |  | 

## Example

```python
from pyplaid.models.wallet_create_request import WalletCreateRequest

# TODO update the JSON string below
json = "{}"
# create an instance of WalletCreateRequest from a JSON string
wallet_create_request_instance = WalletCreateRequest.from_json(json)
# print the JSON string representation of the object
print WalletCreateRequest.to_json()

# convert the object into a dict
wallet_create_request_dict = wallet_create_request_instance.to_dict()
# create an instance of WalletCreateRequest from a dict
wallet_create_request_form_dict = wallet_create_request.from_dict(wallet_create_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


