# TransactionsCategoryRule

A representation of a transactions category rule.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | A unique identifier of the rule created | [optional] 
**item_id** | **str** | A unique identifier of the item the rule was created for | [optional] 
**created_at** | **datetime** | Date and time when a rule was created in [ISO 8601](https://wikipedia.org/wiki/ISO_8601) format ( &#x60;YYYY-MM-DDTHH:mm:ssZ&#x60; ).  | [optional] 
**personal_finance_category** | **str** | Personal finance category unique identifier.  In the personal finance category taxonomy, this field is represented by the detailed category field.  | [optional] 
**rule_details** | [**TransactionsRuleDetails**](TransactionsRuleDetails.md) |  | [optional] 

## Example

```python
from pyplaid.models.transactions_category_rule import TransactionsCategoryRule

# TODO update the JSON string below
json = "{}"
# create an instance of TransactionsCategoryRule from a JSON string
transactions_category_rule_instance = TransactionsCategoryRule.from_json(json)
# print the JSON string representation of the object
print TransactionsCategoryRule.to_json()

# convert the object into a dict
transactions_category_rule_dict = transactions_category_rule_instance.to_dict()
# create an instance of TransactionsCategoryRule from a dict
transactions_category_rule_form_dict = transactions_category_rule.from_dict(transactions_category_rule_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


