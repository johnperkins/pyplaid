# AssetTransactions

No documentation available

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**asset_transaction** | [**List[AssetTransaction]**](AssetTransaction.md) |  | 

## Example

```python
from pyplaid.models.asset_transactions import AssetTransactions

# TODO update the JSON string below
json = "{}"
# create an instance of AssetTransactions from a JSON string
asset_transactions_instance = AssetTransactions.from_json(json)
# print the JSON string representation of the object
print AssetTransactions.to_json()

# convert the object into a dict
asset_transactions_dict = asset_transactions_instance.to_dict()
# create an instance of AssetTransactions from a dict
asset_transactions_form_dict = asset_transactions.from_dict(asset_transactions_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


