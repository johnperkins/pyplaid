# TransactionsRulesRemoveRequest

TransactionsRulesRemoveRequest defines the request schema for `/beta/transactions/rules/v1/remove`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**access_token** | **str** | The access token associated with the Item data is being requested for. | 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**rule_id** | **str** | A rule&#39;s unique identifier | 

## Example

```python
from pyplaid.models.transactions_rules_remove_request import TransactionsRulesRemoveRequest

# TODO update the JSON string below
json = "{}"
# create an instance of TransactionsRulesRemoveRequest from a JSON string
transactions_rules_remove_request_instance = TransactionsRulesRemoveRequest.from_json(json)
# print the JSON string representation of the object
print TransactionsRulesRemoveRequest.to_json()

# convert the object into a dict
transactions_rules_remove_request_dict = transactions_rules_remove_request_instance.to_dict()
# create an instance of TransactionsRulesRemoveRequest from a dict
transactions_rules_remove_request_form_dict = transactions_rules_remove_request.from_dict(transactions_rules_remove_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


