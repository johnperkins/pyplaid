# CreditBankIncomeAccount

The Item's accounts that have Bank Income data.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_id** | **str** | Plaid&#39;s unique identifier for the account. | [optional] 
**mask** | **str** | The last 2-4 alphanumeric characters of an account&#39;s official account number. Note that the mask may be non-unique between an Item&#39;s accounts, and it may also not match the mask that the bank displays to the user. | [optional] 
**name** | **str** | The name of the bank account. | [optional] 
**official_name** | **str** | The official name of the bank account. | [optional] 
**subtype** | [**DepositoryAccountSubtype**](DepositoryAccountSubtype.md) |  | [optional] 
**type** | [**CreditBankIncomeAccountType**](CreditBankIncomeAccountType.md) |  | [optional] 
**owners** | [**List[Owner]**](Owner.md) |  | [optional] 

## Example

```python
from pyplaid.models.credit_bank_income_account import CreditBankIncomeAccount

# TODO update the JSON string below
json = "{}"
# create an instance of CreditBankIncomeAccount from a JSON string
credit_bank_income_account_instance = CreditBankIncomeAccount.from_json(json)
# print the JSON string representation of the object
print CreditBankIncomeAccount.to_json()

# convert the object into a dict
credit_bank_income_account_dict = credit_bank_income_account_instance.to_dict()
# create an instance of CreditBankIncomeAccount from a dict
credit_bank_income_account_form_dict = credit_bank_income_account.from_dict(credit_bank_income_account_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


