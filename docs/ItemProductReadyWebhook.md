# ItemProductReadyWebhook

Fired once Plaid calculates income from an Item.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**webhook_type** | **str** | &#x60;INCOME&#x60; | 
**webhook_code** | **str** | &#x60;PRODUCT_READY&#x60; | 
**item_id** | **str** | The &#x60;item_id&#x60; of the Item associated with this webhook, warning, or error | 
**error** | [**PlaidError**](PlaidError.md) |  | [optional] 
**environment** | [**WebhookEnvironmentValues**](WebhookEnvironmentValues.md) |  | 

## Example

```python
from pyplaid.models.item_product_ready_webhook import ItemProductReadyWebhook

# TODO update the JSON string below
json = "{}"
# create an instance of ItemProductReadyWebhook from a JSON string
item_product_ready_webhook_instance = ItemProductReadyWebhook.from_json(json)
# print the JSON string representation of the object
print ItemProductReadyWebhook.to_json()

# convert the object into a dict
item_product_ready_webhook_dict = item_product_ready_webhook_instance.to_dict()
# create an instance of ItemProductReadyWebhook from a dict
item_product_ready_webhook_form_dict = item_product_ready_webhook.from_dict(item_product_ready_webhook_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


