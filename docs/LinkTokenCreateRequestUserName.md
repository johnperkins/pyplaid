# LinkTokenCreateRequestUserName


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**given_name** | **str** | A string with at least one non-whitespace character, with a max length of 100 characters. | 
**family_name** | **str** | A string with at least one non-whitespace character, with a max length of 100 characters. | 

## Example

```python
from pyplaid.models.link_token_create_request_user_name import LinkTokenCreateRequestUserName

# TODO update the JSON string below
json = "{}"
# create an instance of LinkTokenCreateRequestUserName from a JSON string
link_token_create_request_user_name_instance = LinkTokenCreateRequestUserName.from_json(json)
# print the JSON string representation of the object
print LinkTokenCreateRequestUserName.to_json()

# convert the object into a dict
link_token_create_request_user_name_dict = link_token_create_request_user_name_instance.to_dict()
# create an instance of LinkTokenCreateRequestUserName from a dict
link_token_create_request_user_name_form_dict = link_token_create_request_user_name.from_dict(link_token_create_request_user_name_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


