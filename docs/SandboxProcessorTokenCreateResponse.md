# SandboxProcessorTokenCreateResponse

SandboxProcessorTokenCreateResponse defines the response schema for `/sandbox/processor_token/create`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**processor_token** | **str** | A processor token that can be used to call the &#x60;/processor/&#x60; endpoints. | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.sandbox_processor_token_create_response import SandboxProcessorTokenCreateResponse

# TODO update the JSON string below
json = "{}"
# create an instance of SandboxProcessorTokenCreateResponse from a JSON string
sandbox_processor_token_create_response_instance = SandboxProcessorTokenCreateResponse.from_json(json)
# print the JSON string representation of the object
print SandboxProcessorTokenCreateResponse.to_json()

# convert the object into a dict
sandbox_processor_token_create_response_dict = sandbox_processor_token_create_response_instance.to_dict()
# create an instance of SandboxProcessorTokenCreateResponse from a dict
sandbox_processor_token_create_response_form_dict = sandbox_processor_token_create_response.from_dict(sandbox_processor_token_create_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


