# IndividualScreeningHitNames

Name information for the associated individual watchlist hit

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**full** | **str** | The full name of the individual, including all parts. | 
**is_primary** | **bool** | Primary names are those most commonly used to refer to this person. Only one name will ever be marked as primary. | 
**weak_alias_determination** | [**WeakAliasDetermination**](WeakAliasDetermination.md) |  | 

## Example

```python
from pyplaid.models.individual_screening_hit_names import IndividualScreeningHitNames

# TODO update the JSON string below
json = "{}"
# create an instance of IndividualScreeningHitNames from a JSON string
individual_screening_hit_names_instance = IndividualScreeningHitNames.from_json(json)
# print the JSON string representation of the object
print IndividualScreeningHitNames.to_json()

# convert the object into a dict
individual_screening_hit_names_dict = individual_screening_hit_names_instance.to_dict()
# create an instance of IndividualScreeningHitNames from a dict
individual_screening_hit_names_form_dict = individual_screening_hit_names.from_dict(individual_screening_hit_names_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


