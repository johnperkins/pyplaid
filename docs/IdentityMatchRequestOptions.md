# IdentityMatchRequestOptions

An optional object to filter /identity/match results

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_ids** | **List[str]** | An array of &#x60;account_ids&#x60; to perform fuzzy match | [optional] 

## Example

```python
from pyplaid.models.identity_match_request_options import IdentityMatchRequestOptions

# TODO update the JSON string below
json = "{}"
# create an instance of IdentityMatchRequestOptions from a JSON string
identity_match_request_options_instance = IdentityMatchRequestOptions.from_json(json)
# print the JSON string representation of the object
print IdentityMatchRequestOptions.to_json()

# convert the object into a dict
identity_match_request_options_dict = identity_match_request_options_instance.to_dict()
# create an instance of IdentityMatchRequestOptions from a dict
identity_match_request_options_form_dict = identity_match_request_options.from_dict(identity_match_request_options_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


