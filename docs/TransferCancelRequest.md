# TransferCancelRequest

Defines the request schema for `/transfer/cancel`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**transfer_id** | **str** | Plaid’s unique identifier for a transfer. | 

## Example

```python
from pyplaid.models.transfer_cancel_request import TransferCancelRequest

# TODO update the JSON string below
json = "{}"
# create an instance of TransferCancelRequest from a JSON string
transfer_cancel_request_instance = TransferCancelRequest.from_json(json)
# print the JSON string representation of the object
print TransferCancelRequest.to_json()

# convert the object into a dict
transfer_cancel_request_dict = transfer_cancel_request_instance.to_dict()
# create an instance of TransferCancelRequest from a dict
transfer_cancel_request_form_dict = transfer_cancel_request.from_dict(transfer_cancel_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


