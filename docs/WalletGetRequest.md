# WalletGetRequest

WalletGetRequest defines the request schema for `/wallet/get`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**wallet_id** | **str** | The ID of the e-wallet | 

## Example

```python
from pyplaid.models.wallet_get_request import WalletGetRequest

# TODO update the JSON string below
json = "{}"
# create an instance of WalletGetRequest from a JSON string
wallet_get_request_instance = WalletGetRequest.from_json(json)
# print the JSON string representation of the object
print WalletGetRequest.to_json()

# convert the object into a dict
wallet_get_request_dict = wallet_get_request_instance.to_dict()
# create an instance of WalletGetRequest from a dict
wallet_get_request_form_dict = wallet_get_request.from_dict(wallet_get_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


