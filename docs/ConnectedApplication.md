# ConnectedApplication

Describes the connected application for a particular end user.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**application_id** | **str** | This field will map to the application ID that is returned from /item/applications/list, or provided to the institution in an oauth redirect. | 
**name** | **str** | The name of the application | 
**display_name** | **str** | A human-readable name of the application for display purposes | [optional] 
**logo_url** | **str** | A URL that links to the application logo image. | [optional] 
**application_url** | **str** | The URL for the application&#39;s website | [optional] 
**reason_for_access** | **str** | A string provided by the connected app stating why they use their respective enabled products. | [optional] 
**created_at** | **date** | The date this application was linked in [ISO 8601](https://wikipedia.org/wiki/ISO_8601) (YYYY-MM-DD) format in UTC. | 
**scopes** | [**ScopesNullable**](ScopesNullable.md) |  | [optional] 

## Example

```python
from pyplaid.models.connected_application import ConnectedApplication

# TODO update the JSON string below
json = "{}"
# create an instance of ConnectedApplication from a JSON string
connected_application_instance = ConnectedApplication.from_json(json)
# print the JSON string representation of the object
print ConnectedApplication.to_json()

# convert the object into a dict
connected_application_dict = connected_application_instance.to_dict()
# create an instance of ConnectedApplication from a dict
connected_application_form_dict = connected_application.from_dict(connected_application_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


