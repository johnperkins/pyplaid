# InstitutionsSearchResponse

InstitutionsSearchResponse defines the response schema for `/institutions/search`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**institutions** | [**List[Institution]**](Institution.md) | An array of institutions matching the search criteria | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.institutions_search_response import InstitutionsSearchResponse

# TODO update the JSON string below
json = "{}"
# create an instance of InstitutionsSearchResponse from a JSON string
institutions_search_response_instance = InstitutionsSearchResponse.from_json(json)
# print the JSON string representation of the object
print InstitutionsSearchResponse.to_json()

# convert the object into a dict
institutions_search_response_dict = institutions_search_response_instance.to_dict()
# create an instance of InstitutionsSearchResponse from a dict
institutions_search_response_form_dict = institutions_search_response.from_dict(institutions_search_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


