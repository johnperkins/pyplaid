# ServiceProductFulfillment

A collection of details related to a fulfillment service or product in terms of request, process and result.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**service_product_fulfillment_detail** | [**ServiceProductFulfillmentDetail**](ServiceProductFulfillmentDetail.md) |  | 

## Example

```python
from pyplaid.models.service_product_fulfillment import ServiceProductFulfillment

# TODO update the JSON string below
json = "{}"
# create an instance of ServiceProductFulfillment from a JSON string
service_product_fulfillment_instance = ServiceProductFulfillment.from_json(json)
# print the JSON string representation of the object
print ServiceProductFulfillment.to_json()

# convert the object into a dict
service_product_fulfillment_dict = service_product_fulfillment_instance.to_dict()
# create an instance of ServiceProductFulfillment from a dict
service_product_fulfillment_form_dict = service_product_fulfillment.from_dict(service_product_fulfillment_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


