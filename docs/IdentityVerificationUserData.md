# IdentityVerificationUserData

The identity data that was either collected from the user or provided via API in order to perform an identity verification.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**phone_number** | **str** | A phone number in E.164 format. | [optional] 
**date_of_birth** | **date** | A date in the format YYYY-MM-DD (RFC 3339 Section 5.6). | 
**ip_address** | **str** | An IPv4 or IPV6 address. | 
**email_address** | **str** | A valid email address. | 
**name** | [**UserName**](UserName.md) |  | 
**address** | [**IdentityVerificationUserAddress**](IdentityVerificationUserAddress.md) |  | 
**id_number** | [**UserIDNumber**](UserIDNumber.md) |  | 

## Example

```python
from pyplaid.models.identity_verification_user_data import IdentityVerificationUserData

# TODO update the JSON string below
json = "{}"
# create an instance of IdentityVerificationUserData from a JSON string
identity_verification_user_data_instance = IdentityVerificationUserData.from_json(json)
# print the JSON string representation of the object
print IdentityVerificationUserData.to_json()

# convert the object into a dict
identity_verification_user_data_dict = identity_verification_user_data_instance.to_dict()
# create an instance of IdentityVerificationUserData from a dict
identity_verification_user_data_form_dict = identity_verification_user_data.from_dict(identity_verification_user_data_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


