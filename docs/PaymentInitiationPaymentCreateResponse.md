# PaymentInitiationPaymentCreateResponse

PaymentInitiationPaymentCreateResponse defines the response schema for `/payment_initiation/payment/create`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**payment_id** | **str** | A unique ID identifying the payment | 
**status** | [**PaymentInitiationPaymentCreateStatus**](PaymentInitiationPaymentCreateStatus.md) |  | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.payment_initiation_payment_create_response import PaymentInitiationPaymentCreateResponse

# TODO update the JSON string below
json = "{}"
# create an instance of PaymentInitiationPaymentCreateResponse from a JSON string
payment_initiation_payment_create_response_instance = PaymentInitiationPaymentCreateResponse.from_json(json)
# print the JSON string representation of the object
print PaymentInitiationPaymentCreateResponse.to_json()

# convert the object into a dict
payment_initiation_payment_create_response_dict = payment_initiation_payment_create_response_instance.to_dict()
# create an instance of PaymentInitiationPaymentCreateResponse from a dict
payment_initiation_payment_create_response_form_dict = payment_initiation_payment_create_response.from_dict(payment_initiation_payment_create_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


