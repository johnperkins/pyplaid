# CreditRelayCreateRequest

CreditRelayCreateRequest defines the request schema for `/credit/relay/create`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**report_tokens** | **List[str]** | List of report token strings, with at most one token of each report type. Currently only Asset Report token is supported. | 
**secondary_client_id** | **str** | The &#x60;secondary_client_id&#x60; is the client id of the third party with whom you would like to share the Relay Token. | 
**webhook** | **str** | URL to which Plaid will send webhooks when the Secondary Client successfully retrieves an Asset Report by calling &#x60;/credit/relay/get&#x60;. | [optional] 

## Example

```python
from pyplaid.models.credit_relay_create_request import CreditRelayCreateRequest

# TODO update the JSON string below
json = "{}"
# create an instance of CreditRelayCreateRequest from a JSON string
credit_relay_create_request_instance = CreditRelayCreateRequest.from_json(json)
# print the JSON string representation of the object
print CreditRelayCreateRequest.to_json()

# convert the object into a dict
credit_relay_create_request_dict = credit_relay_create_request_instance.to_dict()
# create an instance of CreditRelayCreateRequest from a dict
credit_relay_create_request_form_dict = credit_relay_create_request.from_dict(credit_relay_create_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


