# BankTransferGetResponse

Defines the response schema for `/bank_transfer/get`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bank_transfer** | [**BankTransfer**](BankTransfer.md) |  | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.bank_transfer_get_response import BankTransferGetResponse

# TODO update the JSON string below
json = "{}"
# create an instance of BankTransferGetResponse from a JSON string
bank_transfer_get_response_instance = BankTransferGetResponse.from_json(json)
# print the JSON string representation of the object
print BankTransferGetResponse.to_json()

# convert the object into a dict
bank_transfer_get_response_dict = bank_transfer_get_response_instance.to_dict()
# create an instance of BankTransferGetResponse from a dict
bank_transfer_get_response_form_dict = bank_transfer_get_response.from_dict(bank_transfer_get_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


