# IdentityVerificationRetryResponse

A identity verification attempt represents a customer's attempt to verify their identity, reflecting the required steps for completing the session, the results for each step, and information collected in the process.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | ID of the associated Identity Verification attempt. | 
**client_user_id** | **str** | An identifier to help you connect this object to your internal systems. For example, your database ID corresponding to this object. | 
**created_at** | **datetime** | An ISO8601 formatted timestamp. | 
**completed_at** | **datetime** | An ISO8601 formatted timestamp. | 
**previous_attempt_id** | **str** | The ID for the Identity Verification preceding this session. This field will only be filled if the current Identity Verification is a retry of a previous attempt. | 
**shareable_url** | **str** | A shareable URL that can be sent directly to the user to complete verification | 
**template** | [**IdentityVerificationTemplateReference**](IdentityVerificationTemplateReference.md) |  | 
**user** | [**IdentityVerificationUserData**](IdentityVerificationUserData.md) |  | 
**status** | [**IdentityVerificationStatus**](IdentityVerificationStatus.md) |  | 
**steps** | [**IdentityVerificationStepSummary**](IdentityVerificationStepSummary.md) |  | 
**documentary_verification** | [**DocumentaryVerification**](DocumentaryVerification.md) |  | 
**kyc_check** | [**KYCCheckDetails**](KYCCheckDetails.md) |  | 
**watchlist_screening_id** | **str** | ID of the associated screening. | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.identity_verification_retry_response import IdentityVerificationRetryResponse

# TODO update the JSON string below
json = "{}"
# create an instance of IdentityVerificationRetryResponse from a JSON string
identity_verification_retry_response_instance = IdentityVerificationRetryResponse.from_json(json)
# print the JSON string representation of the object
print IdentityVerificationRetryResponse.to_json()

# convert the object into a dict
identity_verification_retry_response_dict = identity_verification_retry_response_instance.to_dict()
# create an instance of IdentityVerificationRetryResponse from a dict
identity_verification_retry_response_form_dict = identity_verification_retry_response.from_dict(identity_verification_retry_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


