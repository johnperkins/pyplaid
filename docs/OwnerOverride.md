# OwnerOverride

Data about the owner or owners of an account. Any fields not specified will be filled in with default Sandbox information.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**names** | **List[str]** | A list of names associated with the account by the financial institution. These should always be the names of individuals, even for business accounts. Note that the same name data will be used for all accounts associated with an Item. | 
**phone_numbers** | [**List[PhoneNumber]**](PhoneNumber.md) | A list of phone numbers associated with the account. | 
**emails** | [**List[Email]**](Email.md) | A list of email addresses associated with the account. | 
**addresses** | [**List[Address]**](Address.md) | Data about the various addresses associated with the account. | 

## Example

```python
from pyplaid.models.owner_override import OwnerOverride

# TODO update the JSON string below
json = "{}"
# create an instance of OwnerOverride from a JSON string
owner_override_instance = OwnerOverride.from_json(json)
# print the JSON string representation of the object
print OwnerOverride.to_json()

# convert the object into a dict
owner_override_dict = owner_override_instance.to_dict()
# create an instance of OwnerOverride from a dict
owner_override_form_dict = owner_override.from_dict(owner_override_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


