# PayStubPayPeriodDetails

Details about the pay period.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pay_amount** | **float** | The amount of the paycheck. | 
**distribution_breakdown** | [**List[PayStubDistributionBreakdown]**](PayStubDistributionBreakdown.md) |  | 
**end_date** | **date** | The date on which the pay period ended, in [ISO 8601](https://wikipedia.org/wiki/ISO_8601) format (\&quot;yyyy-mm-dd\&quot;). | 
**gross_earnings** | **float** | Total earnings before tax/deductions. | 
**iso_currency_code** | **str** | The ISO-4217 currency code of the net pay. Always &#x60;null&#x60; if &#x60;unofficial_currency_code&#x60; is non-null. | 
**pay_date** | **date** | The date on which the pay stub was issued, in [ISO 8601](https://wikipedia.org/wiki/ISO_8601) format (\&quot;yyyy-mm-dd\&quot;). | 
**pay_frequency** | **str** | The frequency at which an individual is paid. | 
**start_date** | **date** | The date on which the pay period started, in [ISO 8601](https://wikipedia.org/wiki/ISO_8601) format (\&quot;yyyy-mm-dd\&quot;). | 
**unofficial_currency_code** | **str** | The unofficial currency code associated with the net pay. Always &#x60;null&#x60; if &#x60;iso_currency_code&#x60; is non-&#x60;null&#x60;. Unofficial currency codes are used for currencies that do not have official ISO currency codes, such as cryptocurrencies and the currencies of certain countries.  See the [currency code schema](https://plaid.com/docs/api/accounts#currency-code-schema) for a full listing of supported &#x60;iso_currency_code&#x60;s. | 

## Example

```python
from pyplaid.models.pay_stub_pay_period_details import PayStubPayPeriodDetails

# TODO update the JSON string below
json = "{}"
# create an instance of PayStubPayPeriodDetails from a JSON string
pay_stub_pay_period_details_instance = PayStubPayPeriodDetails.from_json(json)
# print the JSON string representation of the object
print PayStubPayPeriodDetails.to_json()

# convert the object into a dict
pay_stub_pay_period_details_dict = pay_stub_pay_period_details_instance.to_dict()
# create an instance of PayStubPayPeriodDetails from a dict
pay_stub_pay_period_details_form_dict = pay_stub_pay_period_details.from_dict(pay_stub_pay_period_details_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


