# TransactionsRulesCreateRequest

TransactionsRulesCreateRequest defines the request schema for `beta/transactions/rules/v1/create`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**access_token** | **str** | The access token associated with the Item data is being requested for. | 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**personal_finance_category** | **str** | Personal finance detailed category.  See the [&#x60;taxonomy csv file&#x60;](https://plaid.com/documents/transactions-personal-finance-category-taxonomy.csv) for a full list of personal finance categories.  | 
**rule_details** | [**TransactionsRuleDetails**](TransactionsRuleDetails.md) |  | 

## Example

```python
from pyplaid.models.transactions_rules_create_request import TransactionsRulesCreateRequest

# TODO update the JSON string below
json = "{}"
# create an instance of TransactionsRulesCreateRequest from a JSON string
transactions_rules_create_request_instance = TransactionsRulesCreateRequest.from_json(json)
# print the JSON string representation of the object
print TransactionsRulesCreateRequest.to_json()

# convert the object into a dict
transactions_rules_create_request_dict = transactions_rules_create_request_instance.to_dict()
# create an instance of TransactionsRulesCreateRequest from a dict
transactions_rules_create_request_form_dict = transactions_rules_create_request.from_dict(transactions_rules_create_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


