# IndividualWatchlistProgram

A program that configures the active lists, search parameters, and other behavior for initial and ongoing screening of individuals.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | ID of the associated program. | 
**created_at** | **datetime** | An ISO8601 formatted timestamp. | 
**is_rescanning_enabled** | **bool** | Indicator specifying whether the program is enabled and will perform daily rescans. | 
**lists_enabled** | [**List[IndividualWatchlistCode]**](IndividualWatchlistCode.md) | Watchlists enabled for the associated program | 
**name** | **str** | A name for the program to define its purpose. For example, \&quot;High Risk Individuals\&quot;, \&quot;US Cardholders\&quot;, or \&quot;Applicants\&quot;. | 
**name_sensitivity** | [**ProgramNameSensitivity**](ProgramNameSensitivity.md) |  | 
**audit_trail** | [**WatchlistScreeningAuditTrail**](WatchlistScreeningAuditTrail.md) |  | 
**is_archived** | **bool** | Archived programs are read-only and cannot screen new customers nor participate in ongoing monitoring. | 

## Example

```python
from pyplaid.models.individual_watchlist_program import IndividualWatchlistProgram

# TODO update the JSON string below
json = "{}"
# create an instance of IndividualWatchlistProgram from a JSON string
individual_watchlist_program_instance = IndividualWatchlistProgram.from_json(json)
# print the JSON string representation of the object
print IndividualWatchlistProgram.to_json()

# convert the object into a dict
individual_watchlist_program_dict = individual_watchlist_program_instance.to_dict()
# create an instance of IndividualWatchlistProgram from a dict
individual_watchlist_program_form_dict = individual_watchlist_program.from_dict(individual_watchlist_program_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


