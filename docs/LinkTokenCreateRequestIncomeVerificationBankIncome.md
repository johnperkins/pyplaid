# LinkTokenCreateRequestIncomeVerificationBankIncome

Specifies options for initializing Link for use with Bank Income. This field is required if `income_verification` is included in the `products` array and `bank` is specified in `income_source_types`.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**days_requested** | **int** | The number of days of data to request for the Bank Income product | [optional] 
**enable_multiple_items** | **bool** | Whether to enable multiple items to be added in the link session | [optional] [default to False]

## Example

```python
from pyplaid.models.link_token_create_request_income_verification_bank_income import LinkTokenCreateRequestIncomeVerificationBankIncome

# TODO update the JSON string below
json = "{}"
# create an instance of LinkTokenCreateRequestIncomeVerificationBankIncome from a JSON string
link_token_create_request_income_verification_bank_income_instance = LinkTokenCreateRequestIncomeVerificationBankIncome.from_json(json)
# print the JSON string representation of the object
print LinkTokenCreateRequestIncomeVerificationBankIncome.to_json()

# convert the object into a dict
link_token_create_request_income_verification_bank_income_dict = link_token_create_request_income_verification_bank_income_instance.to_dict()
# create an instance of LinkTokenCreateRequestIncomeVerificationBankIncome from a dict
link_token_create_request_income_verification_bank_income_form_dict = link_token_create_request_income_verification_bank_income.from_dict(link_token_create_request_income_verification_bank_income_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


