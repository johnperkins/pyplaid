# SandboxItemFireWebhookResponse

SandboxItemFireWebhookResponse defines the response schema for `/sandbox/item/fire_webhook`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**webhook_fired** | **bool** | Value is &#x60;true&#x60;  if the test&#x60; webhook_code&#x60;  was successfully fired. | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.sandbox_item_fire_webhook_response import SandboxItemFireWebhookResponse

# TODO update the JSON string below
json = "{}"
# create an instance of SandboxItemFireWebhookResponse from a JSON string
sandbox_item_fire_webhook_response_instance = SandboxItemFireWebhookResponse.from_json(json)
# print the JSON string representation of the object
print SandboxItemFireWebhookResponse.to_json()

# convert the object into a dict
sandbox_item_fire_webhook_response_dict = sandbox_item_fire_webhook_response_instance.to_dict()
# create an instance of SandboxItemFireWebhookResponse from a dict
sandbox_item_fire_webhook_response_form_dict = sandbox_item_fire_webhook_response.from_dict(sandbox_item_fire_webhook_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


