# SandboxItemSetVerificationStatusResponse

SandboxItemSetVerificationStatusResponse defines the response schema for `/sandbox/item/set_verification_status`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.sandbox_item_set_verification_status_response import SandboxItemSetVerificationStatusResponse

# TODO update the JSON string below
json = "{}"
# create an instance of SandboxItemSetVerificationStatusResponse from a JSON string
sandbox_item_set_verification_status_response_instance = SandboxItemSetVerificationStatusResponse.from_json(json)
# print the JSON string representation of the object
print SandboxItemSetVerificationStatusResponse.to_json()

# convert the object into a dict
sandbox_item_set_verification_status_response_dict = sandbox_item_set_verification_status_response_instance.to_dict()
# create an instance of SandboxItemSetVerificationStatusResponse from a dict
sandbox_item_set_verification_status_response_form_dict = sandbox_item_set_verification_status_response.from_dict(sandbox_item_set_verification_status_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


