# CreditBankIncomeGetRequestOptions

An optional object for `/credit/bank_income/get` request options.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **int** | How many Bank Income Reports should be fetched. Multiple reports may be available if the report has been re-created or refreshed. If more than one report is available, the most recent reports will be returned first. | [optional] [default to 1]

## Example

```python
from pyplaid.models.credit_bank_income_get_request_options import CreditBankIncomeGetRequestOptions

# TODO update the JSON string below
json = "{}"
# create an instance of CreditBankIncomeGetRequestOptions from a JSON string
credit_bank_income_get_request_options_instance = CreditBankIncomeGetRequestOptions.from_json(json)
# print the JSON string representation of the object
print CreditBankIncomeGetRequestOptions.to_json()

# convert the object into a dict
credit_bank_income_get_request_options_dict = credit_bank_income_get_request_options_instance.to_dict()
# create an instance of CreditBankIncomeGetRequestOptions from a dict
credit_bank_income_get_request_options_form_dict = credit_bank_income_get_request_options.from_dict(credit_bank_income_get_request_options_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


