# BankTransferFailure

The failure reason if the type of this transfer is `\"failed\"` or `\"reversed\"`. Null value otherwise.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ach_return_code** | **str** | The ACH return code, e.g. &#x60;R01&#x60;.  A return code will be provided if and only if the transfer status is &#x60;reversed&#x60;. For a full listing of ACH return codes, see [Bank Transfers errors](https://plaid.com/docs/errors/bank-transfers/#ach-return-codes). | [optional] 
**description** | **str** | A human-readable description of the reason for the failure or reversal. | [optional] 

## Example

```python
from pyplaid.models.bank_transfer_failure import BankTransferFailure

# TODO update the JSON string below
json = "{}"
# create an instance of BankTransferFailure from a JSON string
bank_transfer_failure_instance = BankTransferFailure.from_json(json)
# print the JSON string representation of the object
print BankTransferFailure.to_json()

# convert the object into a dict
bank_transfer_failure_dict = bank_transfer_failure_instance.to_dict()
# create an instance of BankTransferFailure from a dict
bank_transfer_failure_form_dict = bank_transfer_failure.from_dict(bank_transfer_failure_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


