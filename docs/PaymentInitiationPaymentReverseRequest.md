# PaymentInitiationPaymentReverseRequest

PaymentInitiationPaymentReverseRequest defines the request schema for `/payment_initiation/payment/reverse`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**payment_id** | **str** | The ID of the payment to reverse | 
**idempotency_key** | **str** | A random key provided by the client, per unique wallet transaction. Maximum of 128 characters.  The API supports idempotency for safely retrying requests without accidentally performing the same operation twice. If a request to execute a wallet transaction fails due to a network connection error, then after a minimum delay of one minute, you can retry the request with the same idempotency key to guarantee that only a single wallet transaction is created. If the request was successfully processed, it will prevent any transaction that uses the same idempotency key, and was received within 24 hours of the first request, from being processed. | 
**reference** | **str** | A reference for the refund. This must be an alphanumeric string with at most 18 characters and must not contain any special characters or spaces. | 
**amount** | [**PaymentAmountToRefund**](PaymentAmountToRefund.md) |  | [optional] 

## Example

```python
from pyplaid.models.payment_initiation_payment_reverse_request import PaymentInitiationPaymentReverseRequest

# TODO update the JSON string below
json = "{}"
# create an instance of PaymentInitiationPaymentReverseRequest from a JSON string
payment_initiation_payment_reverse_request_instance = PaymentInitiationPaymentReverseRequest.from_json(json)
# print the JSON string representation of the object
print PaymentInitiationPaymentReverseRequest.to_json()

# convert the object into a dict
payment_initiation_payment_reverse_request_dict = payment_initiation_payment_reverse_request_instance.to_dict()
# create an instance of PaymentInitiationPaymentReverseRequest from a dict
payment_initiation_payment_reverse_request_form_dict = payment_initiation_payment_reverse_request.from_dict(payment_initiation_payment_reverse_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


