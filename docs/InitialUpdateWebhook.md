# InitialUpdateWebhook

Fired when an Item's initial transaction pull is completed. Once this webhook has been fired, transaction data for the most recent 30 days can be fetched for the Item. If [Account Select v2](https://plaid.com/docs/link/customization/#account-select) is enabled, this webhook will also be fired if account selections for the Item are updated, with `new_transactions` set to the number of net new transactions pulled after the account selection update.  This webhook is intended for use with `/transactions/get`; if you are using the newer `/transactions/sync` endpoint, this webhook will still be fired to maintain backwards compatibility, but it is recommended to listen for and respond to the `SYNC_UPDATES_AVAILABLE` webhook instead.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**webhook_type** | **str** | &#x60;TRANSACTIONS&#x60; | 
**webhook_code** | **str** | &#x60;INITIAL_UPDATE&#x60; | 
**error** | **str** | The error code associated with the webhook. | [optional] 
**new_transactions** | **float** | The number of new, unfetched transactions available. | 
**item_id** | **str** | The &#x60;item_id&#x60; of the Item associated with this webhook, warning, or error | 
**environment** | [**WebhookEnvironmentValues**](WebhookEnvironmentValues.md) |  | 

## Example

```python
from pyplaid.models.initial_update_webhook import InitialUpdateWebhook

# TODO update the JSON string below
json = "{}"
# create an instance of InitialUpdateWebhook from a JSON string
initial_update_webhook_instance = InitialUpdateWebhook.from_json(json)
# print the JSON string representation of the object
print InitialUpdateWebhook.to_json()

# convert the object into a dict
initial_update_webhook_dict = initial_update_webhook_instance.to_dict()
# create an instance of InitialUpdateWebhook from a dict
initial_update_webhook_form_dict = initial_update_webhook.from_dict(initial_update_webhook_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


