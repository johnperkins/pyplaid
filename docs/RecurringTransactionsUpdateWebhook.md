# RecurringTransactionsUpdateWebhook

Fired when an Item's recurring transactions data is updated. After receipt of this webhook, the updated data can be fetched from `/transactions/recurring/get`.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**webhook_type** | **str** | &#x60;TRANSACTIONS&#x60; | 
**webhook_code** | **str** | &#x60;RECURRING_TRANSACTIONS_UPDATE&#x60; | 
**item_id** | **str** | The &#x60;item_id&#x60; of the Item associated with this webhook, warning, or error | 
**account_ids** | **List[str]** | A list of &#x60;account_ids&#x60; for accounts that have new or updated recurring transactions data. | 
**environment** | [**WebhookEnvironmentValues**](WebhookEnvironmentValues.md) |  | 

## Example

```python
from pyplaid.models.recurring_transactions_update_webhook import RecurringTransactionsUpdateWebhook

# TODO update the JSON string below
json = "{}"
# create an instance of RecurringTransactionsUpdateWebhook from a JSON string
recurring_transactions_update_webhook_instance = RecurringTransactionsUpdateWebhook.from_json(json)
# print the JSON string representation of the object
print RecurringTransactionsUpdateWebhook.to_json()

# convert the object into a dict
recurring_transactions_update_webhook_dict = recurring_transactions_update_webhook_instance.to_dict()
# create an instance of RecurringTransactionsUpdateWebhook from a dict
recurring_transactions_update_webhook_form_dict = recurring_transactions_update_webhook.from_dict(recurring_transactions_update_webhook_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


