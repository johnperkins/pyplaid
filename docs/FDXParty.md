# FDXParty

FDX Participant - an entity or person that is a part of a FDX API transaction

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | Human recognizable common name | 
**type** | [**FDXPartyType**](FDXPartyType.md) |  | 
**home_uri** | **str** | URI for party, where an end user could learn more about the company or application involved in the data sharing chain | [optional] 
**logo_uri** | **str** | URI for a logo asset to be displayed to the end user | [optional] 
**registry** | [**FDXPartyRegistry**](FDXPartyRegistry.md) |  | [optional] 
**registered_entity_name** | **str** | Registered name of party | [optional] 
**registered_entity_id** | **str** | Registered id of party | [optional] 

## Example

```python
from pyplaid.models.fdx_party import FDXParty

# TODO update the JSON string below
json = "{}"
# create an instance of FDXParty from a JSON string
fdx_party_instance = FDXParty.from_json(json)
# print the JSON string representation of the object
print FDXParty.to_json()

# convert the object into a dict
fdx_party_dict = fdx_party_instance.to_dict()
# create an instance of FDXParty from a dict
fdx_party_form_dict = fdx_party.from_dict(fdx_party_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


