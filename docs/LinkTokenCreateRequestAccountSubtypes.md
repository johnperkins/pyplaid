# LinkTokenCreateRequestAccountSubtypes

By default, Link will only display account types that are compatible with all products supplied in the `products` parameter of `/link/token/create`. You can further limit the accounts shown in Link by using `account_filters` to specify the account subtypes to be shown in Link. Only the specified subtypes will be shown. This filtering applies to both the Account Select view (if enabled) and the Institution Select view. Institutions that do not support the selected subtypes will be omitted from Link. To indicate that all subtypes should be shown, use the value `\"all\"`. If the `account_filters` filter is used, any account type for which a filter is not specified will be entirely omitted from Link.  For a full list of valid types and subtypes, see the [Account schema](https://plaid.com/docs/api/accounts#account-type-schema).  For institutions using OAuth, the filter will not affect the list of institutions or accounts shown by the bank in the OAuth window. 

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**depository** | [**LinkTokenCreateDepositoryFilter**](LinkTokenCreateDepositoryFilter.md) |  | [optional] 
**credit** | [**LinkTokenCreateCreditFilter**](LinkTokenCreateCreditFilter.md) |  | [optional] 
**loan** | [**LinkTokenCreateLoanFilter**](LinkTokenCreateLoanFilter.md) |  | [optional] 
**investment** | [**LinkTokenCreateInvestmentFilter**](LinkTokenCreateInvestmentFilter.md) |  | [optional] 

## Example

```python
from pyplaid.models.link_token_create_request_account_subtypes import LinkTokenCreateRequestAccountSubtypes

# TODO update the JSON string below
json = "{}"
# create an instance of LinkTokenCreateRequestAccountSubtypes from a JSON string
link_token_create_request_account_subtypes_instance = LinkTokenCreateRequestAccountSubtypes.from_json(json)
# print the JSON string representation of the object
print LinkTokenCreateRequestAccountSubtypes.to_json()

# convert the object into a dict
link_token_create_request_account_subtypes_dict = link_token_create_request_account_subtypes_instance.to_dict()
# create an instance of LinkTokenCreateRequestAccountSubtypes from a dict
link_token_create_request_account_subtypes_form_dict = link_token_create_request_account_subtypes.from_dict(link_token_create_request_account_subtypes_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


