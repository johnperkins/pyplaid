# AccountFilter

Enumerates the account subtypes that the application wishes for the user to be able to select from. For more details refer to Plaid documentation on account filters.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**depository** | **List[str]** | A list of account subtypes to be filtered. | [optional] 
**credit** | **List[str]** | A list of account subtypes to be filtered. | [optional] 
**loan** | **List[str]** | A list of account subtypes to be filtered. | [optional] 
**investment** | **List[str]** | A list of account subtypes to be filtered. | [optional] 

## Example

```python
from pyplaid.models.account_filter import AccountFilter

# TODO update the JSON string below
json = "{}"
# create an instance of AccountFilter from a JSON string
account_filter_instance = AccountFilter.from_json(json)
# print the JSON string representation of the object
print AccountFilter.to_json()

# convert the object into a dict
account_filter_dict = account_filter_instance.to_dict()
# create an instance of AccountFilter from a dict
account_filter_form_dict = account_filter.from_dict(account_filter_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


