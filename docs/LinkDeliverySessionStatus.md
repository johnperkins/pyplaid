# LinkDeliverySessionStatus

The status of the given Link Delivery Session.  `CREATED`: The session is created but not yet accessed by the user  `OPENED`: The session is opened by the user but not yet completed  `COMPLETED`: The session has been completed by the user  `EXPIRED`: The session has expired

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


