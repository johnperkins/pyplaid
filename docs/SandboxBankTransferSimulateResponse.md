# SandboxBankTransferSimulateResponse

Defines the response schema for `/sandbox/bank_transfer/simulate`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.sandbox_bank_transfer_simulate_response import SandboxBankTransferSimulateResponse

# TODO update the JSON string below
json = "{}"
# create an instance of SandboxBankTransferSimulateResponse from a JSON string
sandbox_bank_transfer_simulate_response_instance = SandboxBankTransferSimulateResponse.from_json(json)
# print the JSON string representation of the object
print SandboxBankTransferSimulateResponse.to_json()

# convert the object into a dict
sandbox_bank_transfer_simulate_response_dict = sandbox_bank_transfer_simulate_response_instance.to_dict()
# create an instance of SandboxBankTransferSimulateResponse from a dict
sandbox_bank_transfer_simulate_response_form_dict = sandbox_bank_transfer_simulate_response.from_dict(sandbox_bank_transfer_simulate_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


