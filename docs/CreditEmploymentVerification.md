# CreditEmploymentVerification

The object containing proof of employment data for an individual.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_id** | **str** | ID of the payroll provider account. | 
**status** | **str** | Current employment status. | 
**start_date** | **date** | Start of employment in ISO 8601 format (YYYY-MM-DD). | 
**end_date** | **date** | End of employment, if applicable. Provided in ISO 8601 format (YYY-MM-DD). | 
**employer** | [**CreditEmployerVerification**](CreditEmployerVerification.md) |  | 
**title** | **str** | Current title of employee. | 
**platform_ids** | [**CreditPlatformIds**](CreditPlatformIds.md) |  | 
**employee_type** | **str** | The type of employment for the individual. &#x60;\&quot;FULL_TIME\&quot;&#x60;: A full-time employee. &#x60;\&quot;PART_TIME\&quot;&#x60;: A part-time employee. &#x60;\&quot;CONTRACTOR\&quot;&#x60;: An employee typically hired externally through a contracting group. &#x60;\&quot;TEMPORARY\&quot;&#x60;: A temporary employee. &#x60;\&quot;OTHER\&quot;&#x60;: The employee type is not one of the above defined types. | 
**last_paystub_date** | **date** | The date of the employee&#39;s most recent paystub in ISO 8601 format (YYYY-MM-DD). | 

## Example

```python
from pyplaid.models.credit_employment_verification import CreditEmploymentVerification

# TODO update the JSON string below
json = "{}"
# create an instance of CreditEmploymentVerification from a JSON string
credit_employment_verification_instance = CreditEmploymentVerification.from_json(json)
# print the JSON string representation of the object
print CreditEmploymentVerification.to_json()

# convert the object into a dict
credit_employment_verification_dict = credit_employment_verification_instance.to_dict()
# create an instance of CreditEmploymentVerification from a dict
credit_employment_verification_form_dict = credit_employment_verification.from_dict(credit_employment_verification_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


