# ReportingInformation

Information about an report identifier and a report name.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reporting_information_identifier** | **str** | No documentation available | 

## Example

```python
from pyplaid.models.reporting_information import ReportingInformation

# TODO update the JSON string below
json = "{}"
# create an instance of ReportingInformation from a JSON string
reporting_information_instance = ReportingInformation.from_json(json)
# print the JSON string representation of the object
print ReportingInformation.to_json()

# convert the object into a dict
reporting_information_dict = reporting_information_instance.to_dict()
# create an instance of ReportingInformation from a dict
reporting_information_form_dict = reporting_information.from_dict(reporting_information_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


