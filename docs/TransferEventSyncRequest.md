# TransferEventSyncRequest

Defines the request schema for `/transfer/event/sync`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**after_id** | **int** | The latest (largest) &#x60;event_id&#x60; fetched via the sync endpoint, or 0 initially. | 
**count** | **int** | The maximum number of transfer events to return. | [optional] [default to 25]

## Example

```python
from pyplaid.models.transfer_event_sync_request import TransferEventSyncRequest

# TODO update the JSON string below
json = "{}"
# create an instance of TransferEventSyncRequest from a JSON string
transfer_event_sync_request_instance = TransferEventSyncRequest.from_json(json)
# print the JSON string representation of the object
print TransferEventSyncRequest.to_json()

# convert the object into a dict
transfer_event_sync_request_dict = transfer_event_sync_request_instance.to_dict()
# create an instance of TransferEventSyncRequest from a dict
transfer_event_sync_request_form_dict = transfer_event_sync_request.from_dict(transfer_event_sync_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


