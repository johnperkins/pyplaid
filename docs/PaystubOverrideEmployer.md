# PaystubOverrideEmployer

The employer on the paystub.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The name of the employer. | [optional] 

## Example

```python
from pyplaid.models.paystub_override_employer import PaystubOverrideEmployer

# TODO update the JSON string below
json = "{}"
# create an instance of PaystubOverrideEmployer from a JSON string
paystub_override_employer_instance = PaystubOverrideEmployer.from_json(json)
# print the JSON string representation of the object
print PaystubOverrideEmployer.to_json()

# convert the object into a dict
paystub_override_employer_dict = paystub_override_employer_instance.to_dict()
# create an instance of PaystubOverrideEmployer from a dict
paystub_override_employer_form_dict = paystub_override_employer.from_dict(paystub_override_employer_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


