# TransferRefundStatus

The status of the refund.  `pending`: A new refund was created; it is in the pending state. `posted`: The refund has been successfully submitted to the payment network. `cancelled`: The refund was cancelled by the client. `failed`: The refund failed or was returned.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


