# MultiDocumentRiskSignal

Object containing risk signals and relevant metadata for a set of uploaded documents

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**document_references** | [**List[RiskSignalDocumentReference]**](RiskSignalDocumentReference.md) | Array of objects containing attributes that could indicate if a document is fraudulent | 
**risk_signals** | [**List[DocumentRiskSignal]**](DocumentRiskSignal.md) | Array of attributes that indicate whether or not there is fraud risk with a set of documents | 

## Example

```python
from pyplaid.models.multi_document_risk_signal import MultiDocumentRiskSignal

# TODO update the JSON string below
json = "{}"
# create an instance of MultiDocumentRiskSignal from a JSON string
multi_document_risk_signal_instance = MultiDocumentRiskSignal.from_json(json)
# print the JSON string representation of the object
print MultiDocumentRiskSignal.to_json()

# convert the object into a dict
multi_document_risk_signal_dict = multi_document_risk_signal_instance.to_dict()
# create an instance of MultiDocumentRiskSignal from a dict
multi_document_risk_signal_form_dict = multi_document_risk_signal.from_dict(multi_document_risk_signal_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


