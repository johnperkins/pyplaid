# ApplicationGetResponse

ApplicationGetResponse defines the response schema for `/application/get`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 
**application** | [**Application**](Application.md) |  | 

## Example

```python
from pyplaid.models.application_get_response import ApplicationGetResponse

# TODO update the JSON string below
json = "{}"
# create an instance of ApplicationGetResponse from a JSON string
application_get_response_instance = ApplicationGetResponse.from_json(json)
# print the JSON string representation of the object
print ApplicationGetResponse.to_json()

# convert the object into a dict
application_get_response_dict = application_get_response_instance.to_dict()
# create an instance of ApplicationGetResponse from a dict
application_get_response_form_dict = application_get_response.from_dict(application_get_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


