# ProcessorStripeBankAccountTokenCreateResponse

ProcessorStripeBankAccountTokenCreateResponse defines the response schema for `/processor/stripe/bank_account/create`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**stripe_bank_account_token** | **str** | A token that can be sent to Stripe for use in making API calls to Plaid | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.processor_stripe_bank_account_token_create_response import ProcessorStripeBankAccountTokenCreateResponse

# TODO update the JSON string below
json = "{}"
# create an instance of ProcessorStripeBankAccountTokenCreateResponse from a JSON string
processor_stripe_bank_account_token_create_response_instance = ProcessorStripeBankAccountTokenCreateResponse.from_json(json)
# print the JSON string representation of the object
print ProcessorStripeBankAccountTokenCreateResponse.to_json()

# convert the object into a dict
processor_stripe_bank_account_token_create_response_dict = processor_stripe_bank_account_token_create_response_instance.to_dict()
# create an instance of ProcessorStripeBankAccountTokenCreateResponse from a dict
processor_stripe_bank_account_token_create_response_form_dict = processor_stripe_bank_account_token_create_response.from_dict(processor_stripe_bank_account_token_create_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


