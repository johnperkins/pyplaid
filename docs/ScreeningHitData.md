# ScreeningHitData

Information associated with the watchlist hit

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dates_of_birth** | [**List[ScreeningHitDateOfBirthItem]**](ScreeningHitDateOfBirthItem.md) | Dates of birth associated with the watchlist hit | [optional] 
**documents** | [**List[ScreeningHitDocumentsItems]**](ScreeningHitDocumentsItems.md) | Documents associated with the watchlist hit | [optional] 
**locations** | [**List[GenericScreeningHitLocationItems]**](GenericScreeningHitLocationItems.md) | Locations associated with the watchlist hit | [optional] 
**names** | [**List[ScreeningHitNamesItems]**](ScreeningHitNamesItems.md) | Names associated with the watchlist hit | [optional] 

## Example

```python
from pyplaid.models.screening_hit_data import ScreeningHitData

# TODO update the JSON string below
json = "{}"
# create an instance of ScreeningHitData from a JSON string
screening_hit_data_instance = ScreeningHitData.from_json(json)
# print the JSON string representation of the object
print ScreeningHitData.to_json()

# convert the object into a dict
screening_hit_data_dict = screening_hit_data_instance.to_dict()
# create an instance of ScreeningHitData from a dict
screening_hit_data_form_dict = screening_hit_data.from_dict(screening_hit_data_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


