# TransferRefund

Represents a refund within the Transfers API.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Plaid’s unique identifier for a refund. | 
**transfer_id** | **str** | The ID of the transfer to refund. | 
**amount** | **str** | The amount of the refund (decimal string with two digits of precision e.g. \&quot;10.00\&quot;). | 
**status** | [**TransferRefundStatus**](TransferRefundStatus.md) |  | 
**created** | **datetime** | The datetime when this refund was created. This will be of the form &#x60;2006-01-02T15:04:05Z&#x60; | 

## Example

```python
from pyplaid.models.transfer_refund import TransferRefund

# TODO update the JSON string below
json = "{}"
# create an instance of TransferRefund from a JSON string
transfer_refund_instance = TransferRefund.from_json(json)
# print the JSON string representation of the object
print TransferRefund.to_json()

# convert the object into a dict
transfer_refund_dict = transfer_refund_instance.to_dict()
# create an instance of TransferRefund from a dict
transfer_refund_form_dict = transfer_refund.from_dict(transfer_refund_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


