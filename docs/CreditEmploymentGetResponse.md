# CreditEmploymentGetResponse

CreditEmploymentGetResponse defines the response schema for `/credit/employment/get`.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**List[CreditEmploymentItem]**](CreditEmploymentItem.md) | Array of employment items. | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.credit_employment_get_response import CreditEmploymentGetResponse

# TODO update the JSON string below
json = "{}"
# create an instance of CreditEmploymentGetResponse from a JSON string
credit_employment_get_response_instance = CreditEmploymentGetResponse.from_json(json)
# print the JSON string representation of the object
print CreditEmploymentGetResponse.to_json()

# convert the object into a dict
credit_employment_get_response_dict = credit_employment_get_response_instance.to_dict()
# create an instance of CreditEmploymentGetResponse from a dict
credit_employment_get_response_form_dict = credit_employment_get_response.from_dict(credit_employment_get_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


