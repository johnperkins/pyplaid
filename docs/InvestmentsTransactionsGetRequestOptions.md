# InvestmentsTransactionsGetRequestOptions

An optional object to filter `/investments/transactions/get` results. If provided, must be non-`null`.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_ids** | **List[str]** | An array of &#x60;account_ids&#x60; to retrieve for the Item. | [optional] 
**count** | **int** | The number of transactions to fetch.  | [optional] [default to 100]
**offset** | **int** | The number of transactions to skip when fetching transaction history | [optional] [default to 0]

## Example

```python
from pyplaid.models.investments_transactions_get_request_options import InvestmentsTransactionsGetRequestOptions

# TODO update the JSON string below
json = "{}"
# create an instance of InvestmentsTransactionsGetRequestOptions from a JSON string
investments_transactions_get_request_options_instance = InvestmentsTransactionsGetRequestOptions.from_json(json)
# print the JSON string representation of the object
print InvestmentsTransactionsGetRequestOptions.to_json()

# convert the object into a dict
investments_transactions_get_request_options_dict = investments_transactions_get_request_options_instance.to_dict()
# create an instance of InvestmentsTransactionsGetRequestOptions from a dict
investments_transactions_get_request_options_form_dict = investments_transactions_get_request_options.from_dict(investments_transactions_get_request_options_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


