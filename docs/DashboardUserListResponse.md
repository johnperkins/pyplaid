# DashboardUserListResponse

Paginated list of dashboard users

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dashboard_users** | [**List[DashboardUser]**](DashboardUser.md) | List of dashboard users | 
**next_cursor** | **str** | An identifier that determines which page of results you receive. | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.dashboard_user_list_response import DashboardUserListResponse

# TODO update the JSON string below
json = "{}"
# create an instance of DashboardUserListResponse from a JSON string
dashboard_user_list_response_instance = DashboardUserListResponse.from_json(json)
# print the JSON string representation of the object
print DashboardUserListResponse.to_json()

# convert the object into a dict
dashboard_user_list_response_dict = dashboard_user_list_response_instance.to_dict()
# create an instance of DashboardUserListResponse from a dict
dashboard_user_list_response_form_dict = dashboard_user_list_response.from_dict(dashboard_user_list_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


