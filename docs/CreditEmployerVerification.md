# CreditEmployerVerification

An object containing employer data.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | Name of employer. | 

## Example

```python
from pyplaid.models.credit_employer_verification import CreditEmployerVerification

# TODO update the JSON string below
json = "{}"
# create an instance of CreditEmployerVerification from a JSON string
credit_employer_verification_instance = CreditEmployerVerification.from_json(json)
# print the JSON string representation of the object
print CreditEmployerVerification.to_json()

# convert the object into a dict
credit_employer_verification_dict = credit_employer_verification_instance.to_dict()
# create an instance of CreditEmployerVerification from a dict
credit_employer_verification_form_dict = credit_employer_verification.from_dict(credit_employer_verification_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


