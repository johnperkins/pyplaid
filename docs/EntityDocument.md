# EntityDocument

An official document, usually issued by a governing body or institution, with an associated identifier.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | [**EntityDocumentType**](EntityDocumentType.md) |  | 
**number** | **str** | The numeric or alphanumeric identifier associated with this document. | 

## Example

```python
from pyplaid.models.entity_document import EntityDocument

# TODO update the JSON string below
json = "{}"
# create an instance of EntityDocument from a JSON string
entity_document_instance = EntityDocument.from_json(json)
# print the JSON string representation of the object
print EntityDocument.to_json()

# convert the object into a dict
entity_document_dict = entity_document_instance.to_dict()
# create an instance of EntityDocument from a dict
entity_document_form_dict = entity_document.from_dict(entity_document_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


