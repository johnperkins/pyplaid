# ItemRemoveRequest

ItemRemoveRequest defines the request schema for `/item/remove`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**access_token** | **str** | The access token associated with the Item data is being requested for. | 

## Example

```python
from pyplaid.models.item_remove_request import ItemRemoveRequest

# TODO update the JSON string below
json = "{}"
# create an instance of ItemRemoveRequest from a JSON string
item_remove_request_instance = ItemRemoveRequest.from_json(json)
# print the JSON string representation of the object
print ItemRemoveRequest.to_json()

# convert the object into a dict
item_remove_request_dict = item_remove_request_instance.to_dict()
# create an instance of ItemRemoveRequest from a dict
item_remove_request_form_dict = item_remove_request.from_dict(item_remove_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


