# InvestmentsDefaultUpdateWebhook

Fired when new transactions have been detected on an investment account.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**webhook_type** | **str** | &#x60;INVESTMENTS_TRANSACTIONS&#x60; | 
**webhook_code** | **str** | &#x60;DEFAULT_UPDATE&#x60; | 
**item_id** | **str** | The &#x60;item_id&#x60; of the Item associated with this webhook, warning, or error | 
**error** | [**PlaidError**](PlaidError.md) |  | [optional] 
**new_investments_transactions** | **float** | The number of new transactions reported since the last time this webhook was fired. | 
**canceled_investments_transactions** | **float** | The number of canceled transactions reported since the last time this webhook was fired. | 
**environment** | [**WebhookEnvironmentValues**](WebhookEnvironmentValues.md) |  | 

## Example

```python
from pyplaid.models.investments_default_update_webhook import InvestmentsDefaultUpdateWebhook

# TODO update the JSON string below
json = "{}"
# create an instance of InvestmentsDefaultUpdateWebhook from a JSON string
investments_default_update_webhook_instance = InvestmentsDefaultUpdateWebhook.from_json(json)
# print the JSON string representation of the object
print InvestmentsDefaultUpdateWebhook.to_json()

# convert the object into a dict
investments_default_update_webhook_dict = investments_default_update_webhook_instance.to_dict()
# create an instance of InvestmentsDefaultUpdateWebhook from a dict
investments_default_update_webhook_form_dict = investments_default_update_webhook.from_dict(investments_default_update_webhook_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


