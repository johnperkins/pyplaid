# PayrollItem

An object containing information about the payroll item.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**item_id** | **str** | The &#x60;item_id&#x60; of the Item associated with this webhook, warning, or error | 
**institution_id** | **str** | The unique identifier of the institution associated with the Item. | 
**institution_name** | **str** | The name of the institution associated with the Item. | 
**accounts** | [**List[PayrollIncomeAccountData]**](PayrollIncomeAccountData.md) |  | 
**payroll_income** | [**List[PayrollIncomeObject]**](PayrollIncomeObject.md) |  | 
**status** | [**PayrollItemStatus**](PayrollItemStatus.md) |  | 
**updated_at** | **datetime** | Timestamp in [ISO 8601](https://wikipedia.org/wiki/ISO_8601) format (YYYY-MM-DDTHH:mm:ssZ) indicating the last time that the Item was updated. | 

## Example

```python
from pyplaid.models.payroll_item import PayrollItem

# TODO update the JSON string below
json = "{}"
# create an instance of PayrollItem from a JSON string
payroll_item_instance = PayrollItem.from_json(json)
# print the JSON string representation of the object
print PayrollItem.to_json()

# convert the object into a dict
payroll_item_dict = payroll_item_instance.to_dict()
# create an instance of PayrollItem from a dict
payroll_item_form_dict = payroll_item.from_dict(payroll_item_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


