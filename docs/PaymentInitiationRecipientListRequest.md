# PaymentInitiationRecipientListRequest

PaymentInitiationRecipientListRequest defines the request schema for `/payment_initiation/recipient/list`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 

## Example

```python
from pyplaid.models.payment_initiation_recipient_list_request import PaymentInitiationRecipientListRequest

# TODO update the JSON string below
json = "{}"
# create an instance of PaymentInitiationRecipientListRequest from a JSON string
payment_initiation_recipient_list_request_instance = PaymentInitiationRecipientListRequest.from_json(json)
# print the JSON string representation of the object
print PaymentInitiationRecipientListRequest.to_json()

# convert the object into a dict
payment_initiation_recipient_list_request_dict = payment_initiation_recipient_list_request_instance.to_dict()
# create an instance of PaymentInitiationRecipientListRequest from a dict
payment_initiation_recipient_list_request_form_dict = payment_initiation_recipient_list_request.from_dict(payment_initiation_recipient_list_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


