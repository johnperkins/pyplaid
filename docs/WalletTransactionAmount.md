# WalletTransactionAmount

The amount and currency of a transaction

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**iso_currency_code** | [**WalletISOCurrencyCode**](WalletISOCurrencyCode.md) |  | 
**value** | **float** | The amount of the transaction. Must contain at most two digits of precision e.g. &#x60;1.23&#x60;. | 

## Example

```python
from pyplaid.models.wallet_transaction_amount import WalletTransactionAmount

# TODO update the JSON string below
json = "{}"
# create an instance of WalletTransactionAmount from a JSON string
wallet_transaction_amount_instance = WalletTransactionAmount.from_json(json)
# print the JSON string representation of the object
print WalletTransactionAmount.to_json()

# convert the object into a dict
wallet_transaction_amount_dict = wallet_transaction_amount_instance.to_dict()
# create an instance of WalletTransactionAmount from a dict
wallet_transaction_amount_form_dict = wallet_transaction_amount.from_dict(wallet_transaction_amount_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


