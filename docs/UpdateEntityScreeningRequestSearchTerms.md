# UpdateEntityScreeningRequestSearchTerms

Search terms for editing an entity watchlist screening

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**entity_watchlist_program_id** | **str** | ID of the associated entity program. | 
**legal_name** | **str** | The name of the organization being screened. | [optional] 
**document_number** | **str** | The numeric or alphanumeric identifier associated with this document. | [optional] 
**email_address** | **str** | A valid email address. | [optional] 
**country** | **str** | Valid, capitalized, two-letter ISO code representing the country of this object. Must be in ISO 3166-1 alpha-2 form. | [optional] 
**phone_number** | **str** | A phone number in E.164 format. | [optional] 
**url** | **str** | An &#39;http&#39; or &#39;https&#39; URL (must begin with either of those). | [optional] 
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | 

## Example

```python
from pyplaid.models.update_entity_screening_request_search_terms import UpdateEntityScreeningRequestSearchTerms

# TODO update the JSON string below
json = "{}"
# create an instance of UpdateEntityScreeningRequestSearchTerms from a JSON string
update_entity_screening_request_search_terms_instance = UpdateEntityScreeningRequestSearchTerms.from_json(json)
# print the JSON string representation of the object
print UpdateEntityScreeningRequestSearchTerms.to_json()

# convert the object into a dict
update_entity_screening_request_search_terms_dict = update_entity_screening_request_search_terms_instance.to_dict()
# create an instance of UpdateEntityScreeningRequestSearchTerms from a dict
update_entity_screening_request_search_terms_form_dict = update_entity_screening_request_search_terms.from_dict(update_entity_screening_request_search_terms_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


