# EmploymentVerification

An object containing proof of employment data for an individual

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | [**EmploymentVerificationStatus**](EmploymentVerificationStatus.md) |  | [optional] 
**start_date** | **date** | Start of employment in ISO 8601 format (YYYY-MM-DD). | [optional] 
**end_date** | **date** | End of employment, if applicable. Provided in ISO 8601 format (YYY-MM-DD). | [optional] 
**employer** | [**EmployerVerification**](EmployerVerification.md) |  | [optional] 
**title** | **str** | Current title of employee. | [optional] 
**platform_ids** | [**PlatformIds**](PlatformIds.md) |  | [optional] 

## Example

```python
from pyplaid.models.employment_verification import EmploymentVerification

# TODO update the JSON string below
json = "{}"
# create an instance of EmploymentVerification from a JSON string
employment_verification_instance = EmploymentVerification.from_json(json)
# print the JSON string representation of the object
print EmploymentVerification.to_json()

# convert the object into a dict
employment_verification_dict = employment_verification_instance.to_dict()
# create an instance of EmploymentVerification from a dict
employment_verification_form_dict = employment_verification.from_dict(employment_verification_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


