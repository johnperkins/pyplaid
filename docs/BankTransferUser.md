# BankTransferUser

The legal name and other information for the account holder.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**legal_name** | **str** | The account holder’s full legal name. If the transfer &#x60;ach_class&#x60; is &#x60;ccd&#x60;, this should be the business name of the account holder. | 
**email_address** | **str** | The account holder’s email. | [optional] 
**routing_number** | **str** | The account holder&#39;s routing number. This field is only used in response data. Do not provide this field when making requests. | [optional] [readonly] 

## Example

```python
from pyplaid.models.bank_transfer_user import BankTransferUser

# TODO update the JSON string below
json = "{}"
# create an instance of BankTransferUser from a JSON string
bank_transfer_user_instance = BankTransferUser.from_json(json)
# print the JSON string representation of the object
print BankTransferUser.to_json()

# convert the object into a dict
bank_transfer_user_dict = bank_transfer_user_instance.to_dict()
# create an instance of BankTransferUser from a dict
bank_transfer_user_form_dict = bank_transfer_user.from_dict(bank_transfer_user_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


