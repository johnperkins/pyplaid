# PaystubDetails

An object representing details that can be found on the paystub.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pay_period_start_date** | **date** | Beginning date of the pay period on the paystub in the &#39;YYYY-MM-DD&#39; format. | [optional] 
**pay_period_end_date** | **date** | Ending date of the pay period on the paystub in the &#39;YYYY-MM-DD&#39; format. | [optional] 
**pay_date** | **date** | Pay date on the paystub in the &#39;YYYY-MM-DD&#39; format. | [optional] 
**paystub_provider** | **str** | The name of the payroll provider that generated the paystub, e.g. ADP | [optional] 
**pay_frequency** | [**PaystubPayFrequency**](PaystubPayFrequency.md) |  | [optional] 

## Example

```python
from pyplaid.models.paystub_details import PaystubDetails

# TODO update the JSON string below
json = "{}"
# create an instance of PaystubDetails from a JSON string
paystub_details_instance = PaystubDetails.from_json(json)
# print the JSON string representation of the object
print PaystubDetails.to_json()

# convert the object into a dict
paystub_details_dict = paystub_details_instance.to_dict()
# create an instance of PaystubDetails from a dict
paystub_details_form_dict = paystub_details.from_dict(paystub_details_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


