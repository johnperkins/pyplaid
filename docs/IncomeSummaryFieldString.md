# IncomeSummaryFieldString

Data about the income summary

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value** | **str** | The value of the field. | 
**verification_status** | [**VerificationStatus**](VerificationStatus.md) |  | 

## Example

```python
from pyplaid.models.income_summary_field_string import IncomeSummaryFieldString

# TODO update the JSON string below
json = "{}"
# create an instance of IncomeSummaryFieldString from a JSON string
income_summary_field_string_instance = IncomeSummaryFieldString.from_json(json)
# print the JSON string representation of the object
print IncomeSummaryFieldString.to_json()

# convert the object into a dict
income_summary_field_string_dict = income_summary_field_string_instance.to_dict()
# create an instance of IncomeSummaryFieldString from a dict
income_summary_field_string_form_dict = income_summary_field_string.from_dict(income_summary_field_string_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


