# PaymentLimits

Requested payment (i.e. receiving money) limits for the end customer.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**average_amount** | **int** | Average payment amount, in dollars. | 
**maximum_amount** | **int** | Maximum payment amount, in dollars. | 
**monthly_amount** | **int** | Monthly payment amount, in dollars. | 

## Example

```python
from pyplaid.models.payment_limits import PaymentLimits

# TODO update the JSON string below
json = "{}"
# create an instance of PaymentLimits from a JSON string
payment_limits_instance = PaymentLimits.from_json(json)
# print the JSON string representation of the object
print PaymentLimits.to_json()

# convert the object into a dict
payment_limits_dict = payment_limits_instance.to_dict()
# create an instance of PaymentLimits from a dict
payment_limits_form_dict = payment_limits.from_dict(payment_limits_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


