# CreditEmploymentItem

The object containing employment items.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**item_id** | **str** | The &#x60;item_id&#x60; of the Item associated with this webhook, warning, or error | 
**employments** | [**List[CreditEmploymentVerification]**](CreditEmploymentVerification.md) |  | 
**employment_report_token** | **str** | Token to represent the underlying Employment data | [optional] 

## Example

```python
from pyplaid.models.credit_employment_item import CreditEmploymentItem

# TODO update the JSON string below
json = "{}"
# create an instance of CreditEmploymentItem from a JSON string
credit_employment_item_instance = CreditEmploymentItem.from_json(json)
# print the JSON string representation of the object
print CreditEmploymentItem.to_json()

# convert the object into a dict
credit_employment_item_dict = credit_employment_item_instance.to_dict()
# create an instance of CreditEmploymentItem from a dict
credit_employment_item_form_dict = credit_employment_item.from_dict(credit_employment_item_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


