# FDXHateoasLink

REST application constraint (Hypermedia As The Engine Of Application State)

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**href** | **str** | URL to invoke the action on the resource | 
**action** | [**FDXHateoasLinkAction**](FDXHateoasLinkAction.md) |  | [optional] 
**rel** | **str** | Relation of this link to its containing entity, as defined by and with many example relation values at [IETF RFC5988](https://datatracker.ietf.org/doc/html/rfc5988) | [optional] 
**types** | [**List[FDXContentTypes]**](FDXContentTypes.md) | Content-types that can be used in the Accept header | [optional] 

## Example

```python
from pyplaid.models.fdx_hateoas_link import FDXHateoasLink

# TODO update the JSON string below
json = "{}"
# create an instance of FDXHateoasLink from a JSON string
fdx_hateoas_link_instance = FDXHateoasLink.from_json(json)
# print the JSON string representation of the object
print FDXHateoasLink.to_json()

# convert the object into a dict
fdx_hateoas_link_dict = fdx_hateoas_link_instance.to_dict()
# create an instance of FDXHateoasLink from a dict
fdx_hateoas_link_form_dict = fdx_hateoas_link.from_dict(fdx_hateoas_link_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


