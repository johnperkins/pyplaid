# Application

Metadata about the application

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**application_id** | **str** | This field will map to the application ID that is returned from /item/applications/list, or provided to the institution in an oauth redirect. | 
**name** | **str** | The name of the application | 
**display_name** | **str** | A human-readable name of the application for display purposes | 
**join_date** | **date** | The date this application was granted production access at Plaid in [ISO 8601](https://wikipedia.org/wiki/ISO_8601) (YYYY-MM-DD) format in UTC. | 
**logo_url** | **str** | A URL that links to the application logo image. | 
**application_url** | **str** | The URL for the application&#39;s website | 
**reason_for_access** | **str** | A string provided by the connected app stating why they use their respective enabled products. | 
**use_case** | **str** | A string representing client’s broad use case as assessed by Plaid. | 
**company_legal_name** | **str** | A string representing the name of client’s legal entity. | 
**city** | **str** | A string representing the city of the client’s headquarters. | 
**region** | **str** | A string representing the region of the client’s headquarters. | 
**postal_code** | **str** | A string representing the postal code of the client’s headquarters. | 
**country_code** | **str** | A string representing the country code of the client’s headquarters. | 

## Example

```python
from pyplaid.models.application import Application

# TODO update the JSON string below
json = "{}"
# create an instance of Application from a JSON string
application_instance = Application.from_json(json)
# print the JSON string representation of the object
print Application.to_json()

# convert the object into a dict
application_dict = application_instance.to_dict()
# create an instance of Application from a dict
application_form_dict = application.from_dict(application_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


