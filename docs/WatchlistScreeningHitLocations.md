# WatchlistScreeningHitLocations

Location information for the associated individual watchlist hit

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**full** | **str** | The full location string, potentially including elements like street, city, postal codes and country codes. Note that this is not necessarily a complete or well-formatted address. | 
**country** | **str** | Valid, capitalized, two-letter ISO code representing the country of this object. Must be in ISO 3166-1 alpha-2 form. | 

## Example

```python
from pyplaid.models.watchlist_screening_hit_locations import WatchlistScreeningHitLocations

# TODO update the JSON string below
json = "{}"
# create an instance of WatchlistScreeningHitLocations from a JSON string
watchlist_screening_hit_locations_instance = WatchlistScreeningHitLocations.from_json(json)
# print the JSON string representation of the object
print WatchlistScreeningHitLocations.to_json()

# convert the object into a dict
watchlist_screening_hit_locations_dict = watchlist_screening_hit_locations_instance.to_dict()
# create an instance of WatchlistScreeningHitLocations from a dict
watchlist_screening_hit_locations_form_dict = watchlist_screening_hit_locations.from_dict(watchlist_screening_hit_locations_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


