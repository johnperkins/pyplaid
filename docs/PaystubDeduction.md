# PaystubDeduction

Deduction on the paystub

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **str** | The description of the deduction, as provided on the paystub. For example: &#x60;\&quot;401(k)\&quot;&#x60;, &#x60;\&quot;FICA MED TAX\&quot;&#x60;. | 
**is_pretax** | **bool** | &#x60;true&#x60; if the deduction is pre-tax; &#x60;false&#x60; otherwise. | 
**total** | **float** | The amount of the deduction. | 

## Example

```python
from pyplaid.models.paystub_deduction import PaystubDeduction

# TODO update the JSON string below
json = "{}"
# create an instance of PaystubDeduction from a JSON string
paystub_deduction_instance = PaystubDeduction.from_json(json)
# print the JSON string representation of the object
print PaystubDeduction.to_json()

# convert the object into a dict
paystub_deduction_dict = paystub_deduction_instance.to_dict()
# create an instance of PaystubDeduction from a dict
paystub_deduction_form_dict = paystub_deduction.from_dict(paystub_deduction_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


