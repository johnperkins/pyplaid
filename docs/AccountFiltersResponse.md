# AccountFiltersResponse

The `account_filters` specified in the original call to `/link/token/create`. 

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**depository** | [**DepositoryFilter**](DepositoryFilter.md) |  | [optional] 
**credit** | [**CreditFilter**](CreditFilter.md) |  | [optional] 
**loan** | [**LoanFilter**](LoanFilter.md) |  | [optional] 
**investment** | [**InvestmentFilter**](InvestmentFilter.md) |  | [optional] 

## Example

```python
from pyplaid.models.account_filters_response import AccountFiltersResponse

# TODO update the JSON string below
json = "{}"
# create an instance of AccountFiltersResponse from a JSON string
account_filters_response_instance = AccountFiltersResponse.from_json(json)
# print the JSON string representation of the object
print AccountFiltersResponse.to_json()

# convert the object into a dict
account_filters_response_dict = account_filters_response_instance.to_dict()
# create an instance of AccountFiltersResponse from a dict
account_filters_response_form_dict = account_filters_response.from_dict(account_filters_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


