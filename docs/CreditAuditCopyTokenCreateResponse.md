# CreditAuditCopyTokenCreateResponse

CreditAuditCopyTokenCreateResponse defines the response schema for `/credit/audit_copy_token/get`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**audit_copy_token** | **str** | A token that can be shared with a third party auditor to allow them to obtain access to the Asset or Income Report. This token should be stored securely. | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.credit_audit_copy_token_create_response import CreditAuditCopyTokenCreateResponse

# TODO update the JSON string below
json = "{}"
# create an instance of CreditAuditCopyTokenCreateResponse from a JSON string
credit_audit_copy_token_create_response_instance = CreditAuditCopyTokenCreateResponse.from_json(json)
# print the JSON string representation of the object
print CreditAuditCopyTokenCreateResponse.to_json()

# convert the object into a dict
credit_audit_copy_token_create_response_dict = credit_audit_copy_token_create_response_instance.to_dict()
# create an instance of CreditAuditCopyTokenCreateResponse from a dict
credit_audit_copy_token_create_response_form_dict = credit_audit_copy_token_create_response.from_dict(credit_audit_copy_token_create_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


