# IncomeVerificationTaxformsGetResponse

IncomeVerificationTaxformsGetResponse defines the response schema for `/income/verification/taxforms/get`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | [optional] 
**document_metadata** | [**List[DocumentMetadata]**](DocumentMetadata.md) |  | 
**taxforms** | [**List[Taxform]**](Taxform.md) | A list of forms. | 
**error** | [**PlaidError**](PlaidError.md) |  | [optional] 

## Example

```python
from pyplaid.models.income_verification_taxforms_get_response import IncomeVerificationTaxformsGetResponse

# TODO update the JSON string below
json = "{}"
# create an instance of IncomeVerificationTaxformsGetResponse from a JSON string
income_verification_taxforms_get_response_instance = IncomeVerificationTaxformsGetResponse.from_json(json)
# print the JSON string representation of the object
print IncomeVerificationTaxformsGetResponse.to_json()

# convert the object into a dict
income_verification_taxforms_get_response_dict = income_verification_taxforms_get_response_instance.to_dict()
# create an instance of IncomeVerificationTaxformsGetResponse from a dict
income_verification_taxforms_get_response_form_dict = income_verification_taxforms_get_response.from_dict(income_verification_taxforms_get_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


