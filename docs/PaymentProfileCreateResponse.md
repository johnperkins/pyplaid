# PaymentProfileCreateResponse

PaymentProfileCreateResponse defines the response schema for `/payment_profile/create`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**payment_profile_token** | **str** | A payment profile token associated with the Payment Profile data that is being requested. | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.payment_profile_create_response import PaymentProfileCreateResponse

# TODO update the JSON string below
json = "{}"
# create an instance of PaymentProfileCreateResponse from a JSON string
payment_profile_create_response_instance = PaymentProfileCreateResponse.from_json(json)
# print the JSON string representation of the object
print PaymentProfileCreateResponse.to_json()

# convert the object into a dict
payment_profile_create_response_dict = payment_profile_create_response_instance.to_dict()
# create an instance of PaymentProfileCreateResponse from a dict
payment_profile_create_response_form_dict = payment_profile_create_response.from_dict(payment_profile_create_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


