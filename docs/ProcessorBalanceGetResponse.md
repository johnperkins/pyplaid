# ProcessorBalanceGetResponse

ProcessorBalanceGetResponse defines the response schema for `/processor/balance/get`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account** | [**AccountBase**](AccountBase.md) |  | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.processor_balance_get_response import ProcessorBalanceGetResponse

# TODO update the JSON string below
json = "{}"
# create an instance of ProcessorBalanceGetResponse from a JSON string
processor_balance_get_response_instance = ProcessorBalanceGetResponse.from_json(json)
# print the JSON string representation of the object
print ProcessorBalanceGetResponse.to_json()

# convert the object into a dict
processor_balance_get_response_dict = processor_balance_get_response_instance.to_dict()
# create an instance of ProcessorBalanceGetResponse from a dict
processor_balance_get_response_form_dict = processor_balance_get_response.from_dict(processor_balance_get_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


