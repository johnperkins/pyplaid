# LiabilitiesObject

An object containing liability accounts

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**credit** | [**List[CreditCardLiability]**](CreditCardLiability.md) | The credit accounts returned. | 
**mortgage** | [**List[MortgageLiability]**](MortgageLiability.md) | The mortgage accounts returned. | 
**student** | [**List[StudentLoan]**](StudentLoan.md) | The student loan accounts returned. | 

## Example

```python
from pyplaid.models.liabilities_object import LiabilitiesObject

# TODO update the JSON string below
json = "{}"
# create an instance of LiabilitiesObject from a JSON string
liabilities_object_instance = LiabilitiesObject.from_json(json)
# print the JSON string representation of the object
print LiabilitiesObject.to_json()

# convert the object into a dict
liabilities_object_dict = liabilities_object_instance.to_dict()
# create an instance of LiabilitiesObject from a dict
liabilities_object_form_dict = liabilities_object.from_dict(liabilities_object_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


