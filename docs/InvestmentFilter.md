# InvestmentFilter

A filter to apply to `investment`-type accounts (or `brokerage`-type accounts for API versions 2018-05-22 and earlier).

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_subtypes** | [**List[InvestmentAccountSubtype]**](InvestmentAccountSubtype.md) | An array of account subtypes to display in Link. If not specified, all account subtypes will be shown. For a full list of valid types and subtypes, see the [Account schema](https://plaid.com/docs/api/accounts#account-type-schema).  | 

## Example

```python
from pyplaid.models.investment_filter import InvestmentFilter

# TODO update the JSON string below
json = "{}"
# create an instance of InvestmentFilter from a JSON string
investment_filter_instance = InvestmentFilter.from_json(json)
# print the JSON string representation of the object
print InvestmentFilter.to_json()

# convert the object into a dict
investment_filter_dict = investment_filter_instance.to_dict()
# create an instance of InvestmentFilter from a dict
investment_filter_form_dict = investment_filter.from_dict(investment_filter_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


