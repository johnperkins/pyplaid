# EarningsBreakdown

An object representing the earnings line items for the pay period.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**canonical_description** | [**EarningsBreakdownCanonicalDescription**](EarningsBreakdownCanonicalDescription.md) |  | [optional] 
**current_amount** | **float** | Raw amount of the earning line item. | [optional] 
**description** | **str** | Description of the earning line item. | [optional] 
**hours** | **float** | Number of hours applicable for this earning. | [optional] 
**iso_currency_code** | **str** | The ISO-4217 currency code of the line item. Always &#x60;null&#x60; if &#x60;unofficial_currency_code&#x60; is non-null. | [optional] 
**rate** | **float** | Hourly rate applicable for this earning. | [optional] 
**unofficial_currency_code** | **str** | The unofficial currency code associated with the line item. Always &#x60;null&#x60; if &#x60;iso_currency_code&#x60; is non-&#x60;null&#x60;. Unofficial currency codes are used for currencies that do not have official ISO currency codes, such as cryptocurrencies and the currencies of certain countries.  See the [currency code schema](https://plaid.com/docs/api/accounts#currency-code-schema) for a full listing of supported &#x60;iso_currency_code&#x60;s. | [optional] 
**ytd_amount** | **float** | The year-to-date amount of the deduction. | [optional] 

## Example

```python
from pyplaid.models.earnings_breakdown import EarningsBreakdown

# TODO update the JSON string below
json = "{}"
# create an instance of EarningsBreakdown from a JSON string
earnings_breakdown_instance = EarningsBreakdown.from_json(json)
# print the JSON string representation of the object
print EarningsBreakdown.to_json()

# convert the object into a dict
earnings_breakdown_dict = earnings_breakdown_instance.to_dict()
# create an instance of EarningsBreakdown from a dict
earnings_breakdown_form_dict = earnings_breakdown.from_dict(earnings_breakdown_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


