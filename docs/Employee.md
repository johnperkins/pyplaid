# Employee

Data about the employee.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address** | [**PaystubAddress**](PaystubAddress.md) |  | 
**name** | **str** | The name of the employee. | 
**marital_status** | **str** | Marital status of the employee - either &#x60;single&#x60; or &#x60;married&#x60;. | [optional] 
**taxpayer_id** | [**TaxpayerID**](TaxpayerID.md) |  | [optional] 

## Example

```python
from pyplaid.models.employee import Employee

# TODO update the JSON string below
json = "{}"
# create an instance of Employee from a JSON string
employee_instance = Employee.from_json(json)
# print the JSON string representation of the object
print Employee.to_json()

# convert the object into a dict
employee_dict = employee_instance.to_dict()
# create an instance of Employee from a dict
employee_form_dict = employee.from_dict(employee_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


