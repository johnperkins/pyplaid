# CreditCategory

Information describing the intent of the transaction. Most relevant for credit use cases, but not limited to such use cases.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**primary** | **str** | A high level category that communicates the broad category of the transaction. | 
**detailed** | **str** | A granular category conveying the transaction&#39;s intent. This field can also be used as a unique identifier for the category. | 

## Example

```python
from pyplaid.models.credit_category import CreditCategory

# TODO update the JSON string below
json = "{}"
# create an instance of CreditCategory from a JSON string
credit_category_instance = CreditCategory.from_json(json)
# print the JSON string representation of the object
print CreditCategory.to_json()

# convert the object into a dict
credit_category_dict = credit_category_instance.to_dict()
# create an instance of CreditCategory from a dict
credit_category_form_dict = credit_category.from_dict(credit_category_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


