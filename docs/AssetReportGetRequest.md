# AssetReportGetRequest

AssetReportGetRequest defines the request schema for `/asset_report/get`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**asset_report_token** | **str** | A token that can be provided to endpoints such as &#x60;/asset_report/get&#x60; or &#x60;/asset_report/pdf/get&#x60; to fetch or update an Asset Report. | 
**include_insights** | **bool** | &#x60;true&#x60; if you would like to retrieve the Asset Report with Insights, &#x60;false&#x60; otherwise. This field defaults to &#x60;false&#x60; if omitted. | [optional] [default to False]
**fast_report** | **bool** | &#x60;true&#x60; to fetch \&quot;fast\&quot; version of asset report. Defaults to false if omitted. | [optional] [default to False]
**options** | [**AssetReportGetRequestOptions**](AssetReportGetRequestOptions.md) |  | [optional] 

## Example

```python
from pyplaid.models.asset_report_get_request import AssetReportGetRequest

# TODO update the JSON string below
json = "{}"
# create an instance of AssetReportGetRequest from a JSON string
asset_report_get_request_instance = AssetReportGetRequest.from_json(json)
# print the JSON string representation of the object
print AssetReportGetRequest.to_json()

# convert the object into a dict
asset_report_get_request_dict = asset_report_get_request_instance.to_dict()
# create an instance of AssetReportGetRequest from a dict
asset_report_get_request_form_dict = asset_report_get_request.from_dict(asset_report_get_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


