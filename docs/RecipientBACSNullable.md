# RecipientBACSNullable

An object containing a BACS account number and sort code. If an IBAN is not provided or if this recipient needs to accept domestic GBP-denominated payments, BACS data is required.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account** | **str** | The account number of the account. Maximum of 10 characters. | [optional] 
**sort_code** | **str** | The 6-character sort code of the account. | [optional] 

## Example

```python
from pyplaid.models.recipient_bacs_nullable import RecipientBACSNullable

# TODO update the JSON string below
json = "{}"
# create an instance of RecipientBACSNullable from a JSON string
recipient_bacs_nullable_instance = RecipientBACSNullable.from_json(json)
# print the JSON string representation of the object
print RecipientBACSNullable.to_json()

# convert the object into a dict
recipient_bacs_nullable_dict = recipient_bacs_nullable_instance.to_dict()
# create an instance of RecipientBACSNullable from a dict
recipient_bacs_nullable_form_dict = recipient_bacs_nullable.from_dict(recipient_bacs_nullable_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


