# CreditPayStubEmployer

Information about the employer on the pay stub.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address** | [**CreditPayStubAddress**](CreditPayStubAddress.md) |  | 
**name** | **str** | The name of the employer on the pay stub. | 

## Example

```python
from pyplaid.models.credit_pay_stub_employer import CreditPayStubEmployer

# TODO update the JSON string below
json = "{}"
# create an instance of CreditPayStubEmployer from a JSON string
credit_pay_stub_employer_instance = CreditPayStubEmployer.from_json(json)
# print the JSON string representation of the object
print CreditPayStubEmployer.to_json()

# convert the object into a dict
credit_pay_stub_employer_dict = credit_pay_stub_employer_instance.to_dict()
# create an instance of CreditPayStubEmployer from a dict
credit_pay_stub_employer_form_dict = credit_pay_stub_employer.from_dict(credit_pay_stub_employer_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


