# IncomeVerificationPrecheckEmployer

Information about the end user's employer

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The employer&#39;s name | [optional] 
**address** | [**IncomeVerificationPrecheckEmployerAddress**](IncomeVerificationPrecheckEmployerAddress.md) |  | [optional] 
**tax_id** | **str** | The employer&#39;s tax id | [optional] 
**url** | **str** | The URL for the employer&#39;s public website | [optional] 

## Example

```python
from pyplaid.models.income_verification_precheck_employer import IncomeVerificationPrecheckEmployer

# TODO update the JSON string below
json = "{}"
# create an instance of IncomeVerificationPrecheckEmployer from a JSON string
income_verification_precheck_employer_instance = IncomeVerificationPrecheckEmployer.from_json(json)
# print the JSON string representation of the object
print IncomeVerificationPrecheckEmployer.to_json()

# convert the object into a dict
income_verification_precheck_employer_dict = income_verification_precheck_employer_instance.to_dict()
# create an instance of IncomeVerificationPrecheckEmployer from a dict
income_verification_precheck_employer_form_dict = income_verification_precheck_employer.from_dict(income_verification_precheck_employer_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


