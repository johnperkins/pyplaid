# WatchlistScreeningSearchTerms

Search terms for creating an individual watchlist screening

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**watchlist_program_id** | **str** | ID of the associated program. | 
**legal_name** | **str** | The legal name of the individual being screened. | 
**date_of_birth** | **date** | A date in the format YYYY-MM-DD (RFC 3339 Section 5.6). | 
**document_number** | **str** | The numeric or alphanumeric identifier associated with this document. | 
**country** | **str** | Valid, capitalized, two-letter ISO code representing the country of this object. Must be in ISO 3166-1 alpha-2 form. | 
**version** | **float** | The current version of the search terms. Starts at &#x60;1&#x60; and increments with each edit to &#x60;search_terms&#x60;. | 

## Example

```python
from pyplaid.models.watchlist_screening_search_terms import WatchlistScreeningSearchTerms

# TODO update the JSON string below
json = "{}"
# create an instance of WatchlistScreeningSearchTerms from a JSON string
watchlist_screening_search_terms_instance = WatchlistScreeningSearchTerms.from_json(json)
# print the JSON string representation of the object
print WatchlistScreeningSearchTerms.to_json()

# convert the object into a dict
watchlist_screening_search_terms_dict = watchlist_screening_search_terms_instance.to_dict()
# create an instance of WatchlistScreeningSearchTerms from a dict
watchlist_screening_search_terms_form_dict = watchlist_screening_search_terms.from_dict(watchlist_screening_search_terms_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


