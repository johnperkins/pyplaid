# SandboxItemSetVerificationStatusRequest

SandboxItemSetVerificationStatusRequest defines the request schema for `/sandbox/item/set_verification_status`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**access_token** | **str** | The access token associated with the Item data is being requested for. | 
**account_id** | **str** | The &#x60;account_id&#x60; of the account whose verification status is to be modified | 
**verification_status** | **str** | The verification status to set the account to. | 

## Example

```python
from pyplaid.models.sandbox_item_set_verification_status_request import SandboxItemSetVerificationStatusRequest

# TODO update the JSON string below
json = "{}"
# create an instance of SandboxItemSetVerificationStatusRequest from a JSON string
sandbox_item_set_verification_status_request_instance = SandboxItemSetVerificationStatusRequest.from_json(json)
# print the JSON string representation of the object
print SandboxItemSetVerificationStatusRequest.to_json()

# convert the object into a dict
sandbox_item_set_verification_status_request_dict = sandbox_item_set_verification_status_request_instance.to_dict()
# create an instance of SandboxItemSetVerificationStatusRequest from a dict
sandbox_item_set_verification_status_request_form_dict = sandbox_item_set_verification_status_request.from_dict(sandbox_item_set_verification_status_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


