# PaymentConsentPeriodicAlignment

Where the payment consent period should start.  `CALENDAR`: line up with a calendar.  `CONSENT`: on the date of consent creation.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


