# ItemImportRequestUserAuth

Object of user ID and auth token pair, permitting Plaid to aggregate a user’s accounts

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**user_id** | **str** | Opaque user identifier | 
**auth_token** | **str** | Authorization token Plaid will use to aggregate this user’s accounts | 

## Example

```python
from pyplaid.models.item_import_request_user_auth import ItemImportRequestUserAuth

# TODO update the JSON string below
json = "{}"
# create an instance of ItemImportRequestUserAuth from a JSON string
item_import_request_user_auth_instance = ItemImportRequestUserAuth.from_json(json)
# print the JSON string representation of the object
print ItemImportRequestUserAuth.to_json()

# convert the object into a dict
item_import_request_user_auth_dict = item_import_request_user_auth_instance.to_dict()
# create an instance of ItemImportRequestUserAuth from a dict
item_import_request_user_auth_form_dict = item_import_request_user_auth.from_dict(item_import_request_user_auth_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


