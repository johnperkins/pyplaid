# NumbersIBANNullable

International Bank Account Number (IBAN).

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------

## Example

```python
from pyplaid.models.numbers_iban_nullable import NumbersIBANNullable

# TODO update the JSON string below
json = "{}"
# create an instance of NumbersIBANNullable from a JSON string
numbers_iban_nullable_instance = NumbersIBANNullable.from_json(json)
# print the JSON string representation of the object
print NumbersIBANNullable.to_json()

# convert the object into a dict
numbers_iban_nullable_dict = numbers_iban_nullable_instance.to_dict()
# create an instance of NumbersIBANNullable from a dict
numbers_iban_nullable_form_dict = numbers_iban_nullable.from_dict(numbers_iban_nullable_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


