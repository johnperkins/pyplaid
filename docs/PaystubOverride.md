# PaystubOverride

An object representing data from a paystub.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**employer** | [**PaystubOverrideEmployer**](PaystubOverrideEmployer.md) |  | [optional] 
**employee** | [**PaystubOverrideEmployee**](PaystubOverrideEmployee.md) |  | [optional] 
**income_breakdown** | [**List[IncomeBreakdown]**](IncomeBreakdown.md) |  | [optional] 
**pay_period_details** | [**PayPeriodDetails**](PayPeriodDetails.md) |  | [optional] 

## Example

```python
from pyplaid.models.paystub_override import PaystubOverride

# TODO update the JSON string below
json = "{}"
# create an instance of PaystubOverride from a JSON string
paystub_override_instance = PaystubOverride.from_json(json)
# print the JSON string representation of the object
print PaystubOverride.to_json()

# convert the object into a dict
paystub_override_dict = paystub_override_instance.to_dict()
# create an instance of PaystubOverride from a dict
paystub_override_form_dict = paystub_override.from_dict(paystub_override_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


