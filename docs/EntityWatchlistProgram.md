# EntityWatchlistProgram

A program that configures the active lists, search parameters, and other behavior for initial and ongoing screening of entities.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | ID of the associated entity program. | 
**created_at** | **datetime** | An ISO8601 formatted timestamp. | 
**is_rescanning_enabled** | **bool** | Indicator specifying whether the program is enabled and will perform daily rescans. | 
**lists_enabled** | [**List[EntityWatchlistCode]**](EntityWatchlistCode.md) | Watchlists enabled for the associated program | 
**name** | **str** | A name for the entity program to define its purpose. For example, \&quot;High Risk Organizations\&quot; or \&quot;Applicants\&quot;. | 
**name_sensitivity** | [**ProgramNameSensitivity**](ProgramNameSensitivity.md) |  | 
**audit_trail** | [**WatchlistScreeningAuditTrail**](WatchlistScreeningAuditTrail.md) |  | 
**is_archived** | **bool** | Archived programs are read-only and cannot screen new customers nor participate in ongoing monitoring. | 

## Example

```python
from pyplaid.models.entity_watchlist_program import EntityWatchlistProgram

# TODO update the JSON string below
json = "{}"
# create an instance of EntityWatchlistProgram from a JSON string
entity_watchlist_program_instance = EntityWatchlistProgram.from_json(json)
# print the JSON string representation of the object
print EntityWatchlistProgram.to_json()

# convert the object into a dict
entity_watchlist_program_dict = entity_watchlist_program_instance.to_dict()
# create an instance of EntityWatchlistProgram from a dict
entity_watchlist_program_form_dict = entity_watchlist_program.from_dict(entity_watchlist_program_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


