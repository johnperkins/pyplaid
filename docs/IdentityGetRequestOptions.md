# IdentityGetRequestOptions

An optional object to filter `/identity/get` results.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_ids** | **List[str]** | A list of &#x60;account_ids&#x60; to retrieve for the Item. Note: An error will be returned if a provided &#x60;account_id&#x60; is not associated with the Item. | [optional] 

## Example

```python
from pyplaid.models.identity_get_request_options import IdentityGetRequestOptions

# TODO update the JSON string below
json = "{}"
# create an instance of IdentityGetRequestOptions from a JSON string
identity_get_request_options_instance = IdentityGetRequestOptions.from_json(json)
# print the JSON string representation of the object
print IdentityGetRequestOptions.to_json()

# convert the object into a dict
identity_get_request_options_dict = identity_get_request_options_instance.to_dict()
# create an instance of IdentityGetRequestOptions from a dict
identity_get_request_options_form_dict = identity_get_request_options.from_dict(identity_get_request_options_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


