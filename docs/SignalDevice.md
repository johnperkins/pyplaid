# SignalDevice

Details about the end user's device

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ip_address** | **str** | The IP address of the device that initiated the transaction | [optional] 
**user_agent** | **str** | The user agent of the device that initiated the transaction (e.g. \&quot;Mozilla/5.0\&quot;) | [optional] 

## Example

```python
from pyplaid.models.signal_device import SignalDevice

# TODO update the JSON string below
json = "{}"
# create an instance of SignalDevice from a JSON string
signal_device_instance = SignalDevice.from_json(json)
# print the JSON string representation of the object
print SignalDevice.to_json()

# convert the object into a dict
signal_device_dict = signal_device_instance.to_dict()
# create an instance of SignalDevice from a dict
signal_device_form_dict = signal_device.from_dict(signal_device_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


