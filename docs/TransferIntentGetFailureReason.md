# TransferIntentGetFailureReason

The reason for a failed transfer intent. Returned only if the transfer intent status is `failed`. Null otherwise.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**error_type** | **str** | A broad categorization of the error. | [optional] 
**error_code** | **str** | A code representing the reason for a failed transfer intent (i.e., an API error or the authorization being declined). | [optional] 
**error_message** | **str** | A human-readable description of the code associated with a failed transfer intent. | [optional] 

## Example

```python
from pyplaid.models.transfer_intent_get_failure_reason import TransferIntentGetFailureReason

# TODO update the JSON string below
json = "{}"
# create an instance of TransferIntentGetFailureReason from a JSON string
transfer_intent_get_failure_reason_instance = TransferIntentGetFailureReason.from_json(json)
# print the JSON string representation of the object
print TransferIntentGetFailureReason.to_json()

# convert the object into a dict
transfer_intent_get_failure_reason_dict = transfer_intent_get_failure_reason_instance.to_dict()
# create an instance of TransferIntentGetFailureReason from a dict
transfer_intent_get_failure_reason_form_dict = transfer_intent_get_failure_reason.from_dict(transfer_intent_get_failure_reason_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


