# WalletTransaction

The transaction details

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**transaction_id** | **str** | A unique ID identifying the transaction | 
**reference** | **str** | A reference for the transaction | 
**type** | **str** | The type of the transaction. The supported transaction types that are returned are: &#x60;BANK_TRANSFER:&#x60; a transaction which credits an e-wallet through an external bank transfer.  &#x60;PAYOUT:&#x60; a transaction which debits an e-wallet by disbursing funds to a counterparty.  &#x60;PIS_PAY_IN:&#x60; a payment which credits an e-wallet through Plaid&#39;s Payment Initiation Services (PIS) APIs. For more information see the [Payment Initiation endpoints](https://plaid.com/docs/api/products/payment-initiation/).  &#x60;REFUND:&#x60; a transaction which debits an e-wallet by refunding a previously initiated payment made through Plaid&#39;s [PIS APIs](https://plaid.com/docs/api/products/payment-initiation/). | 
**amount** | [**WalletTransactionAmount**](WalletTransactionAmount.md) |  | 
**counterparty** | [**WalletTransactionCounterparty**](WalletTransactionCounterparty.md) |  | 
**status** | [**WalletTransactionStatus**](WalletTransactionStatus.md) |  | 
**created_at** | **datetime** | Timestamp when the transaction was created, in [ISO 8601](https://wikipedia.org/wiki/ISO_8601) format. | 
**last_status_update** | **datetime** | The date and time of the last time the &#x60;status&#x60; was updated, in IS0 8601 format | 
**payment_id** | **str** | The payment id that this transaction is associated with, if any. This is present only for transaction types &#x60;PIS_PAY_IN&#x60; and &#x60;REFUND&#x60;. | [optional] 

## Example

```python
from pyplaid.models.wallet_transaction import WalletTransaction

# TODO update the JSON string below
json = "{}"
# create an instance of WalletTransaction from a JSON string
wallet_transaction_instance = WalletTransaction.from_json(json)
# print the JSON string representation of the object
print WalletTransaction.to_json()

# convert the object into a dict
wallet_transaction_dict = wallet_transaction_instance.to_dict()
# create an instance of WalletTransaction from a dict
wallet_transaction_form_dict = wallet_transaction.from_dict(wallet_transaction_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


