# LinkTokenCreateRequestUserStatedIncomeSource

Specifies user stated income sources for the Income product

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**employer** | **str** | The employer corresponding to an income source specified by the user | [optional] 
**category** | [**UserStatedIncomeSourceCategory**](UserStatedIncomeSourceCategory.md) |  | [optional] 
**pay_per_cycle** | **float** | The income amount paid per cycle for a specified income source | [optional] 
**pay_annual** | **float** | The income amount paid annually for a specified income source | [optional] 
**pay_type** | [**UserStatedIncomeSourcePayType**](UserStatedIncomeSourcePayType.md) |  | [optional] 
**pay_frequency** | [**UserStatedIncomeSourceFrequency**](UserStatedIncomeSourceFrequency.md) |  | [optional] 

## Example

```python
from pyplaid.models.link_token_create_request_user_stated_income_source import LinkTokenCreateRequestUserStatedIncomeSource

# TODO update the JSON string below
json = "{}"
# create an instance of LinkTokenCreateRequestUserStatedIncomeSource from a JSON string
link_token_create_request_user_stated_income_source_instance = LinkTokenCreateRequestUserStatedIncomeSource.from_json(json)
# print the JSON string representation of the object
print LinkTokenCreateRequestUserStatedIncomeSource.to_json()

# convert the object into a dict
link_token_create_request_user_stated_income_source_dict = link_token_create_request_user_stated_income_source_instance.to_dict()
# create an instance of LinkTokenCreateRequestUserStatedIncomeSource from a dict
link_token_create_request_user_stated_income_source_form_dict = link_token_create_request_user_stated_income_source.from_dict(link_token_create_request_user_stated_income_source_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


