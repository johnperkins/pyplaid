# AccountsGetRequestOptions

An optional object to filter `/accounts/get` results.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_ids** | **List[str]** | An array of &#x60;account_ids&#x60; to retrieve for the Account. | [optional] 

## Example

```python
from pyplaid.models.accounts_get_request_options import AccountsGetRequestOptions

# TODO update the JSON string below
json = "{}"
# create an instance of AccountsGetRequestOptions from a JSON string
accounts_get_request_options_instance = AccountsGetRequestOptions.from_json(json)
# print the JSON string representation of the object
print AccountsGetRequestOptions.to_json()

# convert the object into a dict
accounts_get_request_options_dict = accounts_get_request_options_instance.to_dict()
# create an instance of AccountsGetRequestOptions from a dict
accounts_get_request_options_form_dict = accounts_get_request_options.from_dict(accounts_get_request_options_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


