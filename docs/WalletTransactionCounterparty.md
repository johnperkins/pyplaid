# WalletTransactionCounterparty

An object representing the e-wallet transaction's counterparty

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The name of the counterparty | 
**numbers** | [**WalletTransactionCounterpartyNumbers**](WalletTransactionCounterpartyNumbers.md) |  | 

## Example

```python
from pyplaid.models.wallet_transaction_counterparty import WalletTransactionCounterparty

# TODO update the JSON string below
json = "{}"
# create an instance of WalletTransactionCounterparty from a JSON string
wallet_transaction_counterparty_instance = WalletTransactionCounterparty.from_json(json)
# print the JSON string representation of the object
print WalletTransactionCounterparty.to_json()

# convert the object into a dict
wallet_transaction_counterparty_dict = wallet_transaction_counterparty_instance.to_dict()
# create an instance of WalletTransactionCounterparty from a dict
wallet_transaction_counterparty_form_dict = wallet_transaction_counterparty.from_dict(wallet_transaction_counterparty_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


