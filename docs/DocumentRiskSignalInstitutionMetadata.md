# DocumentRiskSignalInstitutionMetadata

An object which contains additional metadata about the institution used to compute the verification attribute

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**item_id** | **str** | The &#x60;item_id&#x60; of the Item associated with this webhook, warning, or error | 

## Example

```python
from pyplaid.models.document_risk_signal_institution_metadata import DocumentRiskSignalInstitutionMetadata

# TODO update the JSON string below
json = "{}"
# create an instance of DocumentRiskSignalInstitutionMetadata from a JSON string
document_risk_signal_institution_metadata_instance = DocumentRiskSignalInstitutionMetadata.from_json(json)
# print the JSON string representation of the object
print DocumentRiskSignalInstitutionMetadata.to_json()

# convert the object into a dict
document_risk_signal_institution_metadata_dict = document_risk_signal_institution_metadata_instance.to_dict()
# create an instance of DocumentRiskSignalInstitutionMetadata from a dict
document_risk_signal_institution_metadata_form_dict = document_risk_signal_institution_metadata.from_dict(document_risk_signal_institution_metadata_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


