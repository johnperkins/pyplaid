# PayrollRiskSignalsItem

Object containing fraud risk data pertaining to the item linked as part of the verification

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**item_id** | **str** | The &#x60;item_id&#x60; of the Item associated with this webhook, warning, or error | 
**verification_risk_signals** | [**List[DocumentRiskSignalsObject]**](DocumentRiskSignalsObject.md) | Array of payroll income document authenticity data retrieved for each of the user&#39;s accounts | 

## Example

```python
from pyplaid.models.payroll_risk_signals_item import PayrollRiskSignalsItem

# TODO update the JSON string below
json = "{}"
# create an instance of PayrollRiskSignalsItem from a JSON string
payroll_risk_signals_item_instance = PayrollRiskSignalsItem.from_json(json)
# print the JSON string representation of the object
print PayrollRiskSignalsItem.to_json()

# convert the object into a dict
payroll_risk_signals_item_dict = payroll_risk_signals_item_instance.to_dict()
# create an instance of PayrollRiskSignalsItem from a dict
payroll_risk_signals_item_form_dict = payroll_risk_signals_item.from_dict(payroll_risk_signals_item_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


