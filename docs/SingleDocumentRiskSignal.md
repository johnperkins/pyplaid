# SingleDocumentRiskSignal

Object containing all risk signals and relevant metadata for a single document

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**document_reference** | [**RiskSignalDocumentReference**](RiskSignalDocumentReference.md) |  | 
**risk_signals** | [**List[DocumentRiskSignal]**](DocumentRiskSignal.md) | Array of attributes that indicate whether or not there is fraud risk with a document | 
**risk_summary** | [**DocumentRiskSummary**](DocumentRiskSummary.md) |  | 

## Example

```python
from pyplaid.models.single_document_risk_signal import SingleDocumentRiskSignal

# TODO update the JSON string below
json = "{}"
# create an instance of SingleDocumentRiskSignal from a JSON string
single_document_risk_signal_instance = SingleDocumentRiskSignal.from_json(json)
# print the JSON string representation of the object
print SingleDocumentRiskSignal.to_json()

# convert the object into a dict
single_document_risk_signal_dict = single_document_risk_signal_instance.to_dict()
# create an instance of SingleDocumentRiskSignal from a dict
single_document_risk_signal_form_dict = single_document_risk_signal.from_dict(single_document_risk_signal_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


