# WatchlistScreeningIndividualReviewCreateResponse

A review submitted by a team member for an individual watchlist screening. A review can be either a comment on the current screening state, actions taken against hits attached to the watchlist screening, or both.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | ID of the associated review. | 
**confirmed_hits** | **List[str]** | Hits marked as a true positive after thorough manual review. These hits will never recur or be updated once dismissed. In most cases, confirmed hits indicate that the customer should be rejected. | 
**dismissed_hits** | **List[str]** | Hits marked as a false positive after thorough manual review. These hits will never recur or be updated once dismissed. | 
**comment** | **str** | A comment submitted by a team member as part of reviewing a watchlist screening. | 
**audit_trail** | [**WatchlistScreeningAuditTrail**](WatchlistScreeningAuditTrail.md) |  | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.watchlist_screening_individual_review_create_response import WatchlistScreeningIndividualReviewCreateResponse

# TODO update the JSON string below
json = "{}"
# create an instance of WatchlistScreeningIndividualReviewCreateResponse from a JSON string
watchlist_screening_individual_review_create_response_instance = WatchlistScreeningIndividualReviewCreateResponse.from_json(json)
# print the JSON string representation of the object
print WatchlistScreeningIndividualReviewCreateResponse.to_json()

# convert the object into a dict
watchlist_screening_individual_review_create_response_dict = watchlist_screening_individual_review_create_response_instance.to_dict()
# create an instance of WatchlistScreeningIndividualReviewCreateResponse from a dict
watchlist_screening_individual_review_create_response_form_dict = watchlist_screening_individual_review_create_response.from_dict(watchlist_screening_individual_review_create_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


