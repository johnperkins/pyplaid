# TransferAuthorizationCreateResponse

Defines the response schema for `/transfer/authorization/create`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**authorization** | [**TransferAuthorization**](TransferAuthorization.md) |  | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.transfer_authorization_create_response import TransferAuthorizationCreateResponse

# TODO update the JSON string below
json = "{}"
# create an instance of TransferAuthorizationCreateResponse from a JSON string
transfer_authorization_create_response_instance = TransferAuthorizationCreateResponse.from_json(json)
# print the JSON string representation of the object
print TransferAuthorizationCreateResponse.to_json()

# convert the object into a dict
transfer_authorization_create_response_dict = transfer_authorization_create_response_instance.to_dict()
# create an instance of TransferAuthorizationCreateResponse from a dict
transfer_authorization_create_response_form_dict = transfer_authorization_create_response.from_dict(transfer_authorization_create_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


