# LinkTokenGetResponse

LinkTokenGetResponse defines the response schema for `/link/token/get`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**link_token** | **str** | A &#x60;link_token&#x60;, which can be supplied to Link in order to initialize it and receive a &#x60;public_token&#x60;, which can be exchanged for an &#x60;access_token&#x60;. | 
**created_at** | **datetime** | The creation timestamp for the &#x60;link_token&#x60;, in [ISO 8601](https://wikipedia.org/wiki/ISO_8601) format. | 
**expiration** | **datetime** | The expiration timestamp for the &#x60;link_token&#x60;, in [ISO 8601](https://wikipedia.org/wiki/ISO_8601) format. | 
**metadata** | [**LinkTokenGetMetadataResponse**](LinkTokenGetMetadataResponse.md) |  | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.link_token_get_response import LinkTokenGetResponse

# TODO update the JSON string below
json = "{}"
# create an instance of LinkTokenGetResponse from a JSON string
link_token_get_response_instance = LinkTokenGetResponse.from_json(json)
# print the JSON string representation of the object
print LinkTokenGetResponse.to_json()

# convert the object into a dict
link_token_get_response_dict = link_token_get_response_instance.to_dict()
# create an instance of LinkTokenGetResponse from a dict
link_token_get_response_form_dict = link_token_get_response.from_dict(link_token_get_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


