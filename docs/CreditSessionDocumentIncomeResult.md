# CreditSessionDocumentIncomeResult

The details of a document income verification in Link

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**num_paystubs_uploaded** | **int** | The number of paystubs uploaded by the user. | 
**num_w2s_uploaded** | **int** | The number of w2s uploaded by the user. | 

## Example

```python
from pyplaid.models.credit_session_document_income_result import CreditSessionDocumentIncomeResult

# TODO update the JSON string below
json = "{}"
# create an instance of CreditSessionDocumentIncomeResult from a JSON string
credit_session_document_income_result_instance = CreditSessionDocumentIncomeResult.from_json(json)
# print the JSON string representation of the object
print CreditSessionDocumentIncomeResult.to_json()

# convert the object into a dict
credit_session_document_income_result_dict = credit_session_document_income_result_instance.to_dict()
# create an instance of CreditSessionDocumentIncomeResult from a dict
credit_session_document_income_result_form_dict = credit_session_document_income_result.from_dict(credit_session_document_income_result_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


