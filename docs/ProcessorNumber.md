# ProcessorNumber

An object containing identifying numbers used for making electronic transfers to and from the `account`. The identifying number type (ACH, EFT, IBAN, or BACS) used will depend on the country of the account. An account may have more than one number type. If a particular identifying number type is not used by the `account` for which auth data has been requested, a null value will be returned.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ach** | [**NumbersACHNullable**](NumbersACHNullable.md) |  | [optional] 
**eft** | [**NumbersEFTNullable**](NumbersEFTNullable.md) |  | [optional] 
**international** | [**NumbersInternationalNullable**](NumbersInternationalNullable.md) |  | [optional] 
**bacs** | [**NumbersBACSNullable**](NumbersBACSNullable.md) |  | [optional] 

## Example

```python
from pyplaid.models.processor_number import ProcessorNumber

# TODO update the JSON string below
json = "{}"
# create an instance of ProcessorNumber from a JSON string
processor_number_instance = ProcessorNumber.from_json(json)
# print the JSON string representation of the object
print ProcessorNumber.to_json()

# convert the object into a dict
processor_number_dict = processor_number_instance.to_dict()
# create an instance of ProcessorNumber from a dict
processor_number_form_dict = processor_number.from_dict(processor_number_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


