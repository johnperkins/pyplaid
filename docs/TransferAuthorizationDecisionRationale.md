# TransferAuthorizationDecisionRationale

The rationale for Plaid's decision regarding a proposed transfer. It is always set for `declined` decisions, and may or may not be null for `approved` decisions.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | [**TransferAuthorizationDecisionRationaleCode**](TransferAuthorizationDecisionRationaleCode.md) |  | 
**description** | **str** | A human-readable description of the code associated with a transfer approval or transfer decline. | 

## Example

```python
from pyplaid.models.transfer_authorization_decision_rationale import TransferAuthorizationDecisionRationale

# TODO update the JSON string below
json = "{}"
# create an instance of TransferAuthorizationDecisionRationale from a JSON string
transfer_authorization_decision_rationale_instance = TransferAuthorizationDecisionRationale.from_json(json)
# print the JSON string representation of the object
print TransferAuthorizationDecisionRationale.to_json()

# convert the object into a dict
transfer_authorization_decision_rationale_dict = transfer_authorization_decision_rationale_instance.to_dict()
# create an instance of TransferAuthorizationDecisionRationale from a dict
transfer_authorization_decision_rationale_form_dict = transfer_authorization_decision_rationale.from_dict(transfer_authorization_decision_rationale_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


