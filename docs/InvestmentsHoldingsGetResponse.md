# InvestmentsHoldingsGetResponse

InvestmentsHoldingsGetResponse defines the response schema for `/investments/holdings/get`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accounts** | [**List[AccountBase]**](AccountBase.md) | The accounts associated with the Item | 
**holdings** | [**List[Holding]**](Holding.md) | The holdings belonging to investment accounts associated with the Item. Details of the securities in the holdings are provided in the &#x60;securities&#x60; field.  | 
**securities** | [**List[Security]**](Security.md) | Objects describing the securities held in the accounts associated with the Item.  | 
**item** | [**Item**](Item.md) |  | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.investments_holdings_get_response import InvestmentsHoldingsGetResponse

# TODO update the JSON string below
json = "{}"
# create an instance of InvestmentsHoldingsGetResponse from a JSON string
investments_holdings_get_response_instance = InvestmentsHoldingsGetResponse.from_json(json)
# print the JSON string representation of the object
print InvestmentsHoldingsGetResponse.to_json()

# convert the object into a dict
investments_holdings_get_response_dict = investments_holdings_get_response_instance.to_dict()
# create an instance of InvestmentsHoldingsGetResponse from a dict
investments_holdings_get_response_form_dict = investments_holdings_get_response.from_dict(investments_holdings_get_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


