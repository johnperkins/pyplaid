# FDXNotification

Provides the base fields of a notification. Clients will read the `type` property to determine the expected notification payload

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**notification_id** | **str** | Id of notification | 
**type** | [**FDXNotificationType**](FDXNotificationType.md) |  | 
**sent_on** | **datetime** | ISO 8601 date-time in format &#39;YYYY-MM-DDThh:mm:ss.nnn[Z|[+|-]hh:mm]&#39; according to [IETF RFC3339](https://xml2rfc.tools.ietf.org/public/rfc/html/rfc3339.html#anchor14) | 
**category** | [**FDXNotificationCategory**](FDXNotificationCategory.md) |  | 
**severity** | [**FDXNotificationSeverity**](FDXNotificationSeverity.md) |  | [optional] 
**priority** | [**FDXNotificationPriority**](FDXNotificationPriority.md) |  | [optional] 
**publisher** | [**FDXParty**](FDXParty.md) |  | 
**subscriber** | [**FDXParty**](FDXParty.md) |  | [optional] 
**notification_payload** | [**FDXNotificationPayload**](FDXNotificationPayload.md) |  | 
**url** | [**FDXHateoasLink**](FDXHateoasLink.md) |  | [optional] 

## Example

```python
from pyplaid.models.fdx_notification import FDXNotification

# TODO update the JSON string below
json = "{}"
# create an instance of FDXNotification from a JSON string
fdx_notification_instance = FDXNotification.from_json(json)
# print the JSON string representation of the object
print FDXNotification.to_json()

# convert the object into a dict
fdx_notification_dict = fdx_notification_instance.to_dict()
# create an instance of FDXNotification from a dict
fdx_notification_form_dict = fdx_notification.from_dict(fdx_notification_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


