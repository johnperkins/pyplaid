# TransactionsRuleDetails

A representation of transactions rule details.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**field** | [**TransactionsRuleField**](TransactionsRuleField.md) |  | 
**type** | [**TransactionsRuleType**](TransactionsRuleType.md) |  | 
**query** | **str** | For TRANSACTION_ID field, provide transaction_id. For NAME field, provide a string pattern.  | 

## Example

```python
from pyplaid.models.transactions_rule_details import TransactionsRuleDetails

# TODO update the JSON string below
json = "{}"
# create an instance of TransactionsRuleDetails from a JSON string
transactions_rule_details_instance = TransactionsRuleDetails.from_json(json)
# print the JSON string representation of the object
print TransactionsRuleDetails.to_json()

# convert the object into a dict
transactions_rule_details_dict = transactions_rule_details_instance.to_dict()
# create an instance of TransactionsRuleDetails from a dict
transactions_rule_details_form_dict = transactions_rule_details.from_dict(transactions_rule_details_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


