# DashboardUserGetResponse

Account information associated with a team member with access to the Plaid dashboard.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | ID of the associated user. | 
**created_at** | **datetime** | An ISO8601 formatted timestamp. | 
**email_address** | **str** | A valid email address. | 
**status** | [**DashboardUserStatus**](DashboardUserStatus.md) |  | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.dashboard_user_get_response import DashboardUserGetResponse

# TODO update the JSON string below
json = "{}"
# create an instance of DashboardUserGetResponse from a JSON string
dashboard_user_get_response_instance = DashboardUserGetResponse.from_json(json)
# print the JSON string representation of the object
print DashboardUserGetResponse.to_json()

# convert the object into a dict
dashboard_user_get_response_dict = dashboard_user_get_response_instance.to_dict()
# create an instance of DashboardUserGetResponse from a dict
dashboard_user_get_response_form_dict = dashboard_user_get_response.from_dict(dashboard_user_get_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


