# WatchlistScreeningIndividualUpdateRequest

Request input for editing an individual watchlist screening

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**watchlist_screening_id** | **str** | ID of the associated screening. | 
**search_terms** | [**UpdateIndividualScreeningRequestSearchTerms**](UpdateIndividualScreeningRequestSearchTerms.md) |  | [optional] 
**assignee** | **str** | ID of the associated user. | [optional] 
**status** | [**WatchlistScreeningStatus**](WatchlistScreeningStatus.md) |  | [optional] 
**client_user_id** | **str** | An identifier to help you connect this object to your internal systems. For example, your database ID corresponding to this object. | [optional] 
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**reset_fields** | [**List[WatchlistScreeningIndividualUpdateRequestResettableField]**](WatchlistScreeningIndividualUpdateRequestResettableField.md) | A list of fields to reset back to null | [optional] 

## Example

```python
from pyplaid.models.watchlist_screening_individual_update_request import WatchlistScreeningIndividualUpdateRequest

# TODO update the JSON string below
json = "{}"
# create an instance of WatchlistScreeningIndividualUpdateRequest from a JSON string
watchlist_screening_individual_update_request_instance = WatchlistScreeningIndividualUpdateRequest.from_json(json)
# print the JSON string representation of the object
print WatchlistScreeningIndividualUpdateRequest.to_json()

# convert the object into a dict
watchlist_screening_individual_update_request_dict = watchlist_screening_individual_update_request_instance.to_dict()
# create an instance of WatchlistScreeningIndividualUpdateRequest from a dict
watchlist_screening_individual_update_request_form_dict = watchlist_screening_individual_update_request.from_dict(watchlist_screening_individual_update_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


