# KYCCheckPhoneSummary

Result summary object specifying how the `phone` field matched.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**summary** | [**MatchSummaryCode**](MatchSummaryCode.md) |  | 

## Example

```python
from pyplaid.models.kyc_check_phone_summary import KYCCheckPhoneSummary

# TODO update the JSON string below
json = "{}"
# create an instance of KYCCheckPhoneSummary from a JSON string
kyc_check_phone_summary_instance = KYCCheckPhoneSummary.from_json(json)
# print the JSON string representation of the object
print KYCCheckPhoneSummary.to_json()

# convert the object into a dict
kyc_check_phone_summary_dict = kyc_check_phone_summary_instance.to_dict()
# create an instance of KYCCheckPhoneSummary from a dict
kyc_check_phone_summary_form_dict = kyc_check_phone_summary.from_dict(kyc_check_phone_summary_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


