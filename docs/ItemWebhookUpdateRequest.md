# ItemWebhookUpdateRequest

ItemWebhookUpdateRequest defines the request schema for `/item/webhook/update`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**access_token** | **str** | The access token associated with the Item data is being requested for. | 
**webhook** | **str** | The new webhook URL to associate with the Item. To remove a webhook from an Item, set to &#x60;null&#x60;. | [optional] 

## Example

```python
from pyplaid.models.item_webhook_update_request import ItemWebhookUpdateRequest

# TODO update the JSON string below
json = "{}"
# create an instance of ItemWebhookUpdateRequest from a JSON string
item_webhook_update_request_instance = ItemWebhookUpdateRequest.from_json(json)
# print the JSON string representation of the object
print ItemWebhookUpdateRequest.to_json()

# convert the object into a dict
item_webhook_update_request_dict = item_webhook_update_request_instance.to_dict()
# create an instance of ItemWebhookUpdateRequest from a dict
item_webhook_update_request_form_dict = item_webhook_update_request.from_dict(item_webhook_update_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


