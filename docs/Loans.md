# Loans

A collection of loans that are part of a single deal.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**loan** | [**Loan**](Loan.md) |  | 

## Example

```python
from pyplaid.models.loans import Loans

# TODO update the JSON string below
json = "{}"
# create an instance of Loans from a JSON string
loans_instance = Loans.from_json(json)
# print the JSON string representation of the object
print Loans.to_json()

# convert the object into a dict
loans_dict = loans_instance.to_dict()
# create an instance of Loans from a dict
loans_form_dict = loans.from_dict(loans_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


