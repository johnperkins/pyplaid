# RoleDetail

No documentation available

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**party_role_type** | [**PartyRoleType**](PartyRoleType.md) |  | 

## Example

```python
from pyplaid.models.role_detail import RoleDetail

# TODO update the JSON string below
json = "{}"
# create an instance of RoleDetail from a JSON string
role_detail_instance = RoleDetail.from_json(json)
# print the JSON string representation of the object
print RoleDetail.to_json()

# convert the object into a dict
role_detail_dict = role_detail_instance.to_dict()
# create an instance of RoleDetail from a dict
role_detail_form_dict = role_detail.from_dict(role_detail_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


