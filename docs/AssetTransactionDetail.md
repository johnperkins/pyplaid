# AssetTransactionDetail

No documentation available

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**asset_transaction_unique_identifier** | **str** | A vendor creadted unique Identifier. | 
**asset_transaction_amount** | **float** | AssetTransactionAmountName. | 
**asset_transaction_date** | **date** | Asset Transaction Date. | 
**asset_transaction_post_date** | **date** | Asset Transaction Post Date. | 
**asset_transaction_type** | [**AssetTransactionType**](AssetTransactionType.md) |  | 
**asset_transaction_paid_by_name** | **str** | Populate with who did the transaction. | 
**asset_transaction_type_additional_description** | **str** | FI Provided - examples are atm, cash, check, credit, debit, deposit, directDebit, directDeposit, dividend, fee, interest, other, payment, pointOfSale, repeatPayment, serviceCharge, transfer. | 
**asset_transaction_category_type** | [**AssetTransactionCategoryType**](AssetTransactionCategoryType.md) |  | 
**financial_institution_transaction_identifier** | **str** | FI provided Transaction Identifier. | 

## Example

```python
from pyplaid.models.asset_transaction_detail import AssetTransactionDetail

# TODO update the JSON string below
json = "{}"
# create an instance of AssetTransactionDetail from a JSON string
asset_transaction_detail_instance = AssetTransactionDetail.from_json(json)
# print the JSON string representation of the object
print AssetTransactionDetail.to_json()

# convert the object into a dict
asset_transaction_detail_dict = asset_transaction_detail_instance.to_dict()
# create an instance of AssetTransactionDetail from a dict
asset_transaction_detail_form_dict = asset_transaction_detail.from_dict(asset_transaction_detail_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


