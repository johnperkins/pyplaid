# EntityDocumentType

The kind of official document represented by this object.  `bik` - Russian bank code  `business_number` - A number that uniquely identifies the business within a category of businesses  `imo` - Number assigned to the entity by the International Maritime Organization  `other` - Any document not covered by other categories  `swift` - Number identifying a bank and branch.  `tax_id` - Identification issued for the purpose of collecting taxes

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


