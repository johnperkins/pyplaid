# ValidationSource

No documentation available.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**validation_source_name** | **str** | No documentation available. | 
**validation_source_reference_identifier** | **str** | No documentation available. | 

## Example

```python
from pyplaid.models.validation_source import ValidationSource

# TODO update the JSON string below
json = "{}"
# create an instance of ValidationSource from a JSON string
validation_source_instance = ValidationSource.from_json(json)
# print the JSON string representation of the object
print ValidationSource.to_json()

# convert the object into a dict
validation_source_dict = validation_source_instance.to_dict()
# create an instance of ValidationSource from a dict
validation_source_form_dict = validation_source.from_dict(validation_source_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


