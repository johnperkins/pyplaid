# DefaultUpdateWebhook

Fired when new transaction data is available for an Item. Plaid will typically check for new transaction data several times a day.  This webhook is intended for use with `/transactions/get`; if you are using the newer `/transactions/sync` endpoint, this webhook will still be fired to maintain backwards compatibility, but it is recommended to listen for and respond to the `SYNC_UPDATES_AVAILABLE` webhook instead. 

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**webhook_type** | **str** | &#x60;TRANSACTIONS&#x60; | 
**webhook_code** | **str** | &#x60;DEFAULT_UPDATE&#x60; | 
**error** | [**PlaidError**](PlaidError.md) |  | [optional] 
**new_transactions** | **float** | The number of new transactions detected since the last time this webhook was fired. | 
**item_id** | **str** | The &#x60;item_id&#x60; of the Item the webhook relates to. | 
**environment** | [**WebhookEnvironmentValues**](WebhookEnvironmentValues.md) |  | 

## Example

```python
from pyplaid.models.default_update_webhook import DefaultUpdateWebhook

# TODO update the JSON string below
json = "{}"
# create an instance of DefaultUpdateWebhook from a JSON string
default_update_webhook_instance = DefaultUpdateWebhook.from_json(json)
# print the JSON string representation of the object
print DefaultUpdateWebhook.to_json()

# convert the object into a dict
default_update_webhook_dict = default_update_webhook_instance.to_dict()
# create an instance of DefaultUpdateWebhook from a dict
default_update_webhook_form_dict = default_update_webhook.from_dict(default_update_webhook_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


