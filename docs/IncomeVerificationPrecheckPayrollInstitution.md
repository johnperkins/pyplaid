# IncomeVerificationPrecheckPayrollInstitution

Information about the end user's payroll institution

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The name of payroll institution | [optional] 

## Example

```python
from pyplaid.models.income_verification_precheck_payroll_institution import IncomeVerificationPrecheckPayrollInstitution

# TODO update the JSON string below
json = "{}"
# create an instance of IncomeVerificationPrecheckPayrollInstitution from a JSON string
income_verification_precheck_payroll_institution_instance = IncomeVerificationPrecheckPayrollInstitution.from_json(json)
# print the JSON string representation of the object
print IncomeVerificationPrecheckPayrollInstitution.to_json()

# convert the object into a dict
income_verification_precheck_payroll_institution_dict = income_verification_precheck_payroll_institution_instance.to_dict()
# create an instance of IncomeVerificationPrecheckPayrollInstitution from a dict
income_verification_precheck_payroll_institution_form_dict = income_verification_precheck_payroll_institution.from_dict(income_verification_precheck_payroll_institution_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


