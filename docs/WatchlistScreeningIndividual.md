# WatchlistScreeningIndividual

The screening object allows you to represent a customer in your system, update their profile, and search for them on various watchlists. Note: Rejected customers will not receive new hits, regardless of program configuration.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | ID of the associated screening. | 
**search_terms** | [**WatchlistScreeningSearchTerms**](WatchlistScreeningSearchTerms.md) |  | 
**assignee** | **str** | ID of the associated user. | 
**status** | [**WatchlistScreeningStatus**](WatchlistScreeningStatus.md) |  | 
**client_user_id** | **str** | An identifier to help you connect this object to your internal systems. For example, your database ID corresponding to this object. | 
**audit_trail** | [**WatchlistScreeningAuditTrail**](WatchlistScreeningAuditTrail.md) |  | 

## Example

```python
from pyplaid.models.watchlist_screening_individual import WatchlistScreeningIndividual

# TODO update the JSON string below
json = "{}"
# create an instance of WatchlistScreeningIndividual from a JSON string
watchlist_screening_individual_instance = WatchlistScreeningIndividual.from_json(json)
# print the JSON string representation of the object
print WatchlistScreeningIndividual.to_json()

# convert the object into a dict
watchlist_screening_individual_dict = watchlist_screening_individual_instance.to_dict()
# create an instance of WatchlistScreeningIndividual from a dict
watchlist_screening_individual_form_dict = watchlist_screening_individual.from_dict(watchlist_screening_individual_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


