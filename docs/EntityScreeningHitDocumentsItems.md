# EntityScreeningHitDocumentsItems

Analyzed documents for the associated hit

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**analysis** | [**MatchSummary**](MatchSummary.md) |  | [optional] 
**data** | [**EntityDocument**](EntityDocument.md) |  | [optional] 

## Example

```python
from pyplaid.models.entity_screening_hit_documents_items import EntityScreeningHitDocumentsItems

# TODO update the JSON string below
json = "{}"
# create an instance of EntityScreeningHitDocumentsItems from a JSON string
entity_screening_hit_documents_items_instance = EntityScreeningHitDocumentsItems.from_json(json)
# print the JSON string representation of the object
print EntityScreeningHitDocumentsItems.to_json()

# convert the object into a dict
entity_screening_hit_documents_items_dict = entity_screening_hit_documents_items_instance.to_dict()
# create an instance of EntityScreeningHitDocumentsItems from a dict
entity_screening_hit_documents_items_form_dict = entity_screening_hit_documents_items.from_dict(entity_screening_hit_documents_items_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


