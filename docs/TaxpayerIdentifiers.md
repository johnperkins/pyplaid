# TaxpayerIdentifiers

The collection of TAXPAYER_IDENTIFICATION elements

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**taxpayer_identifier** | [**TaxpayerIdentifier**](TaxpayerIdentifier.md) |  | 

## Example

```python
from pyplaid.models.taxpayer_identifiers import TaxpayerIdentifiers

# TODO update the JSON string below
json = "{}"
# create an instance of TaxpayerIdentifiers from a JSON string
taxpayer_identifiers_instance = TaxpayerIdentifiers.from_json(json)
# print the JSON string representation of the object
print TaxpayerIdentifiers.to_json()

# convert the object into a dict
taxpayer_identifiers_dict = taxpayer_identifiers_instance.to_dict()
# create an instance of TaxpayerIdentifiers from a dict
taxpayer_identifiers_form_dict = taxpayer_identifiers.from_dict(taxpayer_identifiers_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


