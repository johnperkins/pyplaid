# MortgageInterestRate

Object containing metadata about the interest rate for the mortgage.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**percentage** | **float** | Percentage value (interest rate of current mortgage, not APR) of interest payable on a loan. | 
**type** | **str** | The type of interest charged (fixed or variable). | 

## Example

```python
from pyplaid.models.mortgage_interest_rate import MortgageInterestRate

# TODO update the JSON string below
json = "{}"
# create an instance of MortgageInterestRate from a JSON string
mortgage_interest_rate_instance = MortgageInterestRate.from_json(json)
# print the JSON string representation of the object
print MortgageInterestRate.to_json()

# convert the object into a dict
mortgage_interest_rate_dict = mortgage_interest_rate_instance.to_dict()
# create an instance of MortgageInterestRate from a dict
mortgage_interest_rate_form_dict = mortgage_interest_rate.from_dict(mortgage_interest_rate_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


