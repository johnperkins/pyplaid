# ItemPublicTokenExchangeResponse

ItemPublicTokenExchangeResponse defines the response schema for `/item/public_token/exchange`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_token** | **str** | The access token associated with the Item data is being requested for. | 
**item_id** | **str** | The &#x60;item_id&#x60; value of the Item associated with the returned &#x60;access_token&#x60; | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.item_public_token_exchange_response import ItemPublicTokenExchangeResponse

# TODO update the JSON string below
json = "{}"
# create an instance of ItemPublicTokenExchangeResponse from a JSON string
item_public_token_exchange_response_instance = ItemPublicTokenExchangeResponse.from_json(json)
# print the JSON string representation of the object
print ItemPublicTokenExchangeResponse.to_json()

# convert the object into a dict
item_public_token_exchange_response_dict = item_public_token_exchange_response_instance.to_dict()
# create an instance of ItemPublicTokenExchangeResponse from a dict
item_public_token_exchange_response_form_dict = item_public_token_exchange_response.from_dict(item_public_token_exchange_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


