# AssetReportAuditCopyRemoveResponse

AssetReportAuditCopyRemoveResponse defines the response schema for `/asset_report/audit_copy/remove`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**removed** | **bool** | &#x60;true&#x60; if the Audit Copy was successfully removed. | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.asset_report_audit_copy_remove_response import AssetReportAuditCopyRemoveResponse

# TODO update the JSON string below
json = "{}"
# create an instance of AssetReportAuditCopyRemoveResponse from a JSON string
asset_report_audit_copy_remove_response_instance = AssetReportAuditCopyRemoveResponse.from_json(json)
# print the JSON string representation of the object
print AssetReportAuditCopyRemoveResponse.to_json()

# convert the object into a dict
asset_report_audit_copy_remove_response_dict = asset_report_audit_copy_remove_response_instance.to_dict()
# create an instance of AssetReportAuditCopyRemoveResponse from a dict
asset_report_audit_copy_remove_response_form_dict = asset_report_audit_copy_remove_response.from_dict(asset_report_audit_copy_remove_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


