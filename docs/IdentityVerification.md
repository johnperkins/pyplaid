# IdentityVerification

A identity verification attempt represents a customer's attempt to verify their identity, reflecting the required steps for completing the session, the results for each step, and information collected in the process.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | ID of the associated Identity Verification attempt. | 
**client_user_id** | **str** | An identifier to help you connect this object to your internal systems. For example, your database ID corresponding to this object. | 
**created_at** | **datetime** | An ISO8601 formatted timestamp. | 
**completed_at** | **datetime** | An ISO8601 formatted timestamp. | 
**previous_attempt_id** | **str** | The ID for the Identity Verification preceding this session. This field will only be filled if the current Identity Verification is a retry of a previous attempt. | 
**shareable_url** | **str** | A shareable URL that can be sent directly to the user to complete verification | 
**template** | [**IdentityVerificationTemplateReference**](IdentityVerificationTemplateReference.md) |  | 
**user** | [**IdentityVerificationUserData**](IdentityVerificationUserData.md) |  | 
**status** | [**IdentityVerificationStatus**](IdentityVerificationStatus.md) |  | 
**steps** | [**IdentityVerificationStepSummary**](IdentityVerificationStepSummary.md) |  | 
**documentary_verification** | [**DocumentaryVerification**](DocumentaryVerification.md) |  | 
**kyc_check** | [**KYCCheckDetails**](KYCCheckDetails.md) |  | 
**watchlist_screening_id** | **str** | ID of the associated screening. | 

## Example

```python
from pyplaid.models.identity_verification import IdentityVerification

# TODO update the JSON string below
json = "{}"
# create an instance of IdentityVerification from a JSON string
identity_verification_instance = IdentityVerification.from_json(json)
# print the JSON string representation of the object
print IdentityVerification.to_json()

# convert the object into a dict
identity_verification_dict = identity_verification_instance.to_dict()
# create an instance of IdentityVerification from a dict
identity_verification_form_dict = identity_verification.from_dict(identity_verification_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


