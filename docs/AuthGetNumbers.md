# AuthGetNumbers

An object containing identifying numbers used for making electronic transfers to and from the `accounts`. The identifying number type (ACH, EFT, IBAN, or BACS) used will depend on the country of the account. An account may have more than one number type. If a particular identifying number type is not used by any `accounts` for which data has been requested, the array for that type will be empty.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ach** | [**List[NumbersACH]**](NumbersACH.md) | An array of ACH numbers identifying accounts. | 
**eft** | [**List[NumbersEFT]**](NumbersEFT.md) | An array of EFT numbers identifying accounts. | 
**international** | [**List[NumbersInternational]**](NumbersInternational.md) | An array of IBAN numbers identifying accounts. | 
**bacs** | [**List[NumbersBACS]**](NumbersBACS.md) | An array of BACS numbers identifying accounts. | 

## Example

```python
from pyplaid.models.auth_get_numbers import AuthGetNumbers

# TODO update the JSON string below
json = "{}"
# create an instance of AuthGetNumbers from a JSON string
auth_get_numbers_instance = AuthGetNumbers.from_json(json)
# print the JSON string representation of the object
print AuthGetNumbers.to_json()

# convert the object into a dict
auth_get_numbers_dict = auth_get_numbers_instance.to_dict()
# create an instance of AuthGetNumbers from a dict
auth_get_numbers_form_dict = auth_get_numbers.from_dict(auth_get_numbers_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


