# TransactionsGetResponse

TransactionsGetResponse defines the response schema for `/transactions/get`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accounts** | [**List[AccountBase]**](AccountBase.md) | An array containing the &#x60;accounts&#x60; associated with the Item for which transactions are being returned. Each transaction can be mapped to its corresponding account via the &#x60;account_id&#x60; field. | 
**transactions** | [**List[Transaction]**](Transaction.md) | An array containing transactions from the account. Transactions are returned in reverse chronological order, with the most recent at the beginning of the array. The maximum number of transactions returned is determined by the &#x60;count&#x60; parameter. | 
**total_transactions** | **int** | The total number of transactions available within the date range specified. If &#x60;total_transactions&#x60; is larger than the size of the &#x60;transactions&#x60; array, more transactions are available and can be fetched via manipulating the &#x60;offset&#x60; parameter. | 
**item** | [**Item**](Item.md) |  | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.transactions_get_response import TransactionsGetResponse

# TODO update the JSON string below
json = "{}"
# create an instance of TransactionsGetResponse from a JSON string
transactions_get_response_instance = TransactionsGetResponse.from_json(json)
# print the JSON string representation of the object
print TransactionsGetResponse.to_json()

# convert the object into a dict
transactions_get_response_dict = transactions_get_response_instance.to_dict()
# create an instance of TransactionsGetResponse from a dict
transactions_get_response_form_dict = transactions_get_response.from_dict(transactions_get_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


