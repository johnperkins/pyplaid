# IncidentUpdate

An update on the health incident

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **str** | The content of the update. | [optional] 
**status** | **str** | The status of the incident. | [optional] 
**updated_date** | **datetime** | The date when the update was published, in [ISO 8601](https://wikipedia.org/wiki/ISO_8601) format, e.g. &#x60;\&quot;2020-10-30T15:26:48Z\&quot;&#x60;. | [optional] 

## Example

```python
from pyplaid.models.incident_update import IncidentUpdate

# TODO update the JSON string below
json = "{}"
# create an instance of IncidentUpdate from a JSON string
incident_update_instance = IncidentUpdate.from_json(json)
# print the JSON string representation of the object
print IncidentUpdate.to_json()

# convert the object into a dict
incident_update_dict = incident_update_instance.to_dict()
# create an instance of IncidentUpdate from a dict
incident_update_form_dict = incident_update.from_dict(incident_update_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


