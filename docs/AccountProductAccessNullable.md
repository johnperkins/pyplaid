# AccountProductAccessNullable

Allow the application to access specific products on this account

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_data** | **bool** | Allow the application to access account data. Only used by certain partners. If relevant to the partner and unset, defaults to &#x60;true&#x60;. | [optional] [default to True]
**statements** | **bool** | Allow the application to access bank statements. Only used by certain partners. If relevant to the partner and unset, defaults to &#x60;true&#x60;. | [optional] [default to True]
**tax_documents** | **bool** | Allow the application to access tax documents. Only used by certain partners. If relevant to the partner and unset, defaults to &#x60;true&#x60;. | [optional] [default to True]

## Example

```python
from pyplaid.models.account_product_access_nullable import AccountProductAccessNullable

# TODO update the JSON string below
json = "{}"
# create an instance of AccountProductAccessNullable from a JSON string
account_product_access_nullable_instance = AccountProductAccessNullable.from_json(json)
# print the JSON string representation of the object
print AccountProductAccessNullable.to_json()

# convert the object into a dict
account_product_access_nullable_dict = account_product_access_nullable_instance.to_dict()
# create an instance of AccountProductAccessNullable from a dict
account_product_access_nullable_form_dict = account_product_access_nullable.from_dict(account_product_access_nullable_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


