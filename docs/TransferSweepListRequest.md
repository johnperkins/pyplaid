# TransferSweepListRequest

Defines the request schema for `/transfer/sweep/list`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**start_date** | **datetime** | The start datetime of sweeps to return (RFC 3339 format). | [optional] 
**end_date** | **datetime** | The end datetime of sweeps to return (RFC 3339 format). | [optional] 
**count** | **int** | The maximum number of sweeps to return. | [optional] [default to 25]
**offset** | **int** | The number of sweeps to skip before returning results. | [optional] [default to 0]
**originator_client_id** | **str** | Filter sweeps to only those with the specified originator client. | [optional] 

## Example

```python
from pyplaid.models.transfer_sweep_list_request import TransferSweepListRequest

# TODO update the JSON string below
json = "{}"
# create an instance of TransferSweepListRequest from a JSON string
transfer_sweep_list_request_instance = TransferSweepListRequest.from_json(json)
# print the JSON string representation of the object
print TransferSweepListRequest.to_json()

# convert the object into a dict
transfer_sweep_list_request_dict = transfer_sweep_list_request_instance.to_dict()
# create an instance of TransferSweepListRequest from a dict
transfer_sweep_list_request_form_dict = transfer_sweep_list_request.from_dict(transfer_sweep_list_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


