# JWTHeader

A JWT Header, used for webhook validation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | 

## Example

```python
from pyplaid.models.jwt_header import JWTHeader

# TODO update the JSON string below
json = "{}"
# create an instance of JWTHeader from a JSON string
jwt_header_instance = JWTHeader.from_json(json)
# print the JSON string representation of the object
print JWTHeader.to_json()

# convert the object into a dict
jwt_header_dict = jwt_header_instance.to_dict()
# create an instance of JWTHeader from a dict
jwt_header_form_dict = jwt_header.from_dict(jwt_header_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


