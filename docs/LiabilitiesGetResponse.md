# LiabilitiesGetResponse

LiabilitiesGetResponse defines the response schema for `/liabilities/get`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accounts** | [**List[AccountBase]**](AccountBase.md) | An array of accounts associated with the Item | 
**item** | [**Item**](Item.md) |  | 
**liabilities** | [**LiabilitiesObject**](LiabilitiesObject.md) |  | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.liabilities_get_response import LiabilitiesGetResponse

# TODO update the JSON string below
json = "{}"
# create an instance of LiabilitiesGetResponse from a JSON string
liabilities_get_response_instance = LiabilitiesGetResponse.from_json(json)
# print the JSON string representation of the object
print LiabilitiesGetResponse.to_json()

# convert the object into a dict
liabilities_get_response_dict = liabilities_get_response_instance.to_dict()
# create an instance of LiabilitiesGetResponse from a dict
liabilities_get_response_form_dict = liabilities_get_response.from_dict(liabilities_get_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


