# Paystub

An object representing data extracted from the end user's paystub.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**deductions** | [**Deductions**](Deductions.md) |  | 
**doc_id** | **str** | An identifier of the document referenced by the document metadata. | 
**earnings** | [**Earnings**](Earnings.md) |  | 
**employee** | [**Employee**](Employee.md) |  | 
**employer** | [**PaystubEmployer**](PaystubEmployer.md) |  | 
**employment_details** | [**EmploymentDetails**](EmploymentDetails.md) |  | [optional] 
**net_pay** | [**NetPay**](NetPay.md) |  | 
**pay_period_details** | [**PayPeriodDetails**](PayPeriodDetails.md) |  | 
**paystub_details** | [**PaystubDetails**](PaystubDetails.md) |  | [optional] 
**income_breakdown** | [**List[IncomeBreakdown]**](IncomeBreakdown.md) |  | [optional] 
**ytd_earnings** | [**PaystubYTDDetails**](PaystubYTDDetails.md) |  | [optional] 

## Example

```python
from pyplaid.models.paystub import Paystub

# TODO update the JSON string below
json = "{}"
# create an instance of Paystub from a JSON string
paystub_instance = Paystub.from_json(json)
# print the JSON string representation of the object
print Paystub.to_json()

# convert the object into a dict
paystub_dict = paystub_instance.to_dict()
# create an instance of Paystub from a dict
paystub_form_dict = paystub.from_dict(paystub_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


