# EmployersSearchResponse

EmployersSearchResponse defines the response schema for `/employers/search`.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**employers** | [**List[Employer]**](Employer.md) | A list of employers matching the search criteria. | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.employers_search_response import EmployersSearchResponse

# TODO update the JSON string below
json = "{}"
# create an instance of EmployersSearchResponse from a JSON string
employers_search_response_instance = EmployersSearchResponse.from_json(json)
# print the JSON string representation of the object
print EmployersSearchResponse.to_json()

# convert the object into a dict
employers_search_response_dict = employers_search_response_instance.to_dict()
# create an instance of EmployersSearchResponse from a dict
employers_search_response_form_dict = employers_search_response.from_dict(employers_search_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


