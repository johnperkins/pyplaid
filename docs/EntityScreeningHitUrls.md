# EntityScreeningHitUrls

URLs associated with the entity screening hit

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**url** | **str** | An &#39;http&#39; or &#39;https&#39; URL (must begin with either of those). | 

## Example

```python
from pyplaid.models.entity_screening_hit_urls import EntityScreeningHitUrls

# TODO update the JSON string below
json = "{}"
# create an instance of EntityScreeningHitUrls from a JSON string
entity_screening_hit_urls_instance = EntityScreeningHitUrls.from_json(json)
# print the JSON string representation of the object
print EntityScreeningHitUrls.to_json()

# convert the object into a dict
entity_screening_hit_urls_dict = entity_screening_hit_urls_instance.to_dict()
# create an instance of EntityScreeningHitUrls from a dict
entity_screening_hit_urls_form_dict = entity_screening_hit_urls.from_dict(entity_screening_hit_urls_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


