# IncomeVerificationCreateResponse

IncomeVerificationCreateResponse defines the response schema for `/income/verification/create`.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**income_verification_id** | **str** | ID of the verification. This ID is persisted throughout the lifetime of the verification. | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.income_verification_create_response import IncomeVerificationCreateResponse

# TODO update the JSON string below
json = "{}"
# create an instance of IncomeVerificationCreateResponse from a JSON string
income_verification_create_response_instance = IncomeVerificationCreateResponse.from_json(json)
# print the JSON string representation of the object
print IncomeVerificationCreateResponse.to_json()

# convert the object into a dict
income_verification_create_response_dict = income_verification_create_response_instance.to_dict()
# create an instance of IncomeVerificationCreateResponse from a dict
income_verification_create_response_form_dict = income_verification_create_response.from_dict(income_verification_create_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


