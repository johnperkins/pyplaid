# LinkTokenCreateRequestUpdate

Specifies options for initializing Link for [update mode](https://plaid.com/docs/link/update-mode).

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_selection_enabled** | **bool** | If &#x60;true&#x60;, enables [update mode with Account Select](https://plaid.com/docs/link/update-mode/#using-update-mode-to-request-new-accounts). | [optional] [default to False]

## Example

```python
from pyplaid.models.link_token_create_request_update import LinkTokenCreateRequestUpdate

# TODO update the JSON string below
json = "{}"
# create an instance of LinkTokenCreateRequestUpdate from a JSON string
link_token_create_request_update_instance = LinkTokenCreateRequestUpdate.from_json(json)
# print the JSON string representation of the object
print LinkTokenCreateRequestUpdate.to_json()

# convert the object into a dict
link_token_create_request_update_dict = link_token_create_request_update_instance.to_dict()
# create an instance of LinkTokenCreateRequestUpdate from a dict
link_token_create_request_update_form_dict = link_token_create_request_update.from_dict(link_token_create_request_update_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


