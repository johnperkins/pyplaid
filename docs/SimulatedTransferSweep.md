# SimulatedTransferSweep

A sweep returned from the `/sandbox/transfer/sweep/simulate` endpoint. Can be null if there are no transfers to include in a sweep.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Identifier of the sweep. | 
**created** | **datetime** | The datetime when the sweep occurred, in RFC 3339 format. | 
**amount** | **str** | Signed decimal amount of the sweep as it appears on your sweep account ledger (e.g. \&quot;-10.00\&quot;)  If amount is not present, the sweep was net-settled to zero and outstanding debits and credits between the sweep account and Plaid are balanced. | 
**iso_currency_code** | **str** | The currency of the sweep, e.g. \&quot;USD\&quot;. | 
**settled** | **date** | The date when the sweep settled, in the YYYY-MM-DD format. | 

## Example

```python
from pyplaid.models.simulated_transfer_sweep import SimulatedTransferSweep

# TODO update the JSON string below
json = "{}"
# create an instance of SimulatedTransferSweep from a JSON string
simulated_transfer_sweep_instance = SimulatedTransferSweep.from_json(json)
# print the JSON string representation of the object
print SimulatedTransferSweep.to_json()

# convert the object into a dict
simulated_transfer_sweep_dict = simulated_transfer_sweep_instance.to_dict()
# create an instance of SimulatedTransferSweep from a dict
simulated_transfer_sweep_form_dict = simulated_transfer_sweep.from_dict(simulated_transfer_sweep_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


