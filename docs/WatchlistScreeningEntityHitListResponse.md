# WatchlistScreeningEntityHitListResponse

Paginated list of entity watchlist screening hits

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**entity_watchlist_screening_hits** | [**List[EntityWatchlistScreeningHit]**](EntityWatchlistScreeningHit.md) | List of entity watchlist screening hits | 
**next_cursor** | **str** | An identifier that determines which page of results you receive. | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.watchlist_screening_entity_hit_list_response import WatchlistScreeningEntityHitListResponse

# TODO update the JSON string below
json = "{}"
# create an instance of WatchlistScreeningEntityHitListResponse from a JSON string
watchlist_screening_entity_hit_list_response_instance = WatchlistScreeningEntityHitListResponse.from_json(json)
# print the JSON string representation of the object
print WatchlistScreeningEntityHitListResponse.to_json()

# convert the object into a dict
watchlist_screening_entity_hit_list_response_dict = watchlist_screening_entity_hit_list_response_instance.to_dict()
# create an instance of WatchlistScreeningEntityHitListResponse from a dict
watchlist_screening_entity_hit_list_response_form_dict = watchlist_screening_entity_hit_list_response.from_dict(watchlist_screening_entity_hit_list_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


