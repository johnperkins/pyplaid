# LinkOAuthCorrelationIdExchangeResponse

LinkOAuthCorrelationIdExchangeResponse defines the response schema for `/link/oauth/correlation_id/exchange`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**link_token** | **str** | The &#x60;link_token&#x60; associated to the given &#x60;link_correlation_id&#x60;, which can be used to re-initialize Link. | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.link_o_auth_correlation_id_exchange_response import LinkOAuthCorrelationIdExchangeResponse

# TODO update the JSON string below
json = "{}"
# create an instance of LinkOAuthCorrelationIdExchangeResponse from a JSON string
link_o_auth_correlation_id_exchange_response_instance = LinkOAuthCorrelationIdExchangeResponse.from_json(json)
# print the JSON string representation of the object
print LinkOAuthCorrelationIdExchangeResponse.to_json()

# convert the object into a dict
link_o_auth_correlation_id_exchange_response_dict = link_o_auth_correlation_id_exchange_response_instance.to_dict()
# create an instance of LinkOAuthCorrelationIdExchangeResponse from a dict
link_o_auth_correlation_id_exchange_response_form_dict = link_o_auth_correlation_id_exchange_response.from_dict(link_o_auth_correlation_id_exchange_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


