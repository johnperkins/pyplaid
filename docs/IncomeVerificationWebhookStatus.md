# IncomeVerificationWebhookStatus

Status of the income verification webhook

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | 

## Example

```python
from pyplaid.models.income_verification_webhook_status import IncomeVerificationWebhookStatus

# TODO update the JSON string below
json = "{}"
# create an instance of IncomeVerificationWebhookStatus from a JSON string
income_verification_webhook_status_instance = IncomeVerificationWebhookStatus.from_json(json)
# print the JSON string representation of the object
print IncomeVerificationWebhookStatus.to_json()

# convert the object into a dict
income_verification_webhook_status_dict = income_verification_webhook_status_instance.to_dict()
# create an instance of IncomeVerificationWebhookStatus from a dict
income_verification_webhook_status_form_dict = income_verification_webhook_status.from_dict(income_verification_webhook_status_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


