# SignalUser

Details about the end user initiating the transaction (i.e., the account holder).

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | [**SignalPersonName**](SignalPersonName.md) |  | [optional] 
**phone_number** | **str** | The user&#39;s phone number, in E.164 format: +{countrycode}{number}. For example: \&quot;+14151234567\&quot; | [optional] 
**email_address** | **str** | The user&#39;s email address. | [optional] 
**address** | [**SignalAddressData**](SignalAddressData.md) |  | [optional] 

## Example

```python
from pyplaid.models.signal_user import SignalUser

# TODO update the JSON string below
json = "{}"
# create an instance of SignalUser from a JSON string
signal_user_instance = SignalUser.from_json(json)
# print the JSON string representation of the object
print SignalUser.to_json()

# convert the object into a dict
signal_user_dict = signal_user_instance.to_dict()
# create an instance of SignalUser from a dict
signal_user_form_dict = signal_user.from_dict(signal_user_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


