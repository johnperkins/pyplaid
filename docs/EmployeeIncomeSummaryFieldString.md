# EmployeeIncomeSummaryFieldString

The name of the employee, as reported on the paystub.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value** | **str** | The value of the field. | 
**verification_status** | [**VerificationStatus**](VerificationStatus.md) |  | 

## Example

```python
from pyplaid.models.employee_income_summary_field_string import EmployeeIncomeSummaryFieldString

# TODO update the JSON string below
json = "{}"
# create an instance of EmployeeIncomeSummaryFieldString from a JSON string
employee_income_summary_field_string_instance = EmployeeIncomeSummaryFieldString.from_json(json)
# print the JSON string representation of the object
print EmployeeIncomeSummaryFieldString.to_json()

# convert the object into a dict
employee_income_summary_field_string_dict = employee_income_summary_field_string_instance.to_dict()
# create an instance of EmployeeIncomeSummaryFieldString from a dict
employee_income_summary_field_string_form_dict = employee_income_summary_field_string.from_dict(employee_income_summary_field_string_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


