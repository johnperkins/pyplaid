# KYCCheckAddressSummary

Result summary object specifying how the `address` field matched.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**summary** | [**MatchSummaryCode**](MatchSummaryCode.md) |  | 
**po_box** | [**POBoxStatus**](POBoxStatus.md) |  | 
**type** | [**AddressPurposeLabel**](AddressPurposeLabel.md) |  | 

## Example

```python
from pyplaid.models.kyc_check_address_summary import KYCCheckAddressSummary

# TODO update the JSON string below
json = "{}"
# create an instance of KYCCheckAddressSummary from a JSON string
kyc_check_address_summary_instance = KYCCheckAddressSummary.from_json(json)
# print the JSON string representation of the object
print KYCCheckAddressSummary.to_json()

# convert the object into a dict
kyc_check_address_summary_dict = kyc_check_address_summary_instance.to_dict()
# create an instance of KYCCheckAddressSummary from a dict
kyc_check_address_summary_form_dict = kyc_check_address_summary.from_dict(kyc_check_address_summary_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


