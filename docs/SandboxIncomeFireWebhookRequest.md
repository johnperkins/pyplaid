# SandboxIncomeFireWebhookRequest

SandboxIncomeFireWebhookRequest defines the request schema for `/sandbox/income/fire_webhook`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**item_id** | **str** | The Item ID associated with the verification. | 
**user_id** | **str** | The Plaid &#x60;user_id&#x60; of the User associated with this webhook, warning, or error. | [optional] 
**webhook** | **str** | The URL to which the webhook should be sent. | 
**verification_status** | **str** | &#x60;VERIFICATION_STATUS_PROCESSING_COMPLETE&#x60;: The income verification status processing has completed. If the user uploaded multiple documents, this webhook will fire when all documents have finished processing. Call the &#x60;/income/verification/paystubs/get&#x60; endpoint and check the document metadata to see which documents were successfully parsed.  &#x60;VERIFICATION_STATUS_PROCESSING_FAILED&#x60;: A failure occurred when attempting to process the verification documentation.  &#x60;VERIFICATION_STATUS_PENDING_APPROVAL&#x60;: (deprecated) The income verification has been sent to the user for review. | 

## Example

```python
from pyplaid.models.sandbox_income_fire_webhook_request import SandboxIncomeFireWebhookRequest

# TODO update the JSON string below
json = "{}"
# create an instance of SandboxIncomeFireWebhookRequest from a JSON string
sandbox_income_fire_webhook_request_instance = SandboxIncomeFireWebhookRequest.from_json(json)
# print the JSON string representation of the object
print SandboxIncomeFireWebhookRequest.to_json()

# convert the object into a dict
sandbox_income_fire_webhook_request_dict = sandbox_income_fire_webhook_request_instance.to_dict()
# create an instance of SandboxIncomeFireWebhookRequest from a dict
sandbox_income_fire_webhook_request_form_dict = sandbox_income_fire_webhook_request.from_dict(sandbox_income_fire_webhook_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


