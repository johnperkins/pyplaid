# InvestmentsTransactionsGetRequest

InvestmentsTransactionsGetRequest defines the request schema for `/investments/transactions/get`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**access_token** | **str** | The access token associated with the Item data is being requested for. | 
**start_date** | **date** | The earliest date for which to fetch transaction history. Dates should be formatted as YYYY-MM-DD. | 
**end_date** | **date** | The most recent date for which to fetch transaction history. Dates should be formatted as YYYY-MM-DD. | 
**options** | [**InvestmentsTransactionsGetRequestOptions**](InvestmentsTransactionsGetRequestOptions.md) |  | [optional] 

## Example

```python
from pyplaid.models.investments_transactions_get_request import InvestmentsTransactionsGetRequest

# TODO update the JSON string below
json = "{}"
# create an instance of InvestmentsTransactionsGetRequest from a JSON string
investments_transactions_get_request_instance = InvestmentsTransactionsGetRequest.from_json(json)
# print the JSON string representation of the object
print InvestmentsTransactionsGetRequest.to_json()

# convert the object into a dict
investments_transactions_get_request_dict = investments_transactions_get_request_instance.to_dict()
# create an instance of InvestmentsTransactionsGetRequest from a dict
investments_transactions_get_request_form_dict = investments_transactions_get_request.from_dict(investments_transactions_get_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


