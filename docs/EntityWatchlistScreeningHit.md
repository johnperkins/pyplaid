# EntityWatchlistScreeningHit

Data from a government watchlist that has been attached to the screening.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | ID of the associated entity screening hit. | 
**review_status** | [**WatchlistScreeningHitStatus**](WatchlistScreeningHitStatus.md) |  | 
**first_active** | **datetime** | An ISO8601 formatted timestamp. | 
**inactive_since** | **datetime** | An ISO8601 formatted timestamp. | 
**historical_since** | **datetime** | An ISO8601 formatted timestamp. | 
**list_code** | [**EntityWatchlistCode**](EntityWatchlistCode.md) |  | 
**plaid_uid** | **str** | A universal identifier for a watchlist individual that is stable across searches and updates. | 
**source_uid** | **str** | The identifier provided by the source sanction or watchlist. When one is not provided by the source, this is &#x60;null&#x60;. | 
**analysis** | [**EntityScreeningHitAnalysis**](EntityScreeningHitAnalysis.md) |  | [optional] 
**data** | [**EntityScreeningHitData**](EntityScreeningHitData.md) |  | [optional] 

## Example

```python
from pyplaid.models.entity_watchlist_screening_hit import EntityWatchlistScreeningHit

# TODO update the JSON string below
json = "{}"
# create an instance of EntityWatchlistScreeningHit from a JSON string
entity_watchlist_screening_hit_instance = EntityWatchlistScreeningHit.from_json(json)
# print the JSON string representation of the object
print EntityWatchlistScreeningHit.to_json()

# convert the object into a dict
entity_watchlist_screening_hit_dict = entity_watchlist_screening_hit_instance.to_dict()
# create an instance of EntityWatchlistScreeningHit from a dict
entity_watchlist_screening_hit_form_dict = entity_watchlist_screening_hit.from_dict(entity_watchlist_screening_hit_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


