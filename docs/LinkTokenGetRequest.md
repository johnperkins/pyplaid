# LinkTokenGetRequest

LinkTokenGetRequest defines the request schema for `/link/token/get`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**link_token** | **str** | A &#x60;link_token&#x60; from a previous invocation of &#x60;/link/token/create&#x60; | 

## Example

```python
from pyplaid.models.link_token_get_request import LinkTokenGetRequest

# TODO update the JSON string below
json = "{}"
# create an instance of LinkTokenGetRequest from a JSON string
link_token_get_request_instance = LinkTokenGetRequest.from_json(json)
# print the JSON string representation of the object
print LinkTokenGetRequest.to_json()

# convert the object into a dict
link_token_get_request_dict = link_token_get_request_instance.to_dict()
# create an instance of LinkTokenGetRequest from a dict
link_token_get_request_form_dict = link_token_get_request.from_dict(link_token_get_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


