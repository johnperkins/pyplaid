# TransactionsRulesRemoveResponse

TransactionsRulesRemoveResponse defines the response schema for `/beta/transactions/rules/v1/remove`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.transactions_rules_remove_response import TransactionsRulesRemoveResponse

# TODO update the JSON string below
json = "{}"
# create an instance of TransactionsRulesRemoveResponse from a JSON string
transactions_rules_remove_response_instance = TransactionsRulesRemoveResponse.from_json(json)
# print the JSON string representation of the object
print TransactionsRulesRemoveResponse.to_json()

# convert the object into a dict
transactions_rules_remove_response_dict = transactions_rules_remove_response_instance.to_dict()
# create an instance of TransactionsRulesRemoveResponse from a dict
transactions_rules_remove_response_form_dict = transactions_rules_remove_response.from_dict(transactions_rules_remove_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


