# PaymentInitiationPaymentListResponse

PaymentInitiationPaymentListResponse defines the response schema for `/payment_initiation/payment/list`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**payments** | [**List[PaymentInitiationPayment]**](PaymentInitiationPayment.md) | An array of payments that have been created, associated with the given &#x60;client_id&#x60;. | 
**next_cursor** | **datetime** | The value that, when used as the optional &#x60;cursor&#x60; parameter to &#x60;/payment_initiation/payment/list&#x60;, will return the next unreturned payment as its first payment. | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.payment_initiation_payment_list_response import PaymentInitiationPaymentListResponse

# TODO update the JSON string below
json = "{}"
# create an instance of PaymentInitiationPaymentListResponse from a JSON string
payment_initiation_payment_list_response_instance = PaymentInitiationPaymentListResponse.from_json(json)
# print the JSON string representation of the object
print PaymentInitiationPaymentListResponse.to_json()

# convert the object into a dict
payment_initiation_payment_list_response_dict = payment_initiation_payment_list_response_instance.to_dict()
# create an instance of PaymentInitiationPaymentListResponse from a dict
payment_initiation_payment_list_response_form_dict = payment_initiation_payment_list_response.from_dict(payment_initiation_payment_list_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


