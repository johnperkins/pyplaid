# PartnerEndCustomerBillingContact

The billing contact for the end customer. Defaults to partner's technical contact if omitted.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**given_name** | **str** |  | [optional] 
**family_name** | **str** |  | [optional] 
**email** | **str** |  | [optional] 

## Example

```python
from pyplaid.models.partner_end_customer_billing_contact import PartnerEndCustomerBillingContact

# TODO update the JSON string below
json = "{}"
# create an instance of PartnerEndCustomerBillingContact from a JSON string
partner_end_customer_billing_contact_instance = PartnerEndCustomerBillingContact.from_json(json)
# print the JSON string representation of the object
print PartnerEndCustomerBillingContact.to_json()

# convert the object into a dict
partner_end_customer_billing_contact_dict = partner_end_customer_billing_contact_instance.to_dict()
# create an instance of PartnerEndCustomerBillingContact from a dict
partner_end_customer_billing_contact_form_dict = partner_end_customer_billing_contact.from_dict(partner_end_customer_billing_contact_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


