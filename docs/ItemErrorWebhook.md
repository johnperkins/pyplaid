# ItemErrorWebhook

Fired when an error is encountered with an Item. The error can be resolved by having the user go through Link’s update mode.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**webhook_type** | **str** | &#x60;ITEM&#x60; | 
**webhook_code** | **str** | &#x60;ERROR&#x60; | 
**item_id** | **str** | The &#x60;item_id&#x60; of the Item associated with this webhook, warning, or error | 
**error** | [**PlaidError**](PlaidError.md) |  | 
**environment** | [**WebhookEnvironmentValues**](WebhookEnvironmentValues.md) |  | 

## Example

```python
from pyplaid.models.item_error_webhook import ItemErrorWebhook

# TODO update the JSON string below
json = "{}"
# create an instance of ItemErrorWebhook from a JSON string
item_error_webhook_instance = ItemErrorWebhook.from_json(json)
# print the JSON string representation of the object
print ItemErrorWebhook.to_json()

# convert the object into a dict
item_error_webhook_dict = item_error_webhook_instance.to_dict()
# create an instance of ItemErrorWebhook from a dict
item_error_webhook_form_dict = item_error_webhook.from_dict(item_error_webhook_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


