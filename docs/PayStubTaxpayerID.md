# PayStubTaxpayerID

Taxpayer ID of the individual receiving the paystub.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id_type** | **str** | Type of ID, e.g. &#39;SSN&#39;. | 
**id_mask** | **str** | ID mask; i.e. last 4 digits of the taxpayer ID. | 

## Example

```python
from pyplaid.models.pay_stub_taxpayer_id import PayStubTaxpayerID

# TODO update the JSON string below
json = "{}"
# create an instance of PayStubTaxpayerID from a JSON string
pay_stub_taxpayer_id_instance = PayStubTaxpayerID.from_json(json)
# print the JSON string representation of the object
print PayStubTaxpayerID.to_json()

# convert the object into a dict
pay_stub_taxpayer_id_dict = pay_stub_taxpayer_id_instance.to_dict()
# create an instance of PayStubTaxpayerID from a dict
pay_stub_taxpayer_id_form_dict = pay_stub_taxpayer_id.from_dict(pay_stub_taxpayer_id_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


