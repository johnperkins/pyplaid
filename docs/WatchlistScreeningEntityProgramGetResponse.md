# WatchlistScreeningEntityProgramGetResponse

A program that configures the active lists, search parameters, and other behavior for initial and ongoing screening of entities.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | ID of the associated entity program. | 
**created_at** | **datetime** | An ISO8601 formatted timestamp. | 
**is_rescanning_enabled** | **bool** | Indicator specifying whether the program is enabled and will perform daily rescans. | 
**lists_enabled** | [**List[EntityWatchlistCode]**](EntityWatchlistCode.md) | Watchlists enabled for the associated program | 
**name** | **str** | A name for the entity program to define its purpose. For example, \&quot;High Risk Organizations\&quot; or \&quot;Applicants\&quot;. | 
**name_sensitivity** | [**ProgramNameSensitivity**](ProgramNameSensitivity.md) |  | 
**audit_trail** | [**WatchlistScreeningAuditTrail**](WatchlistScreeningAuditTrail.md) |  | 
**is_archived** | **bool** | Archived programs are read-only and cannot screen new customers nor participate in ongoing monitoring. | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.watchlist_screening_entity_program_get_response import WatchlistScreeningEntityProgramGetResponse

# TODO update the JSON string below
json = "{}"
# create an instance of WatchlistScreeningEntityProgramGetResponse from a JSON string
watchlist_screening_entity_program_get_response_instance = WatchlistScreeningEntityProgramGetResponse.from_json(json)
# print the JSON string representation of the object
print WatchlistScreeningEntityProgramGetResponse.to_json()

# convert the object into a dict
watchlist_screening_entity_program_get_response_dict = watchlist_screening_entity_program_get_response_instance.to_dict()
# create an instance of WatchlistScreeningEntityProgramGetResponse from a dict
watchlist_screening_entity_program_get_response_form_dict = watchlist_screening_entity_program_get_response.from_dict(watchlist_screening_entity_program_get_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


