# UserStatedIncomeSourcePayType

The pay type - `GROSS`, `NET`, or `UNKNOWN` for a specified income source

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


