# CreditBankIncomeCause

An error object and associated `item_id` used to identify a specific Item and error when a batch operation operating on multiple Items has encountered an error in one of the Items.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**error_type** | [**CreditBankIncomeErrorType**](CreditBankIncomeErrorType.md) |  | [optional] 
**error_code** | **str** | We use standard HTTP response codes for success and failure notifications, and our errors are further classified by &#x60;error_type&#x60;. In general, 200 HTTP codes correspond to success, 40X codes are for developer- or user-related failures, and 50X codes are for Plaid-related issues. Error fields will be &#x60;null&#x60; if no error has occurred. | [optional] 
**error_message** | **str** | A developer-friendly representation of the error code. This may change over time and is not safe for programmatic use. | [optional] 
**display_message** | **str** | A user-friendly representation of the error code. null if the error is not related to user action. This may change over time and is not safe for programmatic use. | [optional] 
**item_id** | **str** | The &#x60;item_id&#x60; of the Item associated with this warning. | [optional] 

## Example

```python
from pyplaid.models.credit_bank_income_cause import CreditBankIncomeCause

# TODO update the JSON string below
json = "{}"
# create an instance of CreditBankIncomeCause from a JSON string
credit_bank_income_cause_instance = CreditBankIncomeCause.from_json(json)
# print the JSON string representation of the object
print CreditBankIncomeCause.to_json()

# convert the object into a dict
credit_bank_income_cause_dict = credit_bank_income_cause_instance.to_dict()
# create an instance of CreditBankIncomeCause from a dict
credit_bank_income_cause_form_dict = credit_bank_income_cause.from_dict(credit_bank_income_cause_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


