# PaystubAddress

Address on the paystub

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**city** | **str** | The full city name. | [optional] 
**country** | **str** | The ISO 3166-1 alpha-2 country code. | [optional] 
**postal_code** | **str** | The postal code of the address. | [optional] 
**region** | **str** | The region or state Example: &#x60;\&quot;NC\&quot;&#x60; | [optional] 
**street** | **str** | The full street address. | [optional] 
**line1** | **str** | Street address line 1. | [optional] 
**line2** | **str** | Street address line 2. | [optional] 
**state_code** | **str** | The region or state Example: &#x60;\&quot;NC\&quot;&#x60; | [optional] 

## Example

```python
from pyplaid.models.paystub_address import PaystubAddress

# TODO update the JSON string below
json = "{}"
# create an instance of PaystubAddress from a JSON string
paystub_address_instance = PaystubAddress.from_json(json)
# print the JSON string representation of the object
print PaystubAddress.to_json()

# convert the object into a dict
paystub_address_dict = paystub_address_instance.to_dict()
# create an instance of PaystubAddress from a dict
paystub_address_form_dict = paystub_address.from_dict(paystub_address_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


