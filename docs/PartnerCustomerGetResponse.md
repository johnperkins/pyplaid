# PartnerCustomerGetResponse

Response schema for `/partner/customer/get`.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**end_customer** | [**PartnerEndCustomer**](PartnerEndCustomer.md) |  | [optional] 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | [optional] 

## Example

```python
from pyplaid.models.partner_customer_get_response import PartnerCustomerGetResponse

# TODO update the JSON string below
json = "{}"
# create an instance of PartnerCustomerGetResponse from a JSON string
partner_customer_get_response_instance = PartnerCustomerGetResponse.from_json(json)
# print the JSON string representation of the object
print PartnerCustomerGetResponse.to_json()

# convert the object into a dict
partner_customer_get_response_dict = partner_customer_get_response_instance.to_dict()
# create an instance of PartnerCustomerGetResponse from a dict
partner_customer_get_response_form_dict = partner_customer_get_response.from_dict(partner_customer_get_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


