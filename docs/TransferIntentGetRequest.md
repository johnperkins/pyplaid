# TransferIntentGetRequest

Defines the request schema for `/transfer/intent/get`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**transfer_intent_id** | **str** | Plaid&#39;s unique identifier for a transfer intent object. | 

## Example

```python
from pyplaid.models.transfer_intent_get_request import TransferIntentGetRequest

# TODO update the JSON string below
json = "{}"
# create an instance of TransferIntentGetRequest from a JSON string
transfer_intent_get_request_instance = TransferIntentGetRequest.from_json(json)
# print the JSON string representation of the object
print TransferIntentGetRequest.to_json()

# convert the object into a dict
transfer_intent_get_request_dict = transfer_intent_get_request_instance.to_dict()
# create an instance of TransferIntentGetRequest from a dict
transfer_intent_get_request_form_dict = transfer_intent_get_request.from_dict(transfer_intent_get_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


