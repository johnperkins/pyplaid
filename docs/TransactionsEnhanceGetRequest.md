# TransactionsEnhanceGetRequest

TransactionsEnhanceGetRequest defines the request schema for `/transactions/enhance`.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**account_type** | **str** | The type of account for the requested transactions (&#x60;depository&#x60; or &#x60;credit&#x60;). | 
**transactions** | [**List[ClientProvidedRawTransaction]**](ClientProvidedRawTransaction.md) | An array of raw transactions to be enhanced. | 

## Example

```python
from pyplaid.models.transactions_enhance_get_request import TransactionsEnhanceGetRequest

# TODO update the JSON string below
json = "{}"
# create an instance of TransactionsEnhanceGetRequest from a JSON string
transactions_enhance_get_request_instance = TransactionsEnhanceGetRequest.from_json(json)
# print the JSON string representation of the object
print TransactionsEnhanceGetRequest.to_json()

# convert the object into a dict
transactions_enhance_get_request_dict = transactions_enhance_get_request_instance.to_dict()
# create an instance of TransactionsEnhanceGetRequest from a dict
transactions_enhance_get_request_form_dict = transactions_enhance_get_request.from_dict(transactions_enhance_get_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


