# AssetReportFreddieGetResponse

AssetReportFreddieGetResponse defines the response schema for `/asset_report/get`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**deal** | [**AssetReportFreddie**](AssetReportFreddie.md) |  | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 
**schema_version** | **float** | The Verification Of Assets (aka VOA or Freddie Mac Schema) schema version. | 

## Example

```python
from pyplaid.models.asset_report_freddie_get_response import AssetReportFreddieGetResponse

# TODO update the JSON string below
json = "{}"
# create an instance of AssetReportFreddieGetResponse from a JSON string
asset_report_freddie_get_response_instance = AssetReportFreddieGetResponse.from_json(json)
# print the JSON string representation of the object
print AssetReportFreddieGetResponse.to_json()

# convert the object into a dict
asset_report_freddie_get_response_dict = asset_report_freddie_get_response_instance.to_dict()
# create an instance of AssetReportFreddieGetResponse from a dict
asset_report_freddie_get_response_form_dict = asset_report_freddie_get_response.from_dict(asset_report_freddie_get_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


