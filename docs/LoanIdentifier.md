# LoanIdentifier

The information used to identify this loan by various parties to the transaction or other organizations.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**loan_identifier** | **str** | The value of the identifier for the specified type. | 
**loan_identifier_type** | [**LoanIdentifierType**](LoanIdentifierType.md) |  | 

## Example

```python
from pyplaid.models.loan_identifier import LoanIdentifier

# TODO update the JSON string below
json = "{}"
# create an instance of LoanIdentifier from a JSON string
loan_identifier_instance = LoanIdentifier.from_json(json)
# print the JSON string representation of the object
print LoanIdentifier.to_json()

# convert the object into a dict
loan_identifier_dict = loan_identifier_instance.to_dict()
# create an instance of LoanIdentifier from a dict
loan_identifier_form_dict = loan_identifier.from_dict(loan_identifier_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


