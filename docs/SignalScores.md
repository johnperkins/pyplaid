# SignalScores

Risk scoring details broken down by risk category.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customer_initiated_return_risk** | [**CustomerInitiatedReturnRisk**](CustomerInitiatedReturnRisk.md) |  | [optional] 
**bank_initiated_return_risk** | [**BankInitiatedReturnRisk**](BankInitiatedReturnRisk.md) |  | [optional] 

## Example

```python
from pyplaid.models.signal_scores import SignalScores

# TODO update the JSON string below
json = "{}"
# create an instance of SignalScores from a JSON string
signal_scores_instance = SignalScores.from_json(json)
# print the JSON string representation of the object
print SignalScores.to_json()

# convert the object into a dict
signal_scores_dict = signal_scores_instance.to_dict()
# create an instance of SignalScores from a dict
signal_scores_form_dict = signal_scores.from_dict(signal_scores_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


