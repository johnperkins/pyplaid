# CreditPlatformIds

The object containing a set of ids related to an employee.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**employee_id** | **str** | The ID of an employee as given by their employer. | 
**payroll_id** | **str** | The ID of an employee as given by their payroll. | 
**position_id** | **str** | The ID of the position of the employee. | 

## Example

```python
from pyplaid.models.credit_platform_ids import CreditPlatformIds

# TODO update the JSON string below
json = "{}"
# create an instance of CreditPlatformIds from a JSON string
credit_platform_ids_instance = CreditPlatformIds.from_json(json)
# print the JSON string representation of the object
print CreditPlatformIds.to_json()

# convert the object into a dict
credit_platform_ids_dict = credit_platform_ids_instance.to_dict()
# create an instance of CreditPlatformIds from a dict
credit_platform_ids_form_dict = credit_platform_ids.from_dict(credit_platform_ids_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


