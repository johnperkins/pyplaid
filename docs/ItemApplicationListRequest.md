# ItemApplicationListRequest

Request to list connected applications for a user.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**access_token** | **str** | The access token associated with the Item data is being requested for. | [optional] 

## Example

```python
from pyplaid.models.item_application_list_request import ItemApplicationListRequest

# TODO update the JSON string below
json = "{}"
# create an instance of ItemApplicationListRequest from a JSON string
item_application_list_request_instance = ItemApplicationListRequest.from_json(json)
# print the JSON string representation of the object
print ItemApplicationListRequest.to_json()

# convert the object into a dict
item_application_list_request_dict = item_application_list_request_instance.to_dict()
# create an instance of ItemApplicationListRequest from a dict
item_application_list_request_form_dict = item_application_list_request.from_dict(item_application_list_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


