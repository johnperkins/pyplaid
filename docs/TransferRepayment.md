# TransferRepayment

A repayment is created automatically after one or more guaranteed transactions receive a return. If there are multiple eligible returns in a day, they are batched together into a single repayment.  Repayments are sent over ACH, with funds typically available on the next banking day.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**repayment_id** | **str** | Identifier of the repayment. | 
**created** | **datetime** | The datetime when the repayment occurred, in RFC 3339 format. | 
**amount** | **str** | Decimal amount of the repayment as it appears on your account ledger. | 
**iso_currency_code** | **str** | The currency of the repayment, e.g. \&quot;USD\&quot;. | 

## Example

```python
from pyplaid.models.transfer_repayment import TransferRepayment

# TODO update the JSON string below
json = "{}"
# create an instance of TransferRepayment from a JSON string
transfer_repayment_instance = TransferRepayment.from_json(json)
# print the JSON string representation of the object
print TransferRepayment.to_json()

# convert the object into a dict
transfer_repayment_dict = transfer_repayment_instance.to_dict()
# create an instance of TransferRepayment from a dict
transfer_repayment_form_dict = transfer_repayment.from_dict(transfer_repayment_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


