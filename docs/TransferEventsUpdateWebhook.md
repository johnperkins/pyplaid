# TransferEventsUpdateWebhook

Fired when new transfer events are available. Receiving this webhook indicates you should fetch the new events from `/transfer/event/sync`.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**webhook_type** | **str** | &#x60;TRANSFER&#x60; | 
**webhook_code** | **str** | &#x60;TRANSFER_EVENTS_UPDATE&#x60; | 
**environment** | [**WebhookEnvironmentValues**](WebhookEnvironmentValues.md) |  | 

## Example

```python
from pyplaid.models.transfer_events_update_webhook import TransferEventsUpdateWebhook

# TODO update the JSON string below
json = "{}"
# create an instance of TransferEventsUpdateWebhook from a JSON string
transfer_events_update_webhook_instance = TransferEventsUpdateWebhook.from_json(json)
# print the JSON string representation of the object
print TransferEventsUpdateWebhook.to_json()

# convert the object into a dict
transfer_events_update_webhook_dict = transfer_events_update_webhook_instance.to_dict()
# create an instance of TransferEventsUpdateWebhook from a dict
transfer_events_update_webhook_form_dict = transfer_events_update_webhook.from_dict(transfer_events_update_webhook_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


