# IdentityVerificationRetryRequest

Request input for retrying an identity verification attempt

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_user_id** | **str** | An identifier to help you connect this object to your internal systems. For example, your database ID corresponding to this object. | 
**template_id** | **str** | ID of the associated Identity Verification template. | 
**strategy** | [**Strategy**](Strategy.md) |  | 
**steps** | [**IdentityVerificationRetryRequestStepsObject**](IdentityVerificationRetryRequestStepsObject.md) |  | [optional] 
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 

## Example

```python
from pyplaid.models.identity_verification_retry_request import IdentityVerificationRetryRequest

# TODO update the JSON string below
json = "{}"
# create an instance of IdentityVerificationRetryRequest from a JSON string
identity_verification_retry_request_instance = IdentityVerificationRetryRequest.from_json(json)
# print the JSON string representation of the object
print IdentityVerificationRetryRequest.to_json()

# convert the object into a dict
identity_verification_retry_request_dict = identity_verification_retry_request_instance.to_dict()
# create an instance of IdentityVerificationRetryRequest from a dict
identity_verification_retry_request_form_dict = identity_verification_retry_request.from_dict(identity_verification_retry_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


