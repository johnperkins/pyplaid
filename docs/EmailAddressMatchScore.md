# EmailAddressMatchScore

Score found by matching email provided by the API with the email on the account at the financial institution. If the account contains multiple owners, the maximum match score is filled.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**scores** | **int** | Match score for normalized email. 100 is a perfect match and 0 is a no match. If the email is missing from either the API or financial institution, this is empty. | [optional] 

## Example

```python
from pyplaid.models.email_address_match_score import EmailAddressMatchScore

# TODO update the JSON string below
json = "{}"
# create an instance of EmailAddressMatchScore from a JSON string
email_address_match_score_instance = EmailAddressMatchScore.from_json(json)
# print the JSON string representation of the object
print EmailAddressMatchScore.to_json()

# convert the object into a dict
email_address_match_score_dict = email_address_match_score_instance.to_dict()
# create an instance of EmailAddressMatchScore from a dict
email_address_match_score_form_dict = email_address_match_score.from_dict(email_address_match_score_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


