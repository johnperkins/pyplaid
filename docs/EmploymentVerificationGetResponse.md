# EmploymentVerificationGetResponse

EmploymentVerificationGetResponse defines the response schema for `/employment/verification/get`.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**employments** | [**List[EmploymentVerification]**](EmploymentVerification.md) | A list of employment verification summaries. | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.employment_verification_get_response import EmploymentVerificationGetResponse

# TODO update the JSON string below
json = "{}"
# create an instance of EmploymentVerificationGetResponse from a JSON string
employment_verification_get_response_instance = EmploymentVerificationGetResponse.from_json(json)
# print the JSON string representation of the object
print EmploymentVerificationGetResponse.to_json()

# convert the object into a dict
employment_verification_get_response_dict = employment_verification_get_response_instance.to_dict()
# create an instance of EmploymentVerificationGetResponse from a dict
employment_verification_get_response_form_dict = employment_verification_get_response.from_dict(employment_verification_get_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


