# ItemApplicationListUserAuth

User authentication parameters, for clients making a request without an `access_token`. This is only allowed for select clients and will not be supported in the future. Most clients should call /item/import to obtain an access token before making a request.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**user_id** | **str** | Account username. | [optional] 
**fi_username_hash** | **str** | Account username hashed by FI. | [optional] 

## Example

```python
from pyplaid.models.item_application_list_user_auth import ItemApplicationListUserAuth

# TODO update the JSON string below
json = "{}"
# create an instance of ItemApplicationListUserAuth from a JSON string
item_application_list_user_auth_instance = ItemApplicationListUserAuth.from_json(json)
# print the JSON string representation of the object
print ItemApplicationListUserAuth.to_json()

# convert the object into a dict
item_application_list_user_auth_dict = item_application_list_user_auth_instance.to_dict()
# create an instance of ItemApplicationListUserAuth from a dict
item_application_list_user_auth_form_dict = item_application_list_user_auth.from_dict(item_application_list_user_auth_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


