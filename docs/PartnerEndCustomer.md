# PartnerEndCustomer

The details for an end customer.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** |  | [optional] 
**company_name** | **str** |  | [optional] 
**status** | [**PartnerEndCustomerStatus**](PartnerEndCustomerStatus.md) |  | [optional] 

## Example

```python
from pyplaid.models.partner_end_customer import PartnerEndCustomer

# TODO update the JSON string below
json = "{}"
# create an instance of PartnerEndCustomer from a JSON string
partner_end_customer_instance = PartnerEndCustomer.from_json(json)
# print the JSON string representation of the object
print PartnerEndCustomer.to_json()

# convert the object into a dict
partner_end_customer_dict = partner_end_customer_instance.to_dict()
# create an instance of PartnerEndCustomer from a dict
partner_end_customer_form_dict = partner_end_customer.from_dict(partner_end_customer_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


