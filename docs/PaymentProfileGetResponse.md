# PaymentProfileGetResponse

PaymentProfileGetResponse defines the response schema for `/payment_profile/get`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**updated_at** | **datetime** | Timestamp in [ISO 8601](https://wikipedia.org/wiki/ISO_8601) format (&#x60;YYYY-MM-DDTHH:mm:ssZ&#x60;) indicating the last time the given Payment Profile was updated at | 
**created_at** | **datetime** | Timestamp in [ISO 8601](https://wikipedia.org/wiki/ISO_8601) format (&#x60;YYYY-MM-DDTHH:mm:ssZ&#x60;) indicating the time the given Payment Profile was created at | 
**deleted_at** | **datetime** | Timestamp in [ISO 8601](https://wikipedia.org/wiki/ISO_8601) format (&#x60;YYYY-MM-DDTHH:mm:ssZ&#x60;) indicating the time the given Payment Profile was deleted at. Always &#x60;null&#x60; if the Payment Profile has not been deleted | 
**status** | [**PaymentProfileStatus**](PaymentProfileStatus.md) |  | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.payment_profile_get_response import PaymentProfileGetResponse

# TODO update the JSON string below
json = "{}"
# create an instance of PaymentProfileGetResponse from a JSON string
payment_profile_get_response_instance = PaymentProfileGetResponse.from_json(json)
# print the JSON string representation of the object
print PaymentProfileGetResponse.to_json()

# convert the object into a dict
payment_profile_get_response_dict = payment_profile_get_response_instance.to_dict()
# create an instance of PaymentProfileGetResponse from a dict
payment_profile_get_response_form_dict = payment_profile_get_response.from_dict(payment_profile_get_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


