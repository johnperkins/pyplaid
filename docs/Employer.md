# Employer

Data about the employer.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**employer_id** | **str** | Plaid&#39;s unique identifier for the employer. | 
**name** | **str** | The name of the employer | 
**address** | [**AddressDataNullable**](AddressDataNullable.md) |  | 
**confidence_score** | **float** | A number from 0 to 1 indicating Plaid&#39;s level of confidence in the pairing between the employer and the institution (not yet implemented). | 

## Example

```python
from pyplaid.models.employer import Employer

# TODO update the JSON string below
json = "{}"
# create an instance of Employer from a JSON string
employer_instance = Employer.from_json(json)
# print the JSON string representation of the object
print Employer.to_json()

# convert the object into a dict
employer_dict = employer_instance.to_dict()
# create an instance of Employer from a dict
employer_form_dict = employer.from_dict(employer_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


