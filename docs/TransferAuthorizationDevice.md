# TransferAuthorizationDevice

Information about the device being used to initiate the authorization.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ip_address** | **str** | The IP address of the device being used to initiate the authorization. Required for Guarantee. | [optional] 
**user_agent** | **str** | The user agent of the device being used to initiate the authorization. Required for Guarantee. | [optional] 

## Example

```python
from pyplaid.models.transfer_authorization_device import TransferAuthorizationDevice

# TODO update the JSON string below
json = "{}"
# create an instance of TransferAuthorizationDevice from a JSON string
transfer_authorization_device_instance = TransferAuthorizationDevice.from_json(json)
# print the JSON string representation of the object
print TransferAuthorizationDevice.to_json()

# convert the object into a dict
transfer_authorization_device_dict = transfer_authorization_device_instance.to_dict()
# create an instance of TransferAuthorizationDevice from a dict
transfer_authorization_device_form_dict = transfer_authorization_device.from_dict(transfer_authorization_device_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


