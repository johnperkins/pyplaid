# StudentLoanRepaymentModel

Student loan repayment information used to configure Sandbox test data for the Liabilities product

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **str** | The only currently supported value for this field is &#x60;standard&#x60;. | 
**non_repayment_months** | **float** | Configures the number of months before repayment starts. | 
**repayment_months** | **float** | Configures the number of months of repayments before the loan is paid off. | 

## Example

```python
from pyplaid.models.student_loan_repayment_model import StudentLoanRepaymentModel

# TODO update the JSON string below
json = "{}"
# create an instance of StudentLoanRepaymentModel from a JSON string
student_loan_repayment_model_instance = StudentLoanRepaymentModel.from_json(json)
# print the JSON string representation of the object
print StudentLoanRepaymentModel.to_json()

# convert the object into a dict
student_loan_repayment_model_dict = student_loan_repayment_model_instance.to_dict()
# create an instance of StudentLoanRepaymentModel from a dict
student_loan_repayment_model_form_dict = student_loan_repayment_model.from_dict(student_loan_repayment_model_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


