# PaymentInitiationStandingOrderMetadata

Metadata specifically related to valid Payment Initiation standing order configurations for the institution.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**supports_standing_order_end_date** | **bool** | Indicates whether the institution supports closed-ended standing orders by providing an end date. | 
**supports_standing_order_negative_execution_days** | **bool** | This is only applicable to &#x60;MONTHLY&#x60; standing orders. Indicates whether the institution supports negative integers (-1 to -5) for setting up a &#x60;MONTHLY&#x60; standing order relative to the end of the month. | 
**valid_standing_order_intervals** | [**List[PaymentScheduleInterval]**](PaymentScheduleInterval.md) | A list of the valid standing order intervals supported by the institution. | 

## Example

```python
from pyplaid.models.payment_initiation_standing_order_metadata import PaymentInitiationStandingOrderMetadata

# TODO update the JSON string below
json = "{}"
# create an instance of PaymentInitiationStandingOrderMetadata from a JSON string
payment_initiation_standing_order_metadata_instance = PaymentInitiationStandingOrderMetadata.from_json(json)
# print the JSON string representation of the object
print PaymentInitiationStandingOrderMetadata.to_json()

# convert the object into a dict
payment_initiation_standing_order_metadata_dict = payment_initiation_standing_order_metadata_instance.to_dict()
# create an instance of PaymentInitiationStandingOrderMetadata from a dict
payment_initiation_standing_order_metadata_form_dict = payment_initiation_standing_order_metadata.from_dict(payment_initiation_standing_order_metadata_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


