# LiabilitiesGetRequest

LiabilitiesGetRequest defines the request schema for `/liabilities/get`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**access_token** | **str** | The access token associated with the Item data is being requested for. | 
**options** | [**LiabilitiesGetRequestOptions**](LiabilitiesGetRequestOptions.md) |  | [optional] 

## Example

```python
from pyplaid.models.liabilities_get_request import LiabilitiesGetRequest

# TODO update the JSON string below
json = "{}"
# create an instance of LiabilitiesGetRequest from a JSON string
liabilities_get_request_instance = LiabilitiesGetRequest.from_json(json)
# print the JSON string representation of the object
print LiabilitiesGetRequest.to_json()

# convert the object into a dict
liabilities_get_request_dict = liabilities_get_request_instance.to_dict()
# create an instance of LiabilitiesGetRequest from a dict
liabilities_get_request_form_dict = liabilities_get_request.from_dict(liabilities_get_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


