# ScreeningStatusUpdatedWebhook

Fired when an individual screening status has changed, which can occur manually via the dashboard or during ongoing monitoring.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**webhook_type** | **str** | &#x60;SCREENING&#x60; | 
**webhook_code** | **str** | &#x60;STATUS_UPDATED&#x60; | 
**screening_id** | **object** | The ID of the associated screening. | 
**environment** | [**WebhookEnvironmentValues**](WebhookEnvironmentValues.md) |  | 

## Example

```python
from pyplaid.models.screening_status_updated_webhook import ScreeningStatusUpdatedWebhook

# TODO update the JSON string below
json = "{}"
# create an instance of ScreeningStatusUpdatedWebhook from a JSON string
screening_status_updated_webhook_instance = ScreeningStatusUpdatedWebhook.from_json(json)
# print the JSON string representation of the object
print ScreeningStatusUpdatedWebhook.to_json()

# convert the object into a dict
screening_status_updated_webhook_dict = screening_status_updated_webhook_instance.to_dict()
# create an instance of ScreeningStatusUpdatedWebhook from a dict
screening_status_updated_webhook_form_dict = screening_status_updated_webhook.from_dict(screening_status_updated_webhook_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


