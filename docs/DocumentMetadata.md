# DocumentMetadata

An object representing metadata from the end user's uploaded document.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The name of the document. | [optional] 
**status** | **str** | The processing status of the document. | [optional] 
**doc_id** | **str** | An identifier of the document that is also present in the paystub response. | [optional] 
**doc_type** | [**DocType**](DocType.md) |  | [optional] 

## Example

```python
from pyplaid.models.document_metadata import DocumentMetadata

# TODO update the JSON string below
json = "{}"
# create an instance of DocumentMetadata from a JSON string
document_metadata_instance = DocumentMetadata.from_json(json)
# print the JSON string representation of the object
print DocumentMetadata.to_json()

# convert the object into a dict
document_metadata_dict = document_metadata_instance.to_dict()
# create an instance of DocumentMetadata from a dict
document_metadata_form_dict = document_metadata.from_dict(document_metadata_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


