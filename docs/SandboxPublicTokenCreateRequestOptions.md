# SandboxPublicTokenCreateRequestOptions

An optional set of options to be used when configuring the Item. If specified, must not be `null`.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**webhook** | **str** | Specify a webhook to associate with the new Item. | [optional] 
**override_username** | **str** | Test username to use for the creation of the Sandbox Item. Default value is &#x60;user_good&#x60;. | [optional] [default to 'user_good']
**override_password** | **str** | Test password to use for the creation of the Sandbox Item. Default value is &#x60;pass_good&#x60;. | [optional] [default to 'pass_good']
**transactions** | [**SandboxPublicTokenCreateRequestOptionsTransactions**](SandboxPublicTokenCreateRequestOptionsTransactions.md) |  | [optional] 
**income_verification** | [**SandboxPublicTokenCreateRequestOptionsIncomeVerification**](SandboxPublicTokenCreateRequestOptionsIncomeVerification.md) |  | [optional] 

## Example

```python
from pyplaid.models.sandbox_public_token_create_request_options import SandboxPublicTokenCreateRequestOptions

# TODO update the JSON string below
json = "{}"
# create an instance of SandboxPublicTokenCreateRequestOptions from a JSON string
sandbox_public_token_create_request_options_instance = SandboxPublicTokenCreateRequestOptions.from_json(json)
# print the JSON string representation of the object
print SandboxPublicTokenCreateRequestOptions.to_json()

# convert the object into a dict
sandbox_public_token_create_request_options_dict = sandbox_public_token_create_request_options_instance.to_dict()
# create an instance of SandboxPublicTokenCreateRequestOptions from a dict
sandbox_public_token_create_request_options_form_dict = sandbox_public_token_create_request_options.from_dict(sandbox_public_token_create_request_options_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


