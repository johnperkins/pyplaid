# WebhookUpdateAcknowledgedWebhook

Fired when an Item's webhook is updated. This will be sent to the newly specified webhook.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**webhook_type** | **str** | &#x60;ITEM&#x60; | 
**webhook_code** | **str** | &#x60;WEBHOOK_UPDATE_ACKNOWLEDGED&#x60; | 
**item_id** | **str** | The &#x60;item_id&#x60; of the Item associated with this webhook, warning, or error | 
**new_webhook_url** | **str** | The new webhook URL | 
**error** | [**PlaidError**](PlaidError.md) |  | [optional] 
**environment** | [**WebhookEnvironmentValues**](WebhookEnvironmentValues.md) |  | 

## Example

```python
from pyplaid.models.webhook_update_acknowledged_webhook import WebhookUpdateAcknowledgedWebhook

# TODO update the JSON string below
json = "{}"
# create an instance of WebhookUpdateAcknowledgedWebhook from a JSON string
webhook_update_acknowledged_webhook_instance = WebhookUpdateAcknowledgedWebhook.from_json(json)
# print the JSON string representation of the object
print WebhookUpdateAcknowledgedWebhook.to_json()

# convert the object into a dict
webhook_update_acknowledged_webhook_dict = webhook_update_acknowledged_webhook_instance.to_dict()
# create an instance of WebhookUpdateAcknowledgedWebhook from a dict
webhook_update_acknowledged_webhook_form_dict = webhook_update_acknowledged_webhook.from_dict(webhook_update_acknowledged_webhook_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


