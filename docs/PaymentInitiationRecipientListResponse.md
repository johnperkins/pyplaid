# PaymentInitiationRecipientListResponse

PaymentInitiationRecipientListResponse defines the response schema for `/payment_initiation/recipient/list`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**recipients** | [**List[PaymentInitiationRecipient]**](PaymentInitiationRecipient.md) | An array of payment recipients created for Payment Initiation | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.payment_initiation_recipient_list_response import PaymentInitiationRecipientListResponse

# TODO update the JSON string below
json = "{}"
# create an instance of PaymentInitiationRecipientListResponse from a JSON string
payment_initiation_recipient_list_response_instance = PaymentInitiationRecipientListResponse.from_json(json)
# print the JSON string representation of the object
print PaymentInitiationRecipientListResponse.to_json()

# convert the object into a dict
payment_initiation_recipient_list_response_dict = payment_initiation_recipient_list_response_instance.to_dict()
# create an instance of PaymentInitiationRecipientListResponse from a dict
payment_initiation_recipient_list_response_form_dict = payment_initiation_recipient_list_response.from_dict(payment_initiation_recipient_list_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


