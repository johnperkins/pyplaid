# LinkTokenCreateCreditFilter

A filter to apply to `credit`-type accounts

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_subtypes** | [**List[CreditAccountSubtype]**](CreditAccountSubtype.md) | An array of account subtypes to display in Link. If not specified, all account subtypes will be shown. For a full list of valid types and subtypes, see the [Account schema](https://plaid.com/docs/api/accounts#account-type-schema).  | [optional] 

## Example

```python
from pyplaid.models.link_token_create_credit_filter import LinkTokenCreateCreditFilter

# TODO update the JSON string below
json = "{}"
# create an instance of LinkTokenCreateCreditFilter from a JSON string
link_token_create_credit_filter_instance = LinkTokenCreateCreditFilter.from_json(json)
# print the JSON string representation of the object
print LinkTokenCreateCreditFilter.to_json()

# convert the object into a dict
link_token_create_credit_filter_dict = link_token_create_credit_filter_instance.to_dict()
# create an instance of LinkTokenCreateCreditFilter from a dict
link_token_create_credit_filter_form_dict = link_token_create_credit_filter.from_dict(link_token_create_credit_filter_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


