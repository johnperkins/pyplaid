# LinkDeliveryCreateRequest

LinkDeliveryCreateRequest defines the request schema for `/link_delivery/create`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**link_token** | **str** | A &#x60;link_token&#x60; from a previous invocation of &#x60;/link/token/create&#x60; with Link Delivery enabled | 
**delivery_method** | [**LinkDeliveryDeliveryMethod**](LinkDeliveryDeliveryMethod.md) |  | 
**delivery_destination** | **str** | The email or phone number to be used to delivery the URL of the Link Delivery session | 

## Example

```python
from pyplaid.models.link_delivery_create_request import LinkDeliveryCreateRequest

# TODO update the JSON string below
json = "{}"
# create an instance of LinkDeliveryCreateRequest from a JSON string
link_delivery_create_request_instance = LinkDeliveryCreateRequest.from_json(json)
# print the JSON string representation of the object
print LinkDeliveryCreateRequest.to_json()

# convert the object into a dict
link_delivery_create_request_dict = link_delivery_create_request_instance.to_dict()
# create an instance of LinkDeliveryCreateRequest from a dict
link_delivery_create_request_form_dict = link_delivery_create_request.from_dict(link_delivery_create_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


