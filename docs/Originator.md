# Originator

Originator and their status.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Originator’s client ID. | 
**transfer_diligence_status** | [**TransferDiligenceStatus**](TransferDiligenceStatus.md) |  | 

## Example

```python
from pyplaid.models.originator import Originator

# TODO update the JSON string below
json = "{}"
# create an instance of Originator from a JSON string
originator_instance = Originator.from_json(json)
# print the JSON string representation of the object
print Originator.to_json()

# convert the object into a dict
originator_dict = originator_instance.to_dict()
# create an instance of Originator from a dict
originator_form_dict = originator.from_dict(originator_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


