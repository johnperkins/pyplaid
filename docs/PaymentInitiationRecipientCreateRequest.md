# PaymentInitiationRecipientCreateRequest

PaymentInitiationRecipientCreateRequest defines the request schema for `/payment_initiation/recipient/create`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**name** | **str** | The name of the recipient. We recommend using strings of length 18 or less and avoid special characters to ensure compatibility with all institutions. | 
**iban** | **str** | The International Bank Account Number (IBAN) for the recipient. If BACS data is not provided, an IBAN is required. | [optional] 
**bacs** | [**RecipientBACSNullable**](RecipientBACSNullable.md) |  | [optional] 
**address** | [**PaymentInitiationAddress**](PaymentInitiationAddress.md) |  | [optional] 

## Example

```python
from pyplaid.models.payment_initiation_recipient_create_request import PaymentInitiationRecipientCreateRequest

# TODO update the JSON string below
json = "{}"
# create an instance of PaymentInitiationRecipientCreateRequest from a JSON string
payment_initiation_recipient_create_request_instance = PaymentInitiationRecipientCreateRequest.from_json(json)
# print the JSON string representation of the object
print PaymentInitiationRecipientCreateRequest.to_json()

# convert the object into a dict
payment_initiation_recipient_create_request_dict = payment_initiation_recipient_create_request_instance.to_dict()
# create an instance of PaymentInitiationRecipientCreateRequest from a dict
payment_initiation_recipient_create_request_form_dict = payment_initiation_recipient_create_request.from_dict(payment_initiation_recipient_create_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


