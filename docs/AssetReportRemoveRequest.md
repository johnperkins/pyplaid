# AssetReportRemoveRequest

AssetReportRemoveRequest defines the request schema for `/asset_report/remove`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**asset_report_token** | **str** | A token that can be provided to endpoints such as &#x60;/asset_report/get&#x60; or &#x60;/asset_report/pdf/get&#x60; to fetch or update an Asset Report. | 

## Example

```python
from pyplaid.models.asset_report_remove_request import AssetReportRemoveRequest

# TODO update the JSON string below
json = "{}"
# create an instance of AssetReportRemoveRequest from a JSON string
asset_report_remove_request_instance = AssetReportRemoveRequest.from_json(json)
# print the JSON string representation of the object
print AssetReportRemoveRequest.to_json()

# convert the object into a dict
asset_report_remove_request_dict = asset_report_remove_request_instance.to_dict()
# create an instance of AssetReportRemoveRequest from a dict
asset_report_remove_request_form_dict = asset_report_remove_request.from_dict(asset_report_remove_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


