# RemovedTransaction

A representation of a removed transaction

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**transaction_id** | **str** | The ID of the removed transaction. | [optional] 

## Example

```python
from pyplaid.models.removed_transaction import RemovedTransaction

# TODO update the JSON string below
json = "{}"
# create an instance of RemovedTransaction from a JSON string
removed_transaction_instance = RemovedTransaction.from_json(json)
# print the JSON string representation of the object
print RemovedTransaction.to_json()

# convert the object into a dict
removed_transaction_dict = removed_transaction_instance.to_dict()
# create an instance of RemovedTransaction from a dict
removed_transaction_form_dict = removed_transaction.from_dict(removed_transaction_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


