# AssetReportCreateRequestOptions

An optional object to filter `/asset_report/create` results. If provided, must be non-`null`. The optional `user` object is required for the report to be eligible for Fannie Mae's Day 1 Certainty program.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_report_id** | **str** | Client-generated identifier, which can be used by lenders to track loan applications. | [optional] 
**webhook** | **str** | URL to which Plaid will send Assets webhooks, for example when the requested Asset Report is ready. | [optional] 
**include_fast_report** | **bool** | true to return balance and identity earlier as a fast report. Defaults to false if omitted. | [optional] 
**products** | **List[str]** | Additional information that can be included in the asset report. Possible values: &#x60;\&quot;investments\&quot;&#x60; | [optional] 
**user** | [**AssetReportUser**](AssetReportUser.md) |  | [optional] 

## Example

```python
from pyplaid.models.asset_report_create_request_options import AssetReportCreateRequestOptions

# TODO update the JSON string below
json = "{}"
# create an instance of AssetReportCreateRequestOptions from a JSON string
asset_report_create_request_options_instance = AssetReportCreateRequestOptions.from_json(json)
# print the JSON string representation of the object
print AssetReportCreateRequestOptions.to_json()

# convert the object into a dict
asset_report_create_request_options_dict = asset_report_create_request_options_instance.to_dict()
# create an instance of AssetReportCreateRequestOptions from a dict
asset_report_create_request_options_form_dict = asset_report_create_request_options.from_dict(asset_report_create_request_options_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


