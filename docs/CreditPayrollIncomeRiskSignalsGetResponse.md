# CreditPayrollIncomeRiskSignalsGetResponse

CreditPayrollIncomeRiskSignalsGetRequest defines the response schema for `/beta/credit/payroll_income/risk_signals/get`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**List[PayrollRiskSignalsItem]**](PayrollRiskSignalsItem.md) | Array of payroll items. | 
**error** | [**PlaidError**](PlaidError.md) |  | [optional] 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.credit_payroll_income_risk_signals_get_response import CreditPayrollIncomeRiskSignalsGetResponse

# TODO update the JSON string below
json = "{}"
# create an instance of CreditPayrollIncomeRiskSignalsGetResponse from a JSON string
credit_payroll_income_risk_signals_get_response_instance = CreditPayrollIncomeRiskSignalsGetResponse.from_json(json)
# print the JSON string representation of the object
print CreditPayrollIncomeRiskSignalsGetResponse.to_json()

# convert the object into a dict
credit_payroll_income_risk_signals_get_response_dict = credit_payroll_income_risk_signals_get_response_instance.to_dict()
# create an instance of CreditPayrollIncomeRiskSignalsGetResponse from a dict
credit_payroll_income_risk_signals_get_response_form_dict = credit_payroll_income_risk_signals_get_response.from_dict(credit_payroll_income_risk_signals_get_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


