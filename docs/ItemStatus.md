# ItemStatus

An object with information about the status of the Item.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**investments** | [**ItemStatusInvestments**](ItemStatusInvestments.md) |  | [optional] 
**transactions** | [**ItemStatusTransactions**](ItemStatusTransactions.md) |  | [optional] 
**last_webhook** | [**ItemStatusLastWebhook**](ItemStatusLastWebhook.md) |  | [optional] 

## Example

```python
from pyplaid.models.item_status import ItemStatus

# TODO update the JSON string below
json = "{}"
# create an instance of ItemStatus from a JSON string
item_status_instance = ItemStatus.from_json(json)
# print the JSON string representation of the object
print ItemStatus.to_json()

# convert the object into a dict
item_status_dict = item_status_instance.to_dict()
# create an instance of ItemStatus from a dict
item_status_form_dict = item_status.from_dict(item_status_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


