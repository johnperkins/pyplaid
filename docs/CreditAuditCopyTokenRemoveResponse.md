# CreditAuditCopyTokenRemoveResponse

CreditAuditCopyTokenRemoveResponse defines the response schema for `/credit/audit_copy_token/remove`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**removed** | **bool** | &#x60;true&#x60; if the Audit Copy was successfully removed. | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.credit_audit_copy_token_remove_response import CreditAuditCopyTokenRemoveResponse

# TODO update the JSON string below
json = "{}"
# create an instance of CreditAuditCopyTokenRemoveResponse from a JSON string
credit_audit_copy_token_remove_response_instance = CreditAuditCopyTokenRemoveResponse.from_json(json)
# print the JSON string representation of the object
print CreditAuditCopyTokenRemoveResponse.to_json()

# convert the object into a dict
credit_audit_copy_token_remove_response_dict = credit_audit_copy_token_remove_response_instance.to_dict()
# create an instance of CreditAuditCopyTokenRemoveResponse from a dict
credit_audit_copy_token_remove_response_form_dict = credit_audit_copy_token_remove_response.from_dict(credit_audit_copy_token_remove_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


