# TransactionsRefreshRequest

TransactionsRefreshRequest defines the request schema for `/transactions/refresh`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**access_token** | **str** | The access token associated with the Item data is being requested for. | 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 

## Example

```python
from pyplaid.models.transactions_refresh_request import TransactionsRefreshRequest

# TODO update the JSON string below
json = "{}"
# create an instance of TransactionsRefreshRequest from a JSON string
transactions_refresh_request_instance = TransactionsRefreshRequest.from_json(json)
# print the JSON string representation of the object
print TransactionsRefreshRequest.to_json()

# convert the object into a dict
transactions_refresh_request_dict = transactions_refresh_request_instance.to_dict()
# create an instance of TransactionsRefreshRequest from a dict
transactions_refresh_request_form_dict = transactions_refresh_request.from_dict(transactions_refresh_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


