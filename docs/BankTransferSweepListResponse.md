# BankTransferSweepListResponse

BankTransferSweepListResponse defines the response schema for `/bank_transfer/sweep/list`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sweeps** | [**List[BankTransferSweep]**](BankTransferSweep.md) |  | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.bank_transfer_sweep_list_response import BankTransferSweepListResponse

# TODO update the JSON string below
json = "{}"
# create an instance of BankTransferSweepListResponse from a JSON string
bank_transfer_sweep_list_response_instance = BankTransferSweepListResponse.from_json(json)
# print the JSON string representation of the object
print BankTransferSweepListResponse.to_json()

# convert the object into a dict
bank_transfer_sweep_list_response_dict = bank_transfer_sweep_list_response_instance.to_dict()
# create an instance of BankTransferSweepListResponse from a dict
bank_transfer_sweep_list_response_form_dict = bank_transfer_sweep_list_response.from_dict(bank_transfer_sweep_list_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


