# PersonalFinanceCategory

Information describing the intent of the transaction. Most relevant for personal finance use cases, but not limited to such use cases.  See the [`taxonomy csv file`](https://plaid.com/documents/transactions-personal-finance-category-taxonomy.csv) for a full list of personal finance categories.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**primary** | **str** | A high level category that communicates the broad category of the transaction. | 
**detailed** | **str** | A granular category conveying the transaction&#39;s intent. This field can also be used as a unique identifier for the category. | 

## Example

```python
from pyplaid.models.personal_finance_category import PersonalFinanceCategory

# TODO update the JSON string below
json = "{}"
# create an instance of PersonalFinanceCategory from a JSON string
personal_finance_category_instance = PersonalFinanceCategory.from_json(json)
# print the JSON string representation of the object
print PersonalFinanceCategory.to_json()

# convert the object into a dict
personal_finance_category_dict = personal_finance_category_instance.to_dict()
# create an instance of PersonalFinanceCategory from a dict
personal_finance_category_form_dict = personal_finance_category.from_dict(personal_finance_category_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


