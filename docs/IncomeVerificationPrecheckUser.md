# IncomeVerificationPrecheckUser

Information about the user whose eligibility is being evaluated.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**first_name** | **str** | The user&#39;s first name | [optional] 
**last_name** | **str** | The user&#39;s last name | [optional] 
**email_address** | **str** | The user&#39;s email address | [optional] 
**home_address** | [**SignalAddressData**](SignalAddressData.md) |  | [optional] 

## Example

```python
from pyplaid.models.income_verification_precheck_user import IncomeVerificationPrecheckUser

# TODO update the JSON string below
json = "{}"
# create an instance of IncomeVerificationPrecheckUser from a JSON string
income_verification_precheck_user_instance = IncomeVerificationPrecheckUser.from_json(json)
# print the JSON string representation of the object
print IncomeVerificationPrecheckUser.to_json()

# convert the object into a dict
income_verification_precheck_user_dict = income_verification_precheck_user_instance.to_dict()
# create an instance of IncomeVerificationPrecheckUser from a dict
income_verification_precheck_user_form_dict = income_verification_precheck_user.from_dict(income_verification_precheck_user_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


