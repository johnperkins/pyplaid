# SandboxPublicTokenCreateRequest

SandboxPublicTokenCreateRequest defines the request schema for `/sandbox/public_token/create`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**institution_id** | **str** | The ID of the institution the Item will be associated with | 
**initial_products** | [**List[Products]**](Products.md) | The products to initially pull for the Item. May be any products that the specified &#x60;institution_id&#x60;  supports. This array may not be empty. | 
**options** | [**SandboxPublicTokenCreateRequestOptions**](SandboxPublicTokenCreateRequestOptions.md) |  | [optional] 
**user_token** | **str** | The user token associated with the User data is being requested for. | [optional] 

## Example

```python
from pyplaid.models.sandbox_public_token_create_request import SandboxPublicTokenCreateRequest

# TODO update the JSON string below
json = "{}"
# create an instance of SandboxPublicTokenCreateRequest from a JSON string
sandbox_public_token_create_request_instance = SandboxPublicTokenCreateRequest.from_json(json)
# print the JSON string representation of the object
print SandboxPublicTokenCreateRequest.to_json()

# convert the object into a dict
sandbox_public_token_create_request_dict = sandbox_public_token_create_request_instance.to_dict()
# create an instance of SandboxPublicTokenCreateRequest from a dict
sandbox_public_token_create_request_form_dict = sandbox_public_token_create_request.from_dict(sandbox_public_token_create_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


