# EntityWatchlistSearchTerms

Search inputs for creating an entity watchlist screening

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**entity_watchlist_program_id** | **str** | ID of the associated entity program. | 
**legal_name** | **str** | The name of the organization being screened. | 
**document_number** | **str** | The numeric or alphanumeric identifier associated with this document. | [optional] 
**email_address** | **str** | A valid email address. | [optional] 
**country** | **str** | Valid, capitalized, two-letter ISO code representing the country of this object. Must be in ISO 3166-1 alpha-2 form. | [optional] 
**phone_number** | **str** | A phone number in E.164 format. | [optional] 
**url** | **str** | An &#39;http&#39; or &#39;https&#39; URL (must begin with either of those). | [optional] 

## Example

```python
from pyplaid.models.entity_watchlist_search_terms import EntityWatchlistSearchTerms

# TODO update the JSON string below
json = "{}"
# create an instance of EntityWatchlistSearchTerms from a JSON string
entity_watchlist_search_terms_instance = EntityWatchlistSearchTerms.from_json(json)
# print the JSON string representation of the object
print EntityWatchlistSearchTerms.to_json()

# convert the object into a dict
entity_watchlist_search_terms_dict = entity_watchlist_search_terms_instance.to_dict()
# create an instance of EntityWatchlistSearchTerms from a dict
entity_watchlist_search_terms_form_dict = entity_watchlist_search_terms.from_dict(entity_watchlist_search_terms_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


