# WatchlistScreeningHit

Data from a government watchlist or PEP list that has been attached to the screening.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | ID of the associated screening hit. | 
**review_status** | [**WatchlistScreeningHitStatus**](WatchlistScreeningHitStatus.md) |  | 
**first_active** | **datetime** | An ISO8601 formatted timestamp. | 
**inactive_since** | **datetime** | An ISO8601 formatted timestamp. | 
**historical_since** | **datetime** | An ISO8601 formatted timestamp. | 
**list_code** | [**IndividualWatchlistCode**](IndividualWatchlistCode.md) |  | 
**plaid_uid** | **str** | A universal identifier for a watchlist individual that is stable across searches and updates. | 
**source_uid** | **str** | The identifier provided by the source sanction or watchlist. When one is not provided by the source, this is &#x60;null&#x60;. | 
**analysis** | [**ScreeningHitAnalysis**](ScreeningHitAnalysis.md) |  | [optional] 
**data** | [**ScreeningHitData**](ScreeningHitData.md) |  | [optional] 

## Example

```python
from pyplaid.models.watchlist_screening_hit import WatchlistScreeningHit

# TODO update the JSON string below
json = "{}"
# create an instance of WatchlistScreeningHit from a JSON string
watchlist_screening_hit_instance = WatchlistScreeningHit.from_json(json)
# print the JSON string representation of the object
print WatchlistScreeningHit.to_json()

# convert the object into a dict
watchlist_screening_hit_dict = watchlist_screening_hit_instance.to_dict()
# create an instance of WatchlistScreeningHit from a dict
watchlist_screening_hit_form_dict = watchlist_screening_hit.from_dict(watchlist_screening_hit_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


