# SandboxTransferSimulateRequest

Defines the request schema for `/sandbox/transfer/simulate`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**transfer_id** | **str** | Plaid’s unique identifier for a transfer. | 
**event_type** | **str** | The asynchronous event to be simulated. May be: &#x60;posted&#x60;, &#x60;settled&#x60;, &#x60;failed&#x60;, or &#x60;returned&#x60;.  An error will be returned if the event type is incompatible with the current transfer status. Compatible status --&gt; event type transitions include:  &#x60;pending&#x60; --&gt; &#x60;failed&#x60;  &#x60;pending&#x60; --&gt; &#x60;posted&#x60;  &#x60;posted&#x60; --&gt; &#x60;returned&#x60;  &#x60;posted&#x60; --&gt; &#x60;settled&#x60;  | 
**failure_reason** | [**TransferFailure**](TransferFailure.md) |  | [optional] 

## Example

```python
from pyplaid.models.sandbox_transfer_simulate_request import SandboxTransferSimulateRequest

# TODO update the JSON string below
json = "{}"
# create an instance of SandboxTransferSimulateRequest from a JSON string
sandbox_transfer_simulate_request_instance = SandboxTransferSimulateRequest.from_json(json)
# print the JSON string representation of the object
print SandboxTransferSimulateRequest.to_json()

# convert the object into a dict
sandbox_transfer_simulate_request_dict = sandbox_transfer_simulate_request_instance.to_dict()
# create an instance of SandboxTransferSimulateRequest from a dict
sandbox_transfer_simulate_request_form_dict = sandbox_transfer_simulate_request.from_dict(sandbox_transfer_simulate_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


