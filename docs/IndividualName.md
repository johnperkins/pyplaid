# IndividualName

Parent container for name that allows for choice group between parsed and unparsed containers.Parent container for name that allows for choice group between parsed and unparsed containers.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**first_name** | **str** | The first name of the individual represented by the parent object. | 
**last_name** | **str** | The last name of the individual represented by the parent object. | 

## Example

```python
from pyplaid.models.individual_name import IndividualName

# TODO update the JSON string below
json = "{}"
# create an instance of IndividualName from a JSON string
individual_name_instance = IndividualName.from_json(json)
# print the JSON string representation of the object
print IndividualName.to_json()

# convert the object into a dict
individual_name_dict = individual_name_instance.to_dict()
# create an instance of IndividualName from a dict
individual_name_form_dict = individual_name.from_dict(individual_name_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


