# SandboxTransferSweepSimulateResponse

Defines the response schema for `/sandbox/transfer/sweep/simulate`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sweep** | [**SimulatedTransferSweep**](SimulatedTransferSweep.md) |  | [optional] 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.sandbox_transfer_sweep_simulate_response import SandboxTransferSweepSimulateResponse

# TODO update the JSON string below
json = "{}"
# create an instance of SandboxTransferSweepSimulateResponse from a JSON string
sandbox_transfer_sweep_simulate_response_instance = SandboxTransferSweepSimulateResponse.from_json(json)
# print the JSON string representation of the object
print SandboxTransferSweepSimulateResponse.to_json()

# convert the object into a dict
sandbox_transfer_sweep_simulate_response_dict = sandbox_transfer_sweep_simulate_response_instance.to_dict()
# create an instance of SandboxTransferSweepSimulateResponse from a dict
sandbox_transfer_sweep_simulate_response_form_dict = sandbox_transfer_sweep_simulate_response.from_dict(sandbox_transfer_sweep_simulate_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


