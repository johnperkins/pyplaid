# BankTransferEventSyncResponse

Defines the response schema for `/bank_transfer/event/sync`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bank_transfer_events** | [**List[BankTransferEvent]**](BankTransferEvent.md) |  | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.bank_transfer_event_sync_response import BankTransferEventSyncResponse

# TODO update the JSON string below
json = "{}"
# create an instance of BankTransferEventSyncResponse from a JSON string
bank_transfer_event_sync_response_instance = BankTransferEventSyncResponse.from_json(json)
# print the JSON string representation of the object
print BankTransferEventSyncResponse.to_json()

# convert the object into a dict
bank_transfer_event_sync_response_dict = bank_transfer_event_sync_response_instance.to_dict()
# create an instance of BankTransferEventSyncResponse from a dict
bank_transfer_event_sync_response_form_dict = bank_transfer_event_sync_response.from_dict(bank_transfer_event_sync_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


