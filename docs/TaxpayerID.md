# TaxpayerID

Taxpayer ID of the individual receiving the paystub.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id_type** | **str** | Type of ID, e.g. &#39;SSN&#39; | [optional] 
**id_mask** | **str** | ID mask; i.e. last 4 digits of the taxpayer ID | [optional] 
**last_4_digits** | **str** | Last 4 digits of unique number of ID. | [optional] 

## Example

```python
from pyplaid.models.taxpayer_id import TaxpayerID

# TODO update the JSON string below
json = "{}"
# create an instance of TaxpayerID from a JSON string
taxpayer_id_instance = TaxpayerID.from_json(json)
# print the JSON string representation of the object
print TaxpayerID.to_json()

# convert the object into a dict
taxpayer_id_dict = taxpayer_id_instance.to_dict()
# create an instance of TaxpayerID from a dict
taxpayer_id_form_dict = taxpayer_id.from_dict(taxpayer_id_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


