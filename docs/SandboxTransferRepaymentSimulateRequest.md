# SandboxTransferRepaymentSimulateRequest

Defines the request schema for `/sandbox/transfer/repayment/simulate`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 

## Example

```python
from pyplaid.models.sandbox_transfer_repayment_simulate_request import SandboxTransferRepaymentSimulateRequest

# TODO update the JSON string below
json = "{}"
# create an instance of SandboxTransferRepaymentSimulateRequest from a JSON string
sandbox_transfer_repayment_simulate_request_instance = SandboxTransferRepaymentSimulateRequest.from_json(json)
# print the JSON string representation of the object
print SandboxTransferRepaymentSimulateRequest.to_json()

# convert the object into a dict
sandbox_transfer_repayment_simulate_request_dict = sandbox_transfer_repayment_simulate_request_instance.to_dict()
# create an instance of SandboxTransferRepaymentSimulateRequest from a dict
sandbox_transfer_repayment_simulate_request_form_dict = sandbox_transfer_repayment_simulate_request.from_dict(sandbox_transfer_repayment_simulate_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


