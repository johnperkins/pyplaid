# Parties

A collection of objects that define specific parties to a deal. This includes the direct participating parties, such as borrower and seller and the indirect parties such as the credit report provider.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**party** | [**List[Party]**](Party.md) |  | 

## Example

```python
from pyplaid.models.parties import Parties

# TODO update the JSON string below
json = "{}"
# create an instance of Parties from a JSON string
parties_instance = Parties.from_json(json)
# print the JSON string representation of the object
print Parties.to_json()

# convert the object into a dict
parties_dict = parties_instance.to_dict()
# create an instance of Parties from a dict
parties_form_dict = parties.from_dict(parties_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


