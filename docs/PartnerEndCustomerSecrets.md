# PartnerEndCustomerSecrets

The secrets for the newly created end customer in non-Production environments.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sandbox** | **str** | The end customer&#39;s secret key for the Sandbox environment. | [optional] 
**development** | **str** | The end customer&#39;s secret key for the Development environment. | [optional] 

## Example

```python
from pyplaid.models.partner_end_customer_secrets import PartnerEndCustomerSecrets

# TODO update the JSON string below
json = "{}"
# create an instance of PartnerEndCustomerSecrets from a JSON string
partner_end_customer_secrets_instance = PartnerEndCustomerSecrets.from_json(json)
# print the JSON string representation of the object
print PartnerEndCustomerSecrets.to_json()

# convert the object into a dict
partner_end_customer_secrets_dict = partner_end_customer_secrets_instance.to_dict()
# create an instance of PartnerEndCustomerSecrets from a dict
partner_end_customer_secrets_form_dict = partner_end_customer_secrets.from_dict(partner_end_customer_secrets_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


