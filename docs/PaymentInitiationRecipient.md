# PaymentInitiationRecipient

PaymentInitiationRecipient defines a payment initiation recipient

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**recipient_id** | **str** | The ID of the recipient. | 
**name** | **str** | The name of the recipient. | 
**address** | [**PaymentInitiationAddress**](PaymentInitiationAddress.md) |  | [optional] 
**iban** | **str** | The International Bank Account Number (IBAN) for the recipient. | [optional] 
**bacs** | [**RecipientBACSNullable**](RecipientBACSNullable.md) |  | [optional] 

## Example

```python
from pyplaid.models.payment_initiation_recipient import PaymentInitiationRecipient

# TODO update the JSON string below
json = "{}"
# create an instance of PaymentInitiationRecipient from a JSON string
payment_initiation_recipient_instance = PaymentInitiationRecipient.from_json(json)
# print the JSON string representation of the object
print PaymentInitiationRecipient.to_json()

# convert the object into a dict
payment_initiation_recipient_dict = payment_initiation_recipient_instance.to_dict()
# create an instance of PaymentInitiationRecipient from a dict
payment_initiation_recipient_form_dict = payment_initiation_recipient.from_dict(payment_initiation_recipient_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


