# ItemApplicationScopesUpdateRequest

ItemApplicationScopesUpdateRequest defines the request schema for `/item/application/scopes/update`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**access_token** | **str** | The access token associated with the Item data is being requested for. | 
**application_id** | **str** | This field will map to the application ID that is returned from /item/applications/list, or provided to the institution in an oauth redirect. | 
**scopes** | [**Scopes**](Scopes.md) |  | 
**state** | **str** | When scopes are updated during enrollment, this field must be populated with the state sent to the partner in the OAuth Login URI. This field is required when the context is &#x60;ENROLLMENT&#x60;. | [optional] 
**context** | [**ScopesContext**](ScopesContext.md) |  | 

## Example

```python
from pyplaid.models.item_application_scopes_update_request import ItemApplicationScopesUpdateRequest

# TODO update the JSON string below
json = "{}"
# create an instance of ItemApplicationScopesUpdateRequest from a JSON string
item_application_scopes_update_request_instance = ItemApplicationScopesUpdateRequest.from_json(json)
# print the JSON string representation of the object
print ItemApplicationScopesUpdateRequest.to_json()

# convert the object into a dict
item_application_scopes_update_request_dict = item_application_scopes_update_request_instance.to_dict()
# create an instance of ItemApplicationScopesUpdateRequest from a dict
item_application_scopes_update_request_form_dict = item_application_scopes_update_request.from_dict(item_application_scopes_update_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


