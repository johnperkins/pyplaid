# AssetReportFreddieGetRequest

AssetReportFreddieGetResponse defines the request schema for `credit/asset_report/freddie_mac/get`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**audit_copy_token** | **str** | A token that can be shared with a third party auditor to allow them to obtain access to the Asset Report. This token should be stored securely. | 
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 

## Example

```python
from pyplaid.models.asset_report_freddie_get_request import AssetReportFreddieGetRequest

# TODO update the JSON string below
json = "{}"
# create an instance of AssetReportFreddieGetRequest from a JSON string
asset_report_freddie_get_request_instance = AssetReportFreddieGetRequest.from_json(json)
# print the JSON string representation of the object
print AssetReportFreddieGetRequest.to_json()

# convert the object into a dict
asset_report_freddie_get_request_dict = asset_report_freddie_get_request_instance.to_dict()
# create an instance of AssetReportFreddieGetRequest from a dict
asset_report_freddie_get_request_form_dict = asset_report_freddie_get_request.from_dict(asset_report_freddie_get_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


