# TransferRepaymentReturn

Represents a return on a Guaranteed ACH transfer that is included in the specified repayment.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**transfer_id** | **str** | The unique identifier of the guaranteed transfer that was returned. | 
**event_id** | **int** | The unique identifier of the corresponding &#x60;returned&#x60; transfer event. | 
**amount** | **str** | The value of the returned transfer. | 
**iso_currency_code** | **str** | The currency of the repayment, e.g. \&quot;USD\&quot;. | 

## Example

```python
from pyplaid.models.transfer_repayment_return import TransferRepaymentReturn

# TODO update the JSON string below
json = "{}"
# create an instance of TransferRepaymentReturn from a JSON string
transfer_repayment_return_instance = TransferRepaymentReturn.from_json(json)
# print the JSON string representation of the object
print TransferRepaymentReturn.to_json()

# convert the object into a dict
transfer_repayment_return_dict = transfer_repayment_return_instance.to_dict()
# create an instance of TransferRepaymentReturn from a dict
transfer_repayment_return_form_dict = transfer_repayment_return.from_dict(transfer_repayment_return_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


