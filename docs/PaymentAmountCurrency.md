# PaymentAmountCurrency

The ISO-4217 currency code of the payment. For standing orders and payment consents, `\"GBP\"` must be used.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


