# IncomeVerificationDocumentsDownloadRequest

IncomeVerificationDocumentsDownloadRequest defines the request schema for `/income/verification/documents/download`.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**income_verification_id** | **str** | The ID of the verification. | [optional] 
**access_token** | **str** | The access token associated with the Item data is being requested for. | [optional] 
**document_id** | **str** | The document ID to download. If passed, a single document will be returned in the resulting zip file, rather than all document | [optional] 

## Example

```python
from pyplaid.models.income_verification_documents_download_request import IncomeVerificationDocumentsDownloadRequest

# TODO update the JSON string below
json = "{}"
# create an instance of IncomeVerificationDocumentsDownloadRequest from a JSON string
income_verification_documents_download_request_instance = IncomeVerificationDocumentsDownloadRequest.from_json(json)
# print the JSON string representation of the object
print IncomeVerificationDocumentsDownloadRequest.to_json()

# convert the object into a dict
income_verification_documents_download_request_dict = income_verification_documents_download_request_instance.to_dict()
# create an instance of IncomeVerificationDocumentsDownloadRequest from a dict
income_verification_documents_download_request_form_dict = income_verification_documents_download_request.from_dict(income_verification_documents_download_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


