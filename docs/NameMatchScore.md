# NameMatchScore

Score found by matching name provided by the API with the name on the account at the financial institution. If the account contains multiple owners, the maximum match score is filled.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**score** | **int** | Represents the match score for name. 100 is a perfect score, 85-99 means a strong match, 50-84 is a partial match, less than 50 is a weak match and 0 is a complete mismatch. If the name is missing from either the API or financial institution, this is empty. | [optional] 
**is_first_name_or_last_name_match** | **bool** | first or last name completely matched | [optional] 
**is_nickname_match** | **bool** | nickname matched, example Jennifer and Jenn. | [optional] 
**is_business_name_detected** | **bool** | If the name on either of the names that was matched for the score was a business name, with corp, llc, ltd etc in the name. While this being true confirms business name, false means it was either not a business name or Plaid could not detect it as such, since a lot of business names match owner names and are hard to detect. | [optional] 

## Example

```python
from pyplaid.models.name_match_score import NameMatchScore

# TODO update the JSON string below
json = "{}"
# create an instance of NameMatchScore from a JSON string
name_match_score_instance = NameMatchScore.from_json(json)
# print the JSON string representation of the object
print NameMatchScore.to_json()

# convert the object into a dict
name_match_score_dict = name_match_score_instance.to_dict()
# create an instance of NameMatchScore from a dict
name_match_score_form_dict = name_match_score.from_dict(name_match_score_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


