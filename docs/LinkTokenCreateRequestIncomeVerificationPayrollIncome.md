# LinkTokenCreateRequestIncomeVerificationPayrollIncome

Specifies options for initializing Link for use with Payroll Income. This field is required if `income_verification` is included in the `products` array and `payroll` is specified in `income_source_types`.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**flow_types** | [**List[IncomeVerificationPayrollFlowType]**](IncomeVerificationPayrollFlowType.md) | The types of payroll income verification to enable for the link session. If none are specified, then users will see both document and digital payroll income. | [optional] 
**is_update_mode** | **bool** | An identifier to indicate whether the income verification link token will be used for an update or not | [optional] 

## Example

```python
from pyplaid.models.link_token_create_request_income_verification_payroll_income import LinkTokenCreateRequestIncomeVerificationPayrollIncome

# TODO update the JSON string below
json = "{}"
# create an instance of LinkTokenCreateRequestIncomeVerificationPayrollIncome from a JSON string
link_token_create_request_income_verification_payroll_income_instance = LinkTokenCreateRequestIncomeVerificationPayrollIncome.from_json(json)
# print the JSON string representation of the object
print LinkTokenCreateRequestIncomeVerificationPayrollIncome.to_json()

# convert the object into a dict
link_token_create_request_income_verification_payroll_income_dict = link_token_create_request_income_verification_payroll_income_instance.to_dict()
# create an instance of LinkTokenCreateRequestIncomeVerificationPayrollIncome from a dict
link_token_create_request_income_verification_payroll_income_form_dict = link_token_create_request_income_verification_payroll_income.from_dict(link_token_create_request_income_verification_payroll_income_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


