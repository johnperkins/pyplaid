# EntityScreeningHitNames

Name information for the associated entity watchlist hit

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**full** | **str** | The full name of the entity. | 
**is_primary** | **bool** | Primary names are those most commonly used to refer to this entity. Only one name will ever be marked as primary. | 
**weak_alias_determination** | [**WeakAliasDetermination**](WeakAliasDetermination.md) |  | 

## Example

```python
from pyplaid.models.entity_screening_hit_names import EntityScreeningHitNames

# TODO update the JSON string below
json = "{}"
# create an instance of EntityScreeningHitNames from a JSON string
entity_screening_hit_names_instance = EntityScreeningHitNames.from_json(json)
# print the JSON string representation of the object
print EntityScreeningHitNames.to_json()

# convert the object into a dict
entity_screening_hit_names_dict = entity_screening_hit_names_instance.to_dict()
# create an instance of EntityScreeningHitNames from a dict
entity_screening_hit_names_form_dict = entity_screening_hit_names.from_dict(entity_screening_hit_names_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


