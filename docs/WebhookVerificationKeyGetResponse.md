# WebhookVerificationKeyGetResponse

WebhookVerificationKeyGetResponse defines the response schema for `/webhook_verification_key/get`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | [**JWKPublicKey**](JWKPublicKey.md) |  | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.webhook_verification_key_get_response import WebhookVerificationKeyGetResponse

# TODO update the JSON string below
json = "{}"
# create an instance of WebhookVerificationKeyGetResponse from a JSON string
webhook_verification_key_get_response_instance = WebhookVerificationKeyGetResponse.from_json(json)
# print the JSON string representation of the object
print WebhookVerificationKeyGetResponse.to_json()

# convert the object into a dict
webhook_verification_key_get_response_dict = webhook_verification_key_get_response_instance.to_dict()
# create an instance of WebhookVerificationKeyGetResponse from a dict
webhook_verification_key_get_response_form_dict = webhook_verification_key_get_response.from_dict(webhook_verification_key_get_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


