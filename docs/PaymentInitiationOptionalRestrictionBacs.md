# PaymentInitiationOptionalRestrictionBacs

An optional object used to restrict the accounts used for payments. If provided, the end user will be able to send payments only from the specified bank account.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account** | **str** | The account number of the account. Maximum of 10 characters. | [optional] 
**sort_code** | **str** | The 6-character sort code of the account. | [optional] 

## Example

```python
from pyplaid.models.payment_initiation_optional_restriction_bacs import PaymentInitiationOptionalRestrictionBacs

# TODO update the JSON string below
json = "{}"
# create an instance of PaymentInitiationOptionalRestrictionBacs from a JSON string
payment_initiation_optional_restriction_bacs_instance = PaymentInitiationOptionalRestrictionBacs.from_json(json)
# print the JSON string representation of the object
print PaymentInitiationOptionalRestrictionBacs.to_json()

# convert the object into a dict
payment_initiation_optional_restriction_bacs_dict = payment_initiation_optional_restriction_bacs_instance.to_dict()
# create an instance of PaymentInitiationOptionalRestrictionBacs from a dict
payment_initiation_optional_restriction_bacs_form_dict = payment_initiation_optional_restriction_bacs.from_dict(payment_initiation_optional_restriction_bacs_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


