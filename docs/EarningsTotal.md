# EarningsTotal

An object representing both the current pay period and year to date amount for an earning category.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**current_amount** | **float** | Total amount of the earnings for this pay period | [optional] 
**current_pay** | [**Pay**](Pay.md) |  | [optional] 
**ytd_pay** | [**Pay**](Pay.md) |  | [optional] 
**hours** | **float** | Total number of hours worked for this pay period | [optional] 
**iso_currency_code** | **str** | The ISO-4217 currency code of the line item. Always &#x60;null&#x60; if &#x60;unofficial_currency_code&#x60; is non-null. | [optional] 
**unofficial_currency_code** | **str** | The unofficial currency code associated with the security. Always &#x60;null&#x60; if &#x60;iso_currency_code&#x60; is non-&#x60;null&#x60;. Unofficial currency codes are used for currencies that do not have official ISO currency codes, such as cryptocurrencies and the currencies of certain countries.  See the [currency code schema](https://plaid.com/docs/api/accounts#currency-code-schema) for a full listing of supported &#x60;iso_currency_code&#x60;s. | [optional] 
**ytd_amount** | **float** | The total year-to-date amount of the earnings | [optional] 

## Example

```python
from pyplaid.models.earnings_total import EarningsTotal

# TODO update the JSON string below
json = "{}"
# create an instance of EarningsTotal from a JSON string
earnings_total_instance = EarningsTotal.from_json(json)
# print the JSON string representation of the object
print EarningsTotal.to_json()

# convert the object into a dict
earnings_total_dict = earnings_total_instance.to_dict()
# create an instance of EarningsTotal from a dict
earnings_total_form_dict = earnings_total.from_dict(earnings_total_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


