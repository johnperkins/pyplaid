# InstitutionsGetResponse

InstitutionsGetResponse defines the response schema for `/institutions/get`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**institutions** | [**List[Institution]**](Institution.md) | A list of Plaid institutions | 
**total** | **int** | The total number of institutions available via this endpoint | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.institutions_get_response import InstitutionsGetResponse

# TODO update the JSON string below
json = "{}"
# create an instance of InstitutionsGetResponse from a JSON string
institutions_get_response_instance = InstitutionsGetResponse.from_json(json)
# print the JSON string representation of the object
print InstitutionsGetResponse.to_json()

# convert the object into a dict
institutions_get_response_dict = institutions_get_response_instance.to_dict()
# create an instance of InstitutionsGetResponse from a dict
institutions_get_response_form_dict = institutions_get_response.from_dict(institutions_get_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


