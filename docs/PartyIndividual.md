# PartyIndividual

No documentation available

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | [**IndividualName**](IndividualName.md) |  | 

## Example

```python
from pyplaid.models.party_individual import PartyIndividual

# TODO update the JSON string below
json = "{}"
# create an instance of PartyIndividual from a JSON string
party_individual_instance = PartyIndividual.from_json(json)
# print the JSON string representation of the object
print PartyIndividual.to_json()

# convert the object into a dict
party_individual_dict = party_individual_instance.to_dict()
# create an instance of PartyIndividual from a dict
party_individual_form_dict = party_individual.from_dict(party_individual_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


