# CreditPayrollIncomePrecheckRequest

Defines the request schema for `/credit/payroll_income/precheck`.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**user_token** | **str** | The user token associated with the User data is being requested for. | [optional] 
**access_tokens** | **List[str]** | An array of access tokens corresponding to Items belonging to the user whose eligibility is being checked. Note that if the Items specified here are not already initialized with &#x60;transactions&#x60;, providing them in this field will cause these Items to be initialized with (and billed for) the Transactions product. | [optional] 
**employer** | [**IncomeVerificationPrecheckEmployer**](IncomeVerificationPrecheckEmployer.md) |  | [optional] 
**us_military_info** | [**IncomeVerificationPrecheckMilitaryInfo**](IncomeVerificationPrecheckMilitaryInfo.md) |  | [optional] 
**payroll_institution** | [**IncomeVerificationPrecheckPayrollInstitution**](IncomeVerificationPrecheckPayrollInstitution.md) |  | [optional] 

## Example

```python
from pyplaid.models.credit_payroll_income_precheck_request import CreditPayrollIncomePrecheckRequest

# TODO update the JSON string below
json = "{}"
# create an instance of CreditPayrollIncomePrecheckRequest from a JSON string
credit_payroll_income_precheck_request_instance = CreditPayrollIncomePrecheckRequest.from_json(json)
# print the JSON string representation of the object
print CreditPayrollIncomePrecheckRequest.to_json()

# convert the object into a dict
credit_payroll_income_precheck_request_dict = credit_payroll_income_precheck_request_instance.to_dict()
# create an instance of CreditPayrollIncomePrecheckRequest from a dict
credit_payroll_income_precheck_request_form_dict = credit_payroll_income_precheck_request.from_dict(credit_payroll_income_precheck_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


