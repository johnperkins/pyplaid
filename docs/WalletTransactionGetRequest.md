# WalletTransactionGetRequest

WalletTransactionGetRequest defines the request schema for `/wallet/transaction/get`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**transaction_id** | **str** | The ID of the transaction to fetch | 

## Example

```python
from pyplaid.models.wallet_transaction_get_request import WalletTransactionGetRequest

# TODO update the JSON string below
json = "{}"
# create an instance of WalletTransactionGetRequest from a JSON string
wallet_transaction_get_request_instance = WalletTransactionGetRequest.from_json(json)
# print the JSON string representation of the object
print WalletTransactionGetRequest.to_json()

# convert the object into a dict
wallet_transaction_get_request_dict = wallet_transaction_get_request_instance.to_dict()
# create an instance of WalletTransactionGetRequest from a dict
wallet_transaction_get_request_form_dict = wallet_transaction_get_request.from_dict(wallet_transaction_get_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


