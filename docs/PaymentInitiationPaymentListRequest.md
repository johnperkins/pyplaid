# PaymentInitiationPaymentListRequest

PaymentInitiationPaymentListRequest defines the request schema for `/payment_initiation/payment/list`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**count** | **int** | The maximum number of payments to return. If &#x60;count&#x60; is not specified, a maximum of 10 payments will be returned, beginning with the most recent payment before the cursor (if specified). | [optional] [default to 10]
**cursor** | **datetime** | A string in RFC 3339 format (i.e. \&quot;2019-12-06T22:35:49Z\&quot;). Only payments created before the cursor will be returned. | [optional] 
**consent_id** | **str** | The consent ID. If specified, only payments, executed using this consent, will be returned. | [optional] 

## Example

```python
from pyplaid.models.payment_initiation_payment_list_request import PaymentInitiationPaymentListRequest

# TODO update the JSON string below
json = "{}"
# create an instance of PaymentInitiationPaymentListRequest from a JSON string
payment_initiation_payment_list_request_instance = PaymentInitiationPaymentListRequest.from_json(json)
# print the JSON string representation of the object
print PaymentInitiationPaymentListRequest.to_json()

# convert the object into a dict
payment_initiation_payment_list_request_dict = payment_initiation_payment_list_request_instance.to_dict()
# create an instance of PaymentInitiationPaymentListRequest from a dict
payment_initiation_payment_list_request_form_dict = payment_initiation_payment_list_request.from_dict(payment_initiation_payment_list_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


