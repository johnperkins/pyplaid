# DepositSwitchAltCreateRequest

DepositSwitchAltCreateRequest defines the request schema for `/deposit_switch/alt/create`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**target_account** | [**DepositSwitchTargetAccount**](DepositSwitchTargetAccount.md) |  | 
**target_user** | [**DepositSwitchTargetUser**](DepositSwitchTargetUser.md) |  | 
**options** | [**DepositSwitchCreateRequestOptions**](DepositSwitchCreateRequestOptions.md) |  | [optional] 
**country_code** | **str** | ISO-3166-1 alpha-2 country code standard. | [optional] 

## Example

```python
from pyplaid.models.deposit_switch_alt_create_request import DepositSwitchAltCreateRequest

# TODO update the JSON string below
json = "{}"
# create an instance of DepositSwitchAltCreateRequest from a JSON string
deposit_switch_alt_create_request_instance = DepositSwitchAltCreateRequest.from_json(json)
# print the JSON string representation of the object
print DepositSwitchAltCreateRequest.to_json()

# convert the object into a dict
deposit_switch_alt_create_request_dict = deposit_switch_alt_create_request_instance.to_dict()
# create an instance of DepositSwitchAltCreateRequest from a dict
deposit_switch_alt_create_request_form_dict = deposit_switch_alt_create_request.from_dict(deposit_switch_alt_create_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


