# EntityWatchlistScreeningSearchTerms

Search terms associated with an entity used for searching against watchlists

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**entity_watchlist_program_id** | **str** | ID of the associated entity program. | 
**legal_name** | **str** | The name of the organization being screened. | 
**document_number** | **str** | The numeric or alphanumeric identifier associated with this document. | 
**email_address** | **str** | A valid email address. | 
**country** | **str** | Valid, capitalized, two-letter ISO code representing the country of this object. Must be in ISO 3166-1 alpha-2 form. | 
**phone_number** | **str** | A phone number in E.164 format. | 
**url** | **str** | An &#39;http&#39; or &#39;https&#39; URL (must begin with either of those). | 
**version** | **float** | The current version of the search terms. Starts at &#x60;1&#x60; and increments with each edit to &#x60;search_terms&#x60;. | 

## Example

```python
from pyplaid.models.entity_watchlist_screening_search_terms import EntityWatchlistScreeningSearchTerms

# TODO update the JSON string below
json = "{}"
# create an instance of EntityWatchlistScreeningSearchTerms from a JSON string
entity_watchlist_screening_search_terms_instance = EntityWatchlistScreeningSearchTerms.from_json(json)
# print the JSON string representation of the object
print EntityWatchlistScreeningSearchTerms.to_json()

# convert the object into a dict
entity_watchlist_screening_search_terms_dict = entity_watchlist_screening_search_terms_instance.to_dict()
# create an instance of EntityWatchlistScreeningSearchTerms from a dict
entity_watchlist_screening_search_terms_form_dict = entity_watchlist_screening_search_terms.from_dict(entity_watchlist_screening_search_terms_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


