# CreditSessionError

The details of a Link error.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**error_type** | **str** | A broad categorization of the error. | [optional] 
**error_code** | **str** | The particular error code. | [optional] 
**error_message** | **str** | A developer-friendly representation of the error code. | [optional] 
**display_message** | **str** | A user-friendly representation of the error code. &#x60;null&#x60; if the error is not related to user action. | [optional] 

## Example

```python
from pyplaid.models.credit_session_error import CreditSessionError

# TODO update the JSON string below
json = "{}"
# create an instance of CreditSessionError from a JSON string
credit_session_error_instance = CreditSessionError.from_json(json)
# print the JSON string representation of the object
print CreditSessionError.to_json()

# convert the object into a dict
credit_session_error_dict = credit_session_error_instance.to_dict()
# create an instance of CreditSessionError from a dict
credit_session_error_form_dict = credit_session_error.from_dict(credit_session_error_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


