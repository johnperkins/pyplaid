# Credit1099Payer

An object representing a payer used by 1099-MISC tax documents.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address** | [**CreditPayStubAddress**](CreditPayStubAddress.md) |  | [optional] 
**name** | **str** | Name of payer. | [optional] 
**tin** | **str** | Tax identification number of payer. | [optional] 
**telephone_number** | **str** | Telephone number of payer. | [optional] 

## Example

```python
from pyplaid.models.credit1099_payer import Credit1099Payer

# TODO update the JSON string below
json = "{}"
# create an instance of Credit1099Payer from a JSON string
credit1099_payer_instance = Credit1099Payer.from_json(json)
# print the JSON string representation of the object
print Credit1099Payer.to_json()

# convert the object into a dict
credit1099_payer_dict = credit1099_payer_instance.to_dict()
# create an instance of Credit1099Payer from a dict
credit1099_payer_form_dict = credit1099_payer.from_dict(credit1099_payer_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


