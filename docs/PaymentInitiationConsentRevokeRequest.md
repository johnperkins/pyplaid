# PaymentInitiationConsentRevokeRequest

PaymentInitiationConsentRevokeRequest defines the request schema for `/payment_initiation/consent/revoke`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**consent_id** | **str** | The consent ID. | 

## Example

```python
from pyplaid.models.payment_initiation_consent_revoke_request import PaymentInitiationConsentRevokeRequest

# TODO update the JSON string below
json = "{}"
# create an instance of PaymentInitiationConsentRevokeRequest from a JSON string
payment_initiation_consent_revoke_request_instance = PaymentInitiationConsentRevokeRequest.from_json(json)
# print the JSON string representation of the object
print PaymentInitiationConsentRevokeRequest.to_json()

# convert the object into a dict
payment_initiation_consent_revoke_request_dict = payment_initiation_consent_revoke_request_instance.to_dict()
# create an instance of PaymentInitiationConsentRevokeRequest from a dict
payment_initiation_consent_revoke_request_form_dict = payment_initiation_consent_revoke_request.from_dict(payment_initiation_consent_revoke_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


