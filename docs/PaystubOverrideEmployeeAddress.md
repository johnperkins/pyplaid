# PaystubOverrideEmployeeAddress

The address of the employee.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**city** | **str** | The full city name. | [optional] 
**region** | **str** | The region or state Example: &#x60;\&quot;NC\&quot;&#x60; | [optional] 
**street** | **str** | The full street address Example: &#x60;\&quot;564 Main Street, APT 15\&quot;&#x60; | [optional] 
**postal_code** | **str** | 5 digit postal code. | [optional] 
**country** | **str** | The country of the address. | [optional] 

## Example

```python
from pyplaid.models.paystub_override_employee_address import PaystubOverrideEmployeeAddress

# TODO update the JSON string below
json = "{}"
# create an instance of PaystubOverrideEmployeeAddress from a JSON string
paystub_override_employee_address_instance = PaystubOverrideEmployeeAddress.from_json(json)
# print the JSON string representation of the object
print PaystubOverrideEmployeeAddress.to_json()

# convert the object into a dict
paystub_override_employee_address_dict = paystub_override_employee_address_instance.to_dict()
# create an instance of PaystubOverrideEmployeeAddress from a dict
paystub_override_employee_address_form_dict = paystub_override_employee_address.from_dict(paystub_override_employee_address_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


