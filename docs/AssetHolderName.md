# AssetHolderName

No documentation available

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**full_name** | **str** | The unparsed name of either an individual or a legal entity. | 

## Example

```python
from pyplaid.models.asset_holder_name import AssetHolderName

# TODO update the JSON string below
json = "{}"
# create an instance of AssetHolderName from a JSON string
asset_holder_name_instance = AssetHolderName.from_json(json)
# print the JSON string representation of the object
print AssetHolderName.to_json()

# convert the object into a dict
asset_holder_name_dict = asset_holder_name_instance.to_dict()
# create an instance of AssetHolderName from a dict
asset_holder_name_form_dict = asset_holder_name.from_dict(asset_holder_name_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


