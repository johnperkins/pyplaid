# UserCreateResponse

UserCreateResponse defines the response schema for `/user/create`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**user_token** | **str** | The user token associated with the User data is being requested for. | 
**user_id** | **str** | The Plaid &#x60;user_id&#x60; of the User associated with this webhook, warning, or error. | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.user_create_response import UserCreateResponse

# TODO update the JSON string below
json = "{}"
# create an instance of UserCreateResponse from a JSON string
user_create_response_instance = UserCreateResponse.from_json(json)
# print the JSON string representation of the object
print UserCreateResponse.to_json()

# convert the object into a dict
user_create_response_dict = user_create_response_instance.to_dict()
# create an instance of UserCreateResponse from a dict
user_create_response_form_dict = user_create_response.from_dict(user_create_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


