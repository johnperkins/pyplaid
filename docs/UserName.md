# UserName

The full name provided by the user. If the user has not submitted their name, this field will be null. Otherwise, both fields are guaranteed to be filled.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**given_name** | **str** | A string with at least one non-whitespace character, with a max length of 100 characters. | 
**family_name** | **str** | A string with at least one non-whitespace character, with a max length of 100 characters. | 

## Example

```python
from pyplaid.models.user_name import UserName

# TODO update the JSON string below
json = "{}"
# create an instance of UserName from a JSON string
user_name_instance = UserName.from_json(json)
# print the JSON string representation of the object
print UserName.to_json()

# convert the object into a dict
user_name_dict = user_name_instance.to_dict()
# create an instance of UserName from a dict
user_name_form_dict = user_name.from_dict(user_name_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


