# InvestmentsHoldingsGetRequest

InvestmentsHoldingsGetRequest defines the request schema for `/investments/holdings/get`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**access_token** | **str** | The access token associated with the Item data is being requested for. | 
**options** | [**InvestmentHoldingsGetRequestOptions**](InvestmentHoldingsGetRequestOptions.md) |  | [optional] 

## Example

```python
from pyplaid.models.investments_holdings_get_request import InvestmentsHoldingsGetRequest

# TODO update the JSON string below
json = "{}"
# create an instance of InvestmentsHoldingsGetRequest from a JSON string
investments_holdings_get_request_instance = InvestmentsHoldingsGetRequest.from_json(json)
# print the JSON string representation of the object
print InvestmentsHoldingsGetRequest.to_json()

# convert the object into a dict
investments_holdings_get_request_dict = investments_holdings_get_request_instance.to_dict()
# create an instance of InvestmentsHoldingsGetRequest from a dict
investments_holdings_get_request_form_dict = investments_holdings_get_request.from_dict(investments_holdings_get_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


