# EntityScreeningHitEmails

Email address information for the associated entity watchlist hit

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email_address** | **str** | A valid email address. | 

## Example

```python
from pyplaid.models.entity_screening_hit_emails import EntityScreeningHitEmails

# TODO update the JSON string below
json = "{}"
# create an instance of EntityScreeningHitEmails from a JSON string
entity_screening_hit_emails_instance = EntityScreeningHitEmails.from_json(json)
# print the JSON string representation of the object
print EntityScreeningHitEmails.to_json()

# convert the object into a dict
entity_screening_hit_emails_dict = entity_screening_hit_emails_instance.to_dict()
# create an instance of EntityScreeningHitEmails from a dict
entity_screening_hit_emails_form_dict = entity_screening_hit_emails.from_dict(entity_screening_hit_emails_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


