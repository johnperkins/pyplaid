# IncomeVerificationPaystubsGetResponse

IncomeVerificationPaystubsGetResponse defines the response schema for `/income/verification/paystubs/get`.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**document_metadata** | [**List[DocumentMetadata]**](DocumentMetadata.md) | Metadata for an income document. | [optional] 
**paystubs** | [**List[Paystub]**](Paystub.md) |  | 
**error** | [**PlaidError**](PlaidError.md) |  | [optional] 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.income_verification_paystubs_get_response import IncomeVerificationPaystubsGetResponse

# TODO update the JSON string below
json = "{}"
# create an instance of IncomeVerificationPaystubsGetResponse from a JSON string
income_verification_paystubs_get_response_instance = IncomeVerificationPaystubsGetResponse.from_json(json)
# print the JSON string representation of the object
print IncomeVerificationPaystubsGetResponse.to_json()

# convert the object into a dict
income_verification_paystubs_get_response_dict = income_verification_paystubs_get_response_instance.to_dict()
# create an instance of IncomeVerificationPaystubsGetResponse from a dict
income_verification_paystubs_get_response_form_dict = income_verification_paystubs_get_response.from_dict(income_verification_paystubs_get_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


