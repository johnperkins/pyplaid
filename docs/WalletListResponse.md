# WalletListResponse

WalletListResponse defines the response schema for `/wallet/list`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**wallets** | [**List[Wallet]**](Wallet.md) | An array of e-wallets | 
**next_cursor** | **str** | Cursor used for fetching e-wallets created before the latest e-wallet provided in this response | [optional] 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.wallet_list_response import WalletListResponse

# TODO update the JSON string below
json = "{}"
# create an instance of WalletListResponse from a JSON string
wallet_list_response_instance = WalletListResponse.from_json(json)
# print the JSON string representation of the object
print WalletListResponse.to_json()

# convert the object into a dict
wallet_list_response_dict = wallet_list_response_instance.to_dict()
# create an instance of WalletListResponse from a dict
wallet_list_response_form_dict = wallet_list_response.from_dict(wallet_list_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


