# SandboxPublicTokenCreateRequestOptionsTransactions

An optional set of parameters corresponding to transactions options.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**start_date** | **date** | The earliest date for which to fetch transaction history. Dates should be formatted as YYYY-MM-DD. | [optional] 
**end_date** | **date** | The most recent date for which to fetch transaction history. Dates should be formatted as YYYY-MM-DD. | [optional] 

## Example

```python
from pyplaid.models.sandbox_public_token_create_request_options_transactions import SandboxPublicTokenCreateRequestOptionsTransactions

# TODO update the JSON string below
json = "{}"
# create an instance of SandboxPublicTokenCreateRequestOptionsTransactions from a JSON string
sandbox_public_token_create_request_options_transactions_instance = SandboxPublicTokenCreateRequestOptionsTransactions.from_json(json)
# print the JSON string representation of the object
print SandboxPublicTokenCreateRequestOptionsTransactions.to_json()

# convert the object into a dict
sandbox_public_token_create_request_options_transactions_dict = sandbox_public_token_create_request_options_transactions_instance.to_dict()
# create an instance of SandboxPublicTokenCreateRequestOptionsTransactions from a dict
sandbox_public_token_create_request_options_transactions_form_dict = sandbox_public_token_create_request_options_transactions.from_dict(sandbox_public_token_create_request_options_transactions_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


