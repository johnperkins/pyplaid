# ItemApplicationListResponse

Describes the connected application for a particular end user.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | [optional] 
**applications** | [**List[ConnectedApplication]**](ConnectedApplication.md) | A list of connected applications. | 

## Example

```python
from pyplaid.models.item_application_list_response import ItemApplicationListResponse

# TODO update the JSON string below
json = "{}"
# create an instance of ItemApplicationListResponse from a JSON string
item_application_list_response_instance = ItemApplicationListResponse.from_json(json)
# print the JSON string representation of the object
print ItemApplicationListResponse.to_json()

# convert the object into a dict
item_application_list_response_dict = item_application_list_response_instance.to_dict()
# create an instance of ItemApplicationListResponse from a dict
item_application_list_response_form_dict = item_application_list_response.from_dict(item_application_list_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


