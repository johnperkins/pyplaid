# IncomeVerificationStatusWebhook

Fired when the status of an income verification instance has changed. It will typically take several minutes for this webhook to fire after the end user has uploaded their documents in the Document Income flow.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**webhook_type** | **str** | &#x60;\&quot;INCOME\&quot;&#x60; | 
**webhook_code** | **str** | &#x60;INCOME_VERIFICATION&#x60; | 
**item_id** | **str** | The Item ID associated with the verification. | 
**user_id** | **str** | The Plaid &#x60;user_id&#x60; of the User associated with this webhook, warning, or error. | [optional] 
**verification_status** | **str** | &#x60;VERIFICATION_STATUS_PROCESSING_COMPLETE&#x60;: The income verification status processing has completed. If the user uploaded multiple documents, this webhook will fire when all documents have finished processing. Call the &#x60;/income/verification/paystubs/get&#x60; endpoint and check the document metadata to see which documents were successfully parsed.  &#x60;VERIFICATION_STATUS_PROCESSING_FAILED&#x60;: A failure occurred when attempting to process the verification documentation.  &#x60;VERIFICATION_STATUS_PENDING_APPROVAL&#x60;: (deprecated) The income verification has been sent to the user for review. | 
**environment** | [**WebhookEnvironmentValues**](WebhookEnvironmentValues.md) |  | 

## Example

```python
from pyplaid.models.income_verification_status_webhook import IncomeVerificationStatusWebhook

# TODO update the JSON string below
json = "{}"
# create an instance of IncomeVerificationStatusWebhook from a JSON string
income_verification_status_webhook_instance = IncomeVerificationStatusWebhook.from_json(json)
# print the JSON string representation of the object
print IncomeVerificationStatusWebhook.to_json()

# convert the object into a dict
income_verification_status_webhook_dict = income_verification_status_webhook_instance.to_dict()
# create an instance of IncomeVerificationStatusWebhook from a dict
income_verification_status_webhook_form_dict = income_verification_status_webhook.from_dict(income_verification_status_webhook_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


