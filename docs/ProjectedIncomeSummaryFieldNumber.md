# ProjectedIncomeSummaryFieldNumber

The employee's estimated annual salary, as derived from information reported on the paystub.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value** | **float** | The value of the field. | 
**verification_status** | [**VerificationStatus**](VerificationStatus.md) |  | 

## Example

```python
from pyplaid.models.projected_income_summary_field_number import ProjectedIncomeSummaryFieldNumber

# TODO update the JSON string below
json = "{}"
# create an instance of ProjectedIncomeSummaryFieldNumber from a JSON string
projected_income_summary_field_number_instance = ProjectedIncomeSummaryFieldNumber.from_json(json)
# print the JSON string representation of the object
print ProjectedIncomeSummaryFieldNumber.to_json()

# convert the object into a dict
projected_income_summary_field_number_dict = projected_income_summary_field_number_instance.to_dict()
# create an instance of ProjectedIncomeSummaryFieldNumber from a dict
projected_income_summary_field_number_form_dict = projected_income_summary_field_number.from_dict(projected_income_summary_field_number_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


