# WalletGetResponse

WalletGetResponse defines the response schema for `/wallet/get`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**wallet_id** | **str** | A unique ID identifying the e-wallet | 
**balance** | [**WalletBalance**](WalletBalance.md) |  | 
**numbers** | [**WalletNumbers**](WalletNumbers.md) |  | 
**recipient_id** | **str** | The ID of the recipient that corresponds to the e-wallet account numbers | [optional] 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.wallet_get_response import WalletGetResponse

# TODO update the JSON string below
json = "{}"
# create an instance of WalletGetResponse from a JSON string
wallet_get_response_instance = WalletGetResponse.from_json(json)
# print the JSON string representation of the object
print WalletGetResponse.to_json()

# convert the object into a dict
wallet_get_response_dict = wallet_get_response_instance.to_dict()
# create an instance of WalletGetResponse from a dict
wallet_get_response_form_dict = wallet_get_response.from_dict(wallet_get_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


