# Institution

Details relating to a specific financial institution

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**institution_id** | **str** | Unique identifier for the institution | 
**name** | **str** | The official name of the institution | 
**products** | [**List[Products]**](Products.md) | A list of the Plaid products supported by the institution. Note that only institutions that support Instant Auth will return &#x60;auth&#x60; in the product array; institutions that do not list &#x60;auth&#x60; may still support other Auth methods such as Instant Match or Automated Micro-deposit Verification. To identify institutions that support those methods, use the &#x60;auth_metadata&#x60; object. For more details, see [Full Auth coverage](https://plaid.com/docs/auth/coverage/). | 
**country_codes** | [**List[CountryCode]**](CountryCode.md) | A list of the country codes supported by the institution. | 
**url** | **str** | The URL for the institution&#39;s website | [optional] 
**primary_color** | **str** | Hexadecimal representation of the primary color used by the institution | [optional] 
**logo** | **str** | Base64 encoded representation of the institution&#39;s logo | [optional] 
**routing_numbers** | **List[str]** | A partial list of routing numbers associated with the institution. This list is provided for the purpose of looking up institutions by routing number. It is not comprehensive and should never be used as a complete list of routing numbers for an institution. | 
**oauth** | **bool** | Indicates that the institution has a mandatory OAuth login flow. Note that &#x60;oauth&#x60; may be &#x60;false&#x60; even for institutions that support OAuth, if the institution is in the process of migrating to OAuth and some active Items still exist that do not use OAuth. | 
**status** | [**InstitutionStatus**](InstitutionStatus.md) |  | [optional] 
**payment_initiation_metadata** | [**PaymentInitiationMetadata**](PaymentInitiationMetadata.md) |  | [optional] 
**auth_metadata** | [**AuthMetadata**](AuthMetadata.md) |  | [optional] 

## Example

```python
from pyplaid.models.institution import Institution

# TODO update the JSON string below
json = "{}"
# create an instance of Institution from a JSON string
institution_instance = Institution.from_json(json)
# print the JSON string representation of the object
print Institution.to_json()

# convert the object into a dict
institution_dict = institution_instance.to_dict()
# create an instance of Institution from a dict
institution_form_dict = institution.from_dict(institution_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


