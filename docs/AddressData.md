# AddressData

Data about the components comprising an address.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**city** | **str** | The full city name | 
**region** | **str** | The region or state. In API versions 2018-05-22 and earlier, this field is called &#x60;state&#x60;. Example: &#x60;\&quot;NC\&quot;&#x60; | 
**street** | **str** | The full street address Example: &#x60;\&quot;564 Main Street, APT 15\&quot;&#x60; | 
**postal_code** | **str** | The postal code. In API versions 2018-05-22 and earlier, this field is called &#x60;zip&#x60;. | 
**country** | **str** | The ISO 3166-1 alpha-2 country code | 

## Example

```python
from pyplaid.models.address_data import AddressData

# TODO update the JSON string below
json = "{}"
# create an instance of AddressData from a JSON string
address_data_instance = AddressData.from_json(json)
# print the JSON string representation of the object
print AddressData.to_json()

# convert the object into a dict
address_data_dict = address_data_instance.to_dict()
# create an instance of AddressData from a dict
address_data_form_dict = address_data.from_dict(address_data_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


