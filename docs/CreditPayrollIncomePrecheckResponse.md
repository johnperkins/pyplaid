# CreditPayrollIncomePrecheckResponse

Defines the response schema for `/credit/payroll_income/precheck`.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 
**confidence** | [**IncomeVerificationPrecheckConfidence**](IncomeVerificationPrecheckConfidence.md) |  | 

## Example

```python
from pyplaid.models.credit_payroll_income_precheck_response import CreditPayrollIncomePrecheckResponse

# TODO update the JSON string below
json = "{}"
# create an instance of CreditPayrollIncomePrecheckResponse from a JSON string
credit_payroll_income_precheck_response_instance = CreditPayrollIncomePrecheckResponse.from_json(json)
# print the JSON string representation of the object
print CreditPayrollIncomePrecheckResponse.to_json()

# convert the object into a dict
credit_payroll_income_precheck_response_dict = credit_payroll_income_precheck_response_instance.to_dict()
# create an instance of CreditPayrollIncomePrecheckResponse from a dict
credit_payroll_income_precheck_response_form_dict = credit_payroll_income_precheck_response.from_dict(credit_payroll_income_precheck_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


