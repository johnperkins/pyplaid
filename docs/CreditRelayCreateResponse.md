# CreditRelayCreateResponse

CreditRelayCreateResponse defines the response schema for `/credit/relay/create`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**relay_token** | **str** | A token that can be shared with a third party to allow them to access the Asset Report. This token should be stored securely. | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.credit_relay_create_response import CreditRelayCreateResponse

# TODO update the JSON string below
json = "{}"
# create an instance of CreditRelayCreateResponse from a JSON string
credit_relay_create_response_instance = CreditRelayCreateResponse.from_json(json)
# print the JSON string representation of the object
print CreditRelayCreateResponse.to_json()

# convert the object into a dict
credit_relay_create_response_dict = credit_relay_create_response_instance.to_dict()
# create an instance of CreditRelayCreateResponse from a dict
credit_relay_create_response_form_dict = credit_relay_create_response.from_dict(credit_relay_create_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


