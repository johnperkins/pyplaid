# ProductStatus

A representation of the status health of a request type. Auth requests, Balance requests, Identity requests, Investments requests, Liabilities requests, Transactions updates, Investments updates, Liabilities updates, and Item logins each have their own status object.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **str** | This field is deprecated in favor of the &#x60;breakdown&#x60; object, which provides more granular institution health data.  &#x60;HEALTHY&#x60;: the majority of requests are successful &#x60;DEGRADED&#x60;: only some requests are successful &#x60;DOWN&#x60;: all requests are failing | 
**last_status_change** | **datetime** | [ISO 8601](https://wikipedia.org/wiki/ISO_8601) formatted timestamp of the last status change for the institution.  | 
**breakdown** | [**ProductStatusBreakdown**](ProductStatusBreakdown.md) |  | 

## Example

```python
from pyplaid.models.product_status import ProductStatus

# TODO update the JSON string below
json = "{}"
# create an instance of ProductStatus from a JSON string
product_status_instance = ProductStatus.from_json(json)
# print the JSON string representation of the object
print ProductStatus.to_json()

# convert the object into a dict
product_status_dict = product_status_instance.to_dict()
# create an instance of ProductStatus from a dict
product_status_form_dict = product_status.from_dict(product_status_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


