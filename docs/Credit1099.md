# Credit1099

An object representing an end user's 1099 tax form

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**document_id** | **str** | An identifier of the document referenced by the document metadata. | 
**document_metadata** | [**CreditDocumentMetadata**](CreditDocumentMetadata.md) |  | [optional] 
**form_1099_type** | [**Form1099Type**](Form1099Type.md) |  | [optional] 
**recipient** | [**Credit1099Recipient**](Credit1099Recipient.md) |  | [optional] 
**payer** | [**Credit1099Payer**](Credit1099Payer.md) |  | [optional] 
**filer** | [**Credit1099Filer**](Credit1099Filer.md) |  | [optional] 
**tax_year** | **str** | Tax year of the tax form. | [optional] 
**rents** | **float** | Amount in rent by payer. | [optional] 
**royalties** | **float** | Amount in royalties by payer. | [optional] 
**other_income** | **float** | Amount in other income by payer. | [optional] 
**federal_income_tax_withheld** | **float** | Amount of federal income tax withheld from payer. | [optional] 
**fishing_boat_proceeds** | **float** | Amount of fishing boat proceeds from payer. | [optional] 
**medical_and_healthcare_payments** | **float** | Amount of medical and healthcare payments from payer. | [optional] 
**nonemployee_compensation** | **float** | Amount of nonemployee compensation from payer. | [optional] 
**substitute_payments_in_lieu_of_dividends_or_interest** | **float** | Amount of substitute payments made by payer. | [optional] 
**payer_made_direct_sales_of_5000_or_more_of_consumer_products_to_buyer** | **str** | Whether or not payer made direct sales over $5000 of consumer products. | [optional] 
**crop_insurance_proceeds** | **float** | Amount of crop insurance proceeds. | [optional] 
**excess_golden_parachute_payments** | **float** | Amount of golden parachute payments made by payer. | [optional] 
**gross_proceeds_paid_to_an_attorney** | **float** | Amount of gross proceeds paid to an attorney by payer. | [optional] 
**section_409a_deferrals** | **float** | Amount of 409A deferrals earned by payer. | [optional] 
**section_409a_income** | **float** | Amount of 409A income earned by payer. | [optional] 
**state_tax_withheld** | **float** | Amount of state tax withheld of payer for primary state. | [optional] 
**state_tax_withheld_lower** | **float** | Amount of state tax withheld of payer for secondary state. | [optional] 
**payer_state_number** | **str** | Primary state ID. | [optional] 
**payer_state_number_lower** | **str** | Secondary state ID. | [optional] 
**state_income** | **float** | State income reported for primary state. | [optional] 
**state_income_lower** | **float** | State income reported for secondary state. | [optional] 
**transactions_reported** | **str** | One of the values will be provided Payment card Third party network | [optional] 
**pse_name** | **str** | Name of the PSE (Payment Settlement Entity). | [optional] 
**pse_telephone_number** | **str** | Formatted (XXX) XXX-XXXX. Phone number of the PSE (Payment Settlement Entity). | [optional] 
**gross_amount** | **float** | Gross amount reported. | [optional] 
**card_not_present_transaction** | **float** | Amount in card not present transactions. | [optional] 
**merchant_category_code** | **str** | Merchant category of filer. | [optional] 
**number_of_payment_transactions** | **str** | Number of payment transactions made. | [optional] 
**january_amount** | **float** | Amount reported for January. | [optional] 
**february_amount** | **float** | Amount reported for February. | [optional] 
**march_amount** | **float** | Amount reported for March. | [optional] 
**april_amount** | **float** | Amount reported for April. | [optional] 
**may_amount** | **float** | Amount reported for May. | [optional] 
**june_amount** | **float** | Amount reported for June. | [optional] 
**july_amount** | **float** | Amount reported for July. | [optional] 
**august_amount** | **float** | Amount reported for August. | [optional] 
**september_amount** | **float** | Amount reported for September. | [optional] 
**october_amount** | **float** | Amount reported for October. | [optional] 
**november_amount** | **float** | Amount reported for November. | [optional] 
**december_amount** | **float** | Amount reported for December. | [optional] 
**primary_state** | **str** | Primary state of business. | [optional] 
**secondary_state** | **str** | Secondary state of business. | [optional] 
**primary_state_id** | **str** | Primary state ID. | [optional] 
**secondary_state_id** | **str** | Secondary state ID. | [optional] 
**primary_state_income_tax** | **float** | State income tax reported for primary state. | [optional] 
**secondary_state_income_tax** | **float** | State income tax reported for secondary state. | [optional] 

## Example

```python
from pyplaid.models.credit1099 import Credit1099

# TODO update the JSON string below
json = "{}"
# create an instance of Credit1099 from a JSON string
credit1099_instance = Credit1099.from_json(json)
# print the JSON string representation of the object
print Credit1099.to_json()

# convert the object into a dict
credit1099_dict = credit1099_instance.to_dict()
# create an instance of Credit1099 from a dict
credit1099_form_dict = credit1099.from_dict(credit1099_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


