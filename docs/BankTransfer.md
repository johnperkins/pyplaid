# BankTransfer

Represents a bank transfer within the Bank Transfers API.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Plaid’s unique identifier for a bank transfer. | 
**ach_class** | [**ACHClass**](ACHClass.md) |  | 
**account_id** | **str** | The account ID that should be credited/debited for this bank transfer. | 
**type** | [**BankTransferType**](BankTransferType.md) |  | 
**user** | [**BankTransferUser**](BankTransferUser.md) |  | 
**amount** | **str** | The amount of the bank transfer (decimal string with two digits of precision e.g. \&quot;10.00\&quot;). | 
**iso_currency_code** | **str** | The currency of the transfer amount, e.g. \&quot;USD\&quot; | 
**description** | **str** | The description of the transfer. | 
**created** | **datetime** | The datetime when this bank transfer was created. This will be of the form &#x60;2006-01-02T15:04:05Z&#x60; | 
**status** | [**BankTransferStatus**](BankTransferStatus.md) |  | 
**network** | [**BankTransferNetwork**](BankTransferNetwork.md) |  | 
**cancellable** | **bool** | When &#x60;true&#x60;, you can still cancel this bank transfer. | 
**failure_reason** | [**BankTransferFailure**](BankTransferFailure.md) |  | 
**custom_tag** | **str** | A string containing the custom tag provided by the client in the create request. Will be null if not provided. | 
**metadata** | **Dict[str, str]** | The Metadata object is a mapping of client-provided string fields to any string value. The following limitations apply: - The JSON values must be Strings (no nested JSON objects allowed) - Only ASCII characters may be used - Maximum of 50 key/value pairs - Maximum key length of 40 characters - Maximum value length of 500 characters  | 
**origination_account_id** | **str** | Plaid’s unique identifier for the origination account that was used for this transfer. | 
**direction** | [**BankTransferDirection**](BankTransferDirection.md) |  | 

## Example

```python
from pyplaid.models.bank_transfer import BankTransfer

# TODO update the JSON string below
json = "{}"
# create an instance of BankTransfer from a JSON string
bank_transfer_instance = BankTransfer.from_json(json)
# print the JSON string representation of the object
print BankTransfer.to_json()

# convert the object into a dict
bank_transfer_dict = bank_transfer_instance.to_dict()
# create an instance of BankTransfer from a dict
bank_transfer_form_dict = bank_transfer.from_dict(bank_transfer_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


