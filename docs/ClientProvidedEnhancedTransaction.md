# ClientProvidedEnhancedTransaction

A client-provided transaction that Plaid has enhanced.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Unique transaction identifier to tie transactions back to clients&#39; systems. | 
**description** | **str** | The raw description of the transaction. | 
**amount** | **float** | The value of the transaction, denominated in the account&#39;s currency, as stated in &#x60;iso_currency_code&#x60;. Positive values when money moves out of the account; negative values when money moves in. For example, debit card purchases are positive; credit card payments, direct deposits, and refunds are negative. | 
**iso_currency_code** | **str** | The ISO-4217 currency code of the transaction. | 
**enhancements** | [**Enhancements**](Enhancements.md) |  | 

## Example

```python
from pyplaid.models.client_provided_enhanced_transaction import ClientProvidedEnhancedTransaction

# TODO update the JSON string below
json = "{}"
# create an instance of ClientProvidedEnhancedTransaction from a JSON string
client_provided_enhanced_transaction_instance = ClientProvidedEnhancedTransaction.from_json(json)
# print the JSON string representation of the object
print ClientProvidedEnhancedTransaction.to_json()

# convert the object into a dict
client_provided_enhanced_transaction_dict = client_provided_enhanced_transaction_instance.to_dict()
# create an instance of ClientProvidedEnhancedTransaction from a dict
client_provided_enhanced_transaction_form_dict = client_provided_enhanced_transaction.from_dict(client_provided_enhanced_transaction_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


