# ProductStatusBreakdown

A detailed breakdown of the institution's performance for a request type. The values for `success`, `error_plaid`, and `error_institution` sum to 1. The time range used for calculating the breakdown may range from the most recent few minutes to the past six hours. In general, smaller institutions will show status that was calculated over a longer period of time. For Investment updates, which are refreshed less frequently, the period assessed may be 24 hours or more. For more details, see [Institution status details](https://plaid.com/docs/account/activity/#institution-status-details).

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**success** | **float** | The percentage of login attempts that are successful, expressed as a decimal. | 
**error_plaid** | **float** | The percentage of logins that are failing due to an internal Plaid issue, expressed as a decimal.  | 
**error_institution** | **float** | The percentage of logins that are failing due to an issue in the institution&#39;s system, expressed as a decimal. | 
**refresh_interval** | **str** | The &#x60;refresh_interval&#x60; may be &#x60;DELAYED&#x60; or &#x60;STOPPED&#x60; even when the success rate is high. This value is only returned for Transactions status breakdowns. | [optional] 

## Example

```python
from pyplaid.models.product_status_breakdown import ProductStatusBreakdown

# TODO update the JSON string below
json = "{}"
# create an instance of ProductStatusBreakdown from a JSON string
product_status_breakdown_instance = ProductStatusBreakdown.from_json(json)
# print the JSON string representation of the object
print ProductStatusBreakdown.to_json()

# convert the object into a dict
product_status_breakdown_dict = product_status_breakdown_instance.to_dict()
# create an instance of ProductStatusBreakdown from a dict
product_status_breakdown_form_dict = product_status_breakdown.from_dict(product_status_breakdown_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


