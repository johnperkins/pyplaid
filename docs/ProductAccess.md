# ProductAccess

The product access being requested. Used to or disallow product access across all accounts. If unset, defaults to all products allowed.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**statements** | **bool** | Allow access to statements. Only used by certain partners. If relevant to the partner and unset, defaults to &#x60;true&#x60;. | [optional] [default to True]
**identity** | **bool** | Allow access to the Identity product (name, email, phone, address). Only used by certain partners. If relevant to the partner and unset, defaults to &#x60;true&#x60;. | [optional] [default to True]
**auth** | **bool** | Allow access to account number details. Only used by certain partners. If relevant to the partner and unset, defaults to &#x60;true&#x60;. | [optional] [default to True]
**transactions** | **bool** | Allow access to transaction details. Only used by certain partners. If relevant to the partner and unset, defaults to &#x60;true&#x60;. | [optional] [default to True]
**accounts_details_transactions** | **bool** | Allow access to \&quot;accounts_details_transactions\&quot;. Only used by certain partners. If relevant to the partner and unset, defaults to &#x60;true&#x60;. | [optional] [default to True]
**accounts_routing_number** | **bool** | Allow access to \&quot;accounts_routing_number\&quot;. Only used by certain partners. If relevant to the partner and unset, defaults to &#x60;true&#x60;. | [optional] [default to True]
**accounts_statements** | **bool** | Allow access to \&quot;accounts_statements\&quot;. Only used by certain partners. If relevant to the partner and unset, defaults to &#x60;true&#x60;. | [optional] [default to True]
**accounts_tax_statements** | **bool** | Allow access to \&quot;accounts_tax_statements\&quot;. Only used by certain partners. If relevant to the partner and unset, defaults to &#x60;true&#x60;. | [optional] [default to True]
**customers_profiles** | **bool** | Allow access to \&quot;customers_profiles\&quot;. Only used by certain partners. If relevant to the partner and unset, defaults to &#x60;true&#x60;. | [optional] [default to True]

## Example

```python
from pyplaid.models.product_access import ProductAccess

# TODO update the JSON string below
json = "{}"
# create an instance of ProductAccess from a JSON string
product_access_instance = ProductAccess.from_json(json)
# print the JSON string representation of the object
print ProductAccess.to_json()

# convert the object into a dict
product_access_dict = product_access_instance.to_dict()
# create an instance of ProductAccess from a dict
product_access_form_dict = product_access.from_dict(product_access_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


