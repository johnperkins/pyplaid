# AssetsProductReadyWebhook

Fired when the Asset Report has been generated and `/asset_report/get` is ready to be called.  If you attempt to retrieve an Asset Report before this webhook has fired, you’ll receive a response with the HTTP status code 400 and a Plaid error code of `PRODUCT_NOT_READY`.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**webhook_type** | **str** | &#x60;ASSETS&#x60; | 
**webhook_code** | **str** | &#x60;PRODUCT_READY&#x60; | 
**asset_report_id** | **str** | The &#x60;asset_report_id&#x60; that can be provided to &#x60;/asset_report/get&#x60; to retrieve the Asset Report. | 

## Example

```python
from pyplaid.models.assets_product_ready_webhook import AssetsProductReadyWebhook

# TODO update the JSON string below
json = "{}"
# create an instance of AssetsProductReadyWebhook from a JSON string
assets_product_ready_webhook_instance = AssetsProductReadyWebhook.from_json(json)
# print the JSON string representation of the object
print AssetsProductReadyWebhook.to_json()

# convert the object into a dict
assets_product_ready_webhook_dict = assets_product_ready_webhook_instance.to_dict()
# create an instance of AssetsProductReadyWebhook from a dict
assets_product_ready_webhook_form_dict = assets_product_ready_webhook.from_dict(assets_product_ready_webhook_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


