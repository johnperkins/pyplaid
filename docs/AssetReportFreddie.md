# AssetReportFreddie

An object representing an Asset Report with Freddie Mac schema.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**loans** | [**Loans**](Loans.md) |  | 
**parties** | [**Parties**](Parties.md) |  | 
**services** | [**Services**](Services.md) |  | 

## Example

```python
from pyplaid.models.asset_report_freddie import AssetReportFreddie

# TODO update the JSON string below
json = "{}"
# create an instance of AssetReportFreddie from a JSON string
asset_report_freddie_instance = AssetReportFreddie.from_json(json)
# print the JSON string representation of the object
print AssetReportFreddie.to_json()

# convert the object into a dict
asset_report_freddie_dict = asset_report_freddie_instance.to_dict()
# create an instance of AssetReportFreddie from a dict
asset_report_freddie_form_dict = asset_report_freddie.from_dict(asset_report_freddie_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


