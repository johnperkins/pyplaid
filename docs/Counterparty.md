# Counterparty

The counterparty, such as the merchant or financial institution, is extracted by Plaid from the raw description.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The name of the counterparty, such as the merchant or the financial institution, as extracted by Plaid from the raw description. | 
**type** | [**CounterpartyType**](CounterpartyType.md) |  | 

## Example

```python
from pyplaid.models.counterparty import Counterparty

# TODO update the JSON string below
json = "{}"
# create an instance of Counterparty from a JSON string
counterparty_instance = Counterparty.from_json(json)
# print the JSON string representation of the object
print Counterparty.to_json()

# convert the object into a dict
counterparty_dict = counterparty_instance.to_dict()
# create an instance of Counterparty from a dict
counterparty_form_dict = counterparty.from_dict(counterparty_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


