# ProcessorTokenCreateRequest

ProcessorTokenCreateRequest defines the request schema for `/processor/token/create`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**access_token** | **str** | The access token associated with the Item data is being requested for. | 
**account_id** | **str** | The &#x60;account_id&#x60; value obtained from the &#x60;onSuccess&#x60; callback in Link | 
**processor** | **str** | The processor you are integrating with. | 

## Example

```python
from pyplaid.models.processor_token_create_request import ProcessorTokenCreateRequest

# TODO update the JSON string below
json = "{}"
# create an instance of ProcessorTokenCreateRequest from a JSON string
processor_token_create_request_instance = ProcessorTokenCreateRequest.from_json(json)
# print the JSON string representation of the object
print ProcessorTokenCreateRequest.to_json()

# convert the object into a dict
processor_token_create_request_dict = processor_token_create_request_instance.to_dict()
# create an instance of ProcessorTokenCreateRequest from a dict
processor_token_create_request_form_dict = processor_token_create_request.from_dict(processor_token_create_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


