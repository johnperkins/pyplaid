# Scopes

The scopes object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product_access** | [**ProductAccess**](ProductAccess.md) |  | [optional] 
**accounts** | [**List[AccountAccess]**](AccountAccess.md) |  | [optional] 
**new_accounts** | **bool** | Allow access to newly opened accounts as they are opened. If unset, defaults to &#x60;true&#x60;. | [optional] [default to True]

## Example

```python
from pyplaid.models.scopes import Scopes

# TODO update the JSON string below
json = "{}"
# create an instance of Scopes from a JSON string
scopes_instance = Scopes.from_json(json)
# print the JSON string representation of the object
print Scopes.to_json()

# convert the object into a dict
scopes_dict = scopes_instance.to_dict()
# create an instance of Scopes from a dict
scopes_form_dict = scopes.from_dict(scopes_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


