# AssetReportTransactionAllOf


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**date_transacted** | **str** | The date on which the transaction took place, in IS0 8601 format. | [optional] 
**credit_category** | [**CreditCategory**](CreditCategory.md) |  | [optional] 

## Example

```python
from pyplaid.models.asset_report_transaction_all_of import AssetReportTransactionAllOf

# TODO update the JSON string below
json = "{}"
# create an instance of AssetReportTransactionAllOf from a JSON string
asset_report_transaction_all_of_instance = AssetReportTransactionAllOf.from_json(json)
# print the JSON string representation of the object
print AssetReportTransactionAllOf.to_json()

# convert the object into a dict
asset_report_transaction_all_of_dict = asset_report_transaction_all_of_instance.to_dict()
# create an instance of AssetReportTransactionAllOf from a dict
asset_report_transaction_all_of_form_dict = asset_report_transaction_all_of.from_dict(asset_report_transaction_all_of_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


