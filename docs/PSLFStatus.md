# PSLFStatus

Information about the student's eligibility in the Public Service Loan Forgiveness program. This is only returned if the institution is FedLoan (`ins_116527`). 

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**estimated_eligibility_date** | **date** | The estimated date borrower will have completed 120 qualifying monthly payments. Returned in [ISO 8601](https://wikipedia.org/wiki/ISO_8601) format (YYYY-MM-DD). | 
**payments_made** | **float** | The number of qualifying payments that have been made. | 
**payments_remaining** | **float** | The number of qualifying payments remaining. | 

## Example

```python
from pyplaid.models.pslf_status import PSLFStatus

# TODO update the JSON string below
json = "{}"
# create an instance of PSLFStatus from a JSON string
pslf_status_instance = PSLFStatus.from_json(json)
# print the JSON string representation of the object
print PSLFStatus.to_json()

# convert the object into a dict
pslf_status_dict = pslf_status_instance.to_dict()
# create an instance of PSLFStatus from a dict
pslf_status_form_dict = pslf_status.from_dict(pslf_status_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


