# CreditPayrollIncomeRefreshResponse

CreditPayrollIncomeRefreshResponse defines the response schema for `/credit/payroll_income/refresh`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 
**verification_refresh_status** | **str** | The verification refresh status. One of the following:  &#x60;\&quot;USER_PRESENCE_REQUIRED\&quot;&#x60; User presence is required to refresh an income verification. &#x60;\&quot;SUCCESSFUL\&quot;&#x60; The income verification refresh was successful. &#x60;\&quot;NOT_FOUND\&quot;&#x60; No new data was found after the income verification refresh. | 

## Example

```python
from pyplaid.models.credit_payroll_income_refresh_response import CreditPayrollIncomeRefreshResponse

# TODO update the JSON string below
json = "{}"
# create an instance of CreditPayrollIncomeRefreshResponse from a JSON string
credit_payroll_income_refresh_response_instance = CreditPayrollIncomeRefreshResponse.from_json(json)
# print the JSON string representation of the object
print CreditPayrollIncomeRefreshResponse.to_json()

# convert the object into a dict
credit_payroll_income_refresh_response_dict = credit_payroll_income_refresh_response_instance.to_dict()
# create an instance of CreditPayrollIncomeRefreshResponse from a dict
credit_payroll_income_refresh_response_form_dict = credit_payroll_income_refresh_response.from_dict(credit_payroll_income_refresh_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


