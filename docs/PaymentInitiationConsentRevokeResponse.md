# PaymentInitiationConsentRevokeResponse

PaymentInitiationConsentRevokeResponse defines the response schema for `/payment_initation/consent/revoke`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | [optional] 

## Example

```python
from pyplaid.models.payment_initiation_consent_revoke_response import PaymentInitiationConsentRevokeResponse

# TODO update the JSON string below
json = "{}"
# create an instance of PaymentInitiationConsentRevokeResponse from a JSON string
payment_initiation_consent_revoke_response_instance = PaymentInitiationConsentRevokeResponse.from_json(json)
# print the JSON string representation of the object
print PaymentInitiationConsentRevokeResponse.to_json()

# convert the object into a dict
payment_initiation_consent_revoke_response_dict = payment_initiation_consent_revoke_response_instance.to_dict()
# create an instance of PaymentInitiationConsentRevokeResponse from a dict
payment_initiation_consent_revoke_response_form_dict = payment_initiation_consent_revoke_response.from_dict(payment_initiation_consent_revoke_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


