# SandboxPublicTokenCreateRequestOptionsIncomeVerification

A set of parameters for income verification options. This field is required if `income_verification` is included in the `initial_products` array.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**income_source_types** | [**List[IncomeVerificationSourceType]**](IncomeVerificationSourceType.md) | The types of source income data that users will be permitted to share. Options include &#x60;bank&#x60; and &#x60;payroll&#x60;. Currently you can only specify one of these options. | [optional] 
**bank_income** | [**SandboxPublicTokenCreateRequestIncomeVerificationBankIncome**](SandboxPublicTokenCreateRequestIncomeVerificationBankIncome.md) |  | [optional] 

## Example

```python
from pyplaid.models.sandbox_public_token_create_request_options_income_verification import SandboxPublicTokenCreateRequestOptionsIncomeVerification

# TODO update the JSON string below
json = "{}"
# create an instance of SandboxPublicTokenCreateRequestOptionsIncomeVerification from a JSON string
sandbox_public_token_create_request_options_income_verification_instance = SandboxPublicTokenCreateRequestOptionsIncomeVerification.from_json(json)
# print the JSON string representation of the object
print SandboxPublicTokenCreateRequestOptionsIncomeVerification.to_json()

# convert the object into a dict
sandbox_public_token_create_request_options_income_verification_dict = sandbox_public_token_create_request_options_income_verification_instance.to_dict()
# create an instance of SandboxPublicTokenCreateRequestOptionsIncomeVerification from a dict
sandbox_public_token_create_request_options_income_verification_form_dict = sandbox_public_token_create_request_options_income_verification.from_dict(sandbox_public_token_create_request_options_income_verification_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


