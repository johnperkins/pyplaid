# ScreeningHitAnalysis

Analysis information describing why a screening hit matched the provided user information

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dates_of_birth** | [**MatchSummaryCode**](MatchSummaryCode.md) |  | [optional] 
**documents** | [**MatchSummaryCode**](MatchSummaryCode.md) |  | [optional] 
**locations** | [**MatchSummaryCode**](MatchSummaryCode.md) |  | [optional] 
**names** | [**MatchSummaryCode**](MatchSummaryCode.md) |  | [optional] 
**search_terms_version** | **float** | The version of the screening&#39;s &#x60;search_terms&#x60; that were compared when the screening hit was added. screening hits are immutable once they have been reviewed. If changes are detected due to updates to the screening&#39;s &#x60;search_terms&#x60;, the associated program, or the list&#39;s source data prior to review, the screening hit will be updated to reflect those changes. | 

## Example

```python
from pyplaid.models.screening_hit_analysis import ScreeningHitAnalysis

# TODO update the JSON string below
json = "{}"
# create an instance of ScreeningHitAnalysis from a JSON string
screening_hit_analysis_instance = ScreeningHitAnalysis.from_json(json)
# print the JSON string representation of the object
print ScreeningHitAnalysis.to_json()

# convert the object into a dict
screening_hit_analysis_dict = screening_hit_analysis_instance.to_dict()
# create an instance of ScreeningHitAnalysis from a dict
screening_hit_analysis_form_dict = screening_hit_analysis.from_dict(screening_hit_analysis_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


