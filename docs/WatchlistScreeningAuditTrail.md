# WatchlistScreeningAuditTrail

Information about the last change made to the parent object specifying what caused the change as well as when it occurred.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**source** | [**Source**](Source.md) |  | 
**dashboard_user_id** | **str** | ID of the associated user. | 
**timestamp** | **datetime** | An ISO8601 formatted timestamp. | 

## Example

```python
from pyplaid.models.watchlist_screening_audit_trail import WatchlistScreeningAuditTrail

# TODO update the JSON string below
json = "{}"
# create an instance of WatchlistScreeningAuditTrail from a JSON string
watchlist_screening_audit_trail_instance = WatchlistScreeningAuditTrail.from_json(json)
# print the JSON string representation of the object
print WatchlistScreeningAuditTrail.to_json()

# convert the object into a dict
watchlist_screening_audit_trail_dict = watchlist_screening_audit_trail_instance.to_dict()
# create an instance of WatchlistScreeningAuditTrail from a dict
watchlist_screening_audit_trail_form_dict = watchlist_screening_audit_trail.from_dict(watchlist_screening_audit_trail_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


