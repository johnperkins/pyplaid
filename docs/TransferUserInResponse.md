# TransferUserInResponse

The legal name and other information for the account holder.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**legal_name** | **str** | The user&#39;s legal name. | 
**phone_number** | **str** | The user&#39;s phone number. | 
**email_address** | **str** | The user&#39;s email address. | 
**address** | [**TransferUserAddressInResponse**](TransferUserAddressInResponse.md) |  | 

## Example

```python
from pyplaid.models.transfer_user_in_response import TransferUserInResponse

# TODO update the JSON string below
json = "{}"
# create an instance of TransferUserInResponse from a JSON string
transfer_user_in_response_instance = TransferUserInResponse.from_json(json)
# print the JSON string representation of the object
print TransferUserInResponse.to_json()

# convert the object into a dict
transfer_user_in_response_dict = transfer_user_in_response_instance.to_dict()
# create an instance of TransferUserInResponse from a dict
transfer_user_in_response_form_dict = transfer_user_in_response.from_dict(transfer_user_in_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


