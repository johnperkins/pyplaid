# AuthMetadata

Metadata that captures information about the Auth features of an institution.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**supported_methods** | [**AuthSupportedMethods**](AuthSupportedMethods.md) |  | 

## Example

```python
from pyplaid.models.auth_metadata import AuthMetadata

# TODO update the JSON string below
json = "{}"
# create an instance of AuthMetadata from a JSON string
auth_metadata_instance = AuthMetadata.from_json(json)
# print the JSON string representation of the object
print AuthMetadata.to_json()

# convert the object into a dict
auth_metadata_dict = auth_metadata_instance.to_dict()
# create an instance of AuthMetadata from a dict
auth_metadata_form_dict = auth_metadata.from_dict(auth_metadata_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


