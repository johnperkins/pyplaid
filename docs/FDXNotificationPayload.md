# FDXNotificationPayload

Custom key-value pairs payload for a notification

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | ID for the origination entity related to the notification | [optional] 
**id_type** | [**FDXNotificationPayloadIdType**](FDXNotificationPayloadIdType.md) |  | [optional] 
**custom_fields** | [**FDXFiAttribute**](FDXFiAttribute.md) |  | [optional] 

## Example

```python
from pyplaid.models.fdx_notification_payload import FDXNotificationPayload

# TODO update the JSON string below
json = "{}"
# create an instance of FDXNotificationPayload from a JSON string
fdx_notification_payload_instance = FDXNotificationPayload.from_json(json)
# print the JSON string representation of the object
print FDXNotificationPayload.to_json()

# convert the object into a dict
fdx_notification_payload_dict = fdx_notification_payload_instance.to_dict()
# create an instance of FDXNotificationPayload from a dict
fdx_notification_payload_form_dict = fdx_notification_payload.from_dict(fdx_notification_payload_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


