# AccountIdentityMatchScoreAllOf


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**legal_name** | [**NameMatchScore**](NameMatchScore.md) |  | [optional] 
**phone_number** | [**PhoneNumberMatchScore**](PhoneNumberMatchScore.md) |  | [optional] 
**email_address** | [**EmailAddressMatchScore**](EmailAddressMatchScore.md) |  | [optional] 
**address** | [**AddressMatchScore**](AddressMatchScore.md) |  | [optional] 

## Example

```python
from pyplaid.models.account_identity_match_score_all_of import AccountIdentityMatchScoreAllOf

# TODO update the JSON string below
json = "{}"
# create an instance of AccountIdentityMatchScoreAllOf from a JSON string
account_identity_match_score_all_of_instance = AccountIdentityMatchScoreAllOf.from_json(json)
# print the JSON string representation of the object
print AccountIdentityMatchScoreAllOf.to_json()

# convert the object into a dict
account_identity_match_score_all_of_dict = account_identity_match_score_all_of_instance.to_dict()
# create an instance of AccountIdentityMatchScoreAllOf from a dict
account_identity_match_score_all_of_form_dict = account_identity_match_score_all_of.from_dict(account_identity_match_score_all_of_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


