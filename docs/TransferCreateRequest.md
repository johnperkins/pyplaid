# TransferCreateRequest

Defines the request schema for `/transfer/create`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**idempotency_key** | **str** | Deprecated. &#x60;authorization_id&#x60; is now used as idempotency instead.  A random key provided by the client, per unique transfer. Maximum of 50 characters.  The API supports idempotency for safely retrying requests without accidentally performing the same operation twice. For example, if a request to create a transfer fails due to a network connection error, you can retry the request with the same idempotency key to guarantee that only a single transfer is created. | [optional] 
**access_token** | **str** | The Plaid &#x60;access_token&#x60; for the account that will be debited or credited. Required if not using &#x60;payment_profile_token&#x60;. | [optional] 
**account_id** | **str** | The Plaid &#x60;account_id&#x60; for the account that will be debited or credited. Required if not using &#x60;payment_profile_token&#x60;. | [optional] 
**payment_profile_token** | **str** | The payment profile token associated with the Payment Profile that will be debited or credited. Required if not using &#x60;access_token&#x60;. | [optional] 
**authorization_id** | **str** | Plaid’s unique identifier for a transfer authorization. This parameter also serves the purpose of acting as an idempotency identifier. | 
**type** | [**TransferType**](TransferType.md) |  | [optional] 
**network** | [**TransferNetwork**](TransferNetwork.md) |  | [optional] 
**amount** | **str** | The amount of the transfer (decimal string with two digits of precision e.g. \&quot;10.00\&quot;). | [optional] 
**description** | **str** | The transfer description. Maximum of 10 characters. | 
**ach_class** | [**ACHClass**](ACHClass.md) |  | [optional] 
**user** | [**TransferUserInRequestDeprecated**](TransferUserInRequestDeprecated.md) |  | [optional] 
**metadata** | **Dict[str, str]** | The Metadata object is a mapping of client-provided string fields to any string value. The following limitations apply: - The JSON values must be Strings (no nested JSON objects allowed) - Only ASCII characters may be used - Maximum of 50 key/value pairs - Maximum key length of 40 characters - Maximum value length of 500 characters  | [optional] 
**origination_account_id** | **str** | Plaid’s unique identifier for the origination account for this transfer. If you have more than one origination account, this value must be specified. Otherwise, this field should be left blank. | [optional] 
**iso_currency_code** | **str** | The currency of the transfer amount. The default value is \&quot;USD\&quot;. | [optional] 

## Example

```python
from pyplaid.models.transfer_create_request import TransferCreateRequest

# TODO update the JSON string below
json = "{}"
# create an instance of TransferCreateRequest from a JSON string
transfer_create_request_instance = TransferCreateRequest.from_json(json)
# print the JSON string representation of the object
print TransferCreateRequest.to_json()

# convert the object into a dict
transfer_create_request_dict = transfer_create_request_instance.to_dict()
# create an instance of TransferCreateRequest from a dict
transfer_create_request_form_dict = transfer_create_request.from_dict(transfer_create_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


