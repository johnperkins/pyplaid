# CreditSession

Metadata and results for a Link session

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**link_session_id** | **str** | The unique identifier associated with the Link session. This identifier matches the &#x60;link_session_id&#x60; returned in the onSuccess/onExit callbacks. | [optional] 
**session_start_time** | **datetime** | The time when the Link session started | [optional] 
**results** | [**CreditSessionResults**](CreditSessionResults.md) |  | [optional] 
**errors** | [**List[CreditSessionError]**](CreditSessionError.md) | The set of errors that occurred during the Link session. | [optional] 

## Example

```python
from pyplaid.models.credit_session import CreditSession

# TODO update the JSON string below
json = "{}"
# create an instance of CreditSession from a JSON string
credit_session_instance = CreditSession.from_json(json)
# print the JSON string representation of the object
print CreditSession.to_json()

# convert the object into a dict
credit_session_dict = credit_session_instance.to_dict()
# create an instance of CreditSession from a dict
credit_session_form_dict = credit_session.from_dict(credit_session_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


