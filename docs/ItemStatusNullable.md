# ItemStatusNullable

Information about the last successful and failed transactions update for the Item.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**investments** | [**ItemStatusInvestments**](ItemStatusInvestments.md) |  | [optional] 
**transactions** | [**ItemStatusTransactions**](ItemStatusTransactions.md) |  | [optional] 
**last_webhook** | [**ItemStatusLastWebhook**](ItemStatusLastWebhook.md) |  | [optional] 

## Example

```python
from pyplaid.models.item_status_nullable import ItemStatusNullable

# TODO update the JSON string below
json = "{}"
# create an instance of ItemStatusNullable from a JSON string
item_status_nullable_instance = ItemStatusNullable.from_json(json)
# print the JSON string representation of the object
print ItemStatusNullable.to_json()

# convert the object into a dict
item_status_nullable_dict = item_status_nullable_instance.to_dict()
# create an instance of ItemStatusNullable from a dict
item_status_nullable_form_dict = item_status_nullable.from_dict(item_status_nullable_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


