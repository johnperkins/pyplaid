# SandboxItemResetLoginRequest

SandboxItemResetLoginRequest defines the request schema for `/sandbox/item/reset_login`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**access_token** | **str** | The access token associated with the Item data is being requested for. | 

## Example

```python
from pyplaid.models.sandbox_item_reset_login_request import SandboxItemResetLoginRequest

# TODO update the JSON string below
json = "{}"
# create an instance of SandboxItemResetLoginRequest from a JSON string
sandbox_item_reset_login_request_instance = SandboxItemResetLoginRequest.from_json(json)
# print the JSON string representation of the object
print SandboxItemResetLoginRequest.to_json()

# convert the object into a dict
sandbox_item_reset_login_request_dict = sandbox_item_reset_login_request_instance.to_dict()
# create an instance of SandboxItemResetLoginRequest from a dict
sandbox_item_reset_login_request_form_dict = sandbox_item_reset_login_request.from_dict(sandbox_item_reset_login_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


