# KYCCheckDetails

Additional information for the `kyc_check` step. This field will be `null` unless `steps.kyc_check` has reached a terminal state of either `success` or `failed`.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **str** | The outcome status for the associated Identity Verification attempt&#39;s &#x60;kyc_check&#x60; step. This field will always have the same value as &#x60;steps.kyc_check&#x60;. | 
**address** | [**KYCCheckAddressSummary**](KYCCheckAddressSummary.md) |  | 
**name** | [**KYCCheckNameSummary**](KYCCheckNameSummary.md) |  | 
**date_of_birth** | [**KYCCheckDateOfBirthSummary**](KYCCheckDateOfBirthSummary.md) |  | 
**id_number** | [**KYCCheckIDNumberSummary**](KYCCheckIDNumberSummary.md) |  | 
**phone_number** | [**KYCCheckPhoneSummary**](KYCCheckPhoneSummary.md) |  | 

## Example

```python
from pyplaid.models.kyc_check_details import KYCCheckDetails

# TODO update the JSON string below
json = "{}"
# create an instance of KYCCheckDetails from a JSON string
kyc_check_details_instance = KYCCheckDetails.from_json(json)
# print the JSON string representation of the object
print KYCCheckDetails.to_json()

# convert the object into a dict
kyc_check_details_dict = kyc_check_details_instance.to_dict()
# create an instance of KYCCheckDetails from a dict
kyc_check_details_form_dict = kyc_check_details.from_dict(kyc_check_details_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


