# CreditSessionItemAddResult

The details of an item add in Link

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**public_token** | **str** | Returned once a user has successfully linked their Item. | [optional] 
**item_id** | **str** | The Plaid Item ID. The &#x60;item_id&#x60; is always unique; linking the same account at the same institution twice will result in two Items with different &#x60;item_id&#x60; values. Like all Plaid identifiers, the &#x60;item_id&#x60; is case-sensitive. | [optional] 
**institution_id** | **str** | The Plaid Institution ID associated with the Item. | [optional] 

## Example

```python
from pyplaid.models.credit_session_item_add_result import CreditSessionItemAddResult

# TODO update the JSON string below
json = "{}"
# create an instance of CreditSessionItemAddResult from a JSON string
credit_session_item_add_result_instance = CreditSessionItemAddResult.from_json(json)
# print the JSON string representation of the object
print CreditSessionItemAddResult.to_json()

# convert the object into a dict
credit_session_item_add_result_dict = credit_session_item_add_result_instance.to_dict()
# create an instance of CreditSessionItemAddResult from a dict
credit_session_item_add_result_form_dict = credit_session_item_add_result.from_dict(credit_session_item_add_result_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


