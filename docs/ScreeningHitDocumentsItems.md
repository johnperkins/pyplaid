# ScreeningHitDocumentsItems

Analyzed document information for the associated hit

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**analysis** | [**MatchSummary**](MatchSummary.md) |  | [optional] 
**data** | [**WatchlistScreeningDocument**](WatchlistScreeningDocument.md) |  | [optional] 

## Example

```python
from pyplaid.models.screening_hit_documents_items import ScreeningHitDocumentsItems

# TODO update the JSON string below
json = "{}"
# create an instance of ScreeningHitDocumentsItems from a JSON string
screening_hit_documents_items_instance = ScreeningHitDocumentsItems.from_json(json)
# print the JSON string representation of the object
print ScreeningHitDocumentsItems.to_json()

# convert the object into a dict
screening_hit_documents_items_dict = screening_hit_documents_items_instance.to_dict()
# create an instance of ScreeningHitDocumentsItems from a dict
screening_hit_documents_items_form_dict = screening_hit_documents_items.from_dict(screening_hit_documents_items_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


