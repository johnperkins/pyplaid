# UserIDNumber

ID number submitted by the user, currently used only for the Identity Verification product. If the user has not submitted this data yet, this field will be `null`. Otherwise, both fields are guaranteed to be filled.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value** | **str** | Value of identity document value typed in by user. Alpha-numeric, with all formatting characters stripped. | 
**type** | [**IDNumberType**](IDNumberType.md) |  | 

## Example

```python
from pyplaid.models.user_id_number import UserIDNumber

# TODO update the JSON string below
json = "{}"
# create an instance of UserIDNumber from a JSON string
user_id_number_instance = UserIDNumber.from_json(json)
# print the JSON string representation of the object
print UserIDNumber.to_json()

# convert the object into a dict
user_id_number_dict = user_id_number_instance.to_dict()
# create an instance of UserIDNumber from a dict
user_id_number_form_dict = user_id_number.from_dict(user_id_number_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


