# IncomeVerificationCreateRequest

IncomeVerificationCreateRequest defines the request schema for `/income/verification/create`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**webhook** | **str** | The URL endpoint to which Plaid should send webhooks related to the progress of the income verification process. | 
**precheck_id** | **str** | The ID of a precheck created with &#x60;/income/verification/precheck&#x60;. Will be used to improve conversion of the income verification flow. | [optional] 
**options** | [**IncomeVerificationCreateRequestOptions**](IncomeVerificationCreateRequestOptions.md) |  | [optional] 

## Example

```python
from pyplaid.models.income_verification_create_request import IncomeVerificationCreateRequest

# TODO update the JSON string below
json = "{}"
# create an instance of IncomeVerificationCreateRequest from a JSON string
income_verification_create_request_instance = IncomeVerificationCreateRequest.from_json(json)
# print the JSON string representation of the object
print IncomeVerificationCreateRequest.to_json()

# convert the object into a dict
income_verification_create_request_dict = income_verification_create_request_instance.to_dict()
# create an instance of IncomeVerificationCreateRequest from a dict
income_verification_create_request_form_dict = income_verification_create_request.from_dict(income_verification_create_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


