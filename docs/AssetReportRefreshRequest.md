# AssetReportRefreshRequest

AssetReportRefreshRequest defines the request schema for `/asset_report/refresh`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**asset_report_token** | **str** | The &#x60;asset_report_token&#x60; returned by the original call to &#x60;/asset_report/create&#x60; | 
**days_requested** | **int** | The maximum number of days of history to include in the Asset Report. Must be an integer. If not specified, the value from the original call to &#x60;/asset_report/create&#x60; will be used. | [optional] 
**options** | [**AssetReportRefreshRequestOptions**](AssetReportRefreshRequestOptions.md) |  | [optional] 

## Example

```python
from pyplaid.models.asset_report_refresh_request import AssetReportRefreshRequest

# TODO update the JSON string below
json = "{}"
# create an instance of AssetReportRefreshRequest from a JSON string
asset_report_refresh_request_instance = AssetReportRefreshRequest.from_json(json)
# print the JSON string representation of the object
print AssetReportRefreshRequest.to_json()

# convert the object into a dict
asset_report_refresh_request_dict = asset_report_refresh_request_instance.to_dict()
# create an instance of AssetReportRefreshRequest from a dict
asset_report_refresh_request_form_dict = asset_report_refresh_request.from_dict(asset_report_refresh_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


