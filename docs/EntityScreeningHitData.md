# EntityScreeningHitData

Information associated with the entity watchlist hit

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**documents** | [**List[EntityScreeningHitDocumentsItems]**](EntityScreeningHitDocumentsItems.md) | Documents associated with the watchlist hit | [optional] 
**email_addresses** | [**List[EntityScreeningHitEmailsItems]**](EntityScreeningHitEmailsItems.md) | Email addresses associated with the watchlist hit | [optional] 
**locations** | [**List[GenericScreeningHitLocationItems]**](GenericScreeningHitLocationItems.md) | Locations associated with the watchlist hit | [optional] 
**names** | [**List[EntityScreeningHitNamesItems]**](EntityScreeningHitNamesItems.md) | Names associated with the watchlist hit | [optional] 
**phone_numbers** | [**List[EntityScreeningHitsPhoneNumberItems]**](EntityScreeningHitsPhoneNumberItems.md) | Phone numbers associated with the watchlist hit | [optional] 
**urls** | [**List[EntityScreeningHitUrlsItems]**](EntityScreeningHitUrlsItems.md) | URLs associated with the watchlist hit | [optional] 

## Example

```python
from pyplaid.models.entity_screening_hit_data import EntityScreeningHitData

# TODO update the JSON string below
json = "{}"
# create an instance of EntityScreeningHitData from a JSON string
entity_screening_hit_data_instance = EntityScreeningHitData.from_json(json)
# print the JSON string representation of the object
print EntityScreeningHitData.to_json()

# convert the object into a dict
entity_screening_hit_data_dict = entity_screening_hit_data_instance.to_dict()
# create an instance of EntityScreeningHitData from a dict
entity_screening_hit_data_form_dict = entity_screening_hit_data.from_dict(entity_screening_hit_data_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


