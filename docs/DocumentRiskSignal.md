# DocumentRiskSignal

Details about a certain reason as to why a document could potentially be fraudulent.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **str** | The result from the risk signal check. | 
**field** | **str** | The field which the risk signal was computed for | 
**has_fraud_risk** | **bool** | A flag used to quickly identify if the signal indicates that this field is authentic or fraudulent | 
**institution_metadata** | [**DocumentRiskSignalInstitutionMetadata**](DocumentRiskSignalInstitutionMetadata.md) |  | 
**expected_value** | **str** | The expected value of the field, as seen on the document | 
**actual_value** | **str** | The derived value obtained in the risk signal calculation process for this field | 
**signal_description** | **str** | A human-readable explanation providing more detail into the particular risk signal | 
**page_number** | **int** | The relevant page associated with the risk signal | 

## Example

```python
from pyplaid.models.document_risk_signal import DocumentRiskSignal

# TODO update the JSON string below
json = "{}"
# create an instance of DocumentRiskSignal from a JSON string
document_risk_signal_instance = DocumentRiskSignal.from_json(json)
# print the JSON string representation of the object
print DocumentRiskSignal.to_json()

# convert the object into a dict
document_risk_signal_dict = document_risk_signal_instance.to_dict()
# create an instance of DocumentRiskSignal from a dict
document_risk_signal_form_dict = document_risk_signal.from_dict(document_risk_signal_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


