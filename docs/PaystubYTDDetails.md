# PaystubYTDDetails

The amount of income earned year to date, as based on paystub data.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**gross_earnings** | **float** | Year-to-date gross earnings. | [optional] 
**net_earnings** | **float** | Year-to-date net (take home) earnings. | [optional] 

## Example

```python
from pyplaid.models.paystub_ytd_details import PaystubYTDDetails

# TODO update the JSON string below
json = "{}"
# create an instance of PaystubYTDDetails from a JSON string
paystub_ytd_details_instance = PaystubYTDDetails.from_json(json)
# print the JSON string representation of the object
print PaystubYTDDetails.to_json()

# convert the object into a dict
paystub_ytd_details_dict = paystub_ytd_details_instance.to_dict()
# create an instance of PaystubYTDDetails from a dict
paystub_ytd_details_form_dict = paystub_ytd_details.from_dict(paystub_ytd_details_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


