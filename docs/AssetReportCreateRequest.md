# AssetReportCreateRequest

AssetReportCreateRequest defines the request schema for `/asset_report/create`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**access_tokens** | **List[str]** | An array of access tokens corresponding to the Items that will be included in the report. The &#x60;assets&#x60; product must have been initialized for the Items during link; the Assets product cannot be added after initialization. | 
**days_requested** | **int** | The maximum integer number of days of history to include in the Asset Report. If using Fannie Mae Day 1 Certainty, &#x60;days_requested&#x60; must be at least 61 for new originations or at least 31 for refinancings.  An Asset Report requested with \&quot;Additional History\&quot; (that is, with more than 61 days of transaction history) will incur an Additional History fee. | 
**options** | [**AssetReportCreateRequestOptions**](AssetReportCreateRequestOptions.md) |  | [optional] 

## Example

```python
from pyplaid.models.asset_report_create_request import AssetReportCreateRequest

# TODO update the JSON string below
json = "{}"
# create an instance of AssetReportCreateRequest from a JSON string
asset_report_create_request_instance = AssetReportCreateRequest.from_json(json)
# print the JSON string representation of the object
print AssetReportCreateRequest.to_json()

# convert the object into a dict
asset_report_create_request_dict = asset_report_create_request_instance.to_dict()
# create an instance of AssetReportCreateRequest from a dict
asset_report_create_request_form_dict = asset_report_create_request.from_dict(asset_report_create_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


