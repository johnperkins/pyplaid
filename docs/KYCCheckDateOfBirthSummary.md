# KYCCheckDateOfBirthSummary

Result summary object specifying how the `date_of_birth` field matched.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**summary** | [**MatchSummaryCode**](MatchSummaryCode.md) |  | 

## Example

```python
from pyplaid.models.kyc_check_date_of_birth_summary import KYCCheckDateOfBirthSummary

# TODO update the JSON string below
json = "{}"
# create an instance of KYCCheckDateOfBirthSummary from a JSON string
kyc_check_date_of_birth_summary_instance = KYCCheckDateOfBirthSummary.from_json(json)
# print the JSON string representation of the object
print KYCCheckDateOfBirthSummary.to_json()

# convert the object into a dict
kyc_check_date_of_birth_summary_dict = kyc_check_date_of_birth_summary_instance.to_dict()
# create an instance of KYCCheckDateOfBirthSummary from a dict
kyc_check_date_of_birth_summary_form_dict = kyc_check_date_of_birth_summary.from_dict(kyc_check_date_of_birth_summary_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


