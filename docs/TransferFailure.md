# TransferFailure

The failure reason if the event type for a transfer is `\"failed\"` or `\"returned\"`. Null value otherwise.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ach_return_code** | **str** | The ACH return code, e.g. &#x60;R01&#x60;.  A return code will be provided if and only if the transfer status is &#x60;returned&#x60;. For a full listing of ACH return codes, see [Transfer errors](https://plaid.com/docs/errors/transfer/#ach-return-codes). | [optional] 
**description** | **str** | A human-readable description of the reason for the failure or reversal. | [optional] 

## Example

```python
from pyplaid.models.transfer_failure import TransferFailure

# TODO update the JSON string below
json = "{}"
# create an instance of TransferFailure from a JSON string
transfer_failure_instance = TransferFailure.from_json(json)
# print the JSON string representation of the object
print TransferFailure.to_json()

# convert the object into a dict
transfer_failure_dict = transfer_failure_instance.to_dict()
# create an instance of TransferFailure from a dict
transfer_failure_form_dict = transfer_failure.from_dict(transfer_failure_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


