# IdentityVerificationStepUpdatedWebhook

Fired when an end user has completed a step of the Identity Verification process.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**webhook_type** | **str** | &#x60;IDENTITY_VERIFCATION&#x60; | 
**webhook_code** | **str** | &#x60;STEP_UPDATED&#x60; | 
**identity_verification_id** | **object** | The ID of the associated Identity Verification attempt. | 
**environment** | [**WebhookEnvironmentValues**](WebhookEnvironmentValues.md) |  | 

## Example

```python
from pyplaid.models.identity_verification_step_updated_webhook import IdentityVerificationStepUpdatedWebhook

# TODO update the JSON string below
json = "{}"
# create an instance of IdentityVerificationStepUpdatedWebhook from a JSON string
identity_verification_step_updated_webhook_instance = IdentityVerificationStepUpdatedWebhook.from_json(json)
# print the JSON string representation of the object
print IdentityVerificationStepUpdatedWebhook.to_json()

# convert the object into a dict
identity_verification_step_updated_webhook_dict = identity_verification_step_updated_webhook_instance.to_dict()
# create an instance of IdentityVerificationStepUpdatedWebhook from a dict
identity_verification_step_updated_webhook_form_dict = identity_verification_step_updated_webhook.from_dict(identity_verification_step_updated_webhook_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


