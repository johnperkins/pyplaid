# CreditSessionBankIncomeResult

The details of a bank income verification in Link

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | [**CreditSessionBankIncomeStatus**](CreditSessionBankIncomeStatus.md) |  | [optional] 
**item_id** | **str** | The Plaid Item ID. The &#x60;item_id&#x60; is always unique; linking the same account at the same institution twice will result in two Items with different &#x60;item_id&#x60; values. Like all Plaid identifiers, the &#x60;item_id&#x60; is case-sensitive. | [optional] 
**institution_id** | **str** | The Plaid Institution ID associated with the Item. | [optional] 

## Example

```python
from pyplaid.models.credit_session_bank_income_result import CreditSessionBankIncomeResult

# TODO update the JSON string below
json = "{}"
# create an instance of CreditSessionBankIncomeResult from a JSON string
credit_session_bank_income_result_instance = CreditSessionBankIncomeResult.from_json(json)
# print the JSON string representation of the object
print CreditSessionBankIncomeResult.to_json()

# convert the object into a dict
credit_session_bank_income_result_dict = credit_session_bank_income_result_instance.to_dict()
# create an instance of CreditSessionBankIncomeResult from a dict
credit_session_bank_income_result_form_dict = credit_session_bank_income_result.from_dict(credit_session_bank_income_result_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


