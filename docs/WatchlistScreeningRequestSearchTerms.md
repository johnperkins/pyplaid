# WatchlistScreeningRequestSearchTerms

Search inputs for creating a watchlist screening

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**watchlist_program_id** | **str** | ID of the associated program. | 
**legal_name** | **str** | The legal name of the individual being screened. | 
**date_of_birth** | **date** | A date in the format YYYY-MM-DD (RFC 3339 Section 5.6). | [optional] 
**document_number** | **str** | The numeric or alphanumeric identifier associated with this document. | [optional] 
**country** | **str** | Valid, capitalized, two-letter ISO code representing the country of this object. Must be in ISO 3166-1 alpha-2 form. | [optional] 

## Example

```python
from pyplaid.models.watchlist_screening_request_search_terms import WatchlistScreeningRequestSearchTerms

# TODO update the JSON string below
json = "{}"
# create an instance of WatchlistScreeningRequestSearchTerms from a JSON string
watchlist_screening_request_search_terms_instance = WatchlistScreeningRequestSearchTerms.from_json(json)
# print the JSON string representation of the object
print WatchlistScreeningRequestSearchTerms.to_json()

# convert the object into a dict
watchlist_screening_request_search_terms_dict = watchlist_screening_request_search_terms_instance.to_dict()
# create an instance of WatchlistScreeningRequestSearchTerms from a dict
watchlist_screening_request_search_terms_form_dict = watchlist_screening_request_search_terms.from_dict(watchlist_screening_request_search_terms_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


