# W2Box12

Data on the W2 Box 12

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **str** | W2 Box 12 code. | [optional] 
**amount** | **str** | W2 Box 12 amount. | [optional] 

## Example

```python
from pyplaid.models.w2_box12 import W2Box12

# TODO update the JSON string below
json = "{}"
# create an instance of W2Box12 from a JSON string
w2_box12_instance = W2Box12.from_json(json)
# print the JSON string representation of the object
print W2Box12.to_json()

# convert the object into a dict
w2_box12_dict = w2_box12_instance.to_dict()
# create an instance of W2Box12 from a dict
w2_box12_form_dict = w2_box12.from_dict(w2_box12_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


