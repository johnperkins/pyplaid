# TransferSweepGetResponse

Defines the response schema for `/transfer/sweep/get`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sweep** | [**TransferSweep**](TransferSweep.md) |  | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.transfer_sweep_get_response import TransferSweepGetResponse

# TODO update the JSON string below
json = "{}"
# create an instance of TransferSweepGetResponse from a JSON string
transfer_sweep_get_response_instance = TransferSweepGetResponse.from_json(json)
# print the JSON string representation of the object
print TransferSweepGetResponse.to_json()

# convert the object into a dict
transfer_sweep_get_response_dict = transfer_sweep_get_response_instance.to_dict()
# create an instance of TransferSweepGetResponse from a dict
transfer_sweep_get_response_form_dict = transfer_sweep_get_response.from_dict(transfer_sweep_get_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


