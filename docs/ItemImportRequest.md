# ItemImportRequest

ItemImportRequest defines the request schema for `/item/import`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **str** | Your Plaid API &#x60;client_id&#x60;. The &#x60;client_id&#x60; is required and may be provided either in the &#x60;PLAID-CLIENT-ID&#x60; header or as part of a request body. | [optional] 
**secret** | **str** | Your Plaid API &#x60;secret&#x60;. The &#x60;secret&#x60; is required and may be provided either in the &#x60;PLAID-SECRET&#x60; header or as part of a request body. | [optional] 
**products** | [**List[Products]**](Products.md) | Array of product strings | 
**user_auth** | [**ItemImportRequestUserAuth**](ItemImportRequestUserAuth.md) |  | 
**options** | [**ItemImportRequestOptions**](ItemImportRequestOptions.md) |  | [optional] 

## Example

```python
from pyplaid.models.item_import_request import ItemImportRequest

# TODO update the JSON string below
json = "{}"
# create an instance of ItemImportRequest from a JSON string
item_import_request_instance = ItemImportRequest.from_json(json)
# print the JSON string representation of the object
print ItemImportRequest.to_json()

# convert the object into a dict
item_import_request_dict = item_import_request_instance.to_dict()
# create an instance of ItemImportRequest from a dict
item_import_request_form_dict = item_import_request.from_dict(item_import_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


