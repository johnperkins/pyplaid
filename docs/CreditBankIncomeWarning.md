# CreditBankIncomeWarning

The warning associated with the data that was unavailable for the Bank Income Report.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**warning_type** | [**CreditBankIncomeWarningType**](CreditBankIncomeWarningType.md) |  | [optional] 
**warning_code** | [**CreditBankIncomeWarningCode**](CreditBankIncomeWarningCode.md) |  | [optional] 
**cause** | [**CreditBankIncomeCause**](CreditBankIncomeCause.md) |  | [optional] 

## Example

```python
from pyplaid.models.credit_bank_income_warning import CreditBankIncomeWarning

# TODO update the JSON string below
json = "{}"
# create an instance of CreditBankIncomeWarning from a JSON string
credit_bank_income_warning_instance = CreditBankIncomeWarning.from_json(json)
# print the JSON string representation of the object
print CreditBankIncomeWarning.to_json()

# convert the object into a dict
credit_bank_income_warning_dict = credit_bank_income_warning_instance.to_dict()
# create an instance of CreditBankIncomeWarning from a dict
credit_bank_income_warning_form_dict = credit_bank_income_warning.from_dict(credit_bank_income_warning_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


