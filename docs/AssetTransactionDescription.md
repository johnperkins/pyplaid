# AssetTransactionDescription

No documentation available

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**asset_transaction_description** | **str** | Asset Transaction Description String up to 3 occurances 1 required. | 

## Example

```python
from pyplaid.models.asset_transaction_description import AssetTransactionDescription

# TODO update the JSON string below
json = "{}"
# create an instance of AssetTransactionDescription from a JSON string
asset_transaction_description_instance = AssetTransactionDescription.from_json(json)
# print the JSON string representation of the object
print AssetTransactionDescription.to_json()

# convert the object into a dict
asset_transaction_description_dict = asset_transaction_description_instance.to_dict()
# create an instance of AssetTransactionDescription from a dict
asset_transaction_description_form_dict = asset_transaction_description.from_dict(asset_transaction_description_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


