# VerificationOfAsset

No documentation available

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reporting_information** | [**ReportingInformation**](ReportingInformation.md) |  | 
**service_product_fulfillment** | [**ServiceProductFulfillment**](ServiceProductFulfillment.md) |  | 
**verification_of_asset_response** | [**VerificationOfAssetResponse**](VerificationOfAssetResponse.md) |  | 

## Example

```python
from pyplaid.models.verification_of_asset import VerificationOfAsset

# TODO update the JSON string below
json = "{}"
# create an instance of VerificationOfAsset from a JSON string
verification_of_asset_instance = VerificationOfAsset.from_json(json)
# print the JSON string representation of the object
print VerificationOfAsset.to_json()

# convert the object into a dict
verification_of_asset_dict = verification_of_asset_instance.to_dict()
# create an instance of VerificationOfAsset from a dict
verification_of_asset_form_dict = verification_of_asset.from_dict(verification_of_asset_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


