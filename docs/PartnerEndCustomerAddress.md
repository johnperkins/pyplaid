# PartnerEndCustomerAddress

The end customer's address.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**city** | **str** |  | [optional] 
**street** | **str** |  | [optional] 
**region** | **str** |  | [optional] 
**postal_code** | **str** |  | [optional] 
**country_code** | **str** | ISO-3166-1 alpha-2 country code standard. | [optional] 

## Example

```python
from pyplaid.models.partner_end_customer_address import PartnerEndCustomerAddress

# TODO update the JSON string below
json = "{}"
# create an instance of PartnerEndCustomerAddress from a JSON string
partner_end_customer_address_instance = PartnerEndCustomerAddress.from_json(json)
# print the JSON string representation of the object
print PartnerEndCustomerAddress.to_json()

# convert the object into a dict
partner_end_customer_address_dict = partner_end_customer_address_instance.to_dict()
# create an instance of PartnerEndCustomerAddress from a dict
partner_end_customer_address_form_dict = partner_end_customer_address.from_dict(partner_end_customer_address_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


