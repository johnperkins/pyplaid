# IncomeSummary

The verified fields from a paystub verification. All fields are provided as reported on the paystub.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**employer_name** | [**EmployerIncomeSummaryFieldString**](EmployerIncomeSummaryFieldString.md) |  | 
**employee_name** | [**EmployeeIncomeSummaryFieldString**](EmployeeIncomeSummaryFieldString.md) |  | 
**ytd_gross_income** | [**YTDGrossIncomeSummaryFieldNumber**](YTDGrossIncomeSummaryFieldNumber.md) |  | 
**ytd_net_income** | [**YTDNetIncomeSummaryFieldNumber**](YTDNetIncomeSummaryFieldNumber.md) |  | 
**pay_frequency** | [**PayFrequency**](PayFrequency.md) |  | 
**projected_wage** | [**ProjectedIncomeSummaryFieldNumber**](ProjectedIncomeSummaryFieldNumber.md) |  | 
**verified_transaction** | [**TransactionData**](TransactionData.md) |  | 

## Example

```python
from pyplaid.models.income_summary import IncomeSummary

# TODO update the JSON string below
json = "{}"
# create an instance of IncomeSummary from a JSON string
income_summary_instance = IncomeSummary.from_json(json)
# print the JSON string representation of the object
print IncomeSummary.to_json()

# convert the object into a dict
income_summary_dict = income_summary_instance.to_dict()
# create an instance of IncomeSummary from a dict
income_summary_form_dict = income_summary.from_dict(income_summary_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


