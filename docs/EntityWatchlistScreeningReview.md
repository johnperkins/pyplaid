# EntityWatchlistScreeningReview

A review submitted by a team member for an entity watchlist screening. A review can be either a comment on the current screening state, actions taken against hits attached to the watchlist screening, or both.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | ID of the associated entity review. | 
**confirmed_hits** | **List[str]** | Hits marked as a true positive after thorough manual review. These hits will never recur or be updated once dismissed. In most cases, confirmed hits indicate that the customer should be rejected. | 
**dismissed_hits** | **List[str]** | Hits marked as a false positive after thorough manual review. These hits will never recur or be updated once dismissed. | 
**comment** | **str** | A comment submitted by a team member as part of reviewing a watchlist screening. | 
**audit_trail** | [**WatchlistScreeningAuditTrail**](WatchlistScreeningAuditTrail.md) |  | 

## Example

```python
from pyplaid.models.entity_watchlist_screening_review import EntityWatchlistScreeningReview

# TODO update the JSON string below
json = "{}"
# create an instance of EntityWatchlistScreeningReview from a JSON string
entity_watchlist_screening_review_instance = EntityWatchlistScreeningReview.from_json(json)
# print the JSON string representation of the object
print EntityWatchlistScreeningReview.to_json()

# convert the object into a dict
entity_watchlist_screening_review_dict = entity_watchlist_screening_review_instance.to_dict()
# create an instance of EntityWatchlistScreeningReview from a dict
entity_watchlist_screening_review_form_dict = entity_watchlist_screening_review.from_dict(entity_watchlist_screening_review_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


