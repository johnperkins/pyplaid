# IncomeVerificationRefreshResponse

IncomeVerificationRequestResponse defines the response schema for `/income/verification/refresh`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 
**verification_refresh_status** | [**VerificationRefreshStatus**](VerificationRefreshStatus.md) |  | 

## Example

```python
from pyplaid.models.income_verification_refresh_response import IncomeVerificationRefreshResponse

# TODO update the JSON string below
json = "{}"
# create an instance of IncomeVerificationRefreshResponse from a JSON string
income_verification_refresh_response_instance = IncomeVerificationRefreshResponse.from_json(json)
# print the JSON string representation of the object
print IncomeVerificationRefreshResponse.to_json()

# convert the object into a dict
income_verification_refresh_response_dict = income_verification_refresh_response_instance.to_dict()
# create an instance of IncomeVerificationRefreshResponse from a dict
income_verification_refresh_response_form_dict = income_verification_refresh_response.from_dict(income_verification_refresh_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


