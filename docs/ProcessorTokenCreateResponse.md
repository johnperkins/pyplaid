# ProcessorTokenCreateResponse

ProcessorTokenCreateResponse defines the response schema for `/processor/token/create` and `/processor/apex/processor_token/create`

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**processor_token** | **str** | The &#x60;processor_token&#x60; that can then be used by the Plaid partner to make API requests | 
**request_id** | **str** | A unique identifier for the request, which can be used for troubleshooting. This identifier, like all Plaid identifiers, is case sensitive. | 

## Example

```python
from pyplaid.models.processor_token_create_response import ProcessorTokenCreateResponse

# TODO update the JSON string below
json = "{}"
# create an instance of ProcessorTokenCreateResponse from a JSON string
processor_token_create_response_instance = ProcessorTokenCreateResponse.from_json(json)
# print the JSON string representation of the object
print ProcessorTokenCreateResponse.to_json()

# convert the object into a dict
processor_token_create_response_dict = processor_token_create_response_instance.to_dict()
# create an instance of ProcessorTokenCreateResponse from a dict
processor_token_create_response_form_dict = processor_token_create_response.from_dict(processor_token_create_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


