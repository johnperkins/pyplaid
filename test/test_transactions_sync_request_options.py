# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import pyplaid
from pyplaid.models.transactions_sync_request_options import TransactionsSyncRequestOptions  # noqa: E501
from pyplaid.rest import ApiException

class TestTransactionsSyncRequestOptions(unittest.TestCase):
    """TransactionsSyncRequestOptions unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test TransactionsSyncRequestOptions
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `TransactionsSyncRequestOptions`
        """
        model = pyplaid.models.transactions_sync_request_options.TransactionsSyncRequestOptions()  # noqa: E501
        if include_optional :
            return TransactionsSyncRequestOptions(
                include_original_description = True, 
                include_personal_finance_category = True
            )
        else :
            return TransactionsSyncRequestOptions(
        )
        """

    def testTransactionsSyncRequestOptions(self):
        """Test TransactionsSyncRequestOptions"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
