# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import pyplaid
from pyplaid.models.mortgage_interest_rate import MortgageInterestRate  # noqa: E501
from pyplaid.rest import ApiException

class TestMortgageInterestRate(unittest.TestCase):
    """MortgageInterestRate unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test MortgageInterestRate
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `MortgageInterestRate`
        """
        model = pyplaid.models.mortgage_interest_rate.MortgageInterestRate()  # noqa: E501
        if include_optional :
            return MortgageInterestRate(
                percentage = 1.337, 
                type = ''
            )
        else :
            return MortgageInterestRate(
                percentage = 1.337,
                type = '',
        )
        """

    def testMortgageInterestRate(self):
        """Test MortgageInterestRate"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
