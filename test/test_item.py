# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import pyplaid
from pyplaid.models.item import Item  # noqa: E501
from pyplaid.rest import ApiException

class TestItem(unittest.TestCase):
    """Item unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test Item
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `Item`
        """
        model = pyplaid.models.item.Item()  # noqa: E501
        if include_optional :
            return Item(
                item_id = '', 
                institution_id = '', 
                webhook = '', 
                error = { }, 
                available_products = [
                    'assets'
                    ], 
                billed_products = [
                    'assets'
                    ], 
                products = [
                    'assets'
                    ], 
                consented_products = [
                    'assets'
                    ], 
                consent_expiration_time = datetime.datetime.strptime('2013-10-20 19:20:30.00', '%Y-%m-%d %H:%M:%S.%f'), 
                update_type = 'background'
            )
        else :
            return Item(
                item_id = '',
                webhook = '',
                error = { },
                available_products = [
                    'assets'
                    ],
                billed_products = [
                    'assets'
                    ],
                consent_expiration_time = datetime.datetime.strptime('2013-10-20 19:20:30.00', '%Y-%m-%d %H:%M:%S.%f'),
                update_type = 'background',
        )
        """

    def testItem(self):
        """Test Item"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
