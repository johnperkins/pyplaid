# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import pyplaid
from pyplaid.models.account_product_access_nullable import AccountProductAccessNullable  # noqa: E501
from pyplaid.rest import ApiException

class TestAccountProductAccessNullable(unittest.TestCase):
    """AccountProductAccessNullable unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test AccountProductAccessNullable
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `AccountProductAccessNullable`
        """
        model = pyplaid.models.account_product_access_nullable.AccountProductAccessNullable()  # noqa: E501
        if include_optional :
            return AccountProductAccessNullable(
                account_data = True, 
                statements = True, 
                tax_documents = True
            )
        else :
            return AccountProductAccessNullable(
        )
        """

    def testAccountProductAccessNullable(self):
        """Test AccountProductAccessNullable"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
