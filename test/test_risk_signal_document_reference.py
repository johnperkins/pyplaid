# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import pyplaid
from pyplaid.models.risk_signal_document_reference import RiskSignalDocumentReference  # noqa: E501
from pyplaid.rest import ApiException

class TestRiskSignalDocumentReference(unittest.TestCase):
    """RiskSignalDocumentReference unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test RiskSignalDocumentReference
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `RiskSignalDocumentReference`
        """
        model = pyplaid.models.risk_signal_document_reference.RiskSignalDocumentReference()  # noqa: E501
        if include_optional :
            return RiskSignalDocumentReference(
                document_id = '', 
                document_name = ''
            )
        else :
            return RiskSignalDocumentReference(
        )
        """

    def testRiskSignalDocumentReference(self):
        """Test RiskSignalDocumentReference"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
