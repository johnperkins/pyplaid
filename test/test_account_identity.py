# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import pyplaid
from pyplaid.models.account_identity import AccountIdentity  # noqa: E501
from pyplaid.rest import ApiException

class TestAccountIdentity(unittest.TestCase):
    """AccountIdentity unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test AccountIdentity
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `AccountIdentity`
        """
        model = pyplaid.models.account_identity.AccountIdentity()  # noqa: E501
        if include_optional :
            return AccountIdentity(
                account_id = '', 
                balances = { }, 
                mask = '', 
                name = '', 
                official_name = '', 
                type = 'investment', 
                subtype = '401a', 
                verification_status = 'automatically_verified', 
                owners = [
                    { }
                    ]
            )
        else :
            return AccountIdentity(
                account_id = '',
                balances = { },
                mask = '',
                name = '',
                official_name = '',
                type = 'investment',
                subtype = '401a',
                owners = [
                    { }
                    ],
        )
        """

    def testAccountIdentity(self):
        """Test AccountIdentity"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
