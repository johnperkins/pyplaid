# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import pyplaid
from pyplaid.models.transfer_diligence_status import TransferDiligenceStatus  # noqa: E501
from pyplaid.rest import ApiException

class TestTransferDiligenceStatus(unittest.TestCase):
    """TransferDiligenceStatus unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testTransferDiligenceStatus(self):
        """Test TransferDiligenceStatus"""
        # inst = TransferDiligenceStatus()

if __name__ == '__main__':
    unittest.main()
