# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import pyplaid
from pyplaid.models.bank_transfer_event_list_request import BankTransferEventListRequest  # noqa: E501
from pyplaid.rest import ApiException

class TestBankTransferEventListRequest(unittest.TestCase):
    """BankTransferEventListRequest unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test BankTransferEventListRequest
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `BankTransferEventListRequest`
        """
        model = pyplaid.models.bank_transfer_event_list_request.BankTransferEventListRequest()  # noqa: E501
        if include_optional :
            return BankTransferEventListRequest(
                client_id = '', 
                secret = '', 
                start_date = datetime.datetime.strptime('2013-10-20 19:20:30.00', '%Y-%m-%d %H:%M:%S.%f'), 
                end_date = datetime.datetime.strptime('2013-10-20 19:20:30.00', '%Y-%m-%d %H:%M:%S.%f'), 
                bank_transfer_id = '', 
                account_id = '', 
                bank_transfer_type = 'debit', 
                event_types = [
                    'pending'
                    ], 
                count = 1, 
                offset = 0, 
                origination_account_id = '', 
                direction = 'inbound'
            )
        else :
            return BankTransferEventListRequest(
        )
        """

    def testBankTransferEventListRequest(self):
        """Test BankTransferEventListRequest"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
