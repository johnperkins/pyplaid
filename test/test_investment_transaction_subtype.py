# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import pyplaid
from pyplaid.models.investment_transaction_subtype import InvestmentTransactionSubtype  # noqa: E501
from pyplaid.rest import ApiException

class TestInvestmentTransactionSubtype(unittest.TestCase):
    """InvestmentTransactionSubtype unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testInvestmentTransactionSubtype(self):
        """Test InvestmentTransactionSubtype"""
        # inst = InvestmentTransactionSubtype()

if __name__ == '__main__':
    unittest.main()
