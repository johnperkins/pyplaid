# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import pyplaid
from pyplaid.models.asset_report import AssetReport  # noqa: E501
from pyplaid.rest import ApiException

class TestAssetReport(unittest.TestCase):
    """AssetReport unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test AssetReport
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `AssetReport`
        """
        model = pyplaid.models.asset_report.AssetReport()  # noqa: E501
        if include_optional :
            return AssetReport(
                asset_report_id = '', 
                client_report_id = '', 
                date_generated = datetime.datetime.strptime('2013-10-20 19:20:30.00', '%Y-%m-%d %H:%M:%S.%f'), 
                days_requested = 1.337, 
                user = { }, 
                items = [
                    { }
                    ]
            )
        else :
            return AssetReport(
                asset_report_id = '',
                client_report_id = '',
                date_generated = datetime.datetime.strptime('2013-10-20 19:20:30.00', '%Y-%m-%d %H:%M:%S.%f'),
                days_requested = 1.337,
                user = { },
                items = [
                    { }
                    ],
        )
        """

    def testAssetReport(self):
        """Test AssetReport"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
