# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import pyplaid
from pyplaid.models.plaid_error import PlaidError  # noqa: E501
from pyplaid.rest import ApiException

class TestPlaidError(unittest.TestCase):
    """PlaidError unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test PlaidError
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `PlaidError`
        """
        model = pyplaid.models.plaid_error.PlaidError()  # noqa: E501
        if include_optional :
            return PlaidError(
                error_type = 'INVALID_REQUEST', 
                error_code = '', 
                error_message = '', 
                display_message = '', 
                request_id = '', 
                causes = [
                    null
                    ], 
                status = 1.337, 
                documentation_url = '', 
                suggested_action = ''
            )
        else :
            return PlaidError(
                error_type = 'INVALID_REQUEST',
                error_code = '',
                error_message = '',
                display_message = '',
        )
        """

    def testPlaidError(self):
        """Test PlaidError"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
