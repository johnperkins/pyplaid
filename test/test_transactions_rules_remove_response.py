# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import pyplaid
from pyplaid.models.transactions_rules_remove_response import TransactionsRulesRemoveResponse  # noqa: E501
from pyplaid.rest import ApiException

class TestTransactionsRulesRemoveResponse(unittest.TestCase):
    """TransactionsRulesRemoveResponse unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test TransactionsRulesRemoveResponse
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `TransactionsRulesRemoveResponse`
        """
        model = pyplaid.models.transactions_rules_remove_response.TransactionsRulesRemoveResponse()  # noqa: E501
        if include_optional :
            return TransactionsRulesRemoveResponse(
                request_id = ''
            )
        else :
            return TransactionsRulesRemoveResponse(
                request_id = '',
        )
        """

    def testTransactionsRulesRemoveResponse(self):
        """Test TransactionsRulesRemoveResponse"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
