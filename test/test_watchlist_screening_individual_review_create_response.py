# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import pyplaid
from pyplaid.models.watchlist_screening_individual_review_create_response import WatchlistScreeningIndividualReviewCreateResponse  # noqa: E501
from pyplaid.rest import ApiException

class TestWatchlistScreeningIndividualReviewCreateResponse(unittest.TestCase):
    """WatchlistScreeningIndividualReviewCreateResponse unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test WatchlistScreeningIndividualReviewCreateResponse
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `WatchlistScreeningIndividualReviewCreateResponse`
        """
        model = pyplaid.models.watchlist_screening_individual_review_create_response.WatchlistScreeningIndividualReviewCreateResponse()  # noqa: E501
        if include_optional :
            return WatchlistScreeningIndividualReviewCreateResponse(
                id = 'rev_aCLNRxK3UVzn2r', 
                confirmed_hits = [
                    'scrhit_52xR9LKo77r1Np'
                    ], 
                dismissed_hits = [
                    'scrhit_52xR9LKo77r1Np'
                    ], 
                comment = 'These look like legitimate matches, rejecting the customer.', 
                audit_trail = pyplaid.models.watchlist_screening_audit_trail.WatchlistScreeningAuditTrail(
                    source = 'dashboard', 
                    dashboard_user_id = '54350110fedcbaf01234ffee', 
                    timestamp = '2020-07-24T03:26:02Z', ), 
                request_id = ''
            )
        else :
            return WatchlistScreeningIndividualReviewCreateResponse(
                id = 'rev_aCLNRxK3UVzn2r',
                confirmed_hits = [
                    'scrhit_52xR9LKo77r1Np'
                    ],
                dismissed_hits = [
                    'scrhit_52xR9LKo77r1Np'
                    ],
                comment = 'These look like legitimate matches, rejecting the customer.',
                audit_trail = pyplaid.models.watchlist_screening_audit_trail.WatchlistScreeningAuditTrail(
                    source = 'dashboard', 
                    dashboard_user_id = '54350110fedcbaf01234ffee', 
                    timestamp = '2020-07-24T03:26:02Z', ),
                request_id = '',
        )
        """

    def testWatchlistScreeningIndividualReviewCreateResponse(self):
        """Test WatchlistScreeningIndividualReviewCreateResponse"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
