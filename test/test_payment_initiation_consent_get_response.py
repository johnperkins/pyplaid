# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import pyplaid
from pyplaid.models.payment_initiation_consent_get_response import PaymentInitiationConsentGetResponse  # noqa: E501
from pyplaid.rest import ApiException

class TestPaymentInitiationConsentGetResponse(unittest.TestCase):
    """PaymentInitiationConsentGetResponse unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test PaymentInitiationConsentGetResponse
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `PaymentInitiationConsentGetResponse`
        """
        model = pyplaid.models.payment_initiation_consent_get_response.PaymentInitiationConsentGetResponse()  # noqa: E501
        if include_optional :
            return PaymentInitiationConsentGetResponse(
                consent_id = '0', 
                status = 'UNAUTHORISED', 
                created_at = datetime.datetime.strptime('2013-10-20 19:20:30.00', '%Y-%m-%d %H:%M:%S.%f'), 
                recipient_id = '0', 
                reference = '', 
                constraints = { }, 
                scopes = [
                    'ME_TO_ME'
                    ], 
                request_id = ''
            )
        else :
            return PaymentInitiationConsentGetResponse(
                consent_id = '0',
                status = 'UNAUTHORISED',
                created_at = datetime.datetime.strptime('2013-10-20 19:20:30.00', '%Y-%m-%d %H:%M:%S.%f'),
                recipient_id = '0',
                reference = '',
                constraints = { },
                scopes = [
                    'ME_TO_ME'
                    ],
                request_id = '',
        )
        """

    def testPaymentInitiationConsentGetResponse(self):
        """Test PaymentInitiationConsentGetResponse"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
