# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import pyplaid
from pyplaid.models.investment_transaction_type import InvestmentTransactionType  # noqa: E501
from pyplaid.rest import ApiException

class TestInvestmentTransactionType(unittest.TestCase):
    """InvestmentTransactionType unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testInvestmentTransactionType(self):
        """Test InvestmentTransactionType"""
        # inst = InvestmentTransactionType()

if __name__ == '__main__':
    unittest.main()
