# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import pyplaid
from pyplaid.models.watchlist_screening_entity_review_create_request import WatchlistScreeningEntityReviewCreateRequest  # noqa: E501
from pyplaid.rest import ApiException

class TestWatchlistScreeningEntityReviewCreateRequest(unittest.TestCase):
    """WatchlistScreeningEntityReviewCreateRequest unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test WatchlistScreeningEntityReviewCreateRequest
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `WatchlistScreeningEntityReviewCreateRequest`
        """
        model = pyplaid.models.watchlist_screening_entity_review_create_request.WatchlistScreeningEntityReviewCreateRequest()  # noqa: E501
        if include_optional :
            return WatchlistScreeningEntityReviewCreateRequest(
                confirmed_hits = [
                    'enthit_52xR9LKo77r1Np'
                    ], 
                dismissed_hits = [
                    'enthit_52xR9LKo77r1Np'
                    ], 
                comment = 'These look like legitimate matches, rejecting the customer.', 
                client_id = '', 
                secret = '', 
                entity_watchlist_screening_id = 'entscr_52xR9LKo77r1Np'
            )
        else :
            return WatchlistScreeningEntityReviewCreateRequest(
                confirmed_hits = [
                    'enthit_52xR9LKo77r1Np'
                    ],
                dismissed_hits = [
                    'enthit_52xR9LKo77r1Np'
                    ],
                entity_watchlist_screening_id = 'entscr_52xR9LKo77r1Np',
        )
        """

    def testWatchlistScreeningEntityReviewCreateRequest(self):
        """Test WatchlistScreeningEntityReviewCreateRequest"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
