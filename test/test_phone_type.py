# coding: utf-8

"""
    The Plaid API

    The Plaid REST API. Please see https://plaid.com/docs/api for more details.  # noqa: E501

    The version of the OpenAPI document: 2020-09-14_1.202.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import pyplaid
from pyplaid.models.phone_type import PhoneType  # noqa: E501
from pyplaid.rest import ApiException

class TestPhoneType(unittest.TestCase):
    """PhoneType unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testPhoneType(self):
        """Test PhoneType"""
        # inst = PhoneType()

if __name__ == '__main__':
    unittest.main()
